import { createContext, useContext, useState } from "react";
import * as React from "react";

import { Snackbar } from "@mui/material";
import MuiAlert from "@mui/material/Alert";

const AlertContext = createContext(null);

export const useAlert = () => useContext(AlertContext);

const Alert = React.forwardRef(function Alert(props, ref) {
	return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export const AlertProvider = ({ children }) => {
	const [activeAlert, setAlert] = useState({
		message: "",
		level: "success",
	});

	const [open, setOpen] = React.useState(false);

	const handleClose = (event, reason) => {
		if (reason === "clickaway") {
			return;
		}
		setAlert({ message: "", level: "" });
		setOpen(false);
	};

	React.useEffect(() => {
		if (activeAlert.message.length > 0) {
			setOpen(true);
		}
	}, [activeAlert]);

	return (
		<AlertContext.Provider value={{ activeAlert, setAlert }}>
			{children}
			{activeAlert.message.length > 0 ? (
				<Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
					<Alert
						onClose={handleClose}
						severity={activeAlert.level}
						sx={{ width: "100%" }}
					>
						{activeAlert.message}
					</Alert>
				</Snackbar>
			) : null}
		</AlertContext.Provider>
	);
};
