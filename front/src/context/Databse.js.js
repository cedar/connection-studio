import { createContext, useContext, useState } from "react";
import Session from "react-session-api";

const DatabaseContext = createContext(null);

export const useDatabase = () => useContext(DatabaseContext);

export const DatabaseProvider = ({ children }) => {
	const [contextDatabase, setContextDatabase] = useState("");

	const setPersistantDatabase = (database) => {
		setContextDatabase(database);
		Session.set("database", database);
	};

	return (
		<DatabaseContext.Provider
			value={{ contextDatabase, setPersistantDatabase }}
		>
			{children}
		</DatabaseContext.Provider>
	);
};
