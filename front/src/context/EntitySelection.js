import React from "react";

const initialValue = {
    selectionActive: false,
    currentEntities: [],
    setCurrentEntity: () => {},
    selectionState: { entities: {}, relationships: {} },
    updateSelectionState: () => {}
};

export const EntitySelectionContext = React.createContext(initialValue);