import { createContext, useContext, useState } from "react";
import * as React from "react";
import Session from "react-session-api";

const LangContext = createContext(null);

export const useLang = () => useContext(LangContext);

export const LangProvider = ({ children }) => {
	const [activeLang, setLang] = useState(
		"lang" in Session.items() ? Session.get("lang") : "fr"
	);

	const setPersistentLang = (value) => {
		setLang(value);
		Session.set("lang", value);
	};

	return (
		<LangContext.Provider value={{ activeLang, setPersistentLang }}>
			{children}
		</LangContext.Provider>
	);
};
