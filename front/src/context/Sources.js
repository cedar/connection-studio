import React from "react";
import { createContext, useContext, useState } from "react";
import { useParams } from "react-router-dom";
import Session from "react-session-api";

import { fetcherGet } from "fetch/fetcher";

const SourceContext = createContext(null);

export const useSource = () => useContext(SourceContext);

export const SourceProvider = ({ children }) => {
	const [sourcesLength, setSourcesLength] = useState(
		localStorage.getItem("sourcesLength") === null
			? 0
			: localStorage.getItem("sourcesLength")
	);

	const setPersistentSourcesLength = (value) => {
		setSourcesLength(value);
		Session.set("sourcesLength", value);
		localStorage.setItem("sourcesLength", value);
	};

	return (
		<SourceContext.Provider
			value={{ sourcesLength, setPersistentSourcesLength }}
		>
			{children}
		</SourceContext.Provider>
	);
};
