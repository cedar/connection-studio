import { createContext, useContext, useState } from "react";
import Session from "react-session-api";

const AuthContext = createContext(null);

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
	const [isAuth, setIsAuth] = useState(
		"isAUth" in Session.items() ? Session.get("isAuth") : true
	);

	const setPersistentisAuth = (value) => {
		setIsAuth(value);
		Session.set("isAuth", value);
	};

	return (
		<AuthContext.Provider value={{ isAuth, setPersistentisAuth }}>
			{children}
		</AuthContext.Provider>
	);
};
