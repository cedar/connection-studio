import useSWR from "swr";

import { deactivateRetry, fetcherPost } from "./fetcher";

export function useSearch(
	shouldFetchSearch,
	query,
	activeDb,
	parameters,
	shouldSearchOnNormalized
) {
	const {
		data: searchResults,
		error: isSearchResultsError,
		isLoading: isSearchResultsLoading,
	} = useSWR(
		shouldFetchSearch && query
			? [
					"/cl/kwsearch",
					{
						q: query,
						p: 0,
						d: activeDb,
						parameters: parameters,
						ond: shouldSearchOnNormalized,
					},
					{},
			  ]
			: null,
		([url, body, headers]) => fetcherPost(url, body, headers),
		deactivateRetry
	);

	return {
		searchResults,
		isSearchResultsError,
		isSearchResultsLoading,
	};
}
