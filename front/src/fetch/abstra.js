import useSWR from "swr";

import { fetcherGet } from "./fetcher";

// read one abstraction
// this needs: (a) the database name, (b) the execution id (this is given by the set of executions already performed)
// when the user click on the abstraction he wants, it should call this Ajax query with the db name and the execution id
export function useReadAbstraction(
	shouldFetchDbs,
	datasourceId,
	database,
	executionId
) {
	const {
		data: abstractionReadResult,
		error: isReadError,
		isLoading: isReadLoading,
		mutate: readMutate,
	} = useSWR(
		shouldFetchDbs
			? [
					"/abstra/read",
					{
						dbName: database.replace("cl_", ""),
						executionId: executionId,
						datasourceId: datasourceId,
					},
			  ]
			: null,
		([url, params]) => fetcherGet(url, params),
		{ refreshInterval: 0, revalidateOnFocus: false }
	);
	return { abstractionReadResult, isReadError, isReadLoading, readMutate };
}

// load the set of abstra executions that already exists in DB
// this needs: (a) the database name, (b) the datasource id
export function useGetAbstraExecutions(shouldFetchDbs, datasource, database) {
	const {
		data: abstraExistingConfigs,
		error: isGetExecutionsError,
		isLoading: isGetExecutionsLoading,
		mutate: mutateAbstraExecutions,
	} = useSWR(
		shouldFetchDbs
			? [
					"/abstra/executions",
					{
						datasourceId: datasource,
						dbName: database.replace("cl_", ""),
					},
			  ]
			: null,
		([url, params]) => fetcherGet(url, params),
		{ refreshInterval: 0, revalidateOnFocus: false }
	);

	return {
		abstraExistingConfigs,
		isGetExecutionsError,
		isGetExecutionsLoading,
		mutateAbstraExecutions,
	};
}
