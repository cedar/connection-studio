import useSWR from "swr";

import { deactivateRetry, fetcherPost, queueFetcherFile } from "./fetcher";

export const useFile = (shouldFetchFile, file, isLast) => {
	const {
		data: fileResult,
		error: isfileResultError,
		isLoading: isfileResultLoading,
	} = useSWR(
		shouldFetchFile ? ["/cl/import", file] : null,
		([url, file]) => queueFetcherFile(url, file),
		{
			revalidateIfStale: false,
			revalidateOnFocus: false,
			revalidateOnReconnect: false,
			onErrorRetry: (error, key, config, revalidate, { retryCount }) => {
				if (retryCount >= 0) return;
			},
		}
	);

	return { fileResult, isfileResultError, isfileResultLoading };
};

export const useDataSources = (shouldFetchDataSources, database) => {
	const {
		data: dataSources,
		error: isDataSourcesError,
		isLoading: isDataSourcesLoading,
		mutate: mutateDataSources,
	} = useSWR(
		shouldFetchDataSources
			? [
					"/cl/datasources",
					{
						d: database.replace("cl_", ""),
					},
					{},
			  ]
			: null,
		([url, body, headers]) => fetcherPost(url, body, headers),
		deactivateRetry
	);

	return {
		dataSources,
		isDataSourcesError,
		isDataSourcesLoading,
		mutateDataSources,
	};
};

export const useDeleteSource = (shouldDeleteSource, database, dataSourceID) => {
	const {
		data: deleteSource,
		error: isDeleteSourceError,
		isLoading: isDeleteSourceLoading,
	} = useSWR(
		shouldDeleteSource
			? [
					"/cl/deleteSource",
					{
						d: database.replace("cl_", ""),
						ds: dataSourceID,
					},
					{},
			  ]
			: null,
		([url, body, headers]) => fetcherPost(url, body, headers)
	);

	return {
		deleteSource,
		isDeleteSourceError,
		isDeleteSourceLoading,
	};
};

export const useSnippet = (shouldFetchSnippet, database, dataSourceID) => {
	const {
		data: snippet,
		error: isSnippetError,
		isLoading: isSnippetLoading,
	} = useSWR(
		shouldFetchSnippet
			? [
					"/cl/dataSourceSnippet",
					{
						d: database.replace("cl_", ""),
						ds: dataSourceID,
					},
					{},
			  ]
			: null,
		([url, body, headers]) => fetcherPost(url, body, headers)
	);

	return {
		snippet,
		isSnippetError,
		isSnippetLoading,
	};
};
