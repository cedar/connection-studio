import useSWR from "swr";

import { fetcherPost, fetcherGet } from "./fetcher";

export function useMixedQuery(
	database,
	contexts,
	activeSource,
	shouldFetchCleaning
) {
	const {
		data: inStatement,
		error: isStatementError,
		isLoading: isStatementLoading,
	} = useSWR(
		shouldFetchCleaning
			? [
					"/cl/view",
					{
						d: database.replace("cl_", ""),
						ctx: contexts,
						st: "",
						dt: activeSource.type,
						exec_q: false,
					},
			  ]
			: null,
		([url, params]) => fetcherGet(url, params)
	);

	return { inStatement, isStatementError, isStatementLoading };
}

export function useGetData(
	database,
	contexts,
	statement,
	sourceType,
	shouldExecuteQuery
) {
	const {
		data: dataRows,
		error: isDataRowsError,
		isLoading: isDataRowsLoading,
	} = useSWR(
		shouldExecuteQuery
			? [
					"/cl/view",
					{
						d: database.replace("cl_", ""),
						ctx: contexts,
						st: statement,
						dt: sourceType,
						exec_q: true,
					},
			  ]
			: null,
		([url, params]) => fetcherGet(url, params)
	);

	return { dataRows, isDataRowsError, isDataRowsLoading };
}
export function useContexts(shouldFetchContexts, database, dataSources) {
	const d = database.replace("cl_", "");
	const {
		data: extractedContexts,
		error: isContextsError,
		isLoading: isContextsLoading,
	} = useSWR(
		shouldFetchContexts
			? [
					"/cl/contexts",
					{
						d: d,
						ds: dataSources,
					},
			  ]
			: null,
		([url, params]) => fetcherGet(url, params)
	);

	return { extractedContexts, isContextsError, isContextsLoading };
}

export function useUpdateValues(
	shouldUpdateValues,
	database,
	contexts,
	values,
	type
) {
	const d = database.replace("cl_", "");

	var valuesToUpdate = [];
	let seenValues = new Set();
	for (let key in values) {
		let updatesInKey = values[key];
		for (let key2 in updatesInKey) {
			let currentUpdate = updatesInKey[key2];
			let origValue = currentUpdate["oldValue"];

			if (!seenValues.has(origValue)) {
				seenValues.add(origValue);
				currentUpdate["columnIndex"] = key2; // fieldID
				valuesToUpdate.push(currentUpdate);
			}
		}
	}

	const {
		data: updated,
		error: isUpdateError,
		isLoading: isUpdateLoading,
	} = useSWR(
		shouldUpdateValues
			? [
					"/cl/view",
					{
						d: d,
						ctx: contexts,
						val: valuesToUpdate,
						dt: type,
					},
					{},
			  ]
			: null,
		([url, params, headers]) => fetcherPost(url, params, headers)
	);

	return { updated, isUpdateError, isUpdateLoading };
}
