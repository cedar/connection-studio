import useSWR from "swr";

import { deactivateRetry, fetcherGet, fetcherPost, usePost } from "./fetcher";

export const useReadConfig = (database, parameters) => {
	const {
		data: config,
		error: isReadConfigError,
		isLoading: isReadConfigLoading,
	} = useSWR(
		[
			"/cl/config",
			{
				d: database.replace("cl_", ""),
				pmt: parameters,
			},
		],
		([url, params]) => fetcherGet(url, params),
		deactivateRetry
	);

	return {
		config,
		isReadConfigError,
		isReadConfigLoading,
	};
};

export const useSaveConfig = (shouldFetchSaveConfig, database, config) => {
	const {
		data: save,
		error: isSaveConfigError,
		isLoading: isSaveConfigLoading,
	} = usePost(
		shouldFetchSaveConfig,
		"/cl/config",
		{
			d: database.replace("cl_", ""),
			cfg: config,
		},
		{}
	);

	return {
		save,
		isSaveConfigError,
		isSaveConfigLoading,
	};
};
