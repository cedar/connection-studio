import useSWR from "swr";

import { fetcherPost } from "./fetcher";

export const useStatistics = (shouldFetchStatistics, database) => {
	const {
		data: statistics,
		error: isStatisticsError,
		isLoading: isStatisticsLoading,
	} = useSWR(
		shouldFetchStatistics
			? [
					"/cl/statistics",
					{
						d: database.replace("cl_", ""),
					},
					{},
			  ]
			: null,
		([url, body, headers]) => fetcherPost(url, body, headers)
	);

	if (isStatisticsError) {
		const stat = [];
		return {
			stat,
			isStatisticsError,
			isStatisticsLoading,
		};
	} else {
		return {
			statistics,
			isStatisticsError,
			isStatisticsLoading,
		};
	}
};
