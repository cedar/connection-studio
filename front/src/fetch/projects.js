import useSWR from "swr";

import { fetcherGet, fetcherPost } from "./fetcher";

export function useDbs(shouldFetchDbs) {
	const {
		data: dbsList,
		error: isDbError,
		isLoading: isDbLoading,
		mutate: mutateDbs,
	} = useSWR(shouldFetchDbs ? ["/cl/dbs", {}] : null, ([url, params]) =>
		fetcherGet(url, params)
	);

	return { dbsList, isDbError, isDbLoading, mutateDbs };
}

export function useCreateInstance(shouldCreateInstance, database) {
	const {
		data: createdDb,
		error: isCreatedDbsError,
		isLoading: isCreatedDbsLoading,
	} = useSWR(
		shouldCreateInstance
			? [
					"/cl/createDb",
					{
						d: database,
					},
					{},
			  ]
			: null,
		([url, body, headers]) => fetcherPost(url, body, headers)
	);
	return { createdDb, isCreatedDbsError, isCreatedDbsLoading };
}

export function useDeleteInstance(shouldDeleteInstance, database) {
	const {
		data: deleteDb,
		error: isDeleteDbError,
		isLoading: isDeleteDbLoading,
	} = useSWR(
		shouldDeleteInstance
			? [
					"/cl/deleteDb",
					{
						d: database.replace("cl_", ""),
					},
					{},
			  ]
			: null,
		([url, body, headers]) => fetcherPost(url, body, headers)
	);
	return { deleteDb, isDeleteDbError, isDeleteDbLoading };
}

export function useLoadEngine(shouldLoadEngine, database) {
	const {
		data: loadEngine,
		error: isLoadEngineError,
		isLoading: isLoadEngineLoading,
	} = useSWR(
		shouldLoadEngine
			? [
					"/cl/load",
					{
						d: database.replace("cl_", ""),
					},
					{},
			  ]
			: null,
		([url, body, headers]) => fetcherPost(url, body, headers)
	);
	return { loadEngine, isLoadEngineError, isLoadEngineLoading };
}
