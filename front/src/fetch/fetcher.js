import React from "react";

import axios from "axios";
import PQueue from "p-queue";

import { isDefined } from "../utils/utils";

const base_URL = "http://localhost:8080/webservices/";
axios.defaults.baseURL = base_URL;

export function serializeParameters(parameters) {
	function parseContent(content) {
		if (content instanceof Set) content = Array.from(content);
		if (typeof content === "object") content = JSON.stringify(content);
		return content;
	}
	var params = Object.fromEntries(
		Object.entries(parameters).map((e) => [e[0], parseContent(e[1])])
	);
	var encodedParameters = new URLSearchParams(params).toString();
	return encodedParameters;
}

export const fetcherGet = (url, params) =>
	axios
		.get(url, {
			params: params,
			paramsSerializer(params) {
				return serializeParameters(params);
			},
			withCredentials: true,
		})
		.then((res) => res.data);

export const fetcherPost = (url, body, headers) =>
	axios
		.post(url, serializeParameters(body), { withCredentials: true, ...headers })
		.then((res) => res.data);

export const fetcherPut = (url, body, headers) =>
	axios
		.put(url, serializeParameters(body), { withCredentials: true, ...headers })
		.then((res) => res.data);

export const fetchFile = (url, body) => {
	const completeURL = base_URL + url;
	return new Promise(function (resolve, reject) {
		var formData = new FormData();
		var xhr = new XMLHttpRequest();

		xhr.onload = function () {
			if (xhr.status >= 200 && xhr.status < 300) {
				return resolve(xhr.responseText);
			} else {
				return reject(xhr);
			}
		};

		formData.set("file", body.file);
		formData.set("u", body.u);
		formData.set("d", body.database.replace("cl_", ""));
		xhr.open("POST", completeURL);
		xhr.withCredentials = true;
		xhr.setRequestHeader(
			"Acess-Control-Allow-Headers",
			"access-control-allow-credentials"
		);
		xhr.send(formData);
	});
};

export const queue = new PQueue({ concurrency: 1 });

export const queueFetcherFile = (url, file) => {
	return queue.add(() => fetchFile(url, file)).then((res) => res);
};

export const onQueueComplete = (onResultCallback, onCompleteCallback) => {
	queue.on("completed", (result) => {
		if (isDefined(result)) {
			onResultCallback();
		}
		if (queue.size === 0) {
			onCompleteCallback();
		}
	});
};

export const onQueueIdle = (onIdleCallback) => {
	queue.on("idle", () => {
		onIdleCallback();
	});
};

export const deactivateRetry = {
	// revalidateIfStale: false,
	revalidateOnFocus: false,
	// revalidateOnReconnect: false,
	// revalidateOnMount: false,
	refreshWhenOffline: false,
	refreshWhenHidden: false,
	refreshInterval: 0,
	onErrorRetry: (error, key, config, revalidate, { retryCount }) => {
		if (retryCount >= 0) return;
	},
};

export function usePost(shouldFetch, url, body, headers) {
	const [isLoading, setIsLoading] = React.useState(false);
	const [data, setData] = React.useState(null);
	const [error, setError] = React.useState(null);

	React.useEffect(() => {
		if (shouldFetch && !data) {
			axios
				.post(url, serializeParameters(body), {
					withCredentials: true,
					...headers,
				})
				.then((res) => {
					setIsLoading(false);
					setData(res.data);
				})
				.catch((error) => {
					setError(error);
					setIsLoading(false);
				});

			setIsLoading(true);
		}
	});

	return { data, isLoading, error };
}

export const sendPost = (url, body, headers = {}) =>
	axios.post(url, serializeParameters(body), {
		withCredentials: true,
		...headers,
	});
