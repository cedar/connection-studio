import useSWR from "swr";

import { fetcherGet } from "./fetcher";

export function useReadPathWays(
	shouldFetchDbs,
	datasource,
	database,
	executionId
) {
	const {
		data: readInfo,
		error: isReadError,
		isLoading: isReadLoading,
		mutate: mutateRead,
	} = useSWR(
		shouldFetchDbs
			? [
					"/pathways/read",
					{
						dataSourceId: datasource,
						dbPathWaysRun: database.replace("cl_", ""),
						executionId: executionId,
					},
			  ]
			: null,
		([url, params]) => fetcherGet(url, params),
		{ refreshInterval: 0, revalidateOnFocus: false }
	);

	return { readInfo, isReadError, isReadLoading, mutateRead };
}

// load the set of pathways executions that already exists in DB
// this needs: (a) the database name, (b) the datasource id
export function useGetPathWaysExecutions(shouldFetchDbs, datasource, database) {
	const {
		data: pathWaysExistingConfigs,
		error: isGetExecutionsError,
		isLoading: isGetExecutionsLoading,
		mutate: mutatePathWaysExecutions,
	} = useSWR(
		shouldFetchDbs
			? [
					"/pathways/executions",
					{
						datasourceId: datasource,
						dbName: database.replace("cl_", ""),
					},
			  ]
			: null,
		([url, params]) => fetcherGet(url, params),
		{ refreshInterval: 0, revalidateOnFocus: false }
	);

	return {
		pathWaysExistingConfigs,
		isGetExecutionsError,
		isGetExecutionsLoading,
		mutatePathWaysExecutions,
	};
}
