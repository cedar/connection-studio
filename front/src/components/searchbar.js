import * as React from "react";

import SearchIcon from "@mui/icons-material/Search";
import { Button } from "@mui/material";
import Box from "@mui/material/Box";
import InputBase from "@mui/material/InputBase";
import Toolbar from "@mui/material/Toolbar";
import { grey } from "@mui/material/colors";
import { styled, alpha } from "@mui/material/styles";

import { useLang } from "../context/Lang";

const Search = styled("div")(({ theme }) => ({
	position: "relative",
	borderRadius: theme.shape.borderRadius,
	backgroundColor: alpha(theme.palette.common.white, 0.15),
	"&:hover": {
		backgroundColor: alpha(theme.palette.common.white, 0.25),
	},
	marginLeft: 0,
	width: "100%",
	[theme.breakpoints.up("sm")]: {
		marginLeft: theme.spacing(1),
		width: "auto",
	},
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
	padding: theme.spacing(0, 2),
	height: "100%",
	position: "absolute",
	pointerEvents: "none",
	display: "flex",
	alignItems: "center",
	justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
	color: "inherit",
	"& .MuiInputBase-input": {
		padding: theme.spacing(1, 1, 1, 0),
		transition: theme.transitions.create("width"),
		width: "100%",
	},
}));

export default function SearchBar({ getInput, propsSx, trans }) {
	const [input, setInput] = React.useState("");

	const handleChange = (event) => {
		setInput(event.target.value);
	};

	const handleOnClick = () => {
		getInput(input);
	};

	const handleKeyDown = (event) => {
		if (event.code === "Enter" || event.code === "NumpadEnter") {
			getInput(input);
		}
	};
	const { activeLang } = useLang();

	return (
		<Box
			sx={{
				border: 1,
				borderColor: grey[400],
				borderRadius: 40,
				display: "flex",
				alignItems: "center",
				...propsSx,
			}}
		>
			<Toolbar
				sx={{ width: "100%", pr: "0 !important", pl: "12px !important" }}
			>
				<Search sx={{ width: "100% !important" }}>
					<StyledInputBase
						sx={{ width: "100%" }}
						placeholder={trans.placeHolder[activeLang]}
						onChange={handleChange}
						onKeyDown={handleKeyDown}
					/>
				</Search>
			</Toolbar>
			<Button
				onClick={handleOnClick}
				sx={{ borderRadius: 40, height: "100%" }}
				disableRipple={true}
			>
				<SearchIconWrapper>
					<SearchIcon />
				</SearchIconWrapper>
			</Button>
		</Box>
	);
}
