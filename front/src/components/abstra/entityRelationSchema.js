import React from "react";
import ReactFlow, {
	Controls,
	Background,
	useNodesState,
	useEdgesState,
	ControlButton,
} from "reactflow";
import "reactflow/dist/style.css";

import { toPng, toSvg } from "html-to-image";

import "../../css/abstra.css";
import FloatingEdge from "./FloatingEdge";
import SelfConnecting from "./SelfEdge";
import AbstraTable from "./abstraTable";
import { createEdge, createNode } from "./utils";

const nodeTypes = {
	abstraTable: AbstraTable,
};

const edgeTypes = {
	floating: FloatingEdge,
	self: SelfConnecting,
};

const onInit = (reactFlowInstance) => reactFlowInstance.fitView();

const EntityRelationSchema = ({ initialNodes, initialEdges, fileName }) => {
	const [nodes, setNodes, onNodesChange] = useNodesState();
	const [edges, setEdges, onEdgesChange] = useEdgesState();
	const flowRef = React.useRef(null);

	React.useEffect(() => {
		const nodeIdToIndex = {};
		initialNodes.map((node, index) => {
			nodeIdToIndex[node.id] = index;
		});

		let outgoingEdges = {};
		let parsedEdges = [];
		for (const edge of initialEdges) {
			for (let i = 0; i < edge.relations.length; i++) {
				const relation = edge.relations[i];
				outgoingEdges[edge.sourceId] = outgoingEdges[edge.sourceId] + 1 || 1;
				parsedEdges.push(
					createEdge(edge.sourceId, edge.targetId, relation, nodeIdToIndex, i)
				);
			}
		}

		const parsedNodes = initialNodes.map((node, index) => {
			return createNode(node, index, outgoingEdges[node.id] || 0);
		});

		setNodes(parsedNodes);
		setEdges(parsedEdges);
	}, [initialNodes, initialEdges, setEdges, setNodes]);

	return (
		<div className="floatingedges">
			<ReactFlow
				nodes={nodes}
				edges={edges}
				onNodesChange={onNodesChange}
				onEdgesChange={onEdgesChange}
				onInit={onInit}
				fitView
				attributionPosition="top-right"
				nodeTypes={nodeTypes}
				edgeTypes={edgeTypes}
				proOptions={{ hideAttribution: true }}
				ref={flowRef}
			>
				{/* <MiniMap style={minimapStyle} zoomable pannable /> */}
				<Controls>
					<ControlButton
						onClick={() => {
							if (flowRef.current === null) return;
							toSvg(flowRef.current, {
								filter: (node) =>
									!(
										node?.classList?.contains("react-flow__minimap") ||
										node?.classList?.contains("react-flow__controls")
									),
							}).then((dataUrl) => {
								const a = document.createElement("a");
								a.setAttribute("download", `abstraction_${fileName}.svg`);
								a.setAttribute("href", dataUrl);
								a.click();
							});
						}}
					>
						SVG
					</ControlButton>
					<ControlButton
						onClick={() => {
							if (flowRef.current === null) return;
							toPng(flowRef.current, {
								filter: (node) =>
									!(
										node?.classList?.contains("react-flow__minimap") ||
										node?.classList?.contains("react-flow__controls")
									),
							}).then((dataUrl) => {
								const a = document.createElement("a");
								a.setAttribute("download", `abstraction_${fileName}.png`);
								a.setAttribute("href", dataUrl);
								a.click();
							});
						}}
					>
						PNG
					</ControlButton>
				</Controls>
				<Background color="#aaa" gap={16} />
			</ReactFlow>
		</div>
	);
};

export default EntityRelationSchema;
