import React from "react";
import { BaseEdge, EdgeLabelRenderer } from "reactflow";

import { Box } from "@mui/material";

export default function SelfConnecting({
	sourceX,
	sourceY,
	targetX,
	targetY,
	id,
	markerEnd,
	style,
	label,
	labelStyle,
}) {
	const radiusX = (sourceX - targetX) * 0.6;
	const radiusY = 100;
	const edgePath = `M ${sourceX - 5} ${sourceY} A ${radiusX} ${radiusY} 0 1 1 ${
		targetX + 2
	} ${targetY}`;

	return (
		<>
			<BaseEdge path={edgePath} markerEnd={markerEnd} />
			<EdgeLabelRenderer>
				<Box
					style={{
						position: "absolute",
						transform: `translate(-50%, -50%) translate(${
							sourceX + (targetX - sourceX) / 2
						}px,${sourceY - 145}px)`,
						background: "#fff",
						borderRadius: 5,
						fontSize: 14,
						fontWeight: 500,
						zIndex: 10000,
					}}
					className="nodrag nopan"
				>
					<Box
						style={{
							background: labelStyle.color,
							borderRadius: 5,
							padding: 5,
						}}
					>
						<Box>{label}</Box>
					</Box>
				</Box>
			</EdgeLabelRenderer>
		</>
	);
}
