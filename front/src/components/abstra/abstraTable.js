import React, { memo } from "react";
import { Handle } from "reactflow";

import { useTheme } from "@emotion/react";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import {
	Box,
	Checkbox,
	Collapse,
	Divider,
	IconButton,
	List,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Tooltip,
	Typography,
} from "@mui/material";
import { styled } from "@mui/material/styles";

import { EntitySelectionContext } from "../../context/EntitySelection";
import { useLang } from "../../context/Lang";
import { EntityIcon } from "../../utils/entities";
import {
	getInitialSelectionState,
	getUpdatedSelectionState,
} from "../../utils/entitySelection";

const ListIconEntity = styled(ListItemIcon)(() => ({
	marginTop: "auto",
	marginBottom: "auto",
	minWidth: 0,
	marginRight: 15,
}));

const trans = {
	labelled: {
		en: "labelled",
		fr: "nommés",
	},
	records: {
		en: "records",
		fr: "tuples",
	},
	record: {
		en: "record",
		fr: "tuple",
	},
	required: {
		en: "Required?",
		fr: "Requis ?",
	},
};

function AbstraHeader({ id, data }) {
	const { setCurrentEntity } = React.useContext(EntitySelectionContext);

	const { activeLang } = useLang();

	let recordLabel = trans.records[activeLang];
	if (data.collection_size === 1) {
		recordLabel = trans.record[activeLang];
	}
	return (
		<Box
			sx={{ display: "flex", backgroundColor: data.color, px: 1 }}
			onClick={() => setCurrentEntity([id])}
		>
			<ListIconEntity>
				<EntityIcon entityName={data.label} />
			</ListIconEntity>
			<Box sx={{ my: 0.5 }}>
				<Typography variant="h6" component="div" fontSize={18}>
					{data.collection_size} {recordLabel} ({trans.labelled[activeLang]} "
					{data.label}")
				</Typography>
				<Typography fontSize={14} color="text.secondary">
					{data.categoryLabel}
				</Typography>
			</Box>
		</Box>
	);
}

function AbstraRow({
	row,
	depth,
	listIndex,
	selectionState,
	selectionActive,
	handleSelectEntity,
	handleGroupEntity,
	handleClickTopButton,
}) {
	const { activeLang } = useLang();
	const [open, setOpen] = React.useState(false);
	const entityState = React.useMemo(
		() => selectionState.find((i) => i.id === row.selection_id),
		[selectionState, row.selection_id]
	);

	React.useEffect(() => {
		if (!entityState?.checked) {
			handleGroupEntity(row.id, false);
		}
	}, [entityState?.checked]);

	const handleClick = () => {
		setOpen(!open);
	};

	const handleSelectCheck = (e) => {
		e.stopPropagation();

		handleSelectEntity(row.selection_id);
	};

	const handleGroupCheck = (e) => {
		e.stopPropagation();

		handleGroupEntity(row.selection_id);
	};

	let font = "normal";
	if (row.label === "array" || row.label === "map" || row.label === "row") {
		font = "italic";
	}

	const childNodes = row.children ? (
		<Collapse in={open} timeout="auto" unmountOnExit>
			<List component="div" disablePadding>
				{row.children.map((child, index) => {
					return (
						<AbstraRow
							row={child}
							key={row.id}
							depth={depth + 1}
							listIndex={index}
							selectionActive={selectionActive}
							handleSelectEntity={handleSelectEntity}
							handleGroupEntity={handleGroupEntity}
							selectionState={selectionState}
							handleClickTopButton={handleClickTopButton}
						></AbstraRow>
					);
				})}
			</List>
		</Collapse>
	) : null;

	return (
		<>
			<ListItemButton
				sx={{
					pl: (depth === 1 ? 0 : 1) * 3 + (depth - 1) * 2,
					py: 0.5,
					":hover": { "> button": { visibility: "visible" } },
				}}
				onClick={handleClick}
			>
				{depth === 1 ? (
					<IconButton
						sx={{ visibility: "hidden" }}
						onClick={(event) => handleClickTopButton(event, listIndex)}
						size="small"
					>
						<ArrowUpwardIcon sx={{ fontSize: "1rem" }} />
					</IconButton>
				) : null}
				{selectionActive && (
					<Checkbox
						checked={entityState?.checked}
						indeterminate={entityState?.indeterminate}
						onClick={handleSelectCheck}
						size="dense"
						sx={{ ml: -2 }}
					/>
				)}
				<ListItemText
					primary={`${row.label} (${row.frequency}%)`}
					primaryTypographyProps={{
						sx: {
							fontWeight: 450,
							textTransform: "normal",
							fontSize: 13,
							fontStyle: font,
						},
						variant: "primary",
					}}
				/>
				{selectionActive && !entityState?.nbNonLeafChildren && (
					<Tooltip title={trans.required[activeLang]} arrow>
						<Checkbox
							disabled={!entityState?.checked}
							checked={entityState?.required}
							onClick={handleGroupCheck}
							size="dense"
							color="secondary"
							sx={{ mr: -0.5 }}
						/>
					</Tooltip>
				)}
				{row.children ? open ? <ExpandLess /> : <ExpandMore /> : null}
			</ListItemButton>
			{childNodes}
		</>
	);
}

function NestedList({
	id,
	data,
	selectionState,
	selectionActive,
	handleSelectEntity,
	handleGroupEntity,
}) {
	const [rowList, setRowList] = React.useState(data.rows);

	const handleClickTopButton = (event, listIndex) => {
		event.stopPropagation();

		const newList = [...rowList];
		newList.splice(listIndex, 1);
		newList.unshift(rowList[listIndex]);
		setRowList(newList);
	};

	return (
		<List
			sx={{ width: "100%", bgcolor: "background.paper", elevation: 0 }}
			component="div"
			disablePadding
			subheader={<AbstraHeader data={data} id={id} />}
		>
			<Divider />
			{rowList.map((row, index) => {
				return (
					<React.Fragment key={row.id + index}>
						<AbstraRow
							row={row}
							depth={1}
							listIndex={index}
							selectionActive={selectionActive}
							handleSelectEntity={handleSelectEntity}
							handleGroupEntity={handleGroupEntity}
							selectionState={selectionState}
							handleClickTopButton={handleClickTopButton}
						/>
						<Divider />
					</React.Fragment>
				);
			})}
		</List>
	);
}

function AbstraTable({ id, data }) {
	const {
		updateSelectionState,
		selectionActive,
		currentEntities,
		selectionState: globalSelectionState,
	} = React.useContext(EntitySelectionContext);
	const [selectionState, setSelectionState] = React.useState([]);

	const theme = useTheme();
	// is the second condition really needed ?
	const isEntitySelectable = selectionActive && currentEntities.includes(id);

	React.useEffect(() => {
		setSelectionState(getInitialSelectionState(data.rows, id));
	}, [data, id]);

	// Update the global selection state when the local selection state changes
	React.useEffect(() => {
		updateSelectionState((prevState) => {
			let newState = { ...prevState };
			newState.entities[id] = selectionState;
			return newState;
		});
	}, [selectionState]);

	const handleSelectEntity = (clickedId) =>
		setSelectionState((prevState) =>
			getUpdatedSelectionState(prevState, clickedId)
		);

	// Change the "required" state to of an attribute with id "clickedId"
	const handleRequireEntity = (clickedId, required = null) =>
		setSelectionState((prevState) => {
			const newState = prevState.map((state) => ({ ...state }));

			let entity = newState.find((i) => i.id === clickedId);
			if (!entity) {
				return newState;
			}

			if (required == null) {
				required = !entity.required;
			}

			entity.required = required;
			return newState;
		});

	return (
		<Box
			sx={{
				p: 0.4,
				backgroundColor: isEntitySelectable
					? theme.palette.primary.main
					: data.color,
				borderRadius: "4px",
			}}
		>
			<NestedList
				id={id}
				data={data}
				selectionActive={isEntitySelectable}
				selectionState={selectionState}
				handleSelectEntity={handleSelectEntity}
				handleGroupEntity={handleRequireEntity}
			/>
			<Handle type="source" id="source-float" />
			<Handle type="target" id="target-float" />
			<Handle
				type="source"
				id="source-self"
				position="left"
				style={{ top: 25 }}
			/>
			<Handle
				type="target"
				id="target-self"
				position="right"
				style={{ top: 25 }}
			/>
		</Box>
	);
}

export default memo(AbstraTable);
