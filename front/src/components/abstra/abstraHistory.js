import {
	Box,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
} from "@mui/material";

function HistoryEntry({ entry, onClickHistory }) {
	return (
		<TableRow
			onClick={() => onClickHistory(entry.id)}
			sx={{
				cursor: "pointer",
				"&:last-child td, &:last-child th": { border: 0 },
			}}
		>
			<TableCell
				sx={{
					borderRadius: 2,
					"&:hover": { backgroundColor: "lightgrey", transition: "0.3s ease" },
				}}
				align="left"
			>
				{entry.eMax}
			</TableCell>
		</TableRow>
	);
}
export default function AbstraHistory({ data, onClickHistory }) {
	return (
		<Box>
			<Table sx={{ minWidth: 300 }}>
				<TableHead>
					<TableRow>
						<TableCell align="left">Nombre d'entités maximum</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{data
						? data.map((entry, index) => {
								return (
									<HistoryEntry
										key={index}
										entry={entry}
										onClickHistory={onClickHistory}
									></HistoryEntry>
								);
						  })
						: null}
				</TableBody>
			</Table>
		</Box>
	);
}
