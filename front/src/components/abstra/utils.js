import { MarkerType, Position } from "reactflow";

const colors = [
	"rgba(0, 63, 92, 0.5)",
	"rgba(47, 75, 124, 0.5)",
	"rgba(160, 81, 149, 0.5)",
	"rgba(212, 80, 135, 0.5)",
	"rgba(249, 93, 106, 0.5)",
	"rgba(255, 124, 67, 0.5)",
	"rgba(255, 166, 0, 0.5)",
];

export function createEdge(source, target, label, nodeIdToIndex, index) {
	return {
		id: `e${source}-${target}-${index}`,
		type: source === target ? "self" : "floating",
		source: `${source}`,
		target: `${target}`,
		sourceHandle: source === target ? "source-self" : "source-float",
		targetHandle: source === target ? "target-self" : "target-float",
		label: label,
		animated: true,
		markerEnd: {
			type: MarkerType.ArrowClosed,
			width: 35,
			height: 35,
		},
		style: {
			strokeWidth: 2,
		},
		labelStyle: { color: colors[nodeIdToIndex[source] % colors.length] },
	};
}

export function createNode(node, index, numEdges) {
	return {
		id: node.id,
		type: "abstraTable",
		position: { x: index * 450, y: 0 },
		data: {
			numOutgoingEdges: numEdges,
			categoryLabel: node.categoryLabel,
			color: colors[index % colors.length],
			label: node.label,
			collection_size: node.size,
			rows: node.boundary.children,
		},
	};
}

// this helper function returns the intersection point
// of the line between the center of the intersectionNode and the target node
function getNodeIntersection2(intersectionNode, targetNode, index = 0) {
	// https://math.stackexchange.com/questions/1724792/an-algorithm-for-finding-the-intersection-point-between-a-center-of-vision-and-a
	const {
		width: sourceWidth,
		height: sourceHeight,
		positionAbsolute: sourceOrigin,
	} = intersectionNode;

	const {
		width: targetWidth,
		height: targetHeight,
		positionAbsolute: targetOrigin,
	} = targetNode;

	const sourceCenter = {
		x: sourceOrigin.x + sourceWidth / 2,
		y: sourceOrigin.y + sourceHeight / 2,
	};

	const targetCenter = {
		x: targetOrigin.x + targetWidth / 2,
		y: targetOrigin.y + targetHeight / 2,
	};

	const sourcePos =
		sourceCenter.x < targetCenter.x ? Position.Right : Position.Left;

	const sx =
		sourcePos === Position.Right
			? sourceOrigin.x + sourceWidth
			: sourceOrigin.x;
	const sy = sourceOrigin.y + index * 40 + 15;

	let targetPos, tx, ty;
	if (targetCenter.y > sourceCenter.y) {
		targetPos = Position.Top;
		if (targetCenter.x > sourceCenter.x) {
			tx = targetOrigin.x + targetWidth - index * 40 - 15;
		} else {
			tx = targetOrigin.x + index * 40 + 15;
		}
		ty = targetOrigin.y;
	} else {
		targetPos = Position.Bottom;
		if (targetCenter.x > sourceCenter.x) {
			tx = targetOrigin.x + targetWidth - index * 40 - 15;
		} else {
			tx = targetOrigin.x + index * 40 + 15;
		}
		ty = targetOrigin.y + targetHeight;
	}

	return { sx, sy, tx, ty, sourcePos, targetPos };

	// const xx1 = (x1 - x2) / (2 * w) - (y1 - y2) / (2 * h);
	// const yy1 = (x1 - x2) / (2 * w) + (y1 - y2) / (2 * h);
	// const a = 1 / (Math.abs(xx1) + Math.abs(yy1));
	// const xx3 = a * xx1;
	// const yy3 = a * yy1;
	// let x = w * (xx3 + yy3) + x2;
	// let y = h * (-xx3 + yy3) + y2;

	// const offsetTotal = index * 30;
	// const offsetX = Math.min(offsetTotal, edgeRight - x);
	// const offsetY = offsetTotal - offsetX;

	// x += offsetX;
	// y += offsetY;
}

// this helper function returns the intersection point
// of the line between the center of the intersectionNode and the target node
function getNodeIntersection(intersectionNode, targetNode, index, numEdges) {
	const epsilon = 0.1;

	const {
		width: intersectionNodeWidth,
		height: intersectionNodeHeight,
		positionAbsolute: intersectionNodePosition,
	} = intersectionNode;
	const targetPosition = targetNode.positionAbsolute;

	const dx = targetPosition.x - intersectionNodePosition.x;
	const dy = targetPosition.y - intersectionNodePosition.y;
	let x, y;

	if (
		Math.abs(dy) * intersectionNodeWidth >
		Math.abs(dx) * intersectionNodeHeight
	) {
		// Intersection is on top or bottom side
		x =
			intersectionNodePosition.x +
			intersectionNodeWidth / 2 +
			(dx / Math.abs(dy)) * (intersectionNodeHeight / 2);
		y =
			dy > 0
				? intersectionNodePosition.y + intersectionNodeHeight
				: intersectionNodePosition.y;
	} else {
		// Intersection is on left or right side
		x =
			dx > 0
				? intersectionNodePosition.x + intersectionNodeWidth
				: intersectionNodePosition.x;
		y =
			intersectionNodePosition.y +
			intersectionNodeHeight / 2 +
			(dy / Math.abs(dx)) * (intersectionNodeWidth / 2);
	}

	const offset_step = 50;
	const perimeter = 2 * (intersectionNodeWidth + intersectionNodeHeight);
	const offset =
		((index - (numEdges - 1) / 2) * offset_step + perimeter) % perimeter;

	// Define the corner points of the rectangle
	const topLeft = [intersectionNodePosition.x, intersectionNodePosition.y];
	const topRight = [
		intersectionNodePosition.x + intersectionNodeWidth,
		intersectionNodePosition.y,
	];
	const bottomRight = [
		intersectionNodePosition.x + intersectionNodeWidth,
		intersectionNodePosition.y + intersectionNodeHeight,
	];
	const bottomLeft = [
		intersectionNodePosition.x,
		intersectionNodePosition.y + intersectionNodeHeight,
	];

	// Find which segment the point is on and how far along that segment
	let segment = null;
	let distanceAlongSegment = 0;

	function nearlyEqual(a, b, epsilon) {
		return Math.abs(a - b) < epsilon;
	}

	if (nearlyEqual(y, topLeft[1], epsilon)) {
		segment = "top";
		distanceAlongSegment = x - topLeft[0];
	} else if (nearlyEqual(y, bottomLeft[1], epsilon)) {
		segment = "bottom";
		distanceAlongSegment = bottomRight[0] - x;
	} else if (nearlyEqual(x, topLeft[0], epsilon)) {
		segment = "left";
		distanceAlongSegment = bottomLeft[1] - y;
	} else if (nearlyEqual(x, topRight[0], epsilon)) {
		segment = "right";
		distanceAlongSegment = y - topRight[1];
	}

	// Move the point along the perimeter
	let remainingOffset = offset;
	while (remainingOffset !== 0) {
		const reverse = offset < 0;
		switch (segment) {
			case "top":
				if (reverse) segment = "left";
				if (distanceAlongSegment + remainingOffset <= intersectionNodeWidth) {
					x += reverse ? -remainingOffset : remainingOffset;
					remainingOffset = 0;
				} else {
					remainingOffset -= intersectionNodeWidth - distanceAlongSegment;
					[x, y] = reverse ? topLeft : topRight;
					segment = reverse ? "left" : "right";
					distanceAlongSegment = 0;
				}
				break;
			case "right":
				if (reverse) segment = "top";
				if (distanceAlongSegment + remainingOffset <= intersectionNodeHeight) {
					y += reverse ? -remainingOffset : remainingOffset;
					remainingOffset = 0;
				} else {
					remainingOffset -= intersectionNodeHeight - distanceAlongSegment;
					[x, y] = reverse ? topRight : bottomRight;
					segment = reverse ? "top" : "bottom";
					distanceAlongSegment = 0;
				}
				break;
			case "bottom":
				if (reverse) segment = "right";
				if (distanceAlongSegment + remainingOffset <= intersectionNodeWidth) {
					x -= reverse ? -remainingOffset : remainingOffset;
					remainingOffset = 0;
				} else {
					remainingOffset -= intersectionNodeWidth - distanceAlongSegment;
					[x, y] = reverse ? bottomRight : bottomLeft;
					segment = reverse ? "right" : "left";
					distanceAlongSegment = 0;
				}
				break;
			case "left":
				if (reverse) segment = "bottom";
				if (distanceAlongSegment + remainingOffset <= intersectionNodeHeight) {
					y -= reverse ? -remainingOffset : remainingOffset;
					remainingOffset = 0;
				} else {
					remainingOffset -= intersectionNodeHeight - distanceAlongSegment;
					[x, y] = reverse ? bottomLeft : topLeft;
					segment = reverse ? "bottom" : "top";
					distanceAlongSegment = 0;
				}
				break;
		}
	}

	return { x, y };
}

// returns the position (top,right,bottom or right) passed node compared to the intersection point
function getEdgePosition(node, intersectionPoint) {
	const n = { ...node.positionAbsolute, ...node };
	const nx = Math.round(n.x);
	const ny = Math.round(n.y);
	const px = Math.round(intersectionPoint.x);
	const py = Math.round(intersectionPoint.y);

	if (px <= nx + 1) {
		return Position.Left;
	}
	if (px >= nx + n.width - 1) {
		return Position.Right;
	}
	if (py <= ny + 1) {
		return Position.Top;
	}
	if (py >= n.y + n.height - 1) {
		return Position.Bottom;
	}

	return Position.Top;
}

// returns the parameters (sx, sy, tx, ty, sourcePos, targetPos) you need to create an edge
export function getEdgeParams(source, target, index, numEdges) {
	const sourceIntersectionPoint = getNodeIntersection(
		source,
		target,
		index,
		numEdges
	);
	const targetIntersectionPoint = getNodeIntersection(
		target,
		source,
		numEdges - index - 1,
		numEdges
	);

	const sourcePos = getEdgePosition(source, sourceIntersectionPoint);
	const targetPos = getEdgePosition(target, targetIntersectionPoint);

	return {
		sx: sourceIntersectionPoint.x,
		sy: sourceIntersectionPoint.y,
		tx: targetIntersectionPoint.x,
		ty: targetIntersectionPoint.y,
		sourcePos,
		targetPos,
	};
}
