import { Typography, Box } from "@mui/material";
import { grey } from "@mui/material/colors";

export const trans = {
	filePlaceHolder: {
		en: "Filenames",
		fr: "Nom des fichiers",
	},
	warning: {
		en: "No file selected",
		fr: "Aucun fichier sélectionné",
	},
	history: {
		en: "History",
		fr: "Historique",
	},
	historyTitle: {
		en: "Previous summarizations",
		fr: "Anciens résumés",
	},
	runAbstract: {
		en: "Summarize the file",
		fr: "Résumer le fichier",
	},
	labelMaxEntities: {
		en: "Maximum number of entities",
		fr: "Nombre d'entités maximum",
	},
	successMessage: {
		en: "Abstraction ended successfully.",
		fr: "L'abstraction s'est terminée avec succès.",
	},
	extractionSuccessMessage: {
		en: "Data extraction ended successfully.",
		fr: "L'extraction des données s'est terminée avec succès.",
	},
	errorMessage: {
		en: "An error occurred during abstraction.",
		fr: "Une erreur s'est produite durant l'abstraction.",
	},
	selectEntitiesButton: {
		en: "Select entities",
		fr: "Sélectionner des entités",
	},
	exitSelectionButton: {
		en: "Exit selection",
		fr: "Quitter la sélection",
	},
	extractDataButton: {
		en: "Extract data",
		fr: "Extraire les données",
	},
	sortOptions: {
		name: { fr: "Noms", en: "Names" },
		frequency: { fr: "Fréquence", en: "Frequency" },
	},
	sort: {
		fr: "Trier les attributs par",
		en: "Sort attributes by",
	},

	helper: {
		fr: (
			<Typography>
				<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
					But : avoir une visualisation de la structure des données.
				</Box>
				Pour obtenir la visualisation structurelle d'un fichier, sélectionnez-le
				puis cliquez sur "Résumer le fichier". Le nombre d'entités maximum, 6
				par défaut, permet de contrôler le nombre d'objets dans la
				visualisation. <br />
				Attention : l'abstraction n'est pas disponible pour tous les formats de
				fichiers, uniquement ceux compatibles sont montrés dans la liste des
				fichiers à gauche.
				<Box sx={{ fontWeight: 600, color: grey[800], mt: 2, mb: 1 }}>
					Comment lire le résultat ?
				</Box>
				Chaque "boîte" correspond à un ensemble d'objets (appelés tuples)
				similaires. Ces objets ont des propriétés, plus ou moins complexes,
				affichées sous forme de listes déroulantes. Les pourcentages indiquent
				le nombre d'objets ayant cette propriété.
				<Box sx={{ fontWeight: 600, color: grey[800], mt: 2, mb: 1 }}>
					Comment extraire des données dans un tableau ?
				</Box>
				Cliquez sur le bouton "Sélectionner des entités". Sélectionnez l'entité
				et les attributs dont vous voulez extraire les données. Les données
				seront extraites sous forme de tableau. La case à cocher de gauche pour
				chaque attribut indique si cet attribut sera inclus ou non dans le
				tableau. La case à cocher à droite indique si l'attribut est obligatoire
				ou non. Si un attribut est obligatoire, les lignes où l'attribut est
				vide seront rejetées. Lorsque vous avez terminé de sélectionner les
				attributs, cliquez sur le bouton "Extraire les données". Faites défiler
				vers le bas pour voir le résultat.
			</Typography>
		),
		en: (
			<Typography>
				<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
					Goal: a structural visualization of your data.
				</Box>
				To obtain a structural visualization of a file, select it and click on
				"Summarize the file". The maximum number of entities, 6 by default,
				allows you to control the number of objects you will see in the result.
				<br />
				Warning: abstraction is not available for all data formats, only files
				with the appropriate format will appear in the left list.
				<Box sx={{ fontWeight: 600, color: grey[800], mt: 2, mb: 1 }}>
					How to read the result?
				</Box>
				Each "box" is a set of similar objects (called records). These objects
				have properties, more or less complex, displayed in the form of
				drop-down lists. Percentages indicate the number of objects having that
				property.
				<Box sx={{ fontWeight: 600, color: grey[800], mt: 2, mb: 1 }}>
					How to extract data to a table?
				</Box>
				Click on the "Select entities" button. Select the entity and attributes
				you want to extract data from. The data will be extracted in a table
				format. The left checkbox for each attribute signifies whether that
				attribute will be included in the table or not. The right checkbox
				signifies whether the attribute is required or not. If an attribute is
				required, then the rows in which the attribute is empty will be
				discarded. When you are done selecting the attributes, click on the
				"Extract data" button. Scroll down to see the result.
			</Typography>
		),
	},
};
