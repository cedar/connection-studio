import React, { useCallback } from "react";
import {
	useStore,
	getBezierPath,
	EdgeLabelRenderer,
	Position,
} from "reactflow";

import { Box, Checkbox } from "@mui/material";

import { EntitySelectionContext } from "../../context/EntitySelection.js";
import { getEdgeParams } from "./utils.js";

function FloatingEdge({ id, source, target, markerEnd, label, labelStyle }) {
	const {
		selectionState,
		updateSelectionState,
		currentEntities,
		selectionActive,
	} = React.useContext(EntitySelectionContext);

	const sourceNode = useStore(
		useCallback((store) => store.nodeInternals.get(source), [source])
	);
	const targetNode = useStore(
		useCallback((store) => store.nodeInternals.get(target), [target])
	);

	const index = React.useMemo(() => {
		return parseInt(id.split("-")[2]);
	}, [id]);

	const onCheckboxClick = (e) => {
		e.stopPropagation();

		updateSelectionState((prevState) => {
			let newState = { ...prevState };
			newState.relationships[label].checked =
				!newState.relationships[label].checked;
			return newState;
		});
	};

	const isSelectable = React.useMemo(() => {
		if (!selectionActive) {
			return false;
		}

		for (const entity of currentEntities) {
			if (entity === sourceNode.id) {
				return true;
			}
		}

		return false;
	}, [currentEntities, sourceNode]);

	if (!sourceNode || !targetNode) {
		return null;
	}

	const { sx, sy, tx, ty, sourcePos, targetPos } = getEdgeParams(
		sourceNode,
		targetNode,
		index,
		sourceNode.data.numOutgoingEdges
	);

	let [edgePath, labelX, labelY] = getBezierPath({
		sourceX: sx,
		sourceY: sy,
		sourcePosition: sourcePos,
		targetPosition: targetPos,
		targetX: tx,
		targetY: ty,
	});

	let offsetY = 0;
	if (sourcePos == Position.Bottom && targetPos == Position.Top) {
		// labelY += index * 75;
		offsetY = (index - 1) * 0.3;
	} else if (sourcePos == Position.Top && targetPos == Position.Bottom) {
		// labelY -= index * 75;
		offsetY = (index - sourceNode.data.numOutgoingEdges / 2) * 0.3;
	}

	const nx = labelX - sx;
	const ny = labelY - sy;

	return (
		<>
			<path
				id={id}
				className="react-flow__edge-path"
				d={edgePath}
				markerEnd={markerEnd}
			/>

			<EdgeLabelRenderer>
				<Box
					style={{
						position: "absolute",
						transform: `translate(-50%, -50%) translate(${
							labelX + nx * offsetY
						}px,${labelY + ny * offsetY}px)`,
						background: "#fff",
						borderRadius: 5,
						fontSize: 14,
						fontWeight: 500,
						zIndex: 10000,
					}}
				>
					<Box
						style={{
							background: labelStyle.color,
							borderRadius: 5,
							padding: 5,
						}}
					>
						{isSelectable && (
							<Checkbox
								size="small"
								sx={{
									color: "white",
									"&.Mui-checked": {
										color: "white",
									},
									my: -1,
									pointerEvents: "all",
								}}
								className="nodrag nopan"
								checked={selectionState.relationships[label].checked}
								onClick={onCheckboxClick}
							/>
						)}
						{label}
					</Box>
				</Box>
			</EdgeLabelRenderer>
		</>
	);
}

export default FloatingEdge;
