import * as React from "react";

import {
	Fab,
	LinearProgress,
	MenuItem,
	Paper,
	Popover,
	Tab,
	Tabs,
	Typography,
} from "@mui/material";
import { Box } from "@mui/system";

import { useLang } from "../../context/Lang";
import "../../css/cl.css";
import { TYPES } from "../../utils/graph";
import { isDefined } from "../../utils/utils";
import { AlertCard, ResultCard } from "./preview";

const trans = {
	notFound: {
		neighbors: {
			en: "No neighbours...",
			fr: "Pas de voisins...",
		},
		search: {
			en: "No results...",
			fr: "Pas de résultats...",
		},
	},
	disabled: {
		neighbors: {
			p1: {
				en: "All neighbors of node",
				fr: "Tous les voisins du noeud",
			},
			p2: {
				en: "have been added...",
				fr: "ont été ajoutés...",
			},
		},
		search: {
			p1: {
				en: "All results of the query",
				fr: "Tous les résultats de la requête",
			},
			p2: {
				en: "have been added...",
				fr: "ont été ajoutés...",
			},
		},
	},
	tabs: {
		res: {
			en: "Results",
			fr: "Résultats",
		},
		neighbors: {
			en: "Neighbors",
			fr: "Voisins",
		},
	},
	searching: {
		res: {
			en: "Looking for results...",
			fr: "Recherche de résultats...",
		},
		neighbors: {
			en: "Looking for neighbors...",
			fr: "Recherche de voisins...",
		},
	},
};

function AddNeighbordItem({ type, ml, setActiveType, idx, isResults }) {
	const [anchorEl, setAnchorEl] = React.useState(null);

	const handlePopoverOpen = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handlePopoverClose = () => {
		setAnchorEl(null);
	};

	const open = Boolean(anchorEl);

	const handleAddAll = () => {
		setActiveType(type);
	};

	const { activeLang } = useLang();

	const trans = {
		neighbors: { fr: "voisins", en: "neighbors" },
	};

	return (
		<Box sx={{ ml: ml, zIndex: 0 }} key={idx}>
			<Fab
				key={`fab-${idx}`}
				onClick={handleAddAll}
				onMouseEnter={handlePopoverOpen}
				onMouseLeave={handlePopoverClose}
				style={{
					backgroundColor: type.color,
					width: 20,
					height: 20,
					minHeight: 0,
				}}
			></Fab>
			<Popover
				key={`pop-${idx}`}
				id="mouse-over-popover"
				sx={{
					pointerEvents: "none",
				}}
				open={open}
				anchorEl={anchorEl}
				anchorOrigin={{
					vertical: "bottom",
					horizontal: "left",
				}}
				transformOrigin={{
					vertical: "top",
					horizontal: "left",
				}}
				onClose={handlePopoverClose}
				disableRestoreFocus
			>
				<Typography sx={{ p: 1 }}>
					{type.pluralDisplayName[activeLang]}{" "}
					{isResults ? "" : trans.neighbors[activeLang]}
				</Typography>
			</Popover>
		</Box>
	);
}

function AddNeighborsMenu({
	neighbors = null,
	results = null,
	getActiveType,
	isCurrentResultsLoading = null,
}) {
	const [neighborsType, setNeighborsType] = React.useState([]);
	const { activeLang } = useLang();

	const setActiveType = (type) => {
		let activeType =
			type.displayName[activeLang] === ""
				? "all"
				: type.displayName[activeLang];

		getActiveType(activeType);
	};

	React.useEffect(() => {
		if (isDefined(neighbors)) {
			const currentNeighborsTypes = neighbors.map(
				(neighbor) => TYPES[neighbor.node.type]
			);
			const uniqueNeighborsType = [
				...new Map(
					currentNeighborsTypes.map((item) => [
						item["displayName"][activeLang],
						item,
					])
				).values(),
			];
			setNeighborsType([
				{
					displayName: { fr: "", en: "" },
					color: "white",
					pluralDisplayName: { fr: "Tous", en: "All" },
				},
				...uniqueNeighborsType,
			]);
		}
		if (isDefined(results)) {
			setNeighborsType([
				{
					displayName: { fr: "", en: "" },
					color: "white",
					pluralDisplayName: {
						fr: "Afficher tous les résultats",
						en: "Show all results",
					},
				},
			]);
		}
	}, [neighbors, results]);

	return (
		<Box sx={{ display: "flex", my: 2 }}>
			{isCurrentResultsLoading
				? null
				: neighborsType.map((type, idx) => (
						<AddNeighbordItem
							key={idx}
							idx={idx}
							type={type}
							ml={idx === 0 ? 0 : 2}
							setActiveType={setActiveType}
							isResults={isDefined(results)}
						></AddNeighbordItem>
				  ))}
		</Box>
	);
}

const ResultList = ({
	results,
	isCurrentResultsLoading,
	appendNodesToGraph,
	resultType,
	focusNode = null,
	shouldDisablePreview,
	activeType,
	getActiveType,
	query,
	addedNodesCache,
	onScroll,
}) => {
	const { activeLang } = useLang();

	const getNeighborsResult = (result) => {
		result["src"] = focusNode;
		return result;
	};

	// GLOBAL Cache
	// Add node to the global cache and append them to the graph
	const handleOnClickSearch = (event) => {
		const index = parseInt(event.currentTarget.getAttribute("id"));

		let result = filterResults(results, addedNodesCache)[index];

		appendNodesToGraph(result);

		// it's a search result
		if ("nodes" in result) {
			for (let node of result.nodes) {
				addedNodesCache.add(node.global_id);
				// setAddedNodesChache(addedNodesCache);
			}
		} else if ("node" in result) {
			addedNodesCache.add(result.node.global_id);
		}
	};

	// Filter current results to not include the ones already in the graph
	const filterResults = (results, addedNodesCache) => {
		if (!results) {
			return [];
		}

		// it's a search result
		let filteredResults = [];

		for (let result of results) {
			if ("nodes" in result) {
				let alreadyInSize = 0;
				for (let node of result.nodes) {
					if (addedNodesCache.has(node.global_id)) {
						alreadyInSize++;
					}
				}
				if (alreadyInSize !== result.nodes.length) {
					filteredResults.push(result);
				}
			} else if ("node" in result) {
				if (!addedNodesCache.has(result.node.global_id)) {
					filteredResults.push(result);
				}
			}
		}
		return filteredResults;
	};

	// If the user select a node type to add to the graph
	React.useEffect(() => {
		if (isDefined(activeType) && isDefined(results)) {
			let nodes = [];
			let links = [];
			if (resultType === "result") {
				for (let result of results) {
					for (let node of result.nodes) {
						addedNodesCache.add(node.global_id);
					}
					nodes = [...nodes, ...result.nodes];
					links = [...links, ...result.links];
				}
			} else {
				for (let result of results) {
					let nodeType = TYPES[result.node.type].displayName[activeLang];

					if (activeType === "all") {
						if (Object.hasOwn(result, "src")) {
							addedNodesCache.add(result.node.global_id);

							nodes.push(result.node);
							let srcNode = result.src.toObject();
							srcNode.local_id = srcNode.global_id;
							nodes.push(srcNode);
							links.push(result.link);
						}
					} else if (nodeType === activeType) {
						if (Object.hasOwn(result, "src")) {
							addedNodesCache.add(result.node.global_id);

							nodes.push(result.node);
							let srcNode = result.src.toObject();
							srcNode.local_id = srcNode.global_id;
							nodes.push(srcNode);
							links.push(result.link);
						}
					}
				}
			}
			appendNodesToGraph({ nodes: nodes, links: links });
			getActiveType(null);
		}
	}, [activeType]);

	const filteredResults = filterResults(results, addedNodesCache);

	return (
		<Box
			sx={{
				height: "100%",
				maxHeight: "100%",
				overflow: "hidden",
			}}
		>
			{isCurrentResultsLoading ? (
				<Box sx={{ width: "100%", p: 2 }}>
					<Typography sx={{ mb: 2 }}>Looking for neighbors...</Typography>
					<LinearProgress />
				</Box>
			) : isDefined(results) && results.length > 0 ? (
				filteredResults.length > 0 ? (
					<Box
						onScroll={onScroll}
						sx={{
							overflowY: "scroll",
							height: "100%",
							maxHeight: "40vh",
						}}
					>
						{filteredResults.map((result, idx) => {
							return (
								<MenuItem
									component={"div"}
									key={`node_${idx}`}
									sx={{
										display: "flex",
										zIndex: 10,
										width: "100%",
										px: 1,
									}}
									onClick={handleOnClickSearch}
									onMouseDown={(event) => event.stopPropagation()}
									id={idx}
								>
									<ResultCard
										result={
											resultType === "result"
												? result
												: getNeighborsResult(result)
										}
										title={resultType === "result" ? result.rank : idx + 1}
										type={resultType}
										shouldDisablePreview={shouldDisablePreview}
									></ResultCard>
								</MenuItem>
							);
						})}
					</Box>
				) : (
					<AlertCard
						entity={resultType === "result" ? query : focusNode.label}
						p1={
							resultType === "result"
								? trans.disabled.search.p1[activeLang]
								: trans.disabled.neighbors.p1[activeLang]
						}
						p2={
							resultType === "result"
								? trans.disabled.search.p2[activeLang]
								: trans.disabled.neighbors.p2[activeLang]
						}
					></AlertCard>
				)
			) : (
				<div>
					{query ? (
						<AlertCard
							p1={
								resultType === "result"
									? trans.notFound.search[activeLang]
									: trans.notFound.neighbors[activeLang]
							}
						></AlertCard>
					) : null}
				</div>
			)}
		</Box>
	);
};

export default function SideMenu({
	addedNodesCache,
	foundNeighbors,
	isNeighborsLoading,
	searchResults,
	isSearchResultsLoading,
	appendNodesToGraph,
	requestTab,
	getActiveTab,
	query,
	focusNode,
}) {
	const { activeLang } = useLang();

	// Uniform results
	const [searchResultsUniform, setSearchResultsUniform] = React.useState(null);
	React.useEffect(() => {
		// To uniformize every computing, "source" and "target" attribute
		// of links returned by kwsearch is set to node global id (from cl)
		// Its the case for results of "/cl/links" and "/cl/neighbors", and multiple keywords search
		if (searchResults) {
			const newSearchResults = [];
			searchResults.forEach((searchElem) => {
				let newResult = { ...searchElem };
				let newLinks = [];
				newResult.links.forEach((edge) => {
					let newEdge = { ...edge };
					newEdge.source = newResult.nodes[parseInt(edge.source)].global_id;
					newEdge.target = newResult.nodes[parseInt(edge.target)].global_id;
					newLinks.push(newEdge);
				});
				newResult.links = newLinks;
				newSearchResults.push(newResult);
			});

			setSearchResultsUniform(newSearchResults);
		}
	}, [searchResults]);

	//  Active Tab
	const [activeTab, setActiveTab] = React.useState(0);
	const handleChange = (e, tab) => {
		getActiveTab(tab);
		setActiveTab(tab);
	};

	if (requestTab !== -1 && activeTab !== requestTab) {
		setActiveTab(requestTab);
	}

	const getCurrentResults = (tab, searchResults, foundNeighbors) => {
		if (tab === 0) {
			return searchResults;
		} else if (tab === 1) {
			return foundNeighbors;
		}
	};

	const getCurrentResultsLoading = (
		tab,
		isSearchResultsLoading,
		isNeighborsLoading
	) => {
		if (tab === 0) {
			return isSearchResultsLoading;
		} else if (tab === 1) {
			return isNeighborsLoading;
		}
	};

	// Deactive tree preview on scroll
	const [shouldDisablePreview, setShouldDisablePreview] = React.useState(false);
	const handleScroll = () => {
		setShouldDisablePreview(true);
		setTimeout(() => setShouldDisablePreview(false), 200);
	};

	// Get active type in the search results
	const [activeType, setActiveType] = React.useState("");
	const getActiveType = (type) => {
		setActiveType(type);
	};

	// Get the type of the results search/neighbors
	const getResultType = (tab) => {
		if (tab === 0) {
			return "result";
		} else if (tab === 1) {
			return "neighbors";
		}
	};

	const currentResults = getCurrentResults(
		activeTab,
		searchResultsUniform,
		foundNeighbors
	);

	const isCurrentResultsLoading = getCurrentResultsLoading(
		activeTab,
		isSearchResultsLoading,
		isNeighborsLoading
	);

	return (
		<Box
			sx={{
				width: "20%",
				maxHeight: "100% ! important",
				maxWidth: "100% !important",
				ml: 0,
				mr: 0,
				p: 0,
			}}
		>
			<Tabs value={activeTab} onChange={handleChange}>
				<Tab label={trans.tabs.res[activeLang]} />
				<Tab label={trans.tabs.neighbors[activeLang]} />
			</Tabs>

			{activeTab === 1 && foundNeighbors ? (
				<AddNeighborsMenu
					neighbors={foundNeighbors}
					isCurrentResultsLoading={isCurrentResultsLoading}
					getActiveType={getActiveType}
				></AddNeighborsMenu>
			) : null}

			{activeTab === 0 && searchResultsUniform ? (
				<AddNeighborsMenu
					results={searchResultsUniform}
					isCurrentResultsLoading={isCurrentResultsLoading}
					getActiveType={getActiveType}
				></AddNeighborsMenu>
			) : null}
			<Box sx={{ width: "100%" }}>
				<ResultList
					results={currentResults}
					isCurrentResultsLoading={isCurrentResultsLoading}
					appendNodesToGraph={appendNodesToGraph}
					resultType={getResultType(activeTab)}
					addedNodesCache={addedNodesCache}
					focusNode={focusNode}
					shouldDisablePreview={shouldDisablePreview}
					activeType={activeType}
					getActiveType={getActiveType}
					query={query}
					onScroll={handleScroll}
				></ResultList>
			</Box>
		</Box>
	);
}
