import * as React from "react";

import {
	Card,
	CardContent,
	CardHeader,
	Paper,
	Popper,
	Typography,
} from "@mui/material";
import { Box, Container } from "@mui/system";
import * as d3 from "d3";

import { useLang } from "../../context/Lang";
import "../../css/cl.css";
import { D3Graph } from "../../utils/graph";
import {
	buildSimulation,
	drawGraph,
	freezeSimulation,
	getBoundingBox,
	initViewport,
} from "../../utils/simulation";
import { isDefined } from "../../utils/utils";

export function AlertCard({ p1, p2 = null, entity = null }) {
	return (
		<Typography
			sx={{
				mt: 5,
				textAlign: "center",
				width: "100%",
			}}
		>
			<Typography component="span" variant="body1" color="text.secondary">
				{p1}{" "}
				<Box component="span" display="inline" sx={{ fontWeight: 700 }}>
					{entity}{" "}
				</Box>
				{p2}{" "}
			</Typography>
		</Typography>
	);
}

export const DataSource = ({ result }) => {
	console.log(result);

	const set = new Set(result.links.map((link) => link.origin));

	const { activeLang } = useLang();

	const trans = {
		found: {
			en: `From ${set.size} source`,
			fr: `Parmi ${set.size} source`,
		},
		founds: {
			en: `From ${set.size} sources`,
			fr: `Parmi ${set.size} sources`,
		},
	};

	return (
		<Typography color="text.secondary">
			{set.size > 1 ? trans.founds[activeLang] : trans.found[activeLang]}
		</Typography>
	);
};

export const TreePreview = ({ data, shouldOpen, cardRef, title }) => {
	function useGraphRef() {
		const containerRef = React.useRef();
		const setRef = React.useCallback(
			(node) => {
				if (node !== null) {
					containerRef.current = node;
					const { svg, viewport } = initViewport(containerRef, false);

					var boundingBox = getBoundingBox(containerRef);
					const d3Graph = new D3Graph(data, boundingBox);

					const { nodes, texts, links, linksLabel, groups } = drawGraph(
						viewport,
						d3Graph
					);
					const simulation = buildSimulation(
						nodes,
						texts,
						links,
						linksLabel,
						d3Graph,
						boundingBox,
						50,
						-10
					);
					freezeSimulation(simulation);

					let transform;
					let zoom = d3.zoom().on("zoom", (e) => {
						let containsNaN = false;
						for (let [axis, value] of Object.entries(e.transform)) {
							if (isNaN(value)) {
								containsNaN = true;
								break;
							}
						}
						if (!containsNaN) {
							viewport.attr("transform", (transform = e.transform));
						}
					});
					svg.call(zoom).call(zoom.transform, d3.zoomIdentity);

					const nodesBBox = viewport.select(".nodes").node().getBBox();

					var padding = -45,
						hRatio = parseFloat(viewport.attr("width")) / nodesBBox.height,
						wRatio = parseFloat(viewport.attr("height")) / nodesBBox.width;

					zoom.scaleBy(viewport, hRatio < wRatio ? hRatio : wRatio);
				}
			},
			[data]
		);

		return [setRef];
	}
	const [containerRef] = useGraphRef();

	const { activeLang } = useLang();

	const trans = {
		title: {
			en: `Preview N° ${title}`,
			fr: `Pré-Visualisation N° ${title}`,
		},
	};

	return (
		<Box>
			<Popper
				anchorEl={cardRef.current}
				placement={"right"}
				open={shouldOpen}
				component={Paper}
				sx={{
					zIndex: 100,
					width: 500,
				}}
			>
				<Card
					sx={{ width: "100%", height: "100%", p: "0 !important" }}
					elevation={4}
				>
					<CardHeader title={trans.title[activeLang]}></CardHeader>
					<CardContent
						sx={{ width: "100%", height: "100%", p: "0 !important" }}
					>
						<Container
							sx={{ width: "100%", height: 200, p: "0 !important" }}
							component="svg"
							ref={containerRef}
							id="preview-graph-svg"
						></Container>
					</CardContent>
				</Card>
			</Popper>
		</Box>
	);
};

export const ResultCard = ({ result, title, type, shouldDisablePreview }) => {
	const [open, setOpen] = React.useState(false);
	const [currentResult, setCurrentResult] = React.useState({});
	const cardRef = React.useRef();

	if (JSON.stringify(result) !== JSON.stringify(currentResult)) {
		setCurrentResult(result);
	}

	const handleOnShowPreview = () => {
		if (isDefined(shouldDisablePreview) && !shouldDisablePreview) {
			setOpen(true);
		}
	};

	const handleOnHiddePreview = () => {
		setOpen(false);
	};

	React.useEffect(() => {
		if (isDefined(shouldDisablePreview) && shouldDisablePreview) {
			setOpen(false);
		}
	}, [shouldDisablePreview]);

	const { activeLang } = useLang();

	const trans = {
		found: {
			en: "Node found",
			fr: "Noeud trouvé",
		},
		founds: {
			en: "Nodes found",
			fr: "Noeuds trouvés",
		},
		link: {
			en: "Link:",
			fr: "Relation:",
		},
	};

	const Content = ({ type }) => {
		if (type === "result") {
			return (
				<Box>
					<Typography color="text.secondary">
						{result.nodes.length}{" "}
						{result.nodes.length > 1
							? trans.founds[activeLang]
							: trans.found[activeLang]}
					</Typography>
					<DataSource result={result}></DataSource>
					<Typography color="text.secondary">{result.label}</Typography>
					<Typography color="text.secondary">
						Score: {result.score.toFixed(2)}
					</Typography>
				</Box>
			);
		}
		if (type === "neighbors") {
			const nodeLabel = result.node.label.includes("/")
				? `${result.node.label
						.split("/")
						.slice(0, 3)
						.join("/")}...${result.node.label.split("/").slice(-1)}`
				: result.node.label;
			return (
				<Box>
					<Typography color="text.secondary">Label: {nodeLabel}</Typography>
					<Typography color="text.secondary">
						{trans.link[activeLang]} {result.link.label}
					</Typography>
				</Box>
			);
		}
	};

	return (
		<Card
			sx={{ width: "100%", px: 2 }}
			elevation={5}
			onMouseEnter={handleOnShowPreview}
			onMouseLeave={handleOnHiddePreview}
			ref={cardRef}
		>
			<CardContent sx={{ py: 0, pb: 1 }}>
				<Typography sx={{ fontSize: 20, py: 2 }}>N°{title}</Typography>
				<Content type={type}></Content>
			</CardContent>
			<TreePreview
				data={currentResult}
				shouldOpen={open}
				cardRef={cardRef}
				title={title}
			></TreePreview>
		</Card>
	);
};
