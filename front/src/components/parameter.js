import React from "react";
import { useParams } from "react-router-dom";

import SettingsIcon from "@mui/icons-material/Settings";
import {
	Accordion,
	AccordionDetails,
	AccordionSummary,
	Backdrop,
	Box,
	CircularProgress,
	TextField,
	Typography,
} from "@mui/material";
import { sendPost } from "fetch/fetcher";

import { useLang } from "../context/Lang";
import { useReadConfig, useSaveConfig } from "../fetch/parameters";
import {
	SEARCH_PARAMETERS,
	PARAMETERS,
	PARAMETERS_TYPES,
} from "../utils/parameters";
import { isDefined } from "../utils/utils";
import { ContainedButton } from "./button";
import DropDown from "./dropdown";

export function ParameterMenuItem({
	parameters,
	defaultValue,
	parameter,
	saveParameter,
	getCurrentConfig,
}) {
	const { activeLang } = useLang();
	const [isValid, setIsValid] = React.useState(true);

	const getDescription = (parameter) => {
		return parameters[parameter].description[activeLang];
	};
	const getChoice = (parameter) => {
		return Object.keys(parameters[parameter].values).map((key) => ({
			description: parameters[parameter].values[key][activeLang],
			value: parameters[parameter].values[key].id,
		}));
	};

	const keyInCurrentConfig = (key) => {
		let currentConfig = getCurrentConfig();
		if (isDefined(currentConfig) && key in currentConfig) {
			return true;
		}
		return false;
	};

	const getChoicesDefault = (parameter) => {
		const defaultParameter = parameters[parameter].values[defaultValue];
		return {
			description: defaultParameter[activeLang],
			value: defaultParameter.id,
		};
	};

	const getCurrentChoice = (parameter, id) => {
		for (const [value, map] of Object.entries(parameters[parameter].values)) {
			if (map.id === id) {
				saveParameter(parameter, value);
			}
		}
	};

	const handleRealValueChange = (e) => {
		let parameter = e.target.getAttribute("parameter");
		let value = e.target.value;

		switch (parameters[parameter].type) {
			case 1:
				if (isNaN(parseInt(value))) {
					setIsValid(false);
					saveParameter(parameter, value);
				} else {
					setIsValid(true);
					saveParameter(parameter, parseInt(value));
				}
				break;

			case 2:
				if (isNaN(parseInt(value))) {
					setIsValid(false);
					saveParameter(parameter, value);
				} else {
					setIsValid(true);
					saveParameter(parameter, parseFloat(value));
				}
				break;

			case 4:
				setIsValid(true);
				saveParameter(parameter, value);
				break;

			default:
				setIsValid(false);
				saveParameter(parameter, value);
		}
	};

	const trans = {
		error: {
			en: `Not a valid argument ${
				PARAMETERS_TYPES[parameters[parameter].type]
			}`,
			fr: `Argument non valide, doit être de type: ${
				PARAMETERS_TYPES[parameters[parameter].type][activeLang]
			}`,
		},
	};
	const getParameterItem = (parameter) => {
		if (parameters[parameter].type === 0 || parameters[parameter].type === 3) {
			return (
				<DropDown
					sx={{ mr: 3 }}
					inputLabel={getDescription(parameter)}
					items={getChoice(parameter)}
					getChoice={getCurrentChoice}
					width={200}
					value={
						keyInCurrentConfig(parameter) ? null : getChoicesDefault(parameter)
					}
					parameter={parameter}
				></DropDown>
			);
		}
		if (
			parameters[parameter].type === 1 ||
			parameters[parameter].type === 2 ||
			parameters[parameter].type === 4
		) {
			return (
				<TextField
					sx={{ mr: 3 }}
					InputProps={{
						sx: { height: "100%" },
					}}
					inputProps={{
						parameter: parameter,
					}}
					InputLabelProps={{
						shrink: true,
					}}
					multiline
					maxRows={4}
					id="outlined-basic"
					label={getDescription(parameter)}
					variant="outlined"
					value={keyInCurrentConfig(parameter) ? null : defaultValue}
					onChange={handleRealValueChange}
					error={isValid ? false : true}
					helperText={isValid ? null : trans.error[activeLang]}
				/>
			);
		}
	};

	return <Box>{isDefined(parameter) ? getParameterItem(parameter) : null}</Box>;
}

export default function ParametersMenu({ parameters, getParams }) {
	const params = useParams();
	const database = params.database;
	const { activeLang } = useLang();

	const [shouldFetchSaveConfig, setShouldFetchSaveConfig] =
		React.useState(false);

	const [currentConfig, setCurrentConfig] = React.useState({});

	const { save, isSaveConfigError, isSaveConfigLoading } = useSaveConfig(
		shouldFetchSaveConfig,
		database,
		currentConfig
	);
	const saveParameter = (parameter, value) => {
		let valueToString = `${value}`;
		currentConfig[parameter] = valueToString;

		setCurrentConfig({ ...currentConfig });
	};

	const getCurrentConfig = () => {
		return currentConfig;
	};

	const [isLoading, setIsLoading] = React.useState(false);
	const [saved, setData] = React.useState(false);
	const [error, setError] = React.useState(false);

	const handleSaveParameters = () => {
		sendPost("/cl/config", {
			d: database.replace("cl_", ""),
			cfg: currentConfig,
		})
			.then((res) => {
				setIsLoading(false);
				setData(res.data);
			})
			.catch((error) => {
				setError(error);
				setIsLoading(false);
			});

		setIsLoading(true);

		if (getParams) {
			getParams({ ...config, ...currentConfig });
		}
	};

	const { config, isReadConfigError, isReadConfigLoading } = useReadConfig(
		database,
		Object.keys(parameters)
	);

	const [backdropOpen, setBackdropOpen] = React.useState(false);

	const trans = {
		title: {
			en: "Uploaded files",
			fr: "Fichiers chargés",
		},
		paramsTitle: {
			fr: "Afficher les options avancées",
			en: "Display advanced options",
		},
		savebtn: {
			fr: "Sauvegarder les paramètres",
			en: "Save parameters",
		},
	};

	React.useEffect(() => {
		if (shouldFetchSaveConfig && !isSaveConfigLoading && isDefined(save)) {
			setShouldFetchSaveConfig(false);
		}
	}, [shouldFetchSaveConfig, isSaveConfigLoading, save]);

	React.useEffect(() => {
		if (isSaveConfigLoading === true) {
			setBackdropOpen(true);
		}
		if (isSaveConfigLoading === false) {
			setBackdropOpen(false);
		}
	}, [isSaveConfigLoading]);

	return (
		<Box>
			<Accordion elevation={0} sx={{ width: "100%" }}>
				<AccordionSummary sx={{ p: 0 }}>
					<ContainedButton
						sx={{
							height: 40,
							p: 2,
						}}
					>
						<SettingsIcon sx={{ mr: 1 }}></SettingsIcon>
						<Typography sx={{ fontWeight: 700 }}>
							{trans.paramsTitle[activeLang]}
						</Typography>
					</ContainedButton>
				</AccordionSummary>
				{isDefined(config) ? (
					<AccordionDetails sx={{ display: "flex", width: "100%" }}>
						{isDefined(parameters)
							? Object.keys(parameters).map((parameter) => (
									<ParameterMenuItem
										key={parameter}
										parameter={parameter}
										defaultValue={config[parameter]}
										parameters={parameters}
										saveParameter={saveParameter}
										getCurrentConfig={getCurrentConfig}
									></ParameterMenuItem>
							  ))
							: null}
						<ContainedButton onClick={handleSaveParameters}>
							{trans.savebtn[activeLang]}
						</ContainedButton>
					</AccordionDetails>
				) : null}
			</Accordion>
			<Backdrop
				open={backdropOpen}
				sx={{ position: "absolute", color: "#fff", "z-index": "10 !important" }}
			>
				<CircularProgress color="inherit" />
			</Backdrop>
		</Box>
	);
}
