import { useDropzone } from "react-dropzone";

import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import ClearIcon from "@mui/icons-material/Clear";
import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile";
import ReportIcon from "@mui/icons-material/Report";
import {
	TextField,
	Card,
	IconButton,
	ListItem,
	ImageListItem,
	Alert,
} from "@mui/material";
import { Typography } from "@mui/material";
import { green, grey, red } from "@mui/material/colors";
import { Box } from "@mui/system";
import { active } from "d3";

import { useLang } from "../context/Lang";
import folder from "../static/logo_folders.png";

const trans = {
	subtitle: {
		en: "Add one or more files",
		fr: "Ajouter un ou plusieurs fichiers",
	},
	p1: {
		en: "Upload your files here or click to browse your folders",
		fr: "Déposez vos fichiers ici ou cliquez pour parcourir vos dossiers",
	},
	list: {
		en: "My Files",
		fr: "Mes fichiers",
	},
	url: {
		en: "URL (Optional)",
		fr: "URL (Optionnel)",
	},
	uploadError: {
		en: "An error occured while adding the source: ",
		fr: "Une erreur est survenue pendant l'ajout de la source ",
	},
};

const FileTextSize = ({ file }) => {
	const SizeConverter = (size) => {
		let convertedSize;
		if (size / 1000 < 1) {
			convertedSize = `${size} B`;
		}
		if (size / 1000 > 1) {
			convertedSize = `${(size / 1000).toFixed(1)} kB`;
		}
		if (size / 1000000 > 1) {
			convertedSize = `${(size / 1000000).toFixed(1)} MB`;
		}
		if (size / 1000000000 > 1) {
			convertedSize = `${(size / 1000000).toFixed(1)} GB`;
		}
		return convertedSize;
	};

	return (
		<Box
			sx={{
				position: "relative",
				display: "inline-flex",
				flexDirection: "column",
				ml: 3,
			}}
		>
			<Typography
				component="div"
				variant="body2"
				sx={{
					display: "flex",
					alignItems: "center",
					fontWeight: 700,
					whiteSpace: "nowrap",
				}}
			>
				{file.name}
			</Typography>
			<Typography component="div" variant="body2" color="text.secondary">
				{SizeConverter(file.size)}
			</Typography>
		</Box>
	);
};

const URLTextField = ({
	queueIsLoading,
	queueIsFinish,
	handleOnTextChange,
	file,
}) => {
	const { activeLang } = useLang();
	return (
		<Box
			sx={{
				width: "100%",
				ml: 3,
			}}
		>
			{!queueIsLoading && !queueIsFinish && (
				<TextField
					sx={{
						mb: 2.5,
						width: "100%",
					}}
					label={trans.url[activeLang]}
					variant="standard"
					id={file.name}
					onChange={handleOnTextChange}
				/>
			)}
		</Box>
	);
};

const RemoveFileIcon = ({
	queueIsLoading,
	queueIsFinish,
	handleOnDelete,
	file,
}) => {
	return (
		<Box>
			{!queueIsLoading && !queueIsFinish && (
				<IconButton
					id={file.name}
					sx={{ position: "relative", cursor: "pointer", ml: 5 }}
					onClick={handleOnDelete}
				>
					<ClearIcon
						id={file.name}
						sx={{
							fontSize: 20,
							display: "inline-flex",
							alignItems: "center",
							justifyContent: "center",
						}}
					></ClearIcon>
				</IconButton>
			)}
		</Box>
	);
};

export function FileItem({
	file,
	onUpdateFile,
	onDeleteFile,
	uploadStatus,
	uploadMsg,
	queueIsLoading,
	queueIsFinish,
}) {
	const { activeLang } = useLang();

	const handleOnTextChange = (event) => {
		const file = event.target.getAttribute("id");
		var currentFilesForm = {};
		currentFilesForm[file] = { url: event.target.value };
		onUpdateFile(file, event.target.value);
	};

	const handleOnDelete = (event) => {
		const fileToDelete = event.currentTarget.getAttribute("id");
		onDeleteFile(fileToDelete);
	};

	console.log(uploadStatus);

	return (
		<>
			<ListItem
				key={file.name}
				sx={{
					display: "flex",
					position: "relative",
					border: 1,
					borderRadius: 2,
					borderColor: grey[200],
					flexDirection: "row",
					alignItems: "center",
					p: 1,
					mb: 1,
					maxHeight: 75,
				}}
			>
				<InsertDriveFileIcon
					sx={{ fontSize: 32 }}
					style={{ color: grey[600] }}
				></InsertDriveFileIcon>
				<FileTextSize file={file}></FileTextSize>
				<URLTextField
					file={file}
					queueIsLoading={queueIsLoading}
					queueIsFinish={queueIsFinish}
					handleOnTextChange={handleOnTextChange}
				></URLTextField>
				<RemoveFileIcon
					queueIsLoading={queueIsLoading}
					queueIsFinish={queueIsFinish}
					handleOnDelete={handleOnDelete}
					file={file}
				></RemoveFileIcon>

				{uploadStatus === 200 && (
					<CheckCircleIcon sx={{ color: green[500] }}></CheckCircleIcon>
				)}
				{uploadStatus === 500 && (
					<ReportIcon sx={{ color: red[500] }}></ReportIcon>
				)}
			</ListItem>
			{uploadStatus === 500 && (
				<Box sx={{ my: 2 }}>
					<Alert severity="error">
						{trans.uploadError[activeLang]}
						{file.name}: <br></br>"{uploadMsg}"
					</Alert>
				</Box>
			)}
		</>
	);
}

export function FileDropzone({ onDrop }) {
	const { activeLang } = useLang();

	const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

	return (
		<Box>
			<Card
				sx={{
					width: "100%",
					backgroundColor: isDragActive ? grey[50] : grey[100],
					textAlign: "center",
					boxShadow:
						"rgba(0, 0, 0, 0.04) 0px 5px 22px, rgba(0, 0, 0, 0.03) 0px 0px 0px 0.5px;",
					borderRadius: 4,
					border: 1,
					borderColor: "grey.300",
					borderStyle: "dashed",
					cursor: "pointer",
				}}
				elevation={1}
				{...getRootProps()}
			>
				<Box
					sx={{
						display: "flex",
						alignItems: "center",
						alignContent: "center",
						textAlign: "center",
						verticalAlign: "baseline",
						p: 5,
					}}
				>
					<ImageListItem sx={{ width: 100 }}>
						<img src={folder} alt="folders" loading="lazy" />
					</ImageListItem>
					<Box sx={{ display: "flex", flexDirection: "column", ml: 3 }}>
						<Typography gutterBottom variant="h5" component="div">
							{trans.subtitle[activeLang]}
						</Typography>
						<Typography
							gutterBottom
							variant="body2"
							component="div"
							color="text.secondary"
						>
							{trans.p1[activeLang]}
						</Typography>
					</Box>
				</Box>
				<input {...getInputProps()} />
			</Card>
		</Box>
	);
}
