import { Outlet, useLocation } from "react-router-dom";

import { Box } from "@mui/material";

import { useLang } from "../context/Lang";
import NavBar from "./navbar";

export default function Page() {
	let location = useLocation();
	let path = location.pathname.split("/").slice(-1)[0];
	var title = "";

	const { activeLang } = useLang();

	const trans = {
		abstra: { en: "Data abstraction", fr: "Abstraction de données" },
		cl: { en: "Search", fr: "Recherche" },
		pathways: { en: "Paths exploration", fr: "Exploration de chemins" },
		project: { en: "Projects", fr: "Projets" },
		files: { en: "Uploaded files", fr: "Fichiers chargés" },
		stats: { en: "Statistics", fr: "Statistiques" },
		view: { en: "Data view", fr: "Vue des données" },
	};

	switch (path) {
		case "abstra":
			title = trans.abstra[activeLang];
			break;
		case "connectionlens":
			title = trans.cl[activeLang];
			break;
		case "pathways":
			title = trans.pathways[activeLang];
			break;
		case "":
			title = trans.project[activeLang];
			break;
		case "sources":
			title = trans.files[activeLang];
			break;
		case "statistics":
			title = trans.stats[activeLang];
			break;
		case "view":
			title = trans.view[activeLang];
			break;
		default:
			break;
	}
	return (
		<Box sx={{ height: "100vh", width: "100vw" }}>
			<NavBar title={title}>
				<Box
					sx={{
						paddingTop: "8rem",
						paddingBottom: "2rem",
						px: "1.5rem",
						width: "100%",
						height: "100%",
					}}
				>
					<Outlet></Outlet>
				</Box>
			</NavBar>
		</Box>
	);
}
