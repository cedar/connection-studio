import React, { useState } from "react";

import { Storage } from "@mui/icons-material";
import { Box, TextField, ToggleButton, ToggleButtonGroup } from "@mui/material";
import DataSourceCard from "components/datasourceCard";
import SideMenu from "components/menu/sideMenu";
import { useLang } from "context/Lang";

function filterDataSources(file, textSearch, acceptedFormat) {
	if (!file.file.toLowerCase().includes(textSearch)) return false;
	if (!acceptedFormat.includes(file.type.toLowerCase())) return false;
	return true;
}

const trans = {
	filePlaceHolder: {
		en: "Filenames",
		fr: "Noms des fichiers",
	},
};

const supportedFilesType = ["json", "xml", "csv", "rdf"];

export default function FileSideMenu({
	dataSources,
	activeItem,
	handleSelectItem,
}) {
	const [textSearch, setTextSearch] = useState("");
	const { activeLang } = useLang();

	const [acceptedFormat, setAcceptedFormat] =
		React.useState(supportedFilesType);

	const handleFormat = (event, newFormats) => {
		setAcceptedFormat(newFormats);
	};

	return (
		<SideMenu iconButton={<Storage />} textButton={null}>
			<Box sx={{ mb: 2 }}>
				<TextField
					label={trans.filePlaceHolder[activeLang]}
					value={textSearch}
					variant="standard"
					onChange={(event) => {
						setTextSearch(event.target.value);
					}}
					sx={{ mr: 2 }}
				/>
			</Box>
			<Box sx={{ mb: 2 }}>
				<ToggleButtonGroup
					value={acceptedFormat}
					onChange={handleFormat}
					color="primary"
				>
					{supportedFilesType.map((fileFormat) => {
						return (
							<ToggleButton key={fileFormat} value={fileFormat}>
								{fileFormat}
							</ToggleButton>
						);
					})}
				</ToggleButtonGroup>
			</Box>
			<Box>
				{dataSources
					.filter((file) => filterDataSources(file, textSearch, acceptedFormat))
					.map((file) => {
						return (
							<DataSourceCard
								key={file.id}
								id={file.id}
								fileName={file.file}
								format={file.type}
								handleSelectItem={handleSelectItem}
								activeItem={activeItem}
							/>
						);
					})}
			</Box>
		</SideMenu>
	);
}
