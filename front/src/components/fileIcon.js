import { Box, ImageListItem } from "@mui/material";
import csv from "static/csv.png";
import json from "static/json.png";
import rdf from "static/rdf.png";
import xml from "static/xml.png";

function selectIcon(fileType) {
	switch (fileType) {
		case "XML":
			return xml;
		case "CSV":
			return csv;
		case "JSON":
			return json;
		case "RDF":
			return rdf;
		default:
			return null;
	}
}
export default function FileIcon({ fileType }) {
	return (
		<Box sx={{ p: 1.5 }}>
			<ImageListItem sx={{ width: 50 }}>
				<img src={selectIcon(fileType)} alt={fileType} loading="lazy" />
			</ImageListItem>
		</Box>
	);
}
