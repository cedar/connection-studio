import * as React from "react";

import {
	Box,
	TableRow,
	TableCell,
	Typography,
	Table,
	TableHead,
	TableBody,
} from "@mui/material";

import { useLang } from "../../context/Lang";
import { EntityIcon } from "../../utils/entities";

function HistoryEntry({ entry, onClickHistory, entityOption }) {
	return (
		<TableRow
			onClick={() => onClickHistory(entry)}
			sx={{
				cursor: "pointer",
				"&:last-child td, &:last-child th": { border: 0 },
				"&:hover": { backgroundColor: "lightgrey", transition: "0.3s ease" },
			}}
		>
			<TableCell align="left" component="th" scope="row">
				<Box sx={{ display: "flex" }}>
					<EntityIcon entityName={entry.leftEntityType}></EntityIcon>
					<Typography sx={{ my: "auto" }} variant="body2">
						{entityOption[entry.leftEntityType]}
					</Typography>
				</Box>
			</TableCell>
			<TableCell align="left" component="th" scope="row">
				<Box sx={{ display: "flex" }}>
					<EntityIcon entityName={entry.rightEntityType}></EntityIcon>
					<Typography sx={{ my: "auto" }} variant="body2">
						{entityOption[entry.rightEntityType]}
					</Typography>
				</Box>
			</TableCell>
			<TableCell align="left">{entry.maxNbPaths}</TableCell>
			<TableCell align="left">{entry.maxLengthPath}</TableCell>
		</TableRow>
	);
}
export default function PathWaysHistory({
	data,
	onClickHistory,
	entityOption,
}) {
	const { activeLang } = useLang();

	const trans = {
		h1: {
			en: "Source entity",
			fr: "Entité source",
		},
		h2: {
			en: "Target entity",
			fr: "Entité cible",
		},
		h3: {
			en: "Maximum number of paths",
			fr: "Nombre maximum de chemins",
		},
		h4: {
			en: "Maximum length of a path",
			fr: "Longueur maximum d'un chemin",
		},
	};

	const entityOptionDict = {};
	entityOption.forEach((entry) => {
		entityOptionDict[entry.key] = entry.label;
	});

	return (
		<Box>
			<Table sx={{ minWidth: 300 }}>
				<TableHead>
					<TableRow>
						<TableCell align="left">{trans.h1[activeLang]}</TableCell>
						<TableCell align="left">{trans.h2[activeLang]}</TableCell>
						<TableCell align="left">{trans.h3[activeLang]}</TableCell>
						<TableCell align="left">{trans.h4[activeLang]}</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{data
						? data.map((entry, index) => {
								return (
									<HistoryEntry
										key={index}
										entry={entry}
										onClickHistory={onClickHistory}
										entityOption={entityOptionDict}
									></HistoryEntry>
								);
						  })
						: null}
				</TableBody>
			</Table>
		</Box>
	);
}
