import * as React from "react";

import CloseIcon from "@mui/icons-material/Close";
import {
	Box,
	IconButton,
	Chip,
	Dialog,
	DialogTitle,
	Typography,
} from "@mui/material";
import { red } from "@mui/material/colors";

import { useLang } from "../../context/Lang";
import { EntityIcon } from "../../utils/entities";
import PathsWaysTable from "./pathWaysTable";

const rowStyle = {
	cursor: "pointer",
	transition: "0.2s ease",
	borderRadius: 2,
	display: "flex",
	":hover": {
		boxShadow:
			"rgba(0, 0, 0, 0.16) 0px 2px 4px, rgba(0, 0, 0, 0.23) 0px 2px 4px",
	},
	mb: 2,
	p: 1,
	flexWrap:"wrap",
	gap:1,
};

export default function PathWaysRowResult({ row }) {
	row.label = row.label.filter((label) => {
		return label.length > 0;
	});

	const [openResults, setOpenResults] = React.useState(false);
	function handleClick() {
		setOpenResults(true);
	}

	function handleClose() {
		setOpenResults(false);
	}

	const { activeLang } = useLang();

	const trans = {
		t: {
			en: "paths",
			fr: "chemins",
		},
		dialogTitle: {
			en: "Results",
			fr: "Résultats",
		},
	};

	return (
		<>
			<Box sx={rowStyle} onClick={handleClick}>
				{row.label
					.filter((label) => {
						return label.length > 0;
					})
					.map((label, index) => {
						return (
							<Chip
								key={index}
								sx={{ mr: 1 }}
								label={label}
								icon={
									index === 0 || index === row.label.length - 1 ? (
										<Box>
											<EntityIcon
												entityName={
													index === 0 ? row.leftEntityType : row.rightEntityType
												}
											/>
										</Box>
									) : null
								}
							/>
						);
					})}
				<Typography sx={{ py: "auto" }}>
					({row.dataPaths.length} {trans.t[activeLang]})
				</Typography>
			</Box>
			<Dialog
				open={openResults}
				onClose={handleClose}
				fullWidth={true}
				maxWidth="xl"
				scroll="paper"
			>
				<DialogTitle sx={{ m: 0, p: 2 }}>
					{trans.dialogTitle[activeLang]}
					<IconButton
						aria-label="close"
						onClick={handleClose}
						sx={{
							position: "absolute",
							right: 8,
							top: 8,
							color: red[500],
						}}
					>
						<CloseIcon />
					</IconButton>
				</DialogTitle>
				<PathsWaysTable
					label={row.label}
					dataPaths={row.dataPaths}
					row={row}
				></PathsWaysTable>
			</Dialog>
		</>
	);
}
