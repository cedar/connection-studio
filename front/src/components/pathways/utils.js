/**
 * @param {number} a
 * @param {number} b
 */
function compareInt(a, b) {
	if (a === b) return 0;
	if (a > b) return -1;
	return 1;
}

/**
 * @param {{ sizeDataPath: any; dataPaths: string | any[]; }} row_1
 * @param {{ sizeDataPath: any; dataPaths: string | any[]; }} row_2
 * @param {string} filterOption
 */
export function sortPathWaysRows(row_1, row_2, filterOption) {
	if (filterOption === "path_length") {
		return compareInt(row_1.sizeDataPath, row_2.sizeDataPath);
	} else {
		return compareInt(row_1.dataPaths.length, row_2.dataPaths.length);
	}
}

/**
 * @param {any[]} labelArray
 * @param {{ [x: string]: any; }} labelNames
 * @param {any} index
 * @param {any} rightEntity
 * @param {any} leftEntity
 */
export function constructRow(
	labelArray,
	labelNames,
	index,
	rightEntity,
	leftEntity
) {
	const row = { id: index };
	labelArray = labelArray.slice(1, -1);
	labelArray.forEach(
		(/** @type {any} */ label, /** @type {string | number} */ index) => {
			row[`${labelNames[index]}${index}`] = label;
		}
	);
	row["rightEntity"] = rightEntity;
	row["leftEntity"] = leftEntity;
	return row;
}

/**
 * @param {string} value
 * @param {string} entity
 * @param {string} color
 * @param {boolean} isFullText
 */
export function renderCellHighlight(value, entity, color, isFullText) {
	if (!value.toLowerCase().includes(entity.toLowerCase())) {
		if (isFullText) return "";
		return <span>{value}</span>;
	}
		
	const arr = value.toLowerCase().split(entity.toLowerCase());

	return (
		<span>
			{isFullText ? arr[0] : null}
			<div
				style={{
					backgroundColor: color,
					padding: "0.35em",
					borderRadius: "0.35em",
					fontWeight: 500,
					lineHeight: 1,
					display: "inline-block",
					id: "cellH"
				}}
			>
				{entity}
			</div>
			{isFullText ? arr[1] : null}
		</span>
	);
}

/**
 * @param {string} value
 * @param {string} ent_1
 * @param {string} color_1
 * @param {string} ent_2
 * @param {string} color_2
 * @param {boolean} isFullText
 */
export function renderTwoCellHighlight(
	value,
	ent_1,
	color_1,
	ent_2,
	color_2,
	isFullText
) {
	const arr = value.split(ent_1);

	return (
		<span>
			{renderCellHighlight(arr[0], ent_2, color_2, isFullText)}
			<div
				style={{
					backgroundColor: color_1,
					padding: "0.35em",
					borderRadius: "0.35em",
					fontWeight: 500,
					lineHeight: 1,
					display: "inline-block",
				}}
			>
				{ent_1}
			</div>
			{renderCellHighlight(arr[1], ent_2, color_2, isFullText)}
		</span>
	);
}
