import * as React from "react";

import FormatLineSpacingIcon from "@mui/icons-material/FormatLineSpacing";
import { Box, Button } from "@mui/material";
import {
	DataGrid,
	GridToolbarContainer,
	GridToolbarColumnsButton,
	GridToolbarFilterButton,
	GridToolbarExport,
	GridToolbarDensitySelector,
	frFR,
	enUS,
} from "@mui/x-data-grid";

import { useLang } from "../../context/Lang";
import { TYPES } from "../../utils/graph";
import {
	renderTwoCellHighlight,
	renderCellHighlight,
	constructRow,
} from "./utils";

function CustomToolbar({ onClick, isExpanded }) {
	return (
		<GridToolbarContainer>
			<GridToolbarColumnsButton />
			<GridToolbarFilterButton />
			<GridToolbarDensitySelector />
			<GridToolbarExport
				csvOptions={{ fileName: `pathways-${new Date().toISOString()}` }}
			/>
			<Button
				startIcon={<FormatLineSpacingIcon />}
				size="small"
				onClick={onClick}
			>
				{isExpanded ? "Réduire le texte" : "Etendre le texte"}
			</Button>
		</GridToolbarContainer>
	);
}

export default function PathsWaysTable({ label, dataPaths, row }) {
	const { activeLang } = useLang();
	const [isFullText, setIsFullText] = React.useState(true);

	const handleClickButton = () => {
		setIsFullText(!isFullText);
	};

	const columns = label.map((label_i, index) => {
		const column_dict = {
			field: `${label_i}${index}`,
			headerName: label_i,
			flex: 1,
		};
		if (index === label.length - 1 && index === 0) {
			column_dict["renderCell"] = (cellValues) =>
				renderTwoCellHighlight(
					cellValues.value,
					cellValues.row.leftEntity,
					TYPES[row.leftEntityType].color,
					cellValues.row.rightEntity,
					TYPES[row.rightEntityType].color,
					isFullText
				);
		} else if (index === 0) {
			column_dict["renderCell"] = (cellValues) =>
				 renderCellHighlight(
					cellValues.value,
					cellValues.row.leftEntity,
					TYPES[row.leftEntityType].color,
					isFullText
				);
			
		} else if (index === label.length - 1) {
			column_dict["renderCell"] = (cellValues) =>
				renderCellHighlight(
					cellValues.value,
					cellValues.row.rightEntity,
					TYPES[row.rightEntityType].color,
					isFullText
				);
		}

		return column_dict;
	});

	const rows = dataPaths.map((path, index) => {
		return constructRow(
			path.dataPathLabels,
			label,
			index,
			path.rightEntity,
			path.leftEntity
		);
	});

	return (
		<Box sx={{ height: "70vh", width: "100%", p: 2 }}>
			<DataGrid
				rows={rows}
				columns={columns}
				slots={{
					toolbar: () => (
						<CustomToolbar
							onClick={handleClickButton}
							isExpanded={isFullText}
						/>
					),
				}}
				getRowHeight={() => "auto"}
				initialState={{
					pagination: {
						paginationModel: {
							pageSize: 10,
						},
					},
				}}
				localeText={
					activeLang === "en"
						? enUS.components.MuiDataGrid.defaultProps.localeText
						: frFR.components.MuiDataGrid.defaultProps.localeText
				}
				pageSizeOptions={[10, 20, 50]}
				disableRowSelectionOnClick
				sx={{
					"&.MuiDataGrid-root--densityCompact .MuiDataGrid-cell": { py: "8px" },
					"&.MuiDataGrid-root--densityStandard .MuiDataGrid-cell": {
						py: "15px",
					},
					"&.MuiDataGrid-root--densityComfortable .MuiDataGrid-cell": {
						py: "22px",
					},
				}}
			/>
		</Box>
	);
}
