import * as React from "react";
import { useLocation } from "react-router-dom";
import { Link, useParams } from "react-router-dom";

import { Backdrop, IconButton, Paper } from "@mui/material";
import { Button } from "@mui/material";
import { Collapse } from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import { styled } from "@mui/system";
import { useSource } from "context/Sources";
import { useDataSources } from "fetch/sources";
import PropTypes from "prop-types";

import { useAuth } from "../context/Auth";
import { useLang } from "../context/Lang";
import { isDefined } from "../utils/utils";
import EnglishIcon from "./lang/eng";
import FrenchIcon from "./lang/french";

const theme = createTheme({
	transitions: {
		duration: {
			shortest: 150,
			shorter: 200,
			short: 250,
			standard: 500,
			complex: 375,
			enteringScreen: 225,
			leavingScreen: 195,
		},
	},
	palette: {
		background: {
			default: "#FBFAF9",
		},
		primary: {
			main: "#000000",
		},
		text: {
			dark: "#000000",
			light: "#ffffff",
			disabled: "#B9B6B2",
			disabled2: "#B9B6B2",
		},
		disabled: {
			main: "#B9B6B2",
		},
		secondary: {
			// light: "#0066ff",
			main: "#B9B6B2",
			// contrastText: "#ffcc00",
		},
		custom: {
			main: "#1493f7",
			contrastText: "rgba(0, 0, 0, 0.87)",
		},
		neutral: {
			main: "#64748B",
			light: "rgb(251, 251, 250)",
			dark: "rgb(175, 175, 174)",
			contrastText: "rgba(0, 0, 0, 0.87)",
		},

		contrastThreshold: 3,
		tonalOffset: 0.2,
	},
});

const StyledMenuItem = styled(MenuItem)(({ theme }) => ({
	fontSize: "1.5rem !important",
	"&:hover": {
		backgroundColor: "transparent",
	},
}));

const StyledTitle = styled(Typography)(({ theme }) => ({
	fontSize: "1.5rem !important",
	"&:hover": {
		backgroundColor: "transparent",
	},
	"&:click": {
		backgroundColor: "transparent",
	},
	transition: theme.transitions.create(["color", "color"], {
		duration: theme.transitions.duration.standard,
	}),
}));

const MenuItemTitle = ({
	enterMenu,
	handleOnCurrentEnter,
	indice,
	children,
	...props
}) => {
	const [isHover, setIsHover] = React.useState(false);
	const [index, setIndex] = React.useState();

	React.useEffect(() => {
		setIndex(indice);
	}, [indice]);

	const getColor = () => {
		if (enterMenu && isHover) {
			return theme.palette.primary.main;
		}
		if (enterMenu && !isHover) {
			return theme.palette.secondary.main;
		}
		if (!enterMenu) {
			return theme.palette.primary.main;
		}
	};

	const handleOnMouseEnter = () => {
		setIsHover(true);
		handleOnCurrentEnter(index);
	};
	const handleOnMouseLeave = () => {
		setIsHover(false);
		handleOnCurrentEnter(null);
	};

	return (
		<StyledMenuItem
			disableTouchRipple
			onMouseEnter={handleOnMouseEnter}
			onMouseLeave={handleOnMouseLeave}
			{...props}
			sx={{
				color: getColor(),
			}}
		>
			{children}
		</StyledMenuItem>
	);
};

function PopperMenu() {
	const anchorRef = React.useRef();
	const [anchorEl, setAnchorEl] = React.useState(null);
	const [enterMenu, setEnterMenu] = React.useState(false);
	const [current, setCurrent] = React.useState(null);

	const open = Boolean(anchorEl);
	const handleClick = (event) => {
		setAnchorEl(anchorRef.current);
	};
	const handleClose = () => {
		setAnchorEl(null);
	};

	const { activeLang } = useLang();

	const trans = {
		cl: {
			name: { en: "Graph visualization", fr: "Visualisation du graphe" },
			desc: {
				en: "Search in the data by keywords",
				fr: "Chercher dans les données par mots-clés",
			},
		},
		abstra: {
			name: { en: "Data abstraction", fr: "Abstraction de données" },
			desc: {
				en: "Visualising the data structure",
				fr: "Visualiser la structure des données",
			},
		},
		pathways: {
			name: { en: "Paths exploration", fr: "Exploration de chemins" },
			desc: {
				en: "Finding links between entities",
				fr: "Trouver des connexions entre les entités",
			},
		},
		stats: {
			name: { en: "Statistics", fr: "Statistiques" },
			desc: {
				en: "Entities identified in the project",
				fr: "Entités identifiées dans le projet",
			},
		},
		files: {
			name: { en: "Uploaded files", fr: "Fichiers chargés" },
			desc: {
				en: "Files uploaded into the project",
				fr: "Fichiers chargés dans le projet",
			},
		},
		view: {
			name: { en: "Data View", fr: "Vue des données" },
			desc: {
				en: "Data visualisation and cleaning",
				fr: "Visualiser et nettoyer les données",
			},
		},
		explore: { en: "Explore", fr: "Explorer" },
		close: { en: "Close", fr: "Fermer" },
	};

	const params = useParams();
	const database = params.database;
	const contents = [
		{
			name: trans.files.name[activeLang],
			description: trans.files.desc[activeLang],
			to: `/${database}/sources`,
		},
		{
			name: trans.stats.name[activeLang],
			description: trans.stats.desc[activeLang],
			to: `/${database}/statistics`,
		},
		{
			name: trans.view.name[activeLang],
			description: trans.view.desc[activeLang],
			to: `/${database}/view`,
		},

		{
			name: trans.abstra.name[activeLang],
			description: trans.abstra.desc[activeLang],
			to: `/${database}/abstra`,
		},
		{
			name: trans.pathways.name[activeLang],
			description: trans.pathways.desc[activeLang],
			to: `/${database}/pathways`,
		},
		{
			name: trans.cl.name[activeLang],
			description: trans.cl.desc[activeLang],
			to: `/${database}/connectionlens`,
		},
	];

	const handleOnCurrentEnter = (current) => {
		setCurrent(current);
	};

	const { sourcesLength } = useSource();

	return (
		<ThemeProvider theme={theme}>
			<Box display={{ display: "flex", flexGrow: 1 }}>
				<Button
					onClick={handleClick}
					sx={{ color: "white", textTransform: "capitalize" }}
				>
					<Typography variant="h5">{trans.explore[activeLang]}</Typography>
				</Button>
				<Menu
					id="basic-menu"
					anchorEl={anchorEl}
					open={open}
					onClose={handleClose}
					MenuListProps={{
						"aria-labelledby": "basic-button",
					}}
					PaperProps={{
						sx: {
							borderRadius: 2,
							height: "100%",
							p: 3,
							display: "flex",
							overflow: "hidden",
						},
					}}
				>
					<Box sx={{ display: "flex" }}>
						<Box sx={{}}>
							<Box>
								<StyledMenuItem
									onClick={handleClose}
									sx={{ mb: 6 }}
									disableTouchRipple
								>
									<Typography
										sx={{
											textTransform: "capitalize",
											color: theme.palette.secondary.main,
										}}
									>
										{trans.close[activeLang]}
									</Typography>
								</StyledMenuItem>
							</Box>
							<Box
								sx={{ display: "block" }}
								onMouseEnter={() => setEnterMenu(true)}
								onMouseLeave={() => setEnterMenu(false)}
							>
								{contents.map((content, indice) => (
									<Box key={indice}>
										{content.to.includes("statistics") &&
										sourcesLength === 0 ? null : (
											<MenuItemTitle
												key={content.name}
												enterMenu={enterMenu}
												onClick={handleClose}
												component={Link}
												to={content.to}
												handleOnCurrentEnter={handleOnCurrentEnter}
												indice={indice}
											>
												<StyledTitle>{content.name}</StyledTitle>
											</MenuItemTitle>
										)}
									</Box>
								))}
							</Box>
						</Box>
						<Box>
							<Collapse
								in={enterMenu}
								timeout="auto"
								unmountOnExit
								orientation="horizontal"
							>
								<Paper
									elevation={0}
									sx={{
										width: 400,
										height: "100%",
									}}
								>
									<Box
										sx={{
											width: "100%",
											height: "100%",
											mt: 20,
											display: "flex",
										}}
									>
										<Typography
											sx={{
												textAlign: "center",
												width: "100%",
												height: "100%",
											}}
										>
											{isDefined(current)
												? contents[current].description
												: null}
										</Typography>
									</Box>
								</Paper>
							</Collapse>
						</Box>
					</Box>
				</Menu>
				<Typography
					ref={anchorRef}
					sx={{
						width: 30,
						height: 30,
						position: "absolute",
						top: -10,
					}}
				></Typography>
				<Backdrop
					sx={{ color: "#fff", "z-index": "10 !important" }}
					open={open}
				></Backdrop>
			</Box>
		</ThemeProvider>
	);
}

function ElevationScroll(props) {
	const { children, window } = props;
	const trigger = useScrollTrigger({
		disableHysteresis: true,
		threshold: 0,
		target: window ? window() : undefined,
	});

	return React.cloneElement(children, {
		elevation: trigger ? 4 : 0,
	});
}

ElevationScroll.propTypes = {
	children: PropTypes.element.isRequired,
	window: PropTypes.func,
};

function LangSwticher() {
	const [open, setOpen] = React.useState(false);
	const [anchorEl, setAnchorEl] = React.useState(null);
	const anchorRef = React.useRef();
	const { setPersistentLang, activeLang } = useLang();

	const trans = {
		langName: {
			en: { en: "English", fr: "Anglais" },
			fr: { en: "French", fr: "Français" },
		},
	};

	const handleClick = (event) => {
		setAnchorEl(anchorRef.current);
		setOpen(true);
	};
	const handleClose = () => {
		setAnchorEl(null);
		setOpen(false);
	};

	const handleChangeLang = (lang) => {
		setPersistentLang(lang);
		setAnchorEl(null);
		setOpen(false);
	};

	const Icon = ({ lang }) => {
		return lang === "fr" ? (
			<FrenchIcon></FrenchIcon>
		) : (
			<EnglishIcon></EnglishIcon>
		);
	};

	const Langitem = ({ lang }) => {
		let name;
		if (lang === "fr") {
			name = trans.langName.fr[activeLang];
		}
		if (lang === "en") {
			name = trans.langName.en[activeLang];
		}

		const handleClick = () => {
			handleChangeLang(lang);
		};

		return (
			<MenuItem onClick={handleClick} sx={{ p: 0, pr: 1 }}>
				<IconButton sx={{ width: 40, height: 40 }}>
					<Icon lang={lang}></Icon>
				</IconButton>
				<Typography sx={{ pl: 1 }}>{name}</Typography>
			</MenuItem>
		);
	};
	return (
		<Box>
			<Box
				ref={anchorRef}
				sx={{ position: "relative", left: -85, top: 35 }}
			></Box>
			<IconButton onClick={handleClick} sx={{ width: 40, height: 40 }}>
				<Icon lang={activeLang}></Icon>
			</IconButton>
			<Menu
				id="basic-menu"
				anchorEl={anchorEl}
				open={open}
				onClose={handleClose}
				MenuListProps={{
					sx: {
						p: 0,
					},
				}}
				PaperProps={{
					sx: {
						borderRadius: 2,
						p: 1,
						display: "flex",
						overflow: "visible !important",
					},
				}}
			>
				<span
					style={{
						zIndex: 100,
						width: "12px",
						height: "12px",
						content: "",
						display: "block",
						position: "absolute",
						transform: "rotate(-135deg)",
						background: "rgb(255, 255, 255)",
						top: -13,
						left: 91,
						borderRadius: 2,
					}}
				></span>
				<Langitem handleChangeLang={handleChangeLang} lang="fr"></Langitem>
				<Langitem handleChangeLang={handleChangeLang} lang="en"></Langitem>
			</Menu>
		</Box>
	);
}

export default function NavBar(props) {
	const { isAuth } = useAuth();
	let location = useLocation();
	const isHome = location.pathname === "/" ? true : false;
	const params = useParams();
	const database = params.database;
	const { activeLang } = useLang();

	const trans = {
		projectName: {
			en: "Project",
			fr: "Projet",
		},
	};

	return (
		<React.Fragment>
			<CssBaseline />
			<ElevationScroll {...props}>
				<AppBar>
					<Toolbar sx={{ maxHeight: "8rem" }}>
						{isAuth && !isHome ? <PopperMenu /> : null}
						<Typography
							sx={{
								fontWeight: 700,
								overflow: "hidden",
								textTransform: "normal",
								mr: "2vw",
							}}
							variant="h2"
						>
							Connection Studio
						</Typography>
						<Typography
							sx={{
								fontWeight: 500,
								overflow: "hidden",
								textTransform: "normal",
								flexGrow: 1,
							}}
							variant="h3"
						>
							{props.title}
						</Typography>
						<Box>
							<LangSwticher></LangSwticher>
						</Box>
						<Box>
							<Button
								component={Link}
								to={"/"}
								sx={{ color: "white", textTransform: "capitalize", pl: 2 }}
							>
								<Typography
									sx={{
										fontWeight: 500,
										overflow: "hidden",
										textTransform: "normal",
									}}
									variant="h6"
								>
									{database
										? `${trans.projectName[activeLang]}: ${database
												.replace("cl_", "")
												.split("_")
												.join(" ")}`
										: null}
								</Typography>
							</Button>
						</Box>
					</Toolbar>
				</AppBar>
			</ElevationScroll>

			{props.children}
		</React.Fragment>
	);
}
