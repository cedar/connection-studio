import { Box, Card, CardContent } from "@mui/material";

import FileIcon from "./fileIcon";

export default function DataSourceCard({
	fileName,
	id,
	format,
	handleSelectItem,
	activeItem,
}) {
	return (
		<Card
			sx={{
				mb: 1,
				cursor: "pointer",
				backgroundColor: id === activeItem ? "rgba(25,118,210,0.1)" : null,
			}}
			onClick={() => handleSelectItem(id)}
			elevation={2}
		>
			<CardContent sx={{ display: "flex", p: "0 !important" }}>
				<FileIcon fileType={format} />
				<Box
					sx={{
						my: "auto",
						overflowX: "hidden",
						overflowWrap: "break-word",
					}}
					component="div"
				>
					{fileName.split("/").slice(-1)}
				</Box>
			</CardContent>
		</Card>
	);
}
