import React from "react";

import HelpIcon from "@mui/icons-material/Help";
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	IconButton,
} from "@mui/material";
import { blue, grey } from "@mui/material/colors";
import { styled } from "@mui/system";

import { useLang } from "../context/Lang";

export function ContainedButton(props) {
	const StyledButton = styled(Button)({
		backgroundColor: blue[50],
		borderRadius: 8,
		fontWeight: 700,
	});
	return <StyledButton {...props}></StyledButton>;
}

export function OutlinedButton(props) {
	const StyledButton = styled(Button)({
		borderRadius: 8,
		fontWeight: 700,
		color: "black",
		border: "1px solid #e0e0e0 !important",
		backgroundColor: "white",
		":hover": {
			bgcolor: grey,
			border: "1px solid black !important",
		},
	});
	return <StyledButton {...props}></StyledButton>;
}

export function HelpButton({ helper, sx }) {
	const [helperOpen, setHelperOpen] = React.useState(false);

	const handleOnClose = () => {
		setHelperOpen(false);
	};

	const handleOpenHelp = (event) => {
		setHelperOpen(true);
	};

	const { activeLang } = useLang();
	const trans = {
		title: { fr: "Aide", en: "Help" },
		close: { fr: "Quitter", en: "Close" },
	};

	return (
		<Box sx={{ ...sx }}>
			<IconButton onClick={handleOpenHelp}>
				<HelpIcon sx={{ color: blue[700] }}></HelpIcon>
			</IconButton>
			<Dialog open={helperOpen} onClose={handleOnClose} fullWidth maxWidth="md">
				<DialogTitle sx={{ fontWeight: 700 }}>
					{trans.title[activeLang]}
				</DialogTitle>
				<DialogContent>{helper}</DialogContent>
				<DialogActions>
					<Button onClick={handleOnClose}>{trans.close[activeLang]}</Button>
				</DialogActions>
			</Dialog>
		</Box>
	);
}
