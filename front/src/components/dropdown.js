import * as React from "react";

import {
	Autocomplete,
	Paper,
	Popover,
	Popper,
	TextField,
	Typography,
} from "@mui/material";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { red } from "@mui/material/colors";
import { prettifyValue } from "utils/view";

import { isDefined } from "../utils/utils";

export default function DropDown({
	inputLabel,
	getChoice,
	sx,
	items,
	width,
	value,
	parameter,
}) {
	const [choice, setChoice] = React.useState("");

	React.useEffect(() => {
		if (isDefined(value)) {
			if (typeof value === "string") {
				setChoice(value);
			} else {
				setChoice(value.value);
			}
		}
	}, [value]);

	const handleChange = (event) => {
		setChoice(event.target.value);

		if (isDefined(parameter)) {
			getChoice(parameter, event.target.value);
		} else {
			getChoice(event.target.value);
		}
	};

	return (
		<Box sx={{ width: width, ...sx }}>
			<FormControl fullWidth sx={{ height: "100%" }}>
				<InputLabel id="demo-simple-select-label">{inputLabel}</InputLabel>
				<Select
					labelId="demo-simple-select-label"
					id="demo-simple-select"
					value={choice}
					label={inputLabel}
					onChange={handleChange}
					sx={{ height: "100%" }}
				>
					{isDefined(items) && items.length > 0
						? items.map((item, idx) => (
								<MenuItem
									key={`${item}_${idx}`}
									value={typeof item === "string" ? item : item.value}
								>
									{prettifyValue(
										typeof item === "string" ? item : item.description
									)}
								</MenuItem>
						  ))
						: null}
				</Select>
			</FormControl>
		</Box>
	);
}

function DropDownTextField({ label, parserFunction, ...params }) {
	if (params && parserFunction) {
		params.inputProps.value = parserFunction(params.inputProps.value);
	}

	const [displayFull, setDisplayFull] = React.useState(false);
	const [anchorEl, setAnchorEl] = React.useState(null);

	const handleMouseEnter = (event) => {
		setDisplayFull(true);
		setAnchorEl(event.currentTarget);
	};

	const id = displayFull ? "simple-popover" : undefined;

	const handleClose = () => {
		setAnchorEl(null);
	};

	return (
		<TextField onMouseEnter={handleMouseEnter} {...params} label={label} />
		// <Box>

		// 	{/* {displayFull ? (
		// 		<Popover
		// 			id={id}
		// 			open={displayFull}
		// 			anchorEl={anchorEl}
		// 			onClose={handleClose}
		// 			anchorOrigin={{
		// 				vertical: "bottom",
		// 				horizontal: "left",
		// 			}}
		// 		>
		// 			<Typography sx={{ p: 2 }}>The content of the Popover.</Typography>
		// 		</Popover>
		// 	) : null} */}
		// </Box>
	);
}

// function CustomOption({ option }) {
// 	const [shouldPreview, setShouldPreview] = React.useState(false);

// 	const handleOnMouseEnter = (event) => {
// 		setShouldPreview(true);
// 		setAnchorEl(event.currentTarget);
// 	};

// 	const id = shouldPreview ? "simple-popover" : undefined;
// 	const [anchorEl, setAnchorEl] = React.useState(null);

// 	const handleClose = () => {
// 		setAnchorEl(null);
// 	};
// 	return (
// 		<Box onMouseEnter={handleOnMouseEnter}>
// 			<Popover
// 				id={id}
// 				open={shouldPreview}
// 				anchorEl={anchorEl}
// 				onClose={handleClose}
// 				anchorOrigin={{
// 					vertical: "bottom",
// 					horizontal: "left",
// 				}}
// 			></Popover>
// 			<Typography sx={{ p: 2 }}>The content of the Popover.</Typography>

// 			{option}
// 		</Box>
// 	);
// }

export function SearchableDropDown({
	items,
	inputLabel,
	getChoice,
	sx,
	parserFunction,
}) {
	const [value, setValue] = React.useState("");
	const [inputValue, setInputValue] = React.useState("");
	const [options, setOptions] = React.useState("");
	const [valueToID, setValueToID] = React.useState({});

	React.useEffect(() => {
		if (items) {
			let valueToID = {};
			for (let [idx, item] of items.entries()) {
				let parsedItem = item;
				if (parserFunction) {
					parsedItem = parserFunction(item);
				}

				valueToID[parsedItem] = idx;
			}
			setValueToID(valueToID);

			setOptions(items);
			setValue(items[0]);
		}
	}, [items]);

	const handleChange = (event, newValue) => {
		const idx = valueToID[newValue];

		setValue(newValue);
		if (newValue) {
			getChoice(options[idx]);
		}
	};

	return (
		<Box sx={{ ...sx }}>
			{items && inputLabel ? (
				<Autocomplete
					value={value}
					disablePortal
					options={
						parserFunction
							? items.map((elt, idx) => parserFunction(elt))
							: items
					}
					renderInput={(params) => (
						<DropDownTextField
							label={inputLabel}
							parserFunction={parserFunction}
							{...params}
						/>
					)}
					onChange={handleChange}
					inputValue={inputValue}
					onInputChange={(event, newInputValue) => {
						setInputValue(newInputValue);
					}}
					componentsProps={{ paper: { sx: { wordBreak: "break-word" } } }}
					// renderOption={(props, option, { selected }) => (
					// 	<li {...props}>
					// 		<CustomOption option={option}></CustomOption>
					// 	</li>
					// )}
				/>
			) : null}
		</Box>
	);
}
