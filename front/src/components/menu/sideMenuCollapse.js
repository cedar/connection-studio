import * as React from "react";

import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Typography, Button, Collapse } from "@mui/material";
import { Box } from "@mui/system";

import { useLang } from "../../context/Lang";

export default function SideMenu({ iconButton, textButton, ...props }) {
	const [expanded, setExpanded] = React.useState(true);

	const handleExpandClick = () => {
		setExpanded(!expanded);
	};

	const { activeLang } = useLang();

	const trans = {
		btn: {
			en: "Files",
			fr: "Fichiers",
		},
	};

	return (
		<Box>
			<Button onClick={handleExpandClick}>
				{iconButton ? iconButton : <ExpandMoreIcon />}
				<Typography>{trans.btn[activeLang]}</Typography>
			</Button>
			<Collapse
				in={expanded}
				timeout="auto"
				unmountOnExit
				orientation={"horizontal"}
			>
				<Box
					sx={{
						width: "20vw",
						px: 2,
						py: 1,
						maxHeight: "80vh",
						overflow: "auto",
					}}
				>
					{props.children}
				</Box>
			</Collapse>
		</Box>
	);
}
