import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Button, Typography } from "@mui/material";
import { Box } from "@mui/system";

import { useLang } from "../../context/Lang";

export default function SideMenu({ iconButton, textButton = null, ...props }) {
	const { activeLang } = useLang();

	const trans = {
		btn: {
			en: "Files",
			fr: "Fichiers",
		},
	};

	return (
		<Box>
			<Button>
				{iconButton ? iconButton : <ExpandMoreIcon />}
				<Typography>{trans.btn[activeLang]}</Typography>
			</Button>

			<Box
				sx={{
					pl: "1rem",
					pr: "2rem",
					py: 1,
					maxHeight: "80vh",
					overflow: "auto",
				}}
			>
				{props.children}
			</Box>
		</Box>
	);
}
