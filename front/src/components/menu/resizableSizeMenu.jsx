import { Panel, PanelGroup, PanelResizeHandle } from "react-resizable-panels";

import { DragIndicator } from "@mui/icons-material";
import { Divider } from "@mui/material";

export default function ResizableSizeMenu({ menuComponent, children }) {
	return (
		<PanelGroup autoSaveId="filemenu" direction="horizontal">
			<Panel defaultSize={25} minSize={5}>
				{menuComponent}
			</Panel>
			<PanelResizeHandle>
				<DragIndicator sx={{ top: "50%", position: "absolute" }} />
				<Divider orientation="vertical" sx={{ height: "80vh" }} />
			</PanelResizeHandle>
			<Panel defaultSize={75} minSize={25}>
				{children}
			</Panel>
		</PanelGroup>
	);
}
