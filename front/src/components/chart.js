import * as React from "react";
import Chart from "react-apexcharts";

import { Button, CardContent, CardHeader } from "@mui/material";
import { red } from "@mui/material/colors";
import { Container, styled } from "@mui/system";
import { DataGrid, GridToolbar, enUS, frFR } from "@mui/x-data-grid";
import * as d3 from "d3";
import cloud from "d3-cloud";
import { toPng, toSvg } from "html-to-image";

import { useLang } from "../context/Lang";
import { isDefined } from "../utils/utils";
import { ShadowCard } from "./card";

export function ChartContainer({ children, title, ...props }) {
	return (
		<ShadowCard {...props} raised elevation={0}>
			<CardHeader title={title} />
			<CardContent sx={{ mt: 0 }}> {children}</CardContent>
		</ShadowCard>
	);
}
const CHART_HEIGHT = 372;
const LEGEND_HEIGHT = 72;

const StyledChart = styled(Chart)(({ theme }) => ({
	height: CHART_HEIGHT,
	// "& .apexcharts-canvas svg": { height: CHART_HEIGHT },
	"& .apexcharts-canvas svg,.apexcharts-canvas foreignObject": {
		overflow: "visible",
	},
	"& .apexcharts-legend": {
		height: LEGEND_HEIGHT,
		alignContent: "center",
		position: "relative !important",
		borderTop: `solid 1px ${theme.palette.divider}`,
		top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT}px) !important`,
	},
}));

export function Donut({ data, colors, palette, labels }) {
	return (
		<StyledChart
			type="donut"
			series={data}
			options={{
				colors: colors,
				theme: {
					palette: palette,
				},
				chart: {
					id: "basic-donut",
					toolbar: { show: true },
				},
				labels: labels,
				plotOptions: {
					pie: {
						donut: {
							size: "110",
							labels: {
								show: true,
								total: {
									show: true,
									label: "Total",
									color: "black",
									formatter: function (w) {
										return w.globals.seriesTotals.reduce((a, b) => {
											return a + b;
										}, 0);
									},
								},
							},
						},
					},
				},
				legend: {
					position: "bottom",
					horizontalAlign: "center",
					fontSize: "17px",
				},
				dataLabels: { enabled: false, dropShadow: { enabled: false } },
				tooltip: {
					fillSeriesColor: false,
					marker: {
						show: true,
					},
				},
			}}
			height={400}
		/>
	);
}

const StyledChartBar = styled(StyledChart)(({ theme }) => ({
	".apexcharts-gridlines-horizontal ": {
		display: "none",
	},
	".apexcharts-gridline": {
		display: "none",
	},
	".apexcharts-grid-borders line": {
		display: "none",
	},
	".apexcharts-xaxis-tick line": {
		y2: "-10",
	},
	".apexcharts-menu-icon": {
		display: "none",
	},
	" .apexcharts-tooltip": {
		".apexcharts-tooltip-text": {
			fontFamily: "Helvetica, Arial, sans-serif",
			fontSize: " 12px",
		},
		".apexcharts-tooltip-marker": {},
		".apexcharts-tooltip-y-group": {
			padding: "6px 0 5px",
		},
	},
	".apexcharts-legend": {
		height: "100px",
	},
}));

export function Bar({ labels, data, colors, palette, isHorizontal }) {
	return (
		<StyledChartBar
			type="bar"
			series={[{ data: data }]}
			options={{
				colors: colors,
				theme: {
					palette: palette,
				},
				chart: {
					type: "bar",
				},
				xaxis: {
					type: "category",
					categories: labels,
					labels: {
						style: {
							fontSize: "15px",
							fontWeight: 500,
						},
					},
				},
				yaxis: {
					labels: {
						style: {
							fontSize: "17px",
							fontWeight: 500,
						},
					},
				},
				plotOptions: {
					bar: {
						borderRadius: 4,
						horizontal: isHorizontal,
						distributed: true,
					},
				},
				legend: {
					show: false,
					position: "bottom",
					horizontalAlign: "center",
					fontSize: "17px",
				},
				dataLabels: { enabled: false, dropShadow: { enabled: false } },
				tooltip: {
					theme: "dark",
					fixed: {
						enabled: false,
					},
					onDatasetHover: {
						highlightDataSeries: false,
					},
					followCursor: true,
					fillSeriesColor: false,
					marker: {
						show: true,
					},
					y: {
						formatter: function (val) {
							return val;
						},
						title: {
							formatter: function (seriesName, e) {
								return e.w.globals.labels[e.dataPointIndex] + ":";
							},
						},
					},
					x: {
						show: false,
					},
				},
			}}
			height={350}
		/>
	);
}

const StyledChartRadar = styled(Chart)(({ theme }) => ({
	height: CHART_HEIGHT,
	"& .apexcharts-canvas svg": { height: CHART_HEIGHT },
	"& .apexcharts-canvas svg,.apexcharts-canvas foreignObject": {
		overflow: "visible",
	},
	"& .apexcharts-legend": {
		height: LEGEND_HEIGHT,
		alignContent: "center",
		position: "relative !important",
		borderTop: `solid 1px ${theme.palette.divider}`,
		top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT + 100}px) !important`,
	},
}));

export function Radar({ labels, series, colors, palette }) {
	return (
		<StyledChartRadar
			type="radar"
			series={series}
			options={{
				colors: colors,
				theme: {
					palette: palette,
				},
				chart: {
					type: "bar",
				},
				xaxis: {
					type: "category",
					categories: labels,
					labels: {
						style: {
							fontSize: "15px",
							fontWeight: 500,
						},
					},
				},
				yaxis: {
					show: false,
				},
				plotOptions: {
					bar: {
						borderRadius: 4,
						horizontal: true,
						distributed: true,
					},
				},
				stroke: { colors: [red] },
				legend: {
					position: "bottom",
					horizontalAlign: "center",
					fontSize: "17px",
				},
				dataLabels: { enabled: false, dropShadow: { enabled: false } },
				tooltip: {
					theme: "dark",
					fixed: {
						enabled: false,
					},
					onDatasetHover: {
						highlightDataSeries: false,
					},
					followCursor: true,
					fillSeriesColor: false,
					marker: {
						show: true,
					},
					y: {
						formatter: function (val) {
							return val;
						},
						title: {
							formatter: function (seriesName, e) {
								return e.w.globals.labels[e.dataPointIndex] + ":";
							},
						},
					},
					x: {
						show: false,
					},
				},
			}}
		/>
	);
}

export function StackedBar({ labels, series, colors, palette }) {
	return (
		<StyledChart
			type="bar"
			series={series}
			options={{
				colors: colors,
				theme: {
					palette: palette,
				},
				chart: {
					type: "bar",
					stacked: true,
				},
				xaxis: {
					type: "category",
					categories: labels,
					labels: {
						style: {
							fontSize: "15px",
							fontWeight: 500,
						},
					},
				},
				yaxis: {
					labels: {
						style: {
							fontSize: "17px",
							fontWeight: 500,
						},
					},
				},

				stroke: { colors: [red] },
				legend: {
					position: "bottom",
					horizontalAlign: "center",
					fontSize: "17px",
				},
				dataLabels: { enabled: false, dropShadow: { enabled: false } },
				tooltip: {
					theme: "dark",
					fixed: {
						enabled: false,
					},
					onDatasetHover: {
						highlightDataSeries: false,
					},
					followCursor: true,
					fillSeriesColor: false,
					marker: {
						show: true,
					},
					y: {
						formatter: function (val) {
							return val;
						},
						title: {
							formatter: function (seriesName, e) {
								return seriesName + ":";
							},
						},
					},
					x: {
						show: false,
					},
				},
				toolbar: {
					offsetX: 0,
					offsetY: 50,
				},
			}}
			height={350}
		/>
	);
}

export function WordCloud({ words }) {
	const containerRef = React.useRef();

	React.useEffect(() => {
		if (isDefined(containerRef.current)) {
			var margin = { top: 10, right: 10, bottom: 10, left: 10 };

			const containerRect = containerRef.current.getBoundingClientRect();
			const height = containerRect.height - margin.left - margin.right;
			const width = containerRect.width - margin.top - margin.bottom;

			d3.select(containerRef.current).selectAll("*").remove();
			var svg = d3
				.select(containerRef.current)
				.append("svg")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
				.attr("viewBox", [0, 0, width, height]);

			var fontSizeScale = d3.scalePow([0, 1], [15, 70]).exponent(1.2);

			var maxSize = d3.max(words, function (d) {
				return d.size;
			});
			var layout = cloud()
				.size([width, height])
				.words(
					words.map(function (d) {
						return { text: d.word, size: d.size, color: d.color };
					})
				)
				.padding(2)
				.rotate(function () {
					return ~~(Math.random() * 2) * 0;
				})
				.fontSize(function (d) {
					return fontSizeScale(parseInt(d.size) / maxSize);
				})
				.on("end", draw);
			layout.start();

			function draw(words) {
				var X0 = d3.min(words, function (d) {
						return d.x - d.width / 2;
					}),
					X1 = d3.max(words, function (d) {
						return d.x + d.width / 2;
					});

				var Y0 = d3.min(words, function (d) {
						return d.y - d.height / 2;
					}),
					Y1 = d3.max(words, function (d) {
						return d.y + d.height / 2;
					});

				var scaleX = (X1 - X0) / width;
				var scaleY = (Y1 - Y0) / height;

				var scale = 1 / Math.max(scaleX, scaleY);

				var translateX = Math.abs(X0) * scale;
				var translateY = Math.abs(Y0) * scale;

				svg
					.append("g")
					.attr(
						"transform",
						"translate(" +
							translateX +
							"," +
							translateY +
							")" +
							" scale(" +
							scale +
							")"
					)
					.selectAll("text")
					.data(words)
					.enter()
					.append("text")
					.style("font-size", function (d) {
						return `${d.size}px`;
					})
					.style("fill", "#69b3a2")
					.attr("text-anchor", "middle")
					.style("font-family", "Liberation Serif")
					.style("fill", function (d, i) {
						return d.color;
					})
					.attr("transform", function (d) {
						return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
					})

					.text(function (d) {
						return d.text;
					});
			}
		}
	}, [words]);

	return (
		<>
			<Button
				onClick={() => {
					if (containerRef.current === null) return;
					toSvg(containerRef.current).then((dataUrl) => {
						const a = document.createElement("a");
						a.setAttribute("download", `wordcloud.svg`);
						a.setAttribute("href", dataUrl);
						a.click();
					});
				}}
			>
				SVG
			</Button>
			<Button
				onClick={() => {
					if (containerRef.current === null) return;
					toPng(containerRef.current).then((dataUrl) => {
						const a = document.createElement("a");
						a.setAttribute("download", `wordcloud.png`);
						a.setAttribute("href", dataUrl);
						a.click();
					});
				}}
			>
				PNG
			</Button>
			<Container
				sx={{
					height: 500,
					maxWidth: "100% !important",
					ml: 0,
					mr: 0,
					p: "0 !important",
				}}
				ref={containerRef}
				id="words-svg"
			></Container>
		</>
	);
}

export function StatTable({ columns, rows }) {
	const { activeLang } = useLang();
	return (
		<DataGrid
			getRowHeight={() => "auto"}
			rows={rows}
			slots={{ toolbar: GridToolbar }}
			columns={columns}
			initialState={{
				pagination: {
					paginationModel: {
						pageSize: 20,
					},
				},
			}}
			localeText={
				activeLang === "fr"
					? frFR.components.MuiDataGrid.defaultProps.localeText
					: enUS.components.MuiDataGrid.defaultProps.localeText
			}
			pageSizeOptions={[10, 20, 50]}
			disableRowSelectionOnClick
			sx={{
				"&.MuiDataGrid-root--densityCompact .MuiDataGrid-cell": {
					py: "8px",
				},
				"&.MuiDataGrid-root--densityStandard .MuiDataGrid-cell": {
					py: "15px",
				},
				"&.MuiDataGrid-root--densityComfortable .MuiDataGrid-cell": {
					py: "22px",
				},
				mt: 2,
			}}
		/>
	);
}
