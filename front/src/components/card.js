import { Button, Card } from "@mui/material";
import { blue, grey } from "@mui/material/colors";
import { styled } from "@mui/system";

export function ShadowCard(props) {
	const StyledCard = styled(Card)({
		borderRadius: 8,
		boxShadow:
			"rgb(145 158 171 / 20%) 0px 0px 2px 0px, rgb(145 158 171 / 12%) 0px 12px 24px -4px",
	});
	return <StyledCard {...props} raised></StyledCard>;
}
