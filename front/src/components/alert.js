import React from "react";

import CloseIcon from "@mui/icons-material/Close";
import Alert from "@mui/material/Alert";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";

export default function FadeOutAlert({ children, severity = "success" }) {
	const [open, setOpen] = React.useState(true);

	React.useEffect(() => {
		setTimeout(() => {
			setOpen(false);
		}, 5000);
	});

	return (
		<Collapse
			sx={
				{
					// position: "absolute",
					// bottom: 0,
					// right: 20,
				}
			}
			in={open}
		>
			<Alert
				severity={severity}
				action={
					<IconButton
						aria-label="close"
						color="inherit"
						size="small"
						onClick={() => {
							setOpen(false);
						}}
					>
						<CloseIcon fontSize="inherit" />
					</IconButton>
				}
				sx={{ mb: 2, mr: 2 }}
			>
				{children}
			</Alert>
		</Collapse>
	);
}
