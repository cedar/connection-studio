export function isDefined(variable) {
	if (typeof variable === "undefined") {
		return false;
	}
	if (variable === null) {
		return false;
	}
	return true;
}

export function isProcessableFetch(data, isLoading, isError) {
	if (!isDefined(isError) && !isLoading && isDefined(data)) {
		return true;
	}
	return false;
}

export function getFetch(data, isLoading, isError, typeData) {
	if (isProcessableFetch(data, isLoading, isError)) {
		return data;
	}
	return typeData;
}
