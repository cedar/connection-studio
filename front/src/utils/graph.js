const structNodeColorPrimary = "#000000";
const structNodeColorSecondary = "#000000";
const valueNodeColorPrimary = "#bab0ab";
const valueNodeColorSecondary = "#87807C";
export const TYPES = {
	DATA_SOURCE: {
		typeName: "DATA_SOURCE",
		names: ["DATA_SOURCE"],
		displayName: { fr: "Source de données", en: "Data source" },
		pluralDisplayName: { fr: "Sources de données", en: "Data sources" },
		color: "#e15759",
		secondaryColor: "#AD4445",
	},
	ENTITY_PERSON: {
		typeName: "ENTITY_PERSON",
		names: ["ENTITY_PERSON"],
		displayName: { fr: "Personne", en: "Person" },
		pluralDisplayName: { fr: "Personnes", en: "Persons" },
		color: "#f7ba00",
		secondaryColor: "#c39300",
	},
	ENTITY_LOCATION: {
		typeName: "ENTITY_LOCATION",
		names: ["ENTITY_LOCATION"],
		displayName: { fr: "Lieu", en: "Location" },
		pluralDisplayName: { fr: "Lieux", en: "Locations" },
		color: "#59a14f",
		secondaryColor: "#3C6E36",
	},
	ENTITY_ORGANIZATION: {
		typeName: "ENTITY_ORGANIZATION",
		names: ["ENTITY_ORGANIZATION"],
		displayName: { fr: "Organisation", en: "Organization" },
		pluralDisplayName: { fr: "Organisations", en: "Organizations" },
		color: "#3D78FF",
		secondaryColor: "#3131ac",
	},
	URI: {
		typeName: "URI",
		names: ["URI", "RDF_URI"],
		displayName: { fr: "URI", en: "URI" },
		pluralDisplayName: { fr: "URIs", en: "URIs" },
		color: "#af7aa1",
		secondaryColor: "#7D5773",
	},
	RDF_URI: {
		typeName: "URI",
		names: ["URI", "RDF_URI"],
		displayName: { fr: "URI", en: "URI" },
		pluralDisplayName: { fr: "URIs", en: "URIs" },
		color: "#af7aa1",
		secondaryColor: "#7D5773",
	},
	HASHTAG: {
		typeName: "HASHTAG",
		names: ["HASHTAG"],
		displayName: { fr: "Hashtag", en: "Hashtag" },
		pluralDisplayName: { fr: "Hashtags", en: "Hashtags" },
		color: "#ff9da7",
		secondaryColor: "#CC7E86",
	},
	EMAIL: {
		typeName: "EMAIL",
		names: ["EMAIL"],
		displayName: { fr: "Email", en: "Email" },
		pluralDisplayName: { fr: "Emails", en: "Emails" },
		color: "#9c755f",
		secondaryColor: "#694F40",
	},
	DATE: {
		typeName: "DATE",
		names: ["DATE"],
		displayName: { fr: "Date", en: "Date" },
		pluralDisplayName: { fr: "Dates", en: "Dates" },
		color: "#f44336",
		secondaryColor: "#c3352b",
	},
	MENTION: {
		typeName: "MENTION",
		names: ["MENTION"],
		displayName: { fr: "Mention", en: "Mention" },
		pluralDisplayName: { fr: "Mentions", en: "Mentions" },
		color: "#99821C",
		secondaryColor: "#665712",
	},
	HTML_NODE: {
		typeName: "STRUCT",
		names: ["HTML_NODE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	HTML_VALUE: {
		typeName: "VALUE",
		names: ["HTML_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	XML_NODE: {
		typeName: "STRUCT",
		names: ["XML_NODE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	XML_VALUE: {
		typeName: "VALUE",
		names: ["XML_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	XML_ATTRIBUTE_VALUE: {
		typeName: "VALUE",
		names: ["XML_ATTRIBUTE_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	XML_ATTRIBUTE_NODE: {
		typeName: "STRUCT",
		names: ["XML_ATTRIBUTE_NODE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	XML_TAG_VALUE: {
		typeName: "VALUE",
		names: ["XML_TAG_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	XML_TAG_NODE: {
		typeName: "STRUCT",
		names: ["XML_TAG_NODE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	RDF_LITERAL: {
		typeName: "VALUE",
		names: ["RDF_LITERAL"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	RELATIONAL_STRUCT: {
		typeName: "STRUCT",
		names: ["RELATIONAL_STRUCT"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	RELATIONAL_VALUE: {
		typeName: "VALUE",
		names: ["RELATIONAL_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	TEXT_VALUE: {
		typeName: "VALUE",
		names: ["TEXT_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	FIRST_NAME: {
		typeName: "VALUE",
		names: ["FIRST_NAME"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	JSON_VALUE: {
		typeName: "VALUE",
		names: ["JSON_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	JSON_STRUCT: {
		typeName: "STRUCT",
		names: ["JSON_STRUCT"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	JSON_MAP_FIELD: {
		typeName: "STRUCT",
		names: ["JSON_MAP_FIELD"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	JSON_MAP: {
		typeName: "STRUCT",
		names: ["JSON_MAP"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	JSON_ARRAY: {
		typeName: "STRUCT",
		names: ["JSON_ARRAY"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	AMBI_NODE: {
		typeName: "VALUE",
		names: ["AMBI_NODE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	DO_NOT_LINK_VALUE: {
		typeName: "VALUE",
		names: ["DO_NOT_LINK_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	NEO4J_ENTITY: {
		typeName: "STRUCT",
		names: ["NEO4J_ENTITY"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: structNodeColorPrimary,
		secondaryColor: structNodeColorSecondary,
	},
	NEO4J_VALUE: {
		typeName: "VALUE",
		names: ["NEO4J_VALUE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	NEO4J_EDGE: {
		typeName: "VALUE",
		names: ["NEO4J_EDGE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	NORMALIZATION_NODE: {
		typeName: "VALUE",
		names: ["NORMALIZATION_NODE"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
	NORMALIZATION_NODE_EXTRACTION: {
		typeName: "VALUE",
		names: ["NORMALIZATION_NODE_EXTRACTION"],
		displayName: { fr: "Noeud de données", en: "Data node" },
		pluralDisplayName: { fr: "Noeuds de données", en: "Data nodes" },
		color: valueNodeColorPrimary,
		secondaryColor: valueNodeColorSecondary,
	},
};
class DataSource {
	constructor(sourceFile, sourceType) {
		this.file = sourceFile;
		this.typed = !!sourceType;
		if (this.typed) this.type = sourceType;
	}

	hasType() {
		return this.typed;
	}

	setType(type) {
		this.typed = !!type;
		this.type = type;
	}
}

export class Node {
	constructor(node) {
		this.id = node.global_id;
		this.local_id = node.local_id;
		this.label = node.label;
		this.type = node.type;
		this.context = node.context;
		this.dataSource = new DataSource(node.source, node.source_type);
		this.size = 12;
	}

	static from(node) {
		return new Node({
			global_id: node.id,
			label: node.label,
			local_id: node.local_id,
			type: node.type,
			dataSource: node.dataSource,
			source: node.dataSource.file,
			source_type: node.dataSource.type,
			context: node.context,
		});
	}

	getId() {
		return this.id;
	}

	getLabel(graph) {
		if (graph && !this.label.length) {
			let neighbors = graph.getIncoming(this);
			if (neighbors.length === 1) return neighbors[0].link.getLabel();
		}
		return this.label;
	}

	getDataSource() {
		return this.dataSource;
	}

	getContext() {
		return this.context;
	}

	toObject() {
		let node = this;
		return {
			global_id: node.id,
			label: node.label,
			local_id: node.local_id,
			type: node.type,
			dataSource: node.dataSource,
			context: node.context,
			source: node.dataSource.file,
			source_type: node.dataSource.type,
		};
	}

	getLabelHtml(graph) {
		// Set the visible chars and the invisible chars that only appear when hovering over a node

		let label = this.getLabel(graph);
		var visible_chars = new Array(label.length).fill(true);

		switch (this.getType().typeName) {
			case "ENTITY_PERSON":
				let words = label.split(" ");
				if (words.length > 1) {
					visible_chars = [true].concat(
						new Array(words[0].length - 1).fill(false),
						new Array(this.label.length - words[0].length).fill(true)
					);
				}
				break;
			case "EMAIL":
				let adresslength = label.split("@")[0].length;
				visible_chars = new Array(label.length)
					.fill(true)
					.fill(false, 0, adresslength);
				break;
			default:
				if (label.length > 30) visible_chars.fill(false, 15, label.length - 15);
				break;
		}

		let currentlyhiding = false;
		let final_label = "";
		for (var i = 0; i < label.length; i++) {
			if (currentlyhiding) {
				if (visible_chars[i]) {
					currentlyhiding = false;
					final_label += "</tspan>";
					final_label += '<tspan class="ellipsis">[...]</tspan>';
				}
				final_label += label[i];
			} else {
				if (!visible_chars[i]) {
					currentlyhiding = true;
					final_label += "<tspan>";
				}
				final_label += label[i];
			}
		}
		if (currentlyhiding) {
			final_label += "</tspan>";
			final_label += '<tspan class="ellipsis">[...]</tspan>';
		}
		return final_label;
	}

	getType() {
		const typesInfo = TYPES[this.type];
		return { src: this.type, ...typesInfo };
	}

	getColor() {
		return this.getType().color;
	}

	getStrokeColor() {
		return this.getType().secondaryColor;
	}

	hasPosition() {
		if (this.x || this.y) return true;
		return false;
	}

	save() {
		return { id: this.id, x: this.x, y: this.y };
	}
}

export class Edge {
	constructor(edge) {
		this.id = edge.global_id;
		this.label = edge.label;
		this.sameAs = edge.same_as;
		this.score = edge.score;
		this.source = edge.source;
		this.target = edge.target;
		this.sourceId = edge.source;
		this.targetId = edge.target;
		this.type = null;
		this.dataSource = new DataSource(edge.origin, null);
		//     let dataSource = ctx.sources.find((s) => s.file === edge.origin);
		//     if (dataSource) {
		//       this.dataSource = dataSource;
		//     } else {
		//       this.dataSource = new DataSource(edge.origin);
		//       ctx.sources.push(this.dataSource);
		//     }
	}

	static from(edge) {
		// Copy an edge
		return new Edge({
			global_id: edge.id,
			label: edge.label,
			same_as: edge.sameAs,
			score: edge.score,
			source: edge.sourceId,
			target: edge.targetId,
			origin: edge.dataSource.file,
		});
	}

	getId() {
		return this.id;
	}

	getDataSource() {
		return this.dataSource;
	}

	getLabel() {
		return this.label;
	}

	getScore() {
		return this.score;
	}

	isSameAs() {
		return this.sameAs; //IM 22/4/21
	}

	getSource(graph) {
		if (graph && typeof this.source === "string") {
			this.source = graph.getNode(this.source);
		}
		// if (graph && typeof this.source === "string") {
		// 	this.source = graph.getNode(this.source);
		// }
		return this.source;
	}

	getTarget(graph) {
		if (graph && typeof this.target === "string")
			this.target = graph.getNode(this.target);

		// this.target = graph.getNode(this.target);

		return this.target;
	}

	getType(graph) {
		const target = this.getTarget(graph);
		const source = this.getSource(graph);
		// const target = this.target;
		// const source = this.source;

		let sourceType = source.getType().typeName;
		let targetType = target.getType().typeName;

		if (sourceType === targetType) {
			return sourceType;
		} else {
			return "UNK";
		}
	}

	getSourceId() {
		return this.sourceId;
	}

	getTargetId() {
		return this.targetId;
	}

	isAdjacent(node) {
		let nodeId = node.getId();
		return nodeId === this.source.id || nodeId === this.target.id;
	}
}

export class D3Graph {
	constructor(tree, boundingBox) {
		this.nodes = {};
		this.links = {};
		this.nodeIds = new Set();
		this.linkIds = new Set();
		this.boundingBox = boundingBox;
		this.local2GlobalIdMap = new Map();

		if (tree) {
			let nodes;
			let links;
			if (Object.keys(tree).includes("node")) {
				let source = tree.src.toObject();
				source.local_id = source.global_id;
				nodes = [tree.node, source];
				links = [tree.link];
			}

			if (Object.keys(tree).includes("nodes")) {
				nodes = tree.nodes;
				links = tree.links;
			}

			this.addNodes(nodes);
			this.addLinks(links);
		}

		this.resolveEdges();
	}

	static from(graph) {
		let g = new D3Graph();
		g.addNodes(graph.getNodes().map(Node.from));
		g.addLinks(graph.getLinks().map(Edge.from));
		g.local2GlobalIdMap = graph.local2GlobalIdMap;
		g.resolveEdges();
		return g;
	}

	static merge(trees) {
		let graph = new D3Graph();

		for (let tree of trees) {
			graph.addGraph(D3Graph.from(tree));
			graph.addMap(
				new Map([...graph.local2GlobalIdMap, ...tree.local2GlobalIdMap])
			);
		}

		graph.resolveEdges();

		return graph;
	}

	addMap(map) {
		this.local2GlobalIdMap = map;
	}

	resolveEdges() {
		let nodes = this.getNodes();
		let edges = this.getLinks();
		for (let node of nodes) {
			this.local2GlobalIdMap.set(node.local_id, node.id);
		}

		for (let edge of edges) {
			this.resolveEdge(edge);
		}
	}

	resolveEdge(edge) {
		edge.source = this.getNode(parseInt(edge.sourceId));
		edge.target = this.getNode(parseInt(edge.targetId));
	}

	addGraph(graph) {
		let newNodes = this.addNodes(graph.getNodes());
		let newLinks = this.addLinks(graph.getLinks());

		return { nodes: newNodes, links: newLinks };
	}

	getLength() {
		return this.nodeIds.size;
	}

	getNodes() {
		return Object.values(this.nodes);
	}

	getLinks() {
		return Object.values(this.links);
	}

	getNodeIds() {
		return this.nodeIds;
	}

	getLinkIds() {
		return this.linkIds;
	}

	getTypes() {
		let types = this.getNodes().map((node) => node.getType().src);

		let uniqueTypes = new Set(types);

		return uniqueTypes;
	}

	getSources() {
		let sources = this.getNodes()
			.map((node) => node.getDataSource())
			.concat(this.getLinks().map((link) => link.getDataSource()));
		sources = [...new Set(sources)];
		return sources;
	}

	addNode(nodeData) {
		let node;
		if (nodeData.constructor === Object) {
			node = new Node(nodeData);
		}
		if (nodeData.constructor === Node) {
			node = nodeData;
		}
		let nodeId = node.getId();
		if (this.nodeIds.has(nodeId)) return;
		this.nodes[nodeId] = node;
		this.nodeIds.add(nodeId);
		return node;
	}

	addLink(linkData) {
		let link;
		if (linkData.constructor === Object) {
			link = new Edge(linkData);
		}
		if (linkData.constructor === Edge) {
			link = linkData;
		}

		let linkId = link.getId();

		if (this.linkIds.has(linkId)) return;
		this.links[linkId] = link;
		this.linkIds.add(linkId);
		return link;
	}

	addNodes(nodes) {
		let newNodes = nodes.map((node) => this.addNode(node));
		return newNodes.filter((node) => node);
	}

	addLinks(links) {
		let newLinks = links.map((link) => this.addLink(link));

		return newLinks.filter((link) => link);
	}

	addGraphResults(graph) {
		let newGraph = this.addGraph(graph);

		this.resolveEdges(newGraph.links);
		// this.resolveZones(newGraph.nodes);
	}

	addExternalLinks(links) {
		return links;
	}

	// resolveEdge(edge) {
	// 	edge.source = this.getNode(edge.sourceId);
	// 	edge.target = this.getNode(edge.targetId);
	// }

	// resolveEdges(edges) {
	// 	// Resolve the source and target node of an edge
	// 	let graph = this;
	// 	edges.forEach((edge) => graph.resolveEdge(edge));

	// 	// Remove multi-edges
	// 	edges.forEach(function (edge) {
	// 		if (!graph.getLink(edge.getId())) {
	// 			return;
	// 		}
	// 		let debug1 = graph
	// 			.getNeighbors(edge.getSource())
	// 			.filter((neighbor) => neighbor.node.getId() === edge.getTargetId());
	// 		let debug2 = debug1.filter(
	// 			(neighbor) => neighbor.link.getId() !== edge.getId()
	// 		);
	// 		let sameEdges = debug2.map((neighbor) => neighbor.link);
	// 		//  graph.removeLinks(sameEdges);
	// 	});
	// }

	removeNode(node) {
		let nodeId = node.getId();
		this.nodeIds.delete(nodeId);
		delete this.nodes[nodeId];
		this.removeLinks(this.getLinks().filter((link) => link.isAdjacent(node)));
	}

	removeLink(link) {
		let linkId = link.getId();
		this.linkIds.delete(linkId);
		delete this.links[linkId];
	}

	removeNodes(nodes) {
		nodes.forEach((node) => this.removeNode(node));
	}

	removeLinks(links) {
		links.forEach((link) => this.removeLink(link));
	}

	getNode(nodeId) {
		return this.nodes[nodeId];
	}

	getLink(linkId) {
		return this.links[linkId];
	}

	isConnected(node1, node2) {
		if (node1.getId() === node2.getId()) return false;
		return this.getLinks().some(
			(link) => link.isAdjacent(node1) && link.isAdjacent(node2)
		);
	}

	getOutgoing(node) {
		// IM, 23/4/21: Don't filter away sameAs edges
		//let links = this.getLinks().filter(link => !link.isSameAs() && link.getSourceId() === node.getId());
		let links = this.getLinks().filter(
			(link) => link.getSourceId() === node.getId()
		);
		let neighbors = links.map(function (link) {
			return { link: link, node: link.getTarget(), parent: node };
		});
		return neighbors;
	}

	getIncoming(node) {
		// IM, 23/4/21: Don't filter away sameAs edges
		//let links = this.getLinks().filter(link => !link.isSameAs() && link.getTargetId() === node.getId());
		let links = this.getLinks().filter(
			(link) => link.getTargetId() === node.getId()
		);

		let neighbors = links.map(function (link) {
			return { link: link, node: link.getSource(), parent: node };
		});
		return neighbors;
	}

	getNeighbors(node) {
		let links = this.getLinks().filter((link) => link.isAdjacent(node));
		let nodeId = node.getId();
		let graph = this;
		let neighbors = links.map(function (link) {
			if (link.getSource(graph).getId() === nodeId)
				var n = link.getTarget(graph);
			else var n = link.getSource(graph);
			return { link: link, node: n, parent: node };
		});
		return neighbors;
	}

	save() {
		return this.getNodes().map((node) => node.save());
	}

	setNodePostion(node) {
		// Initialize node positions

		if (node.hasPosition()) return;
		let neighbors = this.getNeighbors(node).filter((n) => n.node.hasPosition());

		if (neighbors.length === 0) {
			node.x = this.boundingBox.width / 2;
			node.y = this.boundingBox.height / 2;
		} else {
			let x =
				neighbors.map((d) => d.node.x).reduce((a, b) => a + b, 0) /
				neighbors.length;

			let y =
				neighbors.map((d) => d.node.y).reduce((a, b) => a + b, 0) /
				neighbors.length;

			node.x = x;
			node.y = y;
		}
		node.x += (Math.random() - 0.5) * 5 * node.size;
		node.y += (Math.random() - 0.5) * 5 * node.size;
	}

	// setLinkPostion(link) {
	// 	// Initialize node positions
	// 	if (node.hasPosition()) return;
	// 	let neighbors = this.getNeighbors(node).filter((n) => n.node.hasPosition());

	// 	if (neighbors.length === 0) {
	// 		node.x = this.boundingBox.width / 2;
	// 		node.y = this.boundingBox.height / 2;
	// 	} else {
	// 		let x =
	// 			neighbors.map((d) => d.node.x).reduce((a, b) => a + b, 0) /
	// 			neighbors.length;
	// 		let y =
	// 			neighbors.map((d) => d.node.y).reduce((a, b) => a + b, 0) /
	// 			neighbors.length;
	// 		node.x = x;
	// 		node.y = y;
	// 	}
	// 	node.x += (Math.random() - 0.5) * 5 * node.size;
	// 	node.y += (Math.random() - 0.5) * 5 * node.size;
	// }
}
