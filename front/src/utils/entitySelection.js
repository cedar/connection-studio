export const getUpdatedSelectionState = (selectionState, clickedId) => {
	const newState = selectionState.map((state) => ({ ...state }));

	const updateParent = (id) => {
		const entity = newState.find((i) => i.id === id);
		const parent = newState.find((i) => i.id === entity.parentId);

		if (!parent) return;

		// const childStates = newState.filter((i) => i.parentId == parent.id);
		const childStates = parent.children.map((childId) =>
			newState.find((i) => i.id === childId)
		);

		if (
			childStates.length ===
			childStates.filter(
				(child) => child.checked === true && child.indeterminate === false
			).length
		) {
			parent.checked = true;
			parent.indeterminate = false;
		} else if (
			childStates.length ===
			childStates.filter((child) => child.checked === false).length
		) {
			parent.checked = false;
			parent.indeterminate = false;
		} else {
			parent.checked = true;
			parent.indeterminate = true;
		}

		updateParent(parent.id);
	};

	const setUnselected = (id) => {
		const entity = newState.find((i) => i.id === id);
		entity.checked = false;
		entity.indeterminate = false;

		// const childStates = newState.filter((i) => i.parentId == id);
		const childStates = entity.children.map((childId) =>
			newState.find((i) => i.id === childId)
		);
		for (const child of childStates) {
			setUnselected(child.id);
		}

		updateParent(id);
	};

	const setSelected = (id) => {
		const entity = newState.find((i) => i.id === id);
		entity.checked = true;
		entity.indeterminate = false;

		// const childStates = newState.filter((i) => i.parentId == id);
		const childStates = entity.children.map((childId) =>
			newState.find((i) => i.id === childId)
		);
		for (const child of childStates) {
			setSelected(child.id);
		}

		updateParent(id);
	};

	const entity = newState.find((i) => i.id === clickedId);
	if (!entity) {
		return newState;
	}

	if (entity.checked === true) {
		setUnselected(clickedId);
	} else {
		setSelected(clickedId);
	}

	return newState;
};

export const getInitialSelectionState = (data, mainEntityId) => {
	let selectionState = [];

	const initializeState = (entity, parentId) => {
		const entityState = {
			node_id: entity.id,
			id: parentId + "_" + entity.id,
			parentId: parentId,
			label: entity.label,
			checked: false,
			indeterminate: false, // if it is not checked, but some of its children are
			required: false,
			nbNonLeafChildren: Number.parseInt(entity.nbNonLeafChildren),
			children: [],
		};

		selectionState.push(entityState);

		if (entity.children) {
			for (const child of entity.children) {
				const childState = initializeState(child, entityState.id);
				entityState.children.push(childState.id);
			}
		}

		return entityState;
	};

	if (data) {
		for (const child of data) {
			initializeState(child, mainEntityId);
		}
	}

	return selectionState;
};

export const getPathToEntity = (selectionState, entityId) => {
	const entity = selectionState.find((i) => i.id === entityId);

	if (!entity) {
		return [];
	}

	if (!entity.parentId) {
		return [entity.node_id];
	}

	return getPathToEntity(selectionState, entity.parentId).concat(
		entity.node_id
	);
};

export const getLabelPathToEntity = (selectionState, entityId) => {
	const entity = selectionState.find((i) => i.id === entityId);

	if (!entity) {
		return [];
	}

	if (!entity.parentId) {
		return [entity.label];
	}

	return getLabelPathToEntity(selectionState, entity.parentId).concat(
		entity.label
	);
};
