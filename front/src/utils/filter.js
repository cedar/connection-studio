// /*
//  * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
//  *
//  * Available under MIT license (https://opensource.org/licenses/MIT)
//  *
//  */

// export const globalFilters = [];

// class Filter {
// 	constructor(checked) {
// 		this.checked = checked;
// 	}

// 	toggleChecked() {
// 		this.setChecked(!this.checked);
// 	}

// 	static add(filter) {
// 		globalFilters.push(filter);
// 	}

// 	static isFilteredIn(node) {
// 		for (let FilterType of Filter.order) {
// 			let filters = globalFilters.filter(
// 				(filter) => filter instanceof FilterType && filter.appliesTo(node)
// 			);
// 			if (filters.length) {
// 				if (FilterType.filterIn) {
// 					if (filters.some((filter) => filter.checked)) return true;
// 				} else {
// 					if (filters.some((filter) => !filter.checked)) return false;
// 				}
// 			}
// 		}
// 	}

// 	setChecked(checked) {
// 		this.checked = checked;
// 		Filter.applyFilters();
// 	}

// 	static applyFilters() {
// 		ctx.linkElements.classed(
// 			"filteredIn",
// 			(link) =>
// 				Filter.isFilteredIn(link.source) && Filter.isFilteredIn(link.target)
// 		);
// 		ctx.nodeElements.classed("filteredIn", (node) => Filter.isFilteredIn(node));
// 	}

// 	static save() {
// 		return globalFilters
// 			.filter((f) => !(f instanceof ParentFilter))
// 			.map((filter) => filter.save());
// 	}

// 	static load(filters) {
// 		globalFilters = filters.map(function (f) {
// 			switch (f.type) {
// 				case "type":
// 					return TypeFilter.load(f);
// 				case "kwsearch":
// 					return KwsearchFilter.load(f);
// 				case "node":
// 					return SpecificNodeFilter.load(f);
// 			}
// 		});
// 		this.applyFilters();
// 	}
// }

// class TypeFilter extends Filter {
// 	static filterIn = false;

// 	constructor(type) {
// 		super(true);
// 		this.type = type;
// 		type.f = this;
// 	}

// 	static add(type) {
// 		let filter = globalFilters.find(
// 			(f) => f instanceof TypeFilter && f.type.typeName === type.typeName
// 		);
// 		if (filter) return filter;
// 		filter = new TypeFilter(type);
// 		Filter.add(filter);
// 		return filter;
// 	}

// 	static load(f) {
// 		let type = ctx.types.find((type) => type.typeName === f.typeName);
// 		d3.select("#legend-" + type.typeName).classed("checked", f.checked);
// 		let filter = new TypeFilter(type);
// 		filter.checked = f.checked;
// 		return filter;
// 	}

// 	save() {
// 		return {
// 			type: "type",
// 			typeName: this.type.typeName,
// 			checked: this.checked,
// 		};
// 	}

// 	appliesTo(node) {
// 		return node.getType() === this.type;
// 	}
// }

// class KwsearchFilter extends Filter {
// 	static filterIn = true;

// 	constructor(query) {
// 		super(true);
// 		this.query = query;
// 	}

// 	static add(query) {
// 		let filter = globalFilters.find(
// 			(f) => f instanceof KwsearchFilter && f.query === query
// 		);
// 		if (filter) return filter;
// 		filter = new KwsearchFilter(query);
// 		Filter.add(filter);
// 		return filter;
// 	}

// 	static load(f) {
// 		let filter = new KwsearchFilter(f.query);
// 		filter.checked = f.checked;
// 		f.nodes.forEach((node) => ctx.graph.getNode(node).queries.add(f.query));
// 		return filter;
// 	}

// 	save() {
// 		let f = this;
// 		let nodes = ctx.graph
// 			.getNodes()
// 			.filter((node) => f.appliesTo(node))
// 			.map((node) => node.getId());
// 		return {
// 			type: "kwsearch",
// 			query: this.query,
// 			checked: this.checked,
// 			nodes: nodes,
// 		};
// 	}

// 	appliesTo(node) {
// 		return node.queries.has(this.query);
// 	}
// }

// class SpecificNodeFilter extends Filter {
// 	static filterIn = true;

// 	constructor(node) {
// 		super(true);
// 		this.node = node;
// 	}

// 	static add(node) {
// 		let filter = globalFilters.find(
// 			(f) => f instanceof SpecificNodeFilter && f.node.getId() === node.getId()
// 		);
// 		if (filter) return filter;
// 		filter = new SpecificNodeFilter(node);
// 		Filter.add(filter);
// 		return filter;
// 	}

// 	static load(f) {
// 		let node = ctx.graph.getNode(f.node);
// 		let filter = new SpecificNodeFilter(node);
// 		filter.checked = f.checked;
// 		return filter;
// 	}

// 	save() {
// 		return {
// 			type: "node",
// 			query: this.query,
// 			checked: this.checked,
// 			node: this.node.getId(),
// 		};
// 	}

// 	appliesTo(node) {
// 		return node === this.node;
// 	}
// }

// class ParentFilter extends Filter {
// 	static filterIn = true;

// 	constructor(node) {
// 		super(false);
// 		this.node = node;
// 		node.f = this;
// 	}

// 	static add(node) {
// 		let filter = globalFilters.find(
// 			(f) => f instanceof ParentFilter && f.node.getId() === node.getId()
// 		);
// 		if (filter) return filter;
// 		filter = new ParentFilter(node);
// 		Filter.add(filter);
// 		return filter;
// 	}

// 	appliesTo(node) {
// 		return this.node.zone.has(node);
// 	}
// }

// Filter.order = [TypeFilter, KwsearchFilter, SpecificNodeFilter, ParentFilter];
