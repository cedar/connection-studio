export const PARAMETERS = {
	extractor: {
		description: {
			fr: "Modèle d'extraction",
			en: "Extraction model",
		},
		values: {
			SNER: {
				fr: "Extracteur Stanford",
				en: "Stanford Extractor",
				id: 0,
			},
			FLAIR_NER: {
				fr: "Extracteur Flair",
				en: "Flair Extractor",
				id: 1,
			},
			GPT_NER: {
				fr: "Extracteur ChatGPT 4",
				en: "ChatGPT 4 extractor",
				id: 3,
			},
			NONE: {
				fr: "Aucun",
				en: "None",
				id: 2,
			},
		},
		type: 0,
	},
	default_locale: {
		description: {
			fr: "Langue du fichier",
			en: "File language",
		},
		values: {
			en: {
				fr: "Anglais",
				en: "English",
				id: 0,
			},
			fr: {
				fr: "Français",
				en: "French",
				id: 1,
			},
		},
		type: 0,
	},
	extract_policy: {
		description: {
			fr: "Politique d'extraction",
			en: "Extraction policy",
		},
		type: 4,
	},
	split_sentences: {
		description: {
			fr: "Découpe des textes longs",
			en: "Split long texts",
		},
		values: {
			true: {
				fr: "Vrai",
				en: "True",
				id: 0,
			},
			false: {
				fr: "Faux",
				en: "False",
				id: 1,
			},
		},
		type: 3,
	},
};

export const PARAMETERS_TYPES = {
	0: { fr: "choix", en: "choice" },
	1: { fr: "entier", en: "integer" },
	2: { fr: "décimal", en: "float" },
	3: { fr: "booléen", en: "boolean" },
	4: { fr: "text", en: "texte" },
};

export const SEARCH_PARAMETERS = {
	search_stopper_timeout: {
		description: {
			fr: "Délai (ms)",
			en: "Timeout (ms)",
		},
		values: {
			SNER: {
				fr: "Extracteur Stanford",
				en: "Stanford Extractor",
				id: 0,
			},
			FLAIR_NER: {
				fr: "Extracteur Flair",
				en: "Flair Extractor",
				id: 1,
			},
			NONE: {
				fr: "Aucun",
				en: "None",
				id: 2,
			},
		},
		type: 2,
	},
	search_stopper_topk: {
		description: {
			fr: "Nombre de résultats",
			en: "Number of results",
		},
		values: {
			en: {
				fr: "Anglais",
				en: "English",
				id: 0,
			},
			fr: {
				fr: "Français",
				en: "French",
				id: 1,
			},
		},
		type: 1,
	},
	search_stopper_ATsize: {
		description: {
			fr: " Nombre maximal d'arêtes",
			en: " Maximum number of edges",
		},
		values: {
			en: {
				fr: "Anglais",
				en: "English",
				id: 0,
			},
			fr: {
				fr: "Français",
				en: "French",
				id: 1,
			},
		},
		type: 1,
	},
	gam_bidirectional_search: {
		description: {
			fr: "Effectuer une recherche bidirectionnelle",
			en: "Perform a bidirectional search",
		},
		values: {
			true: {
				fr: "Vrai",
				en: "True",
				id: 0,
			},
			false: {
				fr: "Faux",
				en: "False",
				id: 1,
			},
		},
		type: 3,
	},
};

// timeout (entier, en millisecondes)
// nombre maximum de solutions (entier)
// taille maximale d'une solution (entier)
// gam_bidirectional_search (true, false). Dans un 1er temps seulement les geeks comprendront :)  Nous allons améliorer cela dans un 2e temps.
