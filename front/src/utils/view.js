import { format } from "sql-formatter";

export const prettyPrintStatement = (statement) => {
	statement = statement.replaceAll("\n", " ");
	return format(statement, { language: "sql" });
};

export const getStartEndVariables = (path) => {
	// NB 22 nov 2023: change the context separator here if it changes in CL.
	let contextSeparator = "$";
	let pathBeforeVal = path.split("#")[0];
	let parsedPath = pathBeforeVal.split(contextSeparator);
	let tmpPath1 = parsedPath.slice(0, 1);
	let tmpPath2 = parsedPath.slice(-1);
	let newPath = tmpPath1.concat(tmpPath2);

	return newPath;
};

export const parseError = (errorObj) => {
	let error;
	if (errorObj["response"]) {
		if (errorObj.response["data"]) {
			var errorMessage = errorObj.response.data;
			var psqlException;
			if (errorMessage["message"]) {
				psqlException = errorMessage.message;
			} else {
				if (errorMessage.includes("PSQLException")) {
					var indexPSQLException =
						errorMessage.indexOf("PSQLException") + "PSQLException".length;
					var indexPositionException = errorMessage.indexOf("Position");
					psqlException = errorMessage.slice(
						indexPSQLException,
						indexPositionException
					);
					psqlException = psqlException.replaceAll(":", "");
					psqlException = psqlException.replaceAll('"', "'"); // protect the JSON string against quotes
					psqlException = psqlException.replaceAll("\n", "");
					psqlException = psqlException.trim();
					psqlException = ": " + psqlException; // to separate "Requête Incorrecte" from the isDataRowsError description
				} else {
					psqlException = "Something went wrong.";
				}
			}
			var jsonError = '{"message": "' + psqlException + '"}';
			error = JSON.parse(jsonError);
		} else {
			error = errorObj.response;
		}
	} else {
		error = errorObj;
	}
	return error;
};

export function prettifyValue(input) {
	const regexDollars = /\$/gi;
	const regexNE = /\.#ne/gi;
	const regexVal = /\.#val/gi;

	return (
		input
			.replaceAll(regexDollars, ".")
			// .replaceAll(regexNE, "")
			.replaceAll(regexVal, "#val")
	);
}

export function splitPaths(activePath) {
	if (activePath) {
		console.log("PATH", activePath, activePath.path.split("$"));
	}
}
