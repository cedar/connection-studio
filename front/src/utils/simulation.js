import * as React from "react";

import * as d3 from "d3";

import { isDefined } from "./utils";

export const getBoundingBox = (containerRef) => {
	if (isDefined(containerRef.current)) {
		var margin = { top: 10, right: 30, bottom: 30, left: 40 };
		var container = containerRef.current;
		const containerRect = container.getBoundingClientRect();
		const height = containerRect.height - margin.left - margin.right;
		const width = containerRect.width - margin.top - margin.bottom;

		var boundingBox = { width: width, height: height, margin: margin };
		return boundingBox;
	}
};

export const initViewport = (containerRef, shouldZoom) => {
	if (typeof containerRef.current !== "undefined") {
		var container = containerRef.current;
		const boundingBox = getBoundingBox(containerRef);

		const svg = d3
			.select(container)
			.attr(
				"width",
				boundingBox.width + boundingBox.margin.left + boundingBox.margin.right
			)
			.attr(
				"height",
				boundingBox.height + boundingBox.margin.top + boundingBox.margin.bottom
			)
			.style("background", "white")
			.attr("viewBox", [0, 0, boundingBox.width, boundingBox.height]);

		svg.selectAll("*").remove();

		var defs = svg.append("defs");

		defs
			.append("marker")
			.attr("id", "arrow")
			.attr("viewBox", [0, 0, 10, 10])
			.attr("refX", 21.5)
			.attr("refY", 5)
			.attr("markerWidth", 10)
			.attr("markerHeight", 10)
			.attr("orient", "auto-start-reverse")
			.append("path")
			.attr(
				"d",
				d3.line()([
					[0, 0],
					[10, 5],
					[0, 10],
				])
			)
			.attr("fill", "#555555");

		let viewport = svg.append("g").attr("class", "viewport");

		viewport.append("g").classed("links", true);
		viewport.append("g").classed("nodes", true);

		if (shouldZoom) {
			let transform;
			let zoom = d3.zoom().on("zoom", (e) => {
				viewport.attr("transform", (transform = e.transform));
			});

			svg.call(zoom).call(zoom.transform, d3.zoomIdentity);
		}

		return { svg, viewport };
	}
};

export const drawGraph = (viewport, d3Graph) => {
	const selectGroups = viewport
		.select(".nodes")
		.selectAll("g")
		.data(d3Graph.getNodes());

	const groups = selectGroups
		.enter()
		.append("g")
		.attr("class", (d) => d.getType().typeName);

	const nodes = groups
		.classed("node", true)
		.append("circle")
		.attr("r", 12)
		.style("fill", (d) => d.getColor())
		.style("stroke", (d) => d.getStrokeColor())
		.style("stroke-width", 1);

	// .each((d) => d3Graph.setNodePostion(d));

	const texts = groups
		.append("text")
		.attr("text-anchor", "middle")
		.attr("dy", 20 + 10)
		.text(function (d) {
			return d.label;
		});

	const groupLinks = viewport
		.select(".links")
		.selectAll("line")
		.data(d3Graph.getLinks())
		.enter()
		.append("g")
		.attr("class", (d) => d.getType(d3Graph));

	const links = groupLinks
		.classed("link", true)
		.append("line")
		.attr("stroke", "#555555")
		.attr("stroke-width", "1")
		.attr("marker-end", "url(#arrow)");

	links.exit().remove();

	const linksLabel = groupLinks
		.classed("linkLabel", true)
		.append("text")
		.attr("text-anchor", "middle")
		.attr("alignment-baseline", "central")
		.attr("dy", 20)
		.text(function (d) {
			return d.label;
		});

	return { nodes, texts, links, linksLabel, groups };
};

export const buildSimulation = (
	nodes,
	texts,
	links,
	linksLabel,
	d3Graph,
	boundingBox,
	distance,
	strength
) => {
	var simulation = d3
		.forceSimulation(d3Graph.getNodes())
		.force(
			"link",
			d3
				.forceLink()
				.id(function (d) {
					return d.id;
				})
				.distance(distance)
				.links(d3Graph.getLinks())
				.iterations(100)
		)
		.force("charge", d3.forceManyBody().strength(strength))
		.force(
			"center",
			d3.forceCenter(boundingBox.width / 2, boundingBox.height / 2)
		)
		.on("tick", ticked);

	function ticked() {
		linksLabel.attr("transform", function (d) {
			var src = d.source;
			var tgt = d.target;

			let flip = tgt.x - src.x > 0 ? 180 : 0;

			if (tgt.x - src.x > 0) {
				flip = 180;
			}

			if (tgt.x - src.x === 0) {
				flip = 90;
			}

			if (tgt.x - src.x < 0) {
				flip = 0;
			}

			return `translate(${
				(src.x + tgt.x) / 2
			},${(src.y + tgt.y) / 2}) rotate(${(Math.atan2(tgt.y - src.y, tgt.x - src.x) * flip) / Math.PI})`;
		});

		texts.attr("transform", function (d) {
			return `translate(${d.x},${d.y})`;
		});

		links
			.attr("x1", function (d) {
				return d.source.x;
			})
			.attr("y1", function (d) {
				return d.source.y;
			})
			.attr("x2", function (d) {
				return d.target.x;
			})
			.attr("y2", function (d) {
				return d.target.y;
			});

		nodes
			.attr("cx", function (d) {
				return d.x;
			})
			.attr("cy", function (d) {
				return d.y;
			});
	}

	return simulation;
};

export function drag(simulation) {
	function dragstarted(event) {
		if (!event.active) {
			simulation.alphaTarget(0.3).restart();
		}
		event.subject.fx = event.subject.x;
		event.subject.fy = event.subject.y;
	}

	function dragged(event) {
		event.subject.fx = event.x;
		event.subject.fy = event.y;
	}

	function dragended(event) {
		if (!event.active) {
			simulation.alphaTarget(0);
		}

		event.subject.fx = null;
		event.subject.fy = null;
	}

	function dragsubject(event) {
		return simulation.find(
			event.sourceEvent.offsetX,
			event.sourceEvent.offsetY
		);
	}

	return (
		d3
			.drag()
			// .subject(dragsubject)
			.on("start", dragstarted)
			.on("drag", dragged)
			.on("end", dragended)
	);
}

export const freezeSimulation = (simulation) => {
	for (
		var i = 0,
			n = Math.ceil(
				Math.log(simulation.alphaMin()) / Math.log(1 - simulation.alphaDecay())
			);
		i < n;
		++i
	) {
		simulation.tick();
	}
};
