import AlternateEmailIcon from "@mui/icons-material/AlternateEmail";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import ColorLensIcon from "@mui/icons-material/ColorLens";
import CorporateFareIcon from "@mui/icons-material/CorporateFare";
import EventIcon from "@mui/icons-material/Event";
import InventoryIcon from "@mui/icons-material/Inventory";
import LinkIcon from "@mui/icons-material/Link";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import PersonIcon from "@mui/icons-material/Person";
import QuestionMarkIcon from "@mui/icons-material/QuestionMark";
import TagIcon from "@mui/icons-material/Tag";
import TwitterIcon from "@mui/icons-material/Twitter";

export const colors = {
	person: "red",
	organisation: "blue",
	localisation: "green",
};

export function EntityIcon({ entityName }) {
	switch (entityName) {
		case "person":
			return <PersonIcon />;
		case "CATEGORY_PERSON":
			return <PersonIcon />;
		case "ENTITY_PERSON":
			return <PersonIcon />;

		case "location":
			return <LocationOnIcon />;
		case "CATEGORY_LOCATION":
			return <LocationOnIcon />;
		case "ENTITY_LOCATION":
			return <LocationOnIcon />;

		case "date":
			return <CalendarMonthIcon />;
		case "DATE":
			return <CalendarMonthIcon />;

		case "hashtag":
			return <TagIcon />;
		case "HASHTAG":
			return <TagIcon />;

		case "mention":
			return <TwitterIcon />;
		case "MENTION":
			return <TwitterIcon />;

		case "uri":
			return <LinkIcon />;
		case "URI":
			return <LinkIcon />;

		case "email":
			return <AlternateEmailIcon />;
		case "EMAIL":
			return <AlternateEmailIcon />;

		case "organization":
			return <CorporateFareIcon />;
		case "CATEGORY_ORGANIZATION":
			return <CorporateFareIcon />;
		case "ENTITY_ORGANIZATION":
			return <CorporateFareIcon />;

		case "CATEGORY_CREATIVE_WORK":
			return <ColorLensIcon />;
		case "CATEGORY_EVENT":
			return <EventIcon />;
		case "CATEGORY_PRODUCT":
			return <InventoryIcon />;
		case "CATEGORY_THING":
			return <QuestionMarkIcon />;
		default:
			return null;
	}
}
