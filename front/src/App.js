import * as React from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Session from "react-session-api";

import { ThemeProvider, createTheme } from "@mui/material";
import { AuthProvider, useAuth } from "context/Auth";
import { LangProvider, useLang } from "context/Lang";
import { SourceProvider } from "context/Sources";

import "./App.css";
import Page from "./components/page";
import { AlertProvider } from "./context/SnackBarAlert";
import Abstra from "./pages/Abstra";
import ConnectionLens from "./pages/ConnectionLens";
import PathWays from "./pages/PathWays";
import Projects from "./pages/Projects";
import Sources from "./pages/Sources";
import Stats from "./pages/Stats";
import View from "./pages/View";

const theme = createTheme();

theme.typography.h5 = {
	fontSize: "1.5rem",
	"@media (min-width:600px)": {
		fontSize: "2.5rem",
	},
	"@media (max-width:600px)": {
		fontSize: "1.2rem",
	},
	[theme.breakpoints.up("md")]: {
		fontSize: "1.5rem",
	},
};

theme.typography.h3 = {
	fontSize: "3rem",
	[theme.breakpoints.up("xs")]: {
		fontSize: "1rem",
	},
	[theme.breakpoints.up("sm")]: {
		fontSize: "1.2rem",
	},
	[theme.breakpoints.up("md")]: {
		fontSize: "2rem",
	},
	[theme.breakpoints.up("lg")]: {
		fontSize: "3rem",
	},
};

theme.typography.h2 = {
	fontSize: "3.75rem",
	[theme.breakpoints.up("xs")]: {
		fontSize: "1.2rem",
	},
	[theme.breakpoints.up("sm")]: {
		fontSize: "1.5rem",
	},
	[theme.breakpoints.up("md")]: {
		fontSize: "2.5rem",
	},
	[theme.breakpoints.up("lg")]: {
		fontSize: "3.75rem",
	},
};

function App() {
	Session.config(true, 0);
	return (
		<AlertProvider>
			<AuthProvider>
				<LangProvider>
					<SourceProvider>
						<BrowserRouter basename={process.env.REACT_APP_ROUTER_BASE || ""}>
							<ThemeProvider theme={theme}>
								<Routes>
									<Route
										path="/connectionstudio"
										element={<Navigate to="/" />}
									></Route>

									<Route path="/" element={<Page />}>
										<Route path="/" element={<Projects />}></Route>
										<Route
											path="/:database/sources"
											element={<Sources />}
										></Route>
										<Route
											path="/:database/connectionlens"
											element={<ConnectionLens />}
										></Route>
										<Route path="/:database/view" element={<View />}></Route>

										<Route
											path="/:database/statistics/"
											element={<Stats />}
										></Route>
										<Route
											path="/:database/abstra"
											element={<Abstra />}
										></Route>
										<Route
											path="/:database/pathways"
											element={<PathWays />}
										></Route>
									</Route>
								</Routes>
							</ThemeProvider>
						</BrowserRouter>
					</SourceProvider>
				</LangProvider>
			</AuthProvider>
		</AlertProvider>
	);
}

export default App;
