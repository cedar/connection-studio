import * as React from "react";
import { Navigate, useNavigate } from "react-router-dom";

import CloseIcon from "@mui/icons-material/Close";
import StopCircleIcon from "@mui/icons-material/StopCircle";
import {
	Alert,
	AlertTitle,
	Backdrop,
	Button,
	Card,
	CardContent,
	CircularProgress,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	FormControl,
	Grid,
	IconButton,
	InputLabel,
	MenuItem,
	Paper,
	Select,
	TextField,
	Typography,
	Box,
} from "@mui/material";
import { blue } from "@mui/material/colors";
import { useSource } from "context/Sources";

import FadeOutAlert from "../components/alert";
import { ContainedButton } from "../components/button";
import { useAuth } from "../context/Auth";
import { useLang } from "../context/Lang";
import { fetcherGet, fetcherPost } from "../fetch/fetcher";
import { useDbs, useLoadEngine } from "../fetch/projects";
import { isDefined } from "../utils/utils";

function AddProjectButton({ isAddedDb }) {
	const [openCreateInstance, setOpenCreateInstance] = React.useState(false);
	const [textInput, setTextInput] = React.useState("");
	const [newDatabase, setNewDatabase] = React.useState("");
	const [createdDb, setCreatedDb] = React.useState(-1);
	const [triggerAlert, setTriggerAlert] = React.useState(false);
	const [backdropOpen, setBackdropOpen] = React.useState(false);
	const [shouldCreateInstance, setShouldCreateInstance] = React.useState(false);

	const handleOnAddInstance = () => {
		const parsedTextInput = textInput
			.toLowerCase()
			.replaceAll(" ", "_")
			.replaceAll("-", "_");
		setNewDatabase(parsedTextInput);
		setBackdropOpen(true);
	};

	const handleOnTextInput = (event) => {
		setTextInput(event.target.value);
	};

	const handleKeyDown = (event) => {
		if (event.code === "Enter" || event.code === "NumpadEnter") {
			setTextInput(event.target.value);
			handleOnAddInstance();
		}
	};

	const handleOnClose = () => {
		setNewDatabase("");
		setTextInput("");
		setOpenCreateInstance(false);
	};

	React.useEffect(() => {
		if (newDatabase !== "") {
			setShouldCreateInstance(true);
		}
	}, [newDatabase]);

	React.useEffect(() => {
		if (shouldCreateInstance) {
			const post = fetcherPost(
				"/cl/createDb",
				{
					d: newDatabase,
				},
				{}
			);
			post.then((res) => setCreatedDb(res));
		}
	}, [shouldCreateInstance, newDatabase]);

	React.useEffect(() => {
		if (createdDb === 200) {
			handleOnClose();
			// setTriggerAlert(true);
			isAddedDb();
			setBackdropOpen(false);
			setShouldCreateInstance(false);
			setCreatedDb(-1);
		}
	}, [createdDb, isAddedDb]);

	const { activeLang } = useLang();

	const trans = {
		dialogTitle: {
			en: "Create a project",
			fr: "Créer un projet",
		},
		dialogPlaceHolder: {
			en: "Enter the name of your project",
			fr: "Entrez le nom de votre projet",
		},
		dialogBtn: {
			cancel: {
				en: "Cancel",
				fr: "Annuler",
			},
			add: {
				en: "Add",
				fr: "Créer",
			},
		},
		btn: {
			en: "Create a project",
			fr: "Créer un projet",
		},
		fadeOut: {
			en: "Project created!",
			fr: "Projet créé !",
		},
	};

	return (
		<Box sx={{ my: "auto" }}>
			<Dialog
				open={openCreateInstance}
				onClose={handleOnClose}
				fullWidth
				maxWidth="sm"
			>
				<DialogTitle>{trans.dialogTitle[activeLang]}</DialogTitle>
				<DialogContent>
					<TextField
						sx={{ width: "100%" }}
						label={trans.dialogPlaceHolder[activeLang]}
						variant="standard"
						onChange={handleOnTextInput}
						onKeyDown={handleKeyDown}
					/>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleOnClose}>
						{trans.dialogBtn.cancel[activeLang]}
					</Button>
					<Button onClick={handleOnAddInstance}>
						{trans.dialogBtn.add[activeLang]}
					</Button>
				</DialogActions>
				<Backdrop
					sx={{ color: "#fff", "z-index": "10 !important" }}
					open={backdropOpen}
				>
					<CircularProgress color="inherit" />
				</Backdrop>
			</Dialog>
			<Button
				sx={{ mr: 3, height: "100%" }}
				variant="outlined"
				onClick={(e) => {
					setOpenCreateInstance(true);
				}}
				disableFocusRipple
			>
				{trans.btn[activeLang]}
			</Button>
			{triggerAlert === true ? (
				<FadeOutAlert>{trans.fadeOut[activeLang]}</FadeOutAlert>
			) : null}
		</Box>
	);
}

function ProjectCard({ database, isDeletedDb, setMessageError }) {
	const [dbToDelete, setDbToDelete] = React.useState("");
	const [deleteDb, setDeleteDb] = React.useState(-1);
	const [shouldDeleteInstance, setShouldDeleteInstance] = React.useState(false);
	const [triggerAlert, setTriggerAlert] = React.useState(false);
	const [shouldLoadEngine, setShouldLoadEngine] = React.useState(false);
	const [backdropOpen, setBackdropOpen] = React.useState(false);

	const navigate = useNavigate();

	const { loadEngine, isLoadEngineError, isLoadEngineLoading } = useLoadEngine(
		shouldLoadEngine,
		database.db
	);

	React.useEffect(() => {
		if (isLoadEngineLoading) {
			setBackdropOpen(true);
		}
		if (!isLoadEngineLoading) {
			setBackdropOpen(false);
		}
	}, [isLoadEngineLoading]);

	const handleOnDelete = (event, db) => {
		event.stopPropagation();
		setBackdropOpen(true);
		setDbToDelete(database);
	};

	const { sourcesLength, setPersistentSourcesLength } = useSource();

	const handleOnSelectProject = (event) => {
		event.stopPropagation();
		event.preventDefault();

		fetcherGet("/cl/datasources", {
			d: database.db.replace("cl_", ""),
		}).then((res) => {
			setPersistentSourcesLength(res.length);
			setShouldLoadEngine(true);
		});
	};

	React.useEffect(() => {
		if (dbToDelete !== "" && isDefined(dbToDelete)) {
			setShouldDeleteInstance(true);
		}
	}, [dbToDelete, setShouldDeleteInstance]);

	React.useEffect(() => {
		if (shouldDeleteInstance) {
			setMessageError(null);
			const post = fetcherPost(
				"/cl/deleteDb",
				{
					d: database.db.replace("cl_", ""),
				},
				{}
			);
			post
				.then((res) => {
					setDeleteDb(res);
					setBackdropOpen(false);
				})
				.catch((error) => {
					setMessageError(error.response.data.message);
					setBackdropOpen(false);
					setDeleteDb(-1);
					setShouldDeleteInstance(false);
					setDbToDelete("");
					setOpenDeleteDialog(false);
				});
		}
	}, [shouldDeleteInstance, database]);

	React.useEffect(() => {
		if (deleteDb === 200) {
			isDeletedDb();
			setDeleteDb(-1);
			setShouldDeleteInstance(false);
			setDbToDelete("");
			setOpenDeleteDialog(false);
		}
	}, [deleteDb, isDeletedDb]);

	const handleOnManageSources = (e) => {
		navigate(`${database.db}/sources/`);
	};

	const [openDeleteDialog, setOpenDeleteDialog] = React.useState(false);
	const handleOnOpenDialog = (event) => {
		setOpenDeleteDialog(true);
		event.stopPropagation();
	};

	const handleCloseDialog = (event) => {
		event.stopPropagation();
		setOpenDeleteDialog(false);
	};
	const { activeLang } = useLang();

	const trans = {
		project: {
			en: "Project",
			fr: "Projet",
		},
		noFile: {
			en: "No files uploaded yet, add one!",
			fr: "Pas encore de fichier chargé, ajoutez-en !",
		},
		butn: {
			en: "Manage",
			fr: "Gérer",
		},
		fadeOut: {
			en: "Project added!",
			fr: "Projet ajouté !",
		},
		projectSnippet: {
			size: {
				en: "files",
				fr: "fichiers",
			},
			create: {
				en: "Created on:",
				fr: "Créé le :",
			},
			lastAdd: {
				en: "Latest file addition:",
				fr: "Dernier ajout de fichier :",
			},
		},
		deleteDialog: {
			title: {
				fr: "Vous êtes sur le point de supprimer un projet",
				en: "You are about to delete a project",
			},
			text: {
				fr: "Êtes-vous sûr de vouloir supprimer ce projet ?",
				en: "Are you sure you want to delete this project?",
			},
			yes: {
				fr: "Oui",
				en: "Yes",
			},
			no: {
				fr: "Non",
				en: "No",
			},
		},
	};

	return (
		<MenuItem
			key={database.db}
			sx={{ width: "100%", height: 250, p: 0, display: "block" }}
		>
			<Card
				component={Paper}
				sx={{
					width: "100%",
					height: "100%",
					cursor: "pointer",
					boxShadow: 4,
					borderRadius: 2,
				}}
				onClick={handleOnSelectProject}
				onMouseDown={(event) => {
					event.stopPropagation();
				}}
				elevation={12}
			>
				<CardContent sx={{ height: "100%" }}>
					<Box sx={{ display: "flex" }}>
						<Typography
							gutterBottom
							variant="h5"
							component="div"
							sx={{ textTransform: "capitalize" }}
						>
							{trans.project[activeLang]}{" "}
							{database.db.replace("cl_", "").replaceAll("_", " ")}
						</Typography>
						<IconButton
							aria-label="close"
							color="inherit"
							size="medium"
							sx={{ position: "absolute", right: 0, mr: 1, top: 0 }}
							onClick={handleOnOpenDialog}
							onMouseDown={(event) => {
								event.stopPropagation();
							}}
						>
							<CloseIcon sx={{ cursor: "normal" }} fontSize="inherit" />
						</IconButton>
						<Dialog
							open={openDeleteDialog}
							onClose={handleCloseDialog}
							sx={{ zIndex: "10 !important" }}
							onClick={(e) => e.stopPropagation()}
						>
							<DialogTitle>{trans.deleteDialog.title[activeLang]}</DialogTitle>
							<DialogContent>
								<DialogContentText>
									{trans.deleteDialog.text[activeLang]}
								</DialogContentText>
							</DialogContent>
							<DialogActions>
								<Button onClick={(event) => handleOnDelete(event, database.db)}>
									{trans.deleteDialog.yes[activeLang]}
								</Button>
								<Button onClick={handleCloseDialog} autoFocus>
									{trans.deleteDialog.no[activeLang]}
								</Button>
							</DialogActions>
						</Dialog>
					</Box>

					{database.datasources_size !== 0 ? (
						<Box>
							<Typography variant="body1" color="text.secondary">
								{database.datasources_size}{" "}
								{trans.projectSnippet.size[activeLang]}
							</Typography>
							<Typography variant="body1" color="text.secondary">
								{trans.projectSnippet.create[activeLang]}{" "}
								{database.creation_date.slice(0, -2)}
							</Typography>
							<Typography variant="body1" color="text.secondary">
								{trans.projectSnippet.lastAdd[activeLang]}{" "}
								{database.last_modified_date.slice(0, -2)}
							</Typography>
						</Box>
					) : (
						<Typography variant="body1" color="text.secondary">
							{trans.noFile[activeLang]}
						</Typography>
					)}
				</CardContent>
			</Card>
			<Box
				sx={{
					position: "absolute",
					left: "5%",
					bottom: 0,
					mb: 2,
					width: "90%",
					ml: "auto",
					mr: "auto",
				}}
			>
				<ContainedButton sx={{ width: "100%" }} onClick={handleOnManageSources}>
					{trans.butn[activeLang]}
				</ContainedButton>
			</Box>
			{triggerAlert === true ? (
				<FadeOutAlert>{trans.fadeOut[activeLang]}</FadeOutAlert>
			) : null}

			<Backdrop
				open={backdropOpen}
				sx={{ color: "#fff", "z-index": "1000 !important" }}
			>
				<CircularProgress color="inherit" />
			</Backdrop>
			{isDefined(loadEngine) && loadEngine === 200 && (
				<Navigate
					to={
						sourcesLength === 0
							? `/${database.db}/sources`
							: `/${database.db}/statistics`
					}
					replace={true}
				/>
			)}
		</MenuItem>
	);
}

function sortFunction(option, a, b) {
	switch (option) {
		case "database_name":
			return a.db > b.db ? 1 : b.db > a.db ? -1 : 0;
		case "last_modified_date":
			return a.last_modified_date < b.last_modified_date
				? 1
				: b.last_modified_date < a.last_modified_date
				? -1
				: 0;
		case "creation_date":
			return a.creation_date < b.creation_date
				? 1
				: b.creation_date < a.creation_date
				? -1
				: 0;
		default:
			return 0;
	}
}
export default function Projects() {
	const { setPersistentisAuth } = useAuth();
	const [sortOption, setSortOption] = React.useState("database_name");

	const { dbsList, isDbError, isDbLoading, mutateDbs } = useDbs(true);

	const [projectDeleteErrorMessage, setProjectDeleteErrorMessage] =
		React.useState(null);

	const setMessageError = (message) => {
		setProjectDeleteErrorMessage(message);
	};

	function isAddedDb() {
		mutateDbs();
	}

	function isDeletedDb() {
		mutateDbs();
	}

	React.useEffect(() => {
		if (isDefined(dbsList) && dbsList.length > 0) {
			setPersistentisAuth(true);
		} else {
			setPersistentisAuth(false);

			if (isDbError) {
			}
		}
	}, [dbsList, setPersistentisAuth, isDbError]);

	const handleEndSession = () => {
		fetcherGet("/cl/session", {})
			.then((res) => {
				console.log(res);
			})
			.catch((error) => console.log(error));
	};

	const { activeLang } = useLang();

	const trans = {
		noProject: {
			p1: {
				en: "You don't have a project yet",
				fr: "Vous n'avez pas encore de projet",
			},
			p2: { en: "create one!", fr: "créez-en un !" },
		},
		noProjecterror: {
			t: {
				en: "Connection Error",
				fr: "Erreur de connexion",
			},
			p: {
				en: (
					<div>
						Connection to the Studio database failed.
						<br /> Please make sure that your database management system and
						Tomcat are running.
					</div>
				),
				fr: (
					<div>
						Connection à la base de données du Studio impossible.
						<br /> Assurez-vous que votre système de gestion de bases de données
						et Tomcat soient fonctionnels.
					</div>
				),
			},
		},
		ednSession: { fr: "Terminer la session", en: "End session" },
		sort: {
			en: "Sort by",
			fr: "Trier par",
		},
		sortOptions: {
			database_name: {
				en: "Project name",
				fr: "Nom du projet",
			},
			last_modified_date: {
				en: "Last modification date",
				fr: "Date de la dernière modification",
			},
			creation_date: {
				en: "Creation date",
				fr: "Date de création",
			},
		},
	};

	return (
		<Box sx={{ display: "flex", flexDirection: "column" }}>
			<Box sx={{ position: "relative", display: "flex" }}>
				<FormControl
					size="small"
					sx={{ float: "left", mr: "auto", my: "auto", minWidth: 200 }}
				>
					<InputLabel>{trans.sort[activeLang]}</InputLabel>
					<Select
						value={sortOption}
						label={trans.sort[activeLang]}
						onChange={(event) => {
							setSortOption(event.target.value);
						}}
					>
						<MenuItem value="database_name">
							{trans.sortOptions.database_name[activeLang]}
						</MenuItem>
						<MenuItem value="last_modified_date">
							{trans.sortOptions.last_modified_date[activeLang]}
						</MenuItem>
						<MenuItem value="creation_date">
							{trans.sortOptions.creation_date[activeLang]}
						</MenuItem>
					</Select>
				</FormControl>

				<ContainedButton
					sx={{
						mr: 3,
						height: 40,
						p: 2,
						my: "auto",
					}}
					onClick={handleEndSession}
					onMouseDown={(event) => {
						event.stopPropagation();
					}}
				>
					<StopCircleIcon sx={{ mr: 1 }}></StopCircleIcon>
					{trans.ednSession[activeLang]}
				</ContainedButton>
				<AddProjectButton isAddedDb={isAddedDb}></AddProjectButton>
			</Box>
			{projectDeleteErrorMessage ? (
				<Box
					sx={{
						position: "absolute",
						zIndex: 1000,
						bottom: 0,
					}}
				>
					<FadeOutAlert severity="error">
						{projectDeleteErrorMessage}
					</FadeOutAlert>
				</Box>
			) : null}
			{isDbError || (isDefined(dbsList) && dbsList.length) > 0 ? null : (
				<Box
					sx={{
						display: "flex",
						flexDirection: "row",
						alignContent: "center",
						alignItems: "center",
						p: 1,
						mt: 6,
						ml: "auto",
						mr: "auto",
						maxHeight: 400,
						maxWidth: 800,
					}}
				>
					<Box>
						<Typography
							variant="h1"
							sx={{
								fontWeight: 1000,
								textTransform: "uppercase",
								color: blue[700],
								textAlign: "center",
							}}
						>
							OOPS
						</Typography>
						<Typography
							variant="h3"
							sx={{
								fontWeight: 500,
								textTransform: "uppercase",
								textAlign: "center",
							}}
						>
							<br />
							{trans.noProject.p1[activeLang]}
						</Typography>
						<Typography
							variant="h4"
							sx={{
								fontWeight: 700,
								textTransform: "uppercase",
								textAlign: "center",
								color: blue[700],
							}}
						>
							<br /> {trans.noProject.p2[activeLang]}
						</Typography>
					</Box>
				</Box>
			)}
			{isDbError ? null : (
				<Grid
					container
					sx={{
						mt: 2,
						display: "grid",
						gridTemplateColumns: "repeat(auto-fill, 350px)",
						gridGap: "1rem",
						justifyContent: "space-around",
					}}
				>
					{isDbLoading || !isDefined(dbsList)
						? null
						: dbsList
								.sort((a, b) => sortFunction(sortOption, a, b))
								.map((database) => (
									<Box key={database.db}>
										<ProjectCard
											database={database}
											isDeletedDb={isDeletedDb}
											setMessageError={setMessageError}
										></ProjectCard>
									</Box>
								))}
				</Grid>
			)}
			{isDbError ? (
				<Alert sx={{ position: "relative", mt: 3, p: 3 }} severity="error">
					<AlertTitle sx={{ fontSize: 18 }}>
						{trans.noProjecterror.t[activeLang]}
					</AlertTitle>
					<Typography sx={{ fontSize: 16 }}>
						{trans.noProjecterror.p[activeLang]}
					</Typography>
				</Alert>
			) : null}
		</Box>
	);
}
