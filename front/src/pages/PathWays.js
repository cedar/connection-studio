import * as React from "react";
import { useParams } from "react-router-dom";

import CloseIcon from "@mui/icons-material/Close";
import HistoryIcon from "@mui/icons-material/History";
import LoadingButton from "@mui/lab/LoadingButton";
import {
	Box,
	Button,
	CircularProgress,
	Dialog,
	DialogContent,
	DialogTitle,
	FormControl,
	IconButton,
	InputLabel,
	MenuItem,
	Select,
	Stack,
	TextField,
	ToggleButton,
	ToggleButtonGroup,
	Typography,
} from "@mui/material";
import { blue, grey, red } from "@mui/material/colors";
import FileSideMenu from "components/atomic/FileSideMenu";
import { HelpButton } from "components/button.js";
import ResizableSizeMenu from "components/menu/resizableSizeMenu";
import PathWaysHistory from "components/pathways/pathWaysHistory.js";
import PathWaysRowResult from "components/pathways/rowResult.js";
import { sortPathWaysRows } from "components/pathways/utils.js";
import { useLang } from "context/Lang.js";
import { useAlert } from "context/SnackBarAlert.js";
import { fetcherPost } from "fetch/fetcher.js";
import { useGetPathWaysExecutions, useReadPathWays } from "fetch/pathways";
import { useDataSources } from "fetch/sources";

const labelTypographyStyle = {
	color: "#1976d2",
	fontWeight: 500,
	fontFamily: '"Roboto","Helvetica","Arial",sans-serif;',
	lineHeight: 1.2,
	my: "auto",
};

const trans = {
	filePlaceHolder: {
		en: "File names",
		fr: "Nom des fichiers",
	},
	warning: {
		en: "No file selected",
		fr: "Aucun fichier sélectionné",
	},
	warningNoPath: {
		en: "No data path found for entities of type ",
		fr: "Pas de chemins trouvés entre des entités de types ",
	},
	warningNotExecuted: {
		en: "No research done yet",
		fr: "Pas encore de recherche effectuée",
	},
	history: {
		en: "History",
		fr: "Historique",
	},
	form: {
		co: { fr: "Connecter des", en: "Connect" },
		src: { fr: "", en: "" },
		to: { fr: "à des", en: "to" },
		tgt: { fr: "Cible", en: "Target" },
		depthPath: {
			fr: "Longueur maximum d'un chemin",
			en: "Maximum depth of a path",
		},
	},
	entities: {
		ENTITY_PERSON: { fr: "Personnes", en: "People" },
		ENTITY_ORGANIZATION: { fr: "Organisations", en: "Organizations" },
		ENTITY_LOCATION: { fr: "Lieux", en: "Locations" },
		RDF_URI: { fr: "URIs", en: "URIs" },
		HASHTAG: { fr: "Hashtags", en: "Hashtags" },
		EMAIL: { fr: "Emails", en: "Emails" },
		DATE: { fr: "Dates", en: "Dates" },
		MENTION: { fr: "Mentions Twitter", en: "Twitter citations" },
	},
	loadBtn: { fr: "Rechercher des chemins", en: "Search paths" },
	tglBtn1: { fr: "Nombre de chemins", en: "Number of paths" },
	tglBtn2: { fr: "Longueur des chemins", en: "Length of paths" },
	dialogTitle: { fr: "Recherches précédentes", en: "Previous searches" },
	sort: { fr: "Trier par", en: "Sort by" },
	succesMessage: {
		en: "Path exploration ended successfully.",
		fr: "L'exploration des chemins s'est terminée avec succès.",
	},
	errorMessage: {
		en: "An error occurred during the path exploration.",
		fr: "Une erreur s'est produite durant l'exploration des chemins.",
	},
	helper: {
		fr: (
			<Typography>
				<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
					But : connecter les entités nommées entre elles via des chemins dans
					les données.
				</Box>
				Dans cette page, on cherche à connecter 2 types d'entités ensemble, par
				exemple les personnes et les lieux. L'exploration de chemins va alors
				calculer tous les chemins possibles dans les données pour connecter
				cette paire d'entités.
				<Box sx={{ fontWeight: 600, color: grey[800], mt: 2, mb: 1 }}>
					Comment lire le résultat ?
				</Box>
				Chaque chemin (suite de petites boîtes grises) correspond à un chemin
				type dans les données. En cliquant dessus, on voit les chemins qui
				apparaissent réellement dans les données. Les entités connectées sont
				sur fond coloré.
			</Typography>
		),
		en: (
			<Typography>
				<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
					Goal: connect named entities through data paths.
				</Box>
				In this page, we try to connect 2 types of named entities, for instance
				people and places. The path exploration will compute all the possible
				paths that exist in the data to connect those entities.
				<Box sx={{ fontWeight: 600, color: grey[800], mt: 2, mb: 1 }}>
					How to read the result?
				</Box>
				Each path (list of small gray boxes) corresponds to a meta-path in the
				data. By clicking on one of those, you will see the data-paths, that is
				paths that really appear in the data. Connected entities appear on a
				colored background.
			</Typography>
		),
	},
};

function WarningMessage({ isNoPath, entitySource, entityTarget }) {
	const { activeLang } = useLang();

	return (
		<Box sx={{ mt: "5rem" }}>
			{isNoPath ? (
				<>
					<Typography
						display="inline"
						sx={{
							fontWeight: 300,
							textAlign: "center",
						}}
					>
						{trans.warningNoPath[activeLang]}
					</Typography>
					<Typography
						display="inline"
						sx={{
							fontWeight: 300,
							textAlign: "center",
						}}
						color={blue[700]}
					>
						{trans.entities[entitySource][activeLang]}
					</Typography>
					<Typography
						display="inline"
						sx={{
							fontWeight: 300,
							textAlign: "center",
						}}
					>
						{" "}
						&{" "}
					</Typography>
					<Typography
						display="inline"
						sx={{
							fontWeight: 300,
							textAlign: "center",
						}}
						color={blue[700]}
					>
						{trans.entities[entityTarget][activeLang]}
					</Typography>
				</>
			) : (
				<Typography
					display="inline"
					sx={{
						fontWeight: 300,
						textAlign: "center",
					}}
				>
					{trans.warningNotExecuted[activeLang]}
				</Typography>
			)}
		</Box>
	);
}
export default function PathWays() {
	const params = useParams();
	const database = params.database;

	const { setAlert } = useAlert();
	const [isComputing, setIsComputing] = React.useState(false);
	const [activeItem, setActiveItem] = React.useState();
	const [executionId, setExecutionId] = React.useState();
	const [maximumPathLength, setMaximumPathLength] = React.useState(10);
	const [entitySource, setEntitySource] = React.useState("");
	const [entityTarget, setEntityTarget] = React.useState("");

	const [pathResults, setPathResults] = React.useState([]);
	const [filterOption, setFilterOption] = React.useState("path_number");
	const [searchExecuted, setSearchExecuted] = React.useState(false);
	const [searchLoading, setSearchLoading] = React.useState(false);
	const handleSelectChange = (event, setFunction) => {
		setFunction(event.target.value);
	};

	function handleSelectItem(id) {
		setActiveItem(id);
		setExecutionId(null);
		setPathResults([]);
		setMaximumPathLength(10);
		setEntitySource("");
		setEntityTarget("");
		setSearchExecuted(false);
	}

	const handleFilterChange = (event, newFilter) => {
		if (newFilter !== null) {
			setFilterOption(newFilter);
		}
	};

	const [openHistory, setOpenHistory] = React.useState(false);

	const sortedPathResults = pathResults
		.filter((row) => {
			if (row.dataPaths.length === 0) {
				return false;
			} else {
				return true;
			}
		})
		.sort((a, b) => sortPathWaysRows(a, b, filterOption));

	const { dataSources } = useDataSources(true, database);

	const { readInfo, isReadLoading } = useReadPathWays(
		executionId,
		activeItem,
		database,
		executionId
	);

	const {
		pathWaysExistingConfigs,
		isGetExecutionsLoading,
		mutatePathWaysExecutions,
	} = useGetPathWaysExecutions(activeItem, activeItem, database);

	const handlePathWaysSearch = async () => {
		if (!isComputing) {
			setIsComputing(true);
			const body = {
				dataSourceId: activeItem,
				dbPathWaysRun: database.replace("cl_", ""),
				entityType1: entitySource,
				entityType2: entityTarget,
				maxSizeDataPath: maximumPathLength,
			};
			let pathwaysRes = await fetcherPost("/pathways/run", body).catch(
				function (error) {
					setIsComputing(false);
					setAlert({
						message: trans.errorMessage[activeLang],
						level: "error",
					});
				}
			);
			setIsComputing(false);
			if (pathwaysRes && Array.isArray(pathwaysRes)) {
				setAlert({
					message: trans.succesMessage[activeLang],
					level: "success",
				});
				mutatePathWaysExecutions();
				setPathResults(pathwaysRes);
				setSearchExecuted(true);
			}
		}
	};

	function onClickHistory(entry) {
		setExecutionId(entry.executionId);
		setEntitySource(entry.leftEntityType);
		setEntityTarget(entry.rightEntityType);
		setMaximumPathLength(entry.maxLengthPath);
		setOpenHistory(false);
		setSearchExecuted(true);
	}

	const { activeLang } = useLang();

	const entityOptions = [
		{
			key: "ENTITY_PERSON",
			label: trans.entities["ENTITY_PERSON"][activeLang],
		},
		{
			key: "ENTITY_ORGANIZATION",
			label: trans.entities["ENTITY_ORGANIZATION"][activeLang],
		},
		{
			key: "ENTITY_LOCATION",
			label: trans.entities["ENTITY_LOCATION"][activeLang],
		},
		{
			key: "RDF_URI",
			label: trans.entities["RDF_URI"][activeLang],
		},
		{
			key: "HASHTAG",
			label: trans.entities["HASHTAG"][activeLang],
		},
		{
			key: "EMAIL",
			label: trans.entities["EMAIL"][activeLang],
		},
		{
			key: "DATE",
			label: trans.entities["DATE"][activeLang],
		},
		{
			key: "MENTION",
			label: trans.entities["MENTION"][activeLang],
		},
	];

	React.useEffect(() => {
		if (readInfo) setPathResults(readInfo);
	}, [readInfo]);

	return (
		<Box
			sx={{
				width: "100%",
				height: "100%",
				display: "flex",
				flexDirection: "column",
			}}
		>
			<HelpButton
				sx={{ position: "absolute", right: 20 }}
				helper={trans.helper[activeLang]}
			></HelpButton>
			<Box sx={{ height: "100%", width: "100%", display: "flex" }}>
				<ResizableSizeMenu
					menuComponent={
						<FileSideMenu
							dataSources={dataSources?.length ? dataSources : []}
							handleSelectItem={handleSelectItem}
							activeItem={activeItem}
						/>
					}
				>
					<Box
						sx={{
							height: "100%",
							width: "100%",
							display: "flex",
						}}
					>
						{activeItem ? (
							<Box sx={{ width: "100%", px: 2 }}>
								<Box
									sx={{
										my: 2,
										display: "flex",
										flexWrap: "wrap",
										width: "100%",
									}}
								>
									<Box sx={{ display: "flex", my: "auto" }}>
										<Typography sx={labelTypographyStyle}>
											{trans.form.co[activeLang]}
										</Typography>
										<Box sx={{ minWidth: 150, mx: 1 }}>
											<FormControl fullWidth size="small">
												<InputLabel>Source</InputLabel>
												<Select
													label="ent-source"
													value={entitySource}
													onChange={(event) =>
														handleSelectChange(event, setEntitySource)
													}
												>
													{entityOptions.map((entity, index) => {
														return (
															<MenuItem key={index} value={entity.key}>
																{entity.label}
															</MenuItem>
														);
													})}
												</Select>
											</FormControl>
										</Box>
										<Typography sx={labelTypographyStyle}>
											{trans.form.to[activeLang]}
										</Typography>
										<Box sx={{ minWidth: 150, mx: 1 }}>
											<FormControl fullWidth size="small">
												<InputLabel>{trans.form.tgt[activeLang]}</InputLabel>
												<Select
													label="ent-target"
													value={entityTarget}
													onChange={(event) =>
														handleSelectChange(event, setEntityTarget)
													}
												>
													{entityOptions.map((entity, index) => {
														return (
															<MenuItem key={index} value={entity.key}>
																{entity.label}
															</MenuItem>
														);
													})}
												</Select>
											</FormControl>
										</Box>
									</Box>

									<TextField
										sx={{ ml: 1 }}
										type="number"
										label={trans.form.depthPath[activeLang]}
										variant="standard"
										value={maximumPathLength}
										onChange={(event) =>
											setMaximumPathLength(event.target.value)
										}
									/>
									{entitySource !== "" && entityTarget !== "" ? (
										<LoadingButton
											variant="contained"
											sx={{ my: "auto", ml: 2 }}
											onClick={handlePathWaysSearch}
											loading={isComputing}
										>
											{trans.loadBtn[activeLang]}
										</LoadingButton>
									) : null}

									<Button
										sx={{ ml: 2 }}
										variant="outlined"
										startIcon={<HistoryIcon />}
										onClick={() => setOpenHistory(true)}
										disabled={
											!(
												typeof pathWaysExistingConfigs !== "undefined" &&
												pathWaysExistingConfigs.length > 0
											)
										}
									>
										{trans.history[activeLang]}
									</Button>
								</Box>
								{isReadLoading || isComputing ? (
									<Stack alignItems="center">
										<CircularProgress size="7rem" sx={{ mt: "7rem" }} />
									</Stack>
								) : (
									<Box sx={{ display:"flex", flexDirection:"column", height:"100%"}}>
										{sortedPathResults.length > 0 ? (
											<Box sx={{ py: 1, display: "flex" }}>
												<Typography sx={labelTypographyStyle}>
													{trans.sort[activeLang]}
												</Typography>
												<ToggleButtonGroup
													exclusive
													value={filterOption}
													onChange={handleFilterChange}
													sx={{ mx: 2 }}
													color="primary"
												>
													<ToggleButton
														value="path_number"
														aria-label="centered"
													>
														{trans.tglBtn1[activeLang]}
													</ToggleButton>
													<ToggleButton
														value="path_length"
														aria-label="left aligned"
													>
														{trans.tglBtn2[activeLang]}
													</ToggleButton>
												</ToggleButtonGroup>
											</Box>
										) : (
											<WarningMessage
												isNoPath={searchExecuted}
												entitySource={entitySource}
												entityTarget={entityTarget}
											></WarningMessage>
										)}
										<Box sx={{  height:"100%",display: "flex", flexDirection:"column", overflowY:"auto", paddingBottom:"100px" }}>
											{sortedPathResults.map((row, index) => {
												return <PathWaysRowResult key={index} row={row} />;
											})}
										</Box>
									</Box>
								)}
							</Box>
						) : (
							<Box sx={{ display: "flex", m: "auto" }}>
								<Typography variant="h5">
									{trans.warning[activeLang]}
								</Typography>
							</Box>
						)}
					</Box>
				</ResizableSizeMenu>
				<Dialog
					open={openHistory}
					onClose={() => setOpenHistory(false)}
					fullWidth={true}
					maxWidth="xl"
					scroll="paper"
				>
					<DialogTitle sx={{ m: 0, p: 2 }}>
						{trans.dialogTitle[activeLang]}
						<IconButton
							aria-label="close"
							onClick={() => setOpenHistory(false)}
							sx={{
								position: "absolute",
								right: 8,
								top: 8,
								color: red[500],
							}}
						>
							<CloseIcon />
						</IconButton>
					</DialogTitle>
					{!isGetExecutionsLoading && pathWaysExistingConfigs ? (
						<DialogContent>
							<PathWaysHistory
								data={pathWaysExistingConfigs}
								onClickHistory={onClickHistory}
								entityOption={entityOptions}
							></PathWaysHistory>
						</DialogContent>
					) : null}
				</Dialog>
			</Box>
		</Box>
	);
}
