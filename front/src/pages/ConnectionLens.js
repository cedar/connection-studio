import * as React from "react";
import { useParams } from "react-router-dom";

import CloseIcon from "@mui/icons-material/Close";
import {
	Typography,
	MenuList,
	MenuItem,
	Card,
	Fab,
	CardContent,
	Collapse,
	IconButton,
	FormControlLabel,
	Switch,
	Backdrop,
	CircularProgress,
} from "@mui/material";
import { blue, grey } from "@mui/material/colors";
import { Box, Container } from "@mui/system";
import * as d3 from "d3";

import { HelpButton } from "../components/button";
import SideMenu from "../components/cl/sideMenu";
import ParametersMenu from "../components/parameter";
import SearchBar from "../components/searchbar";
import { useLang } from "../context/Lang";
import "../css/cl.css";
import { fetcherPost, sendPost } from "../fetch/fetcher";
import { D3Graph, TYPES } from "../utils/graph";
import { SEARCH_PARAMETERS } from "../utils/parameters";
import {
	getBoundingBox,
	initViewport,
	drawGraph,
	buildSimulation,
	drag,
} from "../utils/simulation";
import { isDefined } from "../utils/utils";

const trans = {
	placeHolder: {
		en: "Enter keywords...",
		fr: "Entrez des mots-clés...",
	},
	helper: {
		fr: (
			<Typography>
				<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
					But: explorer les données par mots-clés.
				</Box>
				L'idée est de vous permettre d'explorer les données par mots-clés, tels
				que "Macron", "Pfizer", ou "Borne Assemblée". Dans la zone de texte à
				gauche, saisissez un ou plusieurs mots puis tapez sur "Entrer". Si vous
				avez des résultats, vous pouvez cliquer dessus pour les ajouter à la vue
				principale. Sinon essayez d'autres mots-clés. L'exploration fonctionne
				par voisins, c'est-à-dire qu'il faut cliquer sur un noeud pour voir les
				noeuds alentour qui lui sont connectés.
				<Box sx={{ fontWeight: 600, color: grey[800], mt: 2, mb: 1 }}>
					Comment lire le résultat ?
				</Box>
				Une interface de type 'graphe' vous permet d'explorer les données avec
				une vue noeud-lien. Chaque rond (appelé noeud) est une fraction
				d'information ; leur couleur permet de distinguer leur type: noir pour
				la structure, gris pour les valeurs, coloré pour les entités nommées.
				Les flèches/liens (appelées arêtes) connectent les noeuds en fonction de
				leurs relations dans les données.
			</Typography>
		),
		en: (
			<Typography>
				<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
					Goal: explore data through keyword search.
				</Box>
				The goal is to explore data through keywords, such as "Macron",
				"Pfizer", or "Borne Assemblée". In the input text on the left, type one
				or several keywords and press "Enter". If you have results, click on
				them to add them to the main layout. Else try with other keywords. Our
				keyword exploration works with neighbors, that is you need to click on a
				node to get its neighbors, so on and so forth.
				<Box sx={{ fontWeight: 600, color: grey[800], mt: 2, mb: 1 }}>
					How to read the result?
				</Box>
				A graph-based interface let you explore the data through the node-link
				paradigm. Each circle (called node) is a small piece of information ;
				their color distinguish them based on their type: black for structural
				nodes, gray for value nodes and colors for named entities. Arrows/links
				(called edges) connect nodes based on their relation in the data.
			</Typography>
		),
	},
	displayEdge: {
		en: "Always display edge labels",
		fr: "Toujours afficher le label des arêtes",
	},
	onNormalized: {
		en: "Match keywords in edges",
		fr: "Rechercher sur les arêtes aussi",
	},
	informationNode: {
		title: {
			en: "Node information",
			fr: "Information du noeud",
		},
		node: {
			en: "Node",
			fr: "Noeud",
		},
		type: {
			en: "of type",
			fr: "de type",
		},
		source: {
			en: "extracted from the file",
			fr: "extrait depuis le fichier",
		},
		id: { fr: "Identifiant interne", en: "Internal identifier" },
	},
};

function InformationNode({ focusNode, resetFocusNode }) {
	const [checked, setChecked] = React.useState(false);

	React.useEffect(() => {
		if (isDefined(focusNode)) {
			setChecked(true);
		}
	}, [focusNode]);

	const handleOnQuit = () => {
		setChecked(false);
		resetFocusNode();
	};

	const { activeLang } = useLang();

	return (
		<Collapse orientation="horizontal" in={checked} sx={{ height: "100%" }}>
			{isDefined(focusNode) ? (
				<Card sx={{ height: "100%" }}>
					<CardContent sx={{ p: 0 }}>
						<Box sx={{ backgroundColor: blue[50], p: 2, display: "flex" }}>
							<Typography
								sx={{ display: "flex", alignItems: "center", flexGrow: 1 }}
								variant="h6"
							>
								{trans.informationNode.title[activeLang]}
							</Typography>
							<IconButton
								aria-label="close"
								color="inherit"
								size="medium"
								onClick={handleOnQuit}
								onMouseDown={(event) => {
									event.stopPropagation();
								}}
							>
								<CloseIcon sx={{ cursor: "normal" }} />
							</IconButton>
						</Box>
						<Box sx={{ px: 2, pt: 2 }}>
							<Typography
								component="span"
								variant="body1"
								color="text.secondary"
							>
								{trans.informationNode.node[activeLang]}{" "}
								<Box component="span" display="inline" sx={{ fontWeight: 700 }}>
									{focusNode.label}{" "}
								</Box>
								{trans.informationNode.type[activeLang]}{" "}
								<Box component="span" display="inline" sx={{ fontWeight: 700 }}>
									{" "}
									{TYPES[focusNode.type].displayName[activeLang]}{" "}
								</Box>
								{isDefined(focusNode.dataSource.file) ? (
									<Box component="span">
										{trans.informationNode.source[activeLang]}{" "}
										<Box
											component="span"
											display="inline"
											sx={{ fontWeight: 700 }}
										>
											{focusNode.dataSource.file.split("/").slice(-1)}
										</Box>
									</Box>
								) : null}
								<Typography sx={{ mt: 2 }}>
									{trans.informationNode.id[activeLang]}: {focusNode.id}
								</Typography>
							</Typography>
						</Box>
					</CardContent>
				</Card>
			) : null}
		</Collapse>
	);
}

function Legend({ types, updateFilter }) {
	const handleOnMouseEnter = (event) => {
		const target = event.currentTarget;
		const type = target.getAttribute("id");
		updateFilter(type);
	};

	const handleOnMouseLeave = () => {
		updateFilter("");
	};

	const handleOnClick = (type) => {
		updateFilter(type);
	};
	const { activeLang } = useLang();

	const mappedTypes = [];
	if (types.length > 0) {
		const seenType = [];
		for (let type of types) {
			let displayName = TYPES[type].displayName[activeLang];
			if (!seenType.includes(displayName)) {
				mappedTypes.push({ ...TYPES[type] });
				seenType.push(displayName);
			}
		}
	}

	return (
		<Card>
			{isDefined(types) ? (
				<MenuList>
					{mappedTypes.map((type) => (
						<MenuItem key={type.typeName}>
							<Box
								sx={{ p: 0.5, display: "flex" }}
								onMouseEnter={handleOnMouseEnter}
								onMouseLeave={handleOnMouseLeave}
								onClick={handleOnClick}
								key={type.typeName}
								id={type.typeName}
							>
								<Fab
									style={{
										backgroundColor: type.color,
										width: 30,
										height: 30,
										minHeight: 0,
									}}
								></Fab>
								<Typography
									component="div"
									sx={{ alignItems: "center", display: "flex", ml: 1 }}
								>
									{type.displayName[activeLang]}
								</Typography>
							</Box>
						</MenuItem>
					))}
				</MenuList>
			) : null}
		</Card>
	);
}

function Graph({
	displayEdgeLabel,
	getFocusNode,
	getActiveTab,
	removeNode,
	graph,
}) {
	const containerRef = React.useRef();
	const [activerFilter, setActiveFilter] = React.useState("");
	const [allTypes, setAllTypes] = React.useState([]);
	const [focusNode, setFocusNode] = React.useState(null);

	function handleOnMouseOutNode(e) {
		const viewport = d3.select(".viewport");
		viewport.selectAll(".node").classed("hidden", false);
		viewport.selectAll(".link").classed("hidden", false);
		const linksLabel = viewport.selectAll(".link text");
		linksLabel.classed("show", displayEdgeLabel);
	}

	function highlight(isHighlighted, viewport) {
		const nodes = viewport.selectAll(".node");
		const links = viewport.selectAll(".link");
		nodes.classed("hidden", (node) => !isHighlighted(node));
		links.classed(
			"hidden",
			(link) =>
				!isHighlighted(link.getSource()) || !isHighlighted(link.getTarget())
		);
		const linksLabel = viewport.selectAll(".link text");
		linksLabel.classed(
			"show",
			(link) =>
				isHighlighted(link.getSource()) ||
				isHighlighted(link.getTarget() || displayEdgeLabel)
		);
	}

	function handleOnMouseOverNode(event, graph) {
		const viewport = d3.select(".viewport");
		const node = d3.select(event.currentTarget);
		const nodeData = node.data()[0];

		highlight(
			(n) => graph.isConnected(n, nodeData) || n.getId() === nodeData.getId(),
			viewport
		);
	}

	const resetFocusNode = () => {
		return setFocusNode(null);
	};

	const handleOnClickNode = (e) => {
		const node = d3.select(e.currentTarget).data()[0];
		getFocusNode(node);
		getActiveTab(1);
		setFocusNode(node);
	};

	const handleOnContextMenuNode = (e) => {
		e.preventDefault();
		const node = d3.select(e.currentTarget).data()[0];
		removeNode(node);
	};

	const updateFilter = (filter) => {
		setActiveFilter(filter);
	};

	React.useEffect(() => {
		if (isDefined(containerRef.current)) {
			const { svg, viewport } = initViewport(containerRef, true);
			var boundingBox = getBoundingBox(containerRef);

			graph.boundingBox = boundingBox;
			setAllTypes([...graph.getTypes()]);

			const { nodes, texts, links, linksLabel, groups } = drawGraph(
				viewport,
				graph
			);
			const simulation = buildSimulation(
				nodes,
				texts,
				links,
				linksLabel,
				graph,
				boundingBox,
				200,
				-100
			);
			nodes.call(drag(simulation));

			groups
				.on("click", (e) => handleOnClickNode(e))
				.on("contextmenu", (e) => handleOnContextMenuNode(e))
				.on("mouseover", (e) => handleOnMouseOverNode(e, graph))
				.on("mouseout", handleOnMouseOutNode);
		}
	}, [graph, containerRef]);

	React.useEffect(() => {
		if (isDefined(containerRef.current)) {
			const viewport = d3.select(".viewport");
			if (activerFilter !== "") {
				highlight(
					(node) => node.getType().typeName === activerFilter,
					viewport
				);
			}

			if (activerFilter === "") {
				viewport.selectAll(".node").classed("hidden", false);
				viewport.selectAll(".link").classed("hidden", false);
				viewport.selectAll(".link text").classed("show", displayEdgeLabel);
			}
		}
	}, [activerFilter, allTypes, displayEdgeLabel]);

	return (
		<Box sx={{ width: "100%", height: "100%" }}>
			<Box
				sx={{
					position: "absolute",
					right: 0,
					mr: 5,
				}}
			>
				<Box sx={{ width: "250px", ml: "auto" }}>
					<Legend types={allTypes} updateFilter={updateFilter}></Legend>
				</Box>
				<Box sx={{ mt: 5 }}>
					<InformationNode
						focusNode={focusNode}
						resetFocusNode={resetFocusNode}
					></InformationNode>
				</Box>
			</Box>
			<Container
				sx={{
					height: "100%",
					width: "100%",
					maxWidth: "100% !important",
					ml: 0,
					mr: 0,
				}}
				component="svg"
				ref={containerRef}
				id="graph-svg"
			></Container>
		</Box>
	);
}

export default function ConnectionLens() {
	const params = useParams();
	const database = params.database.replace("cl_", "");
	const { activeLang } = useLang();

	const [displayEdgeLabel, setDisplayEdgeLabel] = React.useState(false);

	// Get the cuurent tab
	const [activeTab, setActiveTab] = React.useState(-1);
	const getActiveTab = (tab) => {
		setActiveTab(tab);
	};

	// Handle query search
	const [backdropOpen, setBackdropOpen] = React.useState(false);
	const [query, setQuery] = React.useState("");
	const [parameters, setParams] = React.useState({});
	const [shouldSearchOnNormalized, setShouldSearchOnNormalized] =
		React.useState(false);

	const getParams = (params) => {
		setParams({ ...params });
	};

	const [searchResults, setSearchResults] = React.useState([]);
	const [isSearchResultsLoading, setIsSearchResultsLoading] =
		React.useState(false);

	function getQuery(query) {
		if (query !== "" && isDefined(query)) {
			setQuery(query);
			setActiveTab(0);

			sendPost("/cl/kwsearch", {
				q: query,
				p: 0,
				d: database,
				parameters: parameters,
				ond: shouldSearchOnNormalized,
			})
				.then((res) => {
					setIsSearchResultsLoading(false);
					setSearchResults(res.data);
				})
				.catch((error) => {
					setIsSearchResultsLoading(false);
				});

			setIsSearchResultsLoading(true);
			setBackdropOpen(true);
		}
	}

	React.useEffect(() => {
		if (searchResults) {
			setBackdropOpen(false);
		}
	}, [searchResults]);

	// Build the graph
	const [currentNodes, setCurrentNodes] = React.useState([]);
	const [focusNode, setFocusNode] = React.useState(null);

	const [currentGraph, setCurrentGraph] = React.useState(new D3Graph());
	const [mergeGraph, setMergeGraph] = React.useState(new D3Graph());
	const [nodeToRemove, setNodeToRemove] = React.useState(null);

	const [currentTree, setCurrentTree] = React.useState(null);

	const [incomingLinks, setIncomingLinks] = React.useState([]);

	function appendNodesToGraph(tree) {
		setCurrentTree(tree);
	}

	React.useEffect(() => {
		if (currentTree) {
			const incomingGraph = new D3Graph(currentTree, null);
			setCurrentGraph(mergeGraph);

			const newMergeGraph = D3Graph.merge([incomingGraph, mergeGraph]);
			setMergeGraph(newMergeGraph);
			setCurrentNodes([...incomingGraph.getNodes()]);
		}
	}, [currentTree]);

	React.useEffect(() => {
		if (currentNodes) {
			let response = fetcherPost("/cl/links", {
				_id: "linkid-req-" + Math.random().toString(16).slice(2),
				on: currentGraph.getNodes().map((node) => node.id),
				nn: currentNodes.map((node) => node.id),
				d: database.replace("cl_", ""),
				ond: shouldSearchOnNormalized,
			});

			response
				.then((res) => {
					setIncomingLinks(res.links);
				})
				.catch((error) => console.log(error));
		}
	}, [currentNodes]);

	React.useEffect(() => {
		if (incomingLinks && incomingLinks.length > 0) {
			const newMergeGraph = D3Graph.merge([mergeGraph]);
			newMergeGraph.addLinks(incomingLinks);
			setMergeGraph(newMergeGraph);
			console.log(incomingLinks);
		}
	}, [incomingLinks]);

	// Get the current focus node and get its neighbors
	const [isNeighborsLoading, setIsNeighborsLoading] = React.useState(false);
	const [foundNeighbors, setFoundNeighbors] = React.useState(null);
	const [neighborsError, setNeighborsError] = React.useState(false);

	const getFocusNode = (node) => {
		setFocusNode(node);
	};

	React.useEffect(() => {
		const abortController = new AbortController();

		if (focusNode) {
			sendPost(
				"/cl/neighbors",
				{
					n: focusNode && focusNode.id,
					on: mergeGraph && mergeGraph.getNodes().map((node) => node.id),
					p: 0,
					d: database,
					ond: shouldSearchOnNormalized,
				},
				{ signal: abortController.signal }
			)
				.then((res) => {
					if (abortController.signal.aborted) {
						return;
					}
					setIsNeighborsLoading(false);
					setFoundNeighbors(res.data);
				})
				.catch((error) => {
					setNeighborsError(error);
				});

			setIsNeighborsLoading(true);
		}

		return () => {
			abortController.abort();
		};
	}, [focusNode]);

	const removeNode = (nodeToRemove) => {
		setNodeToRemove(nodeToRemove);
		mergeGraph.removeNode(nodeToRemove);
		const newMergeGraph = D3Graph.from(mergeGraph);
		setMergeGraph(newMergeGraph);
	};

	// Disabled Node
	// Maintain a cache of already added node to avoid adding them multiple time
	const [addedNodesCache, setAddedNodesCache] = React.useState(new Set());

	const triggerNormalizedGraph = (isChecked) => {
		setShouldSearchOnNormalized(isChecked);
		setMergeGraph(new D3Graph());
		setFoundNeighbors(null);
		setSearchResults([]);
		setAddedNodesCache(new Set());
		setFocusNode(null);
		setCurrentNodes([]);
	};

	return (
		<Box
			sx={{
				display: "flex",
				flexDirection: "column",
				flex: "flexGrow",
				width: "100%",
				height: "100%",
			}}
		>
			<Box
				sx={{
					display: "flex",
					mb: "3vh",
					height: 60,
				}}
			>
				<SearchBar
					getInput={getQuery}
					propsSx={{ height: 60 }}
					trans={trans}
				></SearchBar>
				<HelpButton
					sx={{ position: "absolute", right: 20 }}
					helper={trans.helper[activeLang]}
				></HelpButton>
			</Box>

			<ParametersMenu
				getParams={getParams}
				parameters={SEARCH_PARAMETERS}
			></ParametersMenu>

			<Box sx={{ marginRight: "auto" }}>
				<FormControlLabel
					control={
						<Switch
							checked={displayEdgeLabel}
							onChange={(event) => {
								setDisplayEdgeLabel(event.target.checked);
							}}
						/>
					}
					label={
						<Typography sx={{ fontWeight: 450 }}>
							{trans.displayEdge[activeLang]}
						</Typography>
					}
				/>
			</Box>
			<Box>
				<FormControlLabel
					control={
						<Switch
							checked={shouldSearchOnNormalized}
							onChange={(event) => {
								triggerNormalizedGraph(event.target.checked);
							}}
						/>
					}
					label={
						<Typography sx={{ fontWeight: 450 }}>
							{trans.onNormalized[activeLang]}
						</Typography>
					}
				/>
			</Box>

			<Box
				sx={{
					display: "flex",

					height: "100%",
					maxHeight: "100%",
				}}
			>
				<SideMenu
					addedNodesCache={addedNodesCache}
					foundNeighbors={foundNeighbors}
					isNeighborsLoading={isNeighborsLoading}
					searchResults={searchResults}
					isSearchResultsLoading={isSearchResultsLoading}
					appendNodesToGraph={appendNodesToGraph}
					requestTab={activeTab}
					getActiveTab={getActiveTab}
					query={query}
					focusNode={focusNode}
				></SideMenu>

				<Graph
					graph={mergeGraph}
					getFocusNode={getFocusNode}
					getActiveTab={getActiveTab}
					removeNode={removeNode}
					displayEdgeLabel={displayEdgeLabel}
				></Graph>
			</Box>

			<Box>
				<Backdrop
					open={backdropOpen}
					sx={{
						position: "absolute",
						color: "#fff",
						"z-index": "10 !important",
					}}
				>
					<CircularProgress color="inherit" />
				</Backdrop>
			</Box>
		</Box>
	);
}
