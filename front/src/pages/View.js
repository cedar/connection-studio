import React, { Fragment } from "react";
import { useParams } from "react-router-dom";

import ArrowLeftIcon from "@mui/icons-material/ArrowLeft";
import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import CleaningServicesIcon from "@mui/icons-material/CleaningServices";
import DeleteIcon from "@mui/icons-material/Delete";
import PlayCircleFilledIcon from "@mui/icons-material/PlayCircleFilled";
import {
	Alert,
	Backdrop,
	Box,
	Chip,
	CircularProgress,
	Divider,
	FormControl,
	FormLabel,
	IconButton,
	List,
	ListItem,
	Switch,
	TextField,
	ToggleButton,
	Typography,
} from "@mui/material";
import Link from "@mui/material/Link";
import { grey } from "@mui/material/colors";
import {
	DataGrid,
	GridCellEditStopReasons,
	GridToolbarColumnsButton,
	GridToolbarContainer,
	GridToolbarDensitySelector,
	GridToolbarExport,
	GridToolbarFilterButton,
	enUS,
	frFR,
	useGridApiContext,
} from "@mui/x-data-grid";

import { ContainedButton, HelpButton } from "../components/button";
import DropDown, { SearchableDropDown } from "../components/dropdown";
import { useLang } from "../context/Lang";
import { useDataSources } from "../fetch/sources";
import {
	useContexts,
	useGetData,
	useMixedQuery,
	useUpdateValues,
} from "../fetch/view";
import { isDefined } from "../utils/utils";
import {
	getStartEndVariables,
	parseError,
	prettifyValue,
	prettyPrintStatement,
} from "../utils/view";

const trans = {
	file: {
		en: "Select a file",
		fr: "Sélectionner un fichier",
	},
	paths: {
		en: "Select a path",
		fr: "Sélectionner un chemin",
	},
	checkbox: {
		en: "Edit the query",
		fr: "Modifier la requête",
	},
	placeHolder: {
		en: "Enter an SQL query...",
		fr: "Entrez une requête SQL...",
	},
	btn: {
		search: {
			en: "Search",
			fr: "Rechercher",
		},
		update: {
			en: "Save changes",
			fr: "Sauver les modifications",
		},
	},
	editPath: {
		en: "Path",
		fr: "Chemin",
	},
	join: {
		en: "Join required",
		fr: "Jointure requise",
	},
	joinRequired: {
		en: "Required",
		fr: "Requis",
	},
	joinOptional: {
		en: "Optional",
		fr: "Optionnel",
	},
	editVarStart: {
		en: "Starting variable",
		fr: "Variable de départ",
	},
	editVarEnd: {
		en: "Ending variable",
		fr: "Variable d'arrivée",
	},
	showQuery: {
		en: "Show the query",
		fr: "Afficher la requête",
	},
	hideQuery: {
		en: "Hide the query",
		fr: "Cacher la requête",
	},
	query: {
		gen: {
			en: "Generated query",
			fr: "Requête générée",
		},
		success: {
			en: "Correct query",
			fr: "Requête correcte",
		},
		error: {
			en: "Incorrect query",
			fr: "Requête incorrecte",
		},
		eval: {
			en: "Evaluate the query",
			fr: "Évaluer la requête",
		},
	},
	noRes: {
		en: "No result found...",
		fr: "Aucun résultat trouvé...",
	},
	helper: {
		fr: (
			<Typography>
				<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
					But: nettoyer les données.
				</Box>
				Après avoir sélectionné un fichier, vous pouvez explorer les valeurs de
				celui-ci. En cliquant sur une cellule en particulier, vous pouvez
				modifier cette valeur et toutes les autres valeurs identiques seront
				ainsi modifiées.
				<br /> Quand vous avez fini vos modifications, cliquez sur "Sauver les
				modifications". Vous pouvez aussi combiner plusieurs chemins et obtenir
				le résultat sous forme tabulaire. Si vous le souhaitez, vous pouvez
				afficher la requête SQL correspondante. Vous pouvez aussi la modifier,
				après quoi vous devrez cliquer sur "Évaluer la requête" pour obtenir le
				résultat tableau.
			</Typography>
		),
		en: (
			<Typography>
				<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
					Goal: clean your data.
				</Box>
				After selecting a file, you can explore its values by clicking on the
				different paths. By clicking on a particular cell, you can modify its
				value and all identical values will be updated accordingly.
				<br /> When you are done, click on "Save modifications". You can also
				combine several paths together and obtain a tabular result. If you want
				to, you can also display the corresponding SQL query. You can modify it,
				after what you will have to click on "Evaluate the query" to obtain the
				tabular result of your modified query.
			</Typography>
		),
	},
	sliceText: {
		more: { fr: "voir plus", en: "view more" },
		less: { fr: "voir moins", en: "view less" },
	},
};

function CustomToolbar() {
	return (
		<GridToolbarContainer>
			<GridToolbarColumnsButton />
			<GridToolbarFilterButton />
			<GridToolbarDensitySelector />
			<GridToolbarExport
				csvOptions={{ fileName: `data-view-${new Date().toISOString()}` }}
			/>
		</GridToolbarContainer>
	);
}

function PathNode({ label, endOfPath, idx, onTrim }) {
	return (
		<Box
			sx={{
				display: "flex",
				".trimPath": { visibility: "hidden" },
				"&:hover .trimPath": { visibility: "visible" },
				alignItems: "center",
				justifyContent: "center",
				verticalAlign: "middle",
			}}
		>
			<Chip
				sx={{ mt: "auto" }}
				icon={
					idx > 0 ? (
						<ArrowLeftIcon
							className="trimPath"
							style={{
								cursor: "pointer",
								color: "black",
								margin: "0 -6px 0 0",
							}}
							onClick={() => onTrim(idx, "left")}
						/>
					) : null
				}
				label={label}
				onDelete={!endOfPath ? () => onTrim(idx, "right") : null}
				deleteIcon={
					!endOfPath ? (
						<ArrowRightIcon
							className="trimPath"
							style={{
								cursor: "pointer",
								color: "black",
								margin: "0 0 0 -6px",
							}}
						/>
					) : null
				}
			/>
		</Box>
	);
}

function EdgeNode({ label, endOfPath, idx, onTrim }) {
	return (
		<Box sx={{ display: "flex", flexDirection: "column" }}>
			<Typography sx={{ lineHeight: "1rem" }}> {label} </Typography>
			<Box sx={{ mt: "auto", mx: "auto", px: 1 }}>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					width="24"
					height="24"
					viewBox="0 0 24 24"
					fill="none"
					stroke="currentColor"
					strokeWidth="2"
					strokeLinecap="round"
					strokeLinejoin="round"
				>
					<path d="M18 8L22 12L18 16" />
					<path d="M2 12H22" />
				</svg>
			</Box>
		</Box>
	);
}

function Path({ idx, path, onChange }) {
	const [nodes, setNodes] = React.useState(path.split("$"));

	const onTrim = (id, direction) => {
		const newNodes = [...nodes];
		if (direction === "left") {
			setNodes(newNodes.slice(id).join("$"));
			onChange(idx, newNodes.slice(id).join("$"));
		} else {
			setNodes(newNodes.slice(0, id + 1));
			onChange(idx, newNodes.slice(0, id + 1).join("$"));
		}
	};
	return (
		<Box
			sx={{
				display: "flex",
				flexWrap: "wrap",
				maxWidth: "70%",
				mt: 0.5,
			}}
		>
			{nodes.map((node, idx) => {
				return idx % 2 === 0 ? (
					<PathNode
						idx={idx}
						key={`${node}_${idx}`}
						label={node}
						endOfPath={idx === nodes.length - 1}
						onTrim={onTrim}
					/>
				) : (
					<EdgeNode
						idx={idx}
						key={`${node}_${idx}`}
						label={node}
						endOfPath={idx === nodes.length - 1}
						onTrim={onTrim}
					/>
				);
			})}
		</Box>
	);
}

function ContextsList({
	activePaths,
	setActivePaths,
	variablesValue,
	radioValue,
	setVariablesValue,
	setRadioValue,
	setShouldExecuteQuery,
}) {
	const { activeLang } = useLang();
	const handleOnChangePath = (idx, updatedPath) => {
		activePaths[idx]["path"] = updatedPath;
		setActivePaths([...activePaths]);
		setShouldExecuteQuery(false);
	};

	const handleOnChangeVariable = (event) => {
		const idx = event.target.getAttribute("id");
		const pos = parseInt(event.target.getAttribute("pos"));
		const variable = event.target.value;
		variablesValue[idx][pos] = variable;
		setVariablesValue([...variablesValue]);
		setShouldExecuteQuery(false);
	};

	const handleChangeRadio = (event) => {
		const idx = event.target.getAttribute("id");
		radioValue[idx] = event.target.checked ? "required" : "option";
		setRadioValue([...radioValue]);
		setShouldExecuteQuery(false);
	};

	const handleOnDelete = (idx) => {
		setActivePaths((activePaths) =>
			activePaths.filter((path, id) => id !== idx)
		);

		setRadioValue((radioValue) =>
			radioValue.filter((radio, id) => id !== parseInt(idx))
		);

		setVariablesValue((variablesValue) =>
			variablesValue.filter((variables, id) => id !== parseInt(idx))
		);
		setShouldExecuteQuery(false);
	};

	const shareCommonVariables = (variablesValue, idx) => {
		let currentValues = new Set(variablesValue[idx]);

		for (let [px, pathVariables] of variablesValue.entries()) {
			if (px !== idx) {
				for (let variable of pathVariables) {
					if (currentValues.has(variable)) {
						return true;
					}
				}
			}
		}

		return false;
	};

	return (
		<Box sx={{ mt: 2 }}>
			<List>
				{activePaths.map((path, idx) => (
					<Fragment key={`${path.path}${path.file}${idx}`}>
						<Divider
							textAlign="left"
							component="li"
							sx={{ width: "70%", mt: 1.5 }}
						>
							{`${trans.editPath[activeLang]} ${idx + 1} : ${path["file"]}`}
						</Divider>
						<ListItem
							sx={{
								justifyContent: "space-between",
								p: 0,
							}}
						>
							<Path
								idx={idx}
								path={path["path"]}
								onChange={handleOnChangePath}
							/>
							<Box
								sx={{
									display: "flex",
									float: "right",
									ml: "auto",
								}}
							>
								{idx !== 0 && (
									<FormControl
										sx={{ ml: 3, display: "flex" }}
										className={`form-join-${idx}`}
									>
										<FormLabel>{trans.join[activeLang]}</FormLabel>
										<Switch
											id={`${idx}`}
											color="primary"
											value={radioValue[idx]}
											onChange={handleChangeRadio}
											sx={{ mx: "auto" }}
										/>
									</FormControl>
								)}

								{activePaths.length > 1 ? (
									<>
										<TextField
											sx={{ width: 140, ml: 2 }}
											inputProps={{ pos: 0 }}
											id={String(idx)}
											label={`${trans.editVarStart[activeLang]}`}
											defaultValue={variablesValue[idx][0]}
											onChange={handleOnChangeVariable}
										/>
										<TextField
											sx={{ width: 140, ml: 2 }}
											id={String(idx)}
											inputProps={{ pos: 1 }}
											label={`${trans.editVarEnd[activeLang]}`}
											defaultValue={variablesValue[idx][1]}
											onChange={handleOnChangeVariable}
										/>
									</>
								) : null}

								<IconButton onClick={() => handleOnDelete(idx)}>
									<DeleteIcon></DeleteIcon>
								</IconButton>
							</Box>
						</ListItem>
					</Fragment>
				))}
			</List>
		</Box>
	);
}

function ExpandableCell({ value, lang, ...params }) {
	const [expanded, setExpanded] = React.useState(false);

	const handOnChange = () => {
		setExpanded(!expanded);
		params.row[`${params.field}_expand`] =
			!params.row[`${params.field}_expand`];
	};

	React.useEffect(() => {
		const expand_state = params.row[`${params.field}_expand`];
		setExpanded(expand_state);
	}, [params.row, params.field]);

	function renderValue(value) {
		if (value) {
			return expanded ? value : value.slice(0, 200);
		}
	}

	function renderLink() {
		if (value) {
			return (
				value.length > 200 && (
					<Link
						type="button"
						component="button"
						sx={{ fontSize: "inherit" }}
						onClick={handOnChange}
					>
						{expanded ? trans.sliceText.less[lang] : trans.sliceText.more[lang]}
					</Link>
				)
			);
		}
	}

	return (
		<div>
			{renderValue(value)}&nbsp;
			{renderLink()}
		</div>
	);
}

function EditCell(props) {
	const { id, value, field, hasFocus } = props;
	const apiRef = useGridApiContext();
	const ref = React.useRef();

	React.useLayoutEffect(() => {
		if (hasFocus) {
			ref.current.focus();
		}
	}, [hasFocus]);

	const handleValueChange = (event) => {
		const newValue = event.target.value; // The new value entered by the user
		apiRef.current.setEditCellValue({ id, field, value: newValue });
	};

	return (
		<TextField
			multiline
			sx={{ border: "none", width: "100%", height: "100%" }}
			ref={ref}
			id="outlined-basic"
			variant="standard"
			value={value}
			onChange={handleValueChange}
			InputProps={{
				disableUnderline: true,
			}}
		/>
	);
}
function DataView({
	database,
	contexts,
	activeSource,
	statement,
	shouldExecuteQuery,
	shouldUpdateValues,
	setDataError,
}) {
	const { activeLang } = useLang();

	const [activeRows, setActiveRows] = React.useState({});
	const [displayRows, setDisplayRows] = React.useState([]);
	const [columnData, setColumnData] = React.useState([]);
	const [IdToField, setIdToField] = React.useState({});
	const [fieldIdToValueIdToRowId, setfieldIdToValueIdToRowId] = React.useState(
		{}
	);
	const [noResutFound, setNoResutFound] = React.useState(false);
	const [backdropOpen, setBackdropOpen] = React.useState(false);

	const cleanStatement = (statement) => {
		return statement
			.replaceAll("\n", " ")
			.replaceAll("\t", " ")
			.replace(/\s+/g, " ")
			.trim();
	};

	const { dataRows, isDataRowsError, isDataRowsLoading } = useGetData(
		database,
		contexts,
		statement ? cleanStatement(statement) : "",
		activeSource ? activeSource.type : null,
		shouldExecuteQuery
	);

	React.useEffect(() => {
		if (isDataRowsLoading) {
			setBackdropOpen(true);
		}
	}, [isDataRowsLoading]);

	React.useEffect(() => {
		if (isDefined(dataRows) && dataRows) {
			var parseData;
			if (typeof dataRows == "string") {
				// NB July 6th, 2023:
				// in the data view, we show the normalized labels (column "normalabel"), thus:
				// - labels cannot contain " (double quotes) because they have been removed - thus the JSON cannot break
				// - but they can contain \ (backslash) which break JSON - thus we remove them
				let stringData = dataRows;
				stringData = stringData.replaceAll("\n", " ");
				stringData = stringData.replaceAll("\r", " ");
				stringData = stringData.replaceAll("\t", " ");
				stringData = stringData.replaceAll("\\", " ");
				parseData = JSON.parse(stringData);
			} else {
				parseData = dataRows;
				// setDataError(null);
			}

			const { columns, rows } = parseData;

			var newColumnData = [];
			var IdToField = {};
			for (let column of columns) {
				var frontColumn = {
					...column,
					headerName: column.field,
					width: 150,
					editable: column.editable === "false" ? false : true,
					flex: 1,
					renderCell: (params) => (
						<ExpandableCell {...params} lang={activeLang} />
					),
					renderEditCell: (params) => <EditCell {...params} />,
				};

				newColumnData.push(frontColumn);
				IdToField[frontColumn.id] = frontColumn.field;
			}

			setColumnData(newColumnData);
			setActiveRows(rows);
			setIdToField(IdToField);
			setBackdropOpen(false);
			setDataError(null);
			if (Object.keys(rows).length === 0) {
				setNoResutFound(true);
			} else {
				setNoResutFound(false);
			}
		}
	}, [dataRows, setDataError, activeLang]);

	React.useEffect(() => {
		const fieldToId = Object.fromEntries(
			Object.entries(IdToField).map(([key, value]) => [value, key])
		);
		var fieldIdToValueIdToRowId = {};
		var fieldIdToValueToId = {};
		var newDisplayRows = [];

		Object.entries(activeRows).forEach((entry) => {
			var [id, row] = entry;
			row["id"] = parseInt(id);
			row["fieldToValue"] = {};

			for (let field of Object.keys(fieldToId)) {
				const value = row[field];

				const fieldExpandState = `${field}_expand`;
				if (!(fieldExpandState in row)) {
					row[`${field}_expand`] = false;
				}

				const fieldId = fieldToId[field];
				if (!(fieldId in fieldIdToValueIdToRowId)) {
					fieldIdToValueIdToRowId[fieldId] = {};
				}

				if (!(fieldId in fieldIdToValueToId)) {
					fieldIdToValueToId[fieldId] = {};
				}

				if (!(value in fieldIdToValueToId[fieldId])) {
					fieldIdToValueToId[fieldId][value] = Object.keys(
						fieldIdToValueToId[fieldId]
					).length;
				}

				const valueId = fieldIdToValueToId[fieldId][value];
				if (!(valueId in fieldIdToValueIdToRowId[fieldId])) {
					fieldIdToValueIdToRowId[fieldId][valueId] = [];
				}

				fieldIdToValueIdToRowId[fieldId][valueId].push(row["id"]);
				row["fieldToValue"][fieldId] = valueId;
			}

			newDisplayRows.push(row);
		});

		setDisplayRows(newDisplayRows);
		setfieldIdToValueIdToRowId(fieldIdToValueIdToRowId);
	}, [activeRows, IdToField]);

	React.useEffect(() => {
		if (isDataRowsError) {
			setBackdropOpen(false);
			var error = parseError(isDataRowsError);
			setDataError(error);
		}
	}, [isDataRowsError, setDataError]);

	const [valuesToUpdate, setValuesToUpdate] = React.useState({});

	const { updated } = useUpdateValues(
		shouldUpdateValues,
		database,
		contexts,
		valuesToUpdate,
		activeSource.type
	);

	// FIXME:
	// Will be used to set a message to the user to tell him if update is successfull or not
	React.useEffect(() => {
		if (updated) {
		}
	});

	return (
		<Box>
			{Object.keys(activeRows).length > 0 ? (
				<DataGrid
					sx={{
						overflow: "hidden",
						"&.MuiDataGrid-root--densityCompact .MuiDataGrid-cell": {
							py: "8px",
						},
						"&.MuiDataGrid-root--densityStandard .MuiDataGrid-cell": {
							py: "15px",
						},
						"&.MuiDataGrid-root--densityComfortable .MuiDataGrid-cell": {
							py: "22px",
						},
						mt: 2,
					}}
					// rowsHeight={100}
					getRowHeight={() => "auto"}
					getEstimatedRowHeight={() => 200}
					rows={displayRows}
					autoHeight={true}
					slots={{
						toolbar: CustomToolbar,
					}}
					columns={columnData}
					initialState={{
						pagination: {
							paginationModel: {
								pageSize: 20,
							},
						},
					}}
					localeText={
						activeLang === "fr"
							? frFR.components.MuiDataGrid.defaultProps.localeText
							: enUS.components.MuiDataGrid.defaultProps.localeText
					}
					pageSizeOptions={[10, 20, 50]}
					disableRowSelectionOnClick
					onCellEditStop={(params, event) => {
						const fieldId = params.colDef.id;
						const valueId = params.row.fieldToValue[fieldId];
						const rowsToUpdate = fieldIdToValueIdToRowId[fieldId][valueId];

						var updatedRows = { ...activeRows };

						if (event.target) {
							rowsToUpdate.forEach((id) => {
								var updatedRow = { ...activeRows[id] };

								// Keep the cell expanded after stopping edit it
								const editedField = IdToField[fieldId];

								updatedRows[id] = updatedRow;

								// Update the new value of the cell

								const updatedValue = event.target.value;
								updatedRow[editedField] = updatedValue;
								updatedRow[`${editedField}_expand`] = true;

								let oldValue = activeRows[id][editedField];
								if (!(id in valuesToUpdate)) {
									valuesToUpdate[id] = {};
								}
								if (!(fieldId in valuesToUpdate[id])) {
									valuesToUpdate[id][fieldId] = {
										newValue: updatedValue,
										oldValue: oldValue,
									};
								} else {
									valuesToUpdate[id][fieldId] = {
										newValue: updatedValue,
										oldValue: valuesToUpdate[id][fieldId].oldValue,
									};
								}
							});
							setActiveRows(updatedRows);
							setValuesToUpdate({ ...valuesToUpdate });

							if (params.reason === GridCellEditStopReasons.cellFocusOut) {
								event.defaultMuiPrevented = true;
							}
						}
					}}
				/>
			) : null}
			{noResutFound ? trans.noRes[activeLang] : null}
			<Backdrop
				open={backdropOpen}
				sx={{ color: "#fff", "z-index": "10 !important" }}
			>
				<CircularProgress color="inherit" />
			</Backdrop>
		</Box>
	);
}

export default function View() {
	const database = useParams().database;
	const [activeSource, setActiveSource] = React.useState({});
	const [shouldFetchContexts, setShouldFetchContexts] = React.useState(false);
	const [paths, setPaths] = React.useState([]);

	const { extractedContexts } = useContexts(shouldFetchContexts, database, [
		activeSource.id,
	]);

	React.useEffect(() => {
		if (isDefined(extractedContexts)) {
			setPaths(extractedContexts);
		}
	}, [extractedContexts]);

	const [sources, setSources] = React.useState([]);
	const { dataSources } = useDataSources(true, database);

	React.useEffect(() => {
		const getSourceName = (source) => {
			let path = source.file;
			let parsedPath = path.split("/");
			let fileName = parsedPath[parsedPath.length - 1];
			return fileName;
		};

		if (isDefined(dataSources)) {
			var sources = [];
			for (let source of dataSources) {
				let fileName = getSourceName(source);
				if (["XML", "CSV", "JSON", "RDF"].includes(source.type))
					sources.push({ name: fileName, id: source.id, type: source.type });
			}
			setSources(sources);
		}
	}, [dataSources]);

	const getCurrentSources = (sourceName) => {
		var activeSource = {};
		if (isDefined(sources)) {
			for (let source of sources) {
				if (source.name === sourceName) {
					activeSource = source;
				}
			}
		}
		setActiveSource(activeSource);
		setShouldFetchContexts(true);
	};

	const [radioValue, setRadioValue] = React.useState([]);
	// const [path2Variable, setPath2Variable] = React.useState({});
	const [variablesValue, setVariablesValue] = React.useState([]);
	const [contexts, setContexts] = React.useState([]);

	const [activePaths, setActivePaths] = React.useState([]);
	const [shouldFetchStatement, setShouldFetchStatement] = React.useState(false);
	const [displayStatement, setDisplayStatement] = React.useState(false);
	const [statement, setStatement] = React.useState("");

	const getChoice = (path) => {
		setActivePaths([
			...activePaths,
			{
				path: path,
				source: activeSource.type,
				file: activeSource.name,
			},
		]);

		radioValue.push("option");
		setRadioValue([...radioValue]);
		const currentIdx = variablesValue.length > 0 ? variablesValue.length : -1;
		let currentVariables = [`var${currentIdx + 1}`, `var${currentIdx + 2}`];

		// setPath2Variable({ ...path2Variable });
		variablesValue.push(currentVariables);
		setVariablesValue([...variablesValue]);
		setShouldExecuteQuery(false);
	};

	const [dataError, setDataError] = React.useState(null);

	React.useEffect(() => {
		if (activePaths) {
			const mapJoin = { required: "join", option: "outerjoin" };
			const uniqueVars = new Set();

			for (let path of activePaths) {
				const variables = getStartEndVariables(path["path"]);

				for (let variable of variables) {
					uniqueVars.add(variable);
				}
			}
			var var2char = {};
			let idx = 0;
			for (let uniqueVar of uniqueVars) {
				var2char[uniqueVar] = `var${idx}`;
				idx++;
			}

			let newContexts = [];
			let isOneVariableEmpty = false;
			for (let i = 0; i < activePaths.length; i++) {
				var currentPath = activePaths[i]["path"];
				var joinValue = radioValue[i];

				newContexts.push(currentPath); // add context path
				newContexts.push(mapJoin[joinValue]); // add join predicate

				for (let variable of variablesValue[i]) {
					// add join variables
					newContexts.push(variable);

					if (!variable) {
						isOneVariableEmpty = true;
					}
				}

				let useSimpleQuery = false;
				if (useSimpleQuery) {
				} else {
					newContexts.push(activePaths[i]["source"]); // add datasource type
				}
			}
			setContexts(newContexts);
			if (newContexts.length > 0) {
				// if one variable is empty we shouldn't fetch a new stmnt
				if (!isOneVariableEmpty) {
					setShouldFetchStatement(true);
				} else {
					setShouldFetchStatement(false);
				}
			}
		}
	}, [activePaths, radioValue, variablesValue]);

	const { inStatement, isStatementError } = useMixedQuery(
		database,
		contexts,
		activeSource,
		shouldFetchStatement
	);

	React.useEffect(() => {
		if (isDefined(inStatement) && inStatement) {
			setStatement(prettyPrintStatement(inStatement["statement"]));
			setDataError(null);
		} else {
		}
	}, [inStatement]);

	React.useEffect(() => {
		if (isStatementError) {
			var error = parseError(isStatementError);
			setDataError(error);
		}
	}, [isStatementError]);

	const [shouldExecuteQuery, setShouldExecuteQuery] = React.useState(false);

	const handleOnSearch = () => {
		setStatement(prettyPrintStatement(statement));
		setShouldExecuteQuery(true);
	};

	const handleDisplayStatement = () => {
		setDisplayStatement(!displayStatement);
	};

	const handleChangeStatement = (event) => {
		setStatement(event.currentTarget.value);
	};

	const [shouldUpdateValues, setShouldUpdateValues] = React.useState(false);

	const handleOnUpdate = () => {
		setShouldUpdateValues(true);
	};

	const { activeLang } = useLang();

	return (
		<Box>
			<HelpButton
				sx={{ position: "absolute", right: 20 }}
				helper={trans.helper[activeLang]}
			></HelpButton>

			<Box sx={{ mb: 3 }}>
				<DropDown
					inputLabel={trans.file[activeLang]}
					items={sources.map((source) => source.name)}
					getChoice={getCurrentSources}
					width={600}
				></DropDown>
			</Box>

			<Box sx={{ display: "flex" }}>
				<SearchableDropDown
					items={paths}
					inputLabel={trans.paths[activeLang]}
					sx={{ width: "80%", mr: 3 }}
					getChoice={getChoice}
					parserFunction={prettifyValue}
				></SearchableDropDown>
				<Typography sx={{ flexGrow: 1 }}></Typography>
				<ToggleButton
					sx={{ mr: 3, height: 60 }}
					color="primary"
					value="displayStatement"
					selected={displayStatement}
					onChange={handleDisplayStatement}
				>
					<Typography sx={{ textTransform: "initial" }}>
						{displayStatement
							? trans.hideQuery[activeLang]
							: trans.showQuery[activeLang]}
					</Typography>
				</ToggleButton>
				<ContainedButton
					sx={{
						width: "20%",
						height: 60,
						p: 2,
					}}
					onClick={handleOnSearch}
					onMouseDown={(event) => {
						event.stopPropagation();
					}}
				>
					<PlayCircleFilledIcon sx={{ mr: 1 }}></PlayCircleFilledIcon>
					{trans.query.eval[activeLang]}
				</ContainedButton>
				<Box sx={{ height: "100%", ml: 3 }}>
					<ContainedButton
						sx={{
							width: "100%",
							height: 60,
							p: 2,
						}}
						onClick={handleOnUpdate}
						onMouseDown={(event) => {
							event.stopPropagation();
						}}
					>
						<CleaningServicesIcon sx={{ mr: 1 }}></CleaningServicesIcon>
						{trans.btn.update[activeLang]}
					</ContainedButton>
				</Box>
			</Box>

			<ContextsList
				activePaths={activePaths}
				setActivePaths={setActivePaths}
				variablesValue={variablesValue}
				radioValue={radioValue}
				setVariablesValue={setVariablesValue}
				setRadioValue={setRadioValue}
				setShouldExecuteQuery={setShouldExecuteQuery}
			></ContextsList>

			{dataError ? (
				<Alert severity="error">{`${
					trans.query.error[activeLang]
				} ${dataError.message.split("PSQLException").slice(-1)}`}</Alert>
			) : null}

			{displayStatement && statement ? (
				<Box sx={{ display: "flex", flexDirection: "column" }}>
					{dataError ? (
						<TextField
							error
							multiline
							label={trans.query.gen[activeLang]}
							value={statement.replace("\\\n", "\n")}
							onChange={handleChangeStatement}
							sx={{ flexGrow: 1 }}
							helperText={`${trans.query.error[activeLang]} ${dataError.message
								.split("PSQLException")
								.slice(-1)}`}
						/>
					) : (
						<TextField
							multiline
							label={trans.query.gen[activeLang]}
							value={statement}
							onChange={handleChangeStatement}
							sx={{ flexGrow: 1 }}
							helperText={trans.query.success[activeLang]}
						/>
					)}
				</Box>
			) : null}

			<DataView
				database={database}
				contexts={contexts}
				activeSource={activeSource}
				statement={statement}
				shouldExecuteQuery={shouldExecuteQuery}
				shouldUpdateValues={shouldUpdateValues}
				setDataError={setDataError}
			></DataView>
		</Box>
	);
}
