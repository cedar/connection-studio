import * as React from "react";
import { useParams } from "react-router-dom";

import AddCircleIcon from "@mui/icons-material/AddCircle";
import DeleteIcon from "@mui/icons-material/Delete";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import {
	Button,
	Dialog,
	DialogTitle,
	DialogContent,
	DialogActions,
	IconButton,
	Paper,
	TableRow,
	TableBody,
	TableCell,
	TableContainer,
	Table,
	TableHead,
	Backdrop,
	CircularProgress,
	List,
	LinearProgress,
} from "@mui/material";
import { Typography } from "@mui/material";
import { blue } from "@mui/material/colors";
import { Box } from "@mui/system";
import { useSource } from "context/Sources";
import PQueue from "p-queue";

import {
	ContainedButton,
	HelpButton,
	OutlinedButton,
} from "../components/button";
import ParametersMenu from "../components/parameter";
import { FileDropzone, FileItem } from "../components/uploadFile";
import { useLang } from "../context/Lang";
import { fetchFile, fetcherGet } from "../fetch/fetcher";
import { useDeleteSource, useSnippet } from "../fetch/sources";
import { PARAMETERS } from "../utils/parameters";
import { isDefined } from "../utils/utils";

const trans = {
	title: {
		en: "Uploaded files",
		fr: "Fichiers chargés",
	},
	lang: {
		title: {
			fr: "Langue du fichier",
			en: "File language",
		},
		options: {
			en: ["French", "English"],
			fr: ["Français", "Anglais"],
		},
		codeMap: { French: "fr", English: "en", Français: "fr", Anglais: "en" },
	},
	extract: {
		title: {
			fr: "Modèle d'extraction",
			en: "Extraction model",
		},
		options: {
			en: [
				"ChatGPT 4 extractor",
				"Stanford Extractor",
				"Flair Extractor",
				"None",
			],
			fr: [
				"Extracteur ChatGPT 4",
				"Extracteur Stanford",
				"Extracteur Flair",
				"Aucun",
			],
		},
		codeMap: {
			"Stanford Extractor": "SNER",
			"Flair Extractor": "FLAIR_NER",
			None: "NONE",
			"Extracteur Stanford": "SNER",
			"Extracteur Flair": "FLAIR_NER",
			Aucun: "NONE",
			"Extracteur ChatGPT 4": "GPT_NER",
			"ChatGPT 4 extractor": "GPT_NER",
		},
	},
	dateExtract: { fr: "Extraire les dates", en: "Extract dates" },
	paramsTitle: {
		fr: "Afficher les options avancées",
		en: "Display advanced options",
	},
	helper: {
		fr: (
			<Typography>
				Ajoutez quelques sources de données dans votre projet en cliquant sur le
				bouton "Ajouter". Pensez à sélectionner la langue de vos fichiers en
				déroulant les fonctionnalités avancées.
			</Typography>
		),
		en: (
			<Typography>
				Add some data sources in your project using the button "Add". Do not
				forget to select the language of your file(s) by scrolling down the
				"advanced features" button.
			</Typography>
		),
	},
	subtitle: {
		en: "Add one or more files",
		fr: "Ajouter un ou plusieurs fichiers",
	},
	p1: {
		en: "Upload your files here or click to browse your folders",
		fr: "Déposez vos fichiers ici ou cliquez pour parcourir vos dossiers",
	},
	list: {
		en: "My Files",
		fr: "Mes fichiers",
	},
	url: {
		en: "URL (Optional)",
		fr: "URL (Optionnel)",
	},
	dialogTitle: {
		end: {
			en: "Loading complete!",
			fr: "Chargement terminé !",
		},
		start: {
			en: "Add files",
			fr: "Ajouter des fichiers",
		},
	},
	dialogActions: {
		quit: {
			en: "Close",
			fr: "Fermer",
		},
		cancel: {
			en: "Cancel",
			fr: "Annuler",
		},
		add: {
			en: "Add",
			fr: "Ajouter",
		},
	},
	dialogTitleSnippet: {
		en: "File",
		fr: "Fichier",
	},
	file: { en: "File", fr: "Fichier" },
	path: { en: "Path", fr: "Chemin" },
	create: { en: "Creation date", fr: "Date de création" },
};

function FilesList({
	files,
	onDeleteFile,
	onUpdateFile,
	filesUploadReport,
	queueIsLoading,
	queueIsFinish,
}) {
	const { activeLang } = useLang();

	return (
		<List>
			{Object.keys(files).length > 0 ? (
				<Typography variant="h5" sx={{ mt: 3 }}>
					{trans.list[activeLang]}
				</Typography>
			) : null}
			{Object.keys(files).map((file, id) => (
				<FileItem
					key={file}
					file={files[file]["file"]}
					uploadStatus={
						filesUploadReport[file] && filesUploadReport[file].status
					}
					uploadMsg={filesUploadReport[file] && filesUploadReport[file].msg}
					queueIsLoading={queueIsLoading}
					queueIsFinish={queueIsFinish}
					onUpdateFile={onUpdateFile}
					onDeleteFile={onDeleteFile}
				></FileItem>
			))}
		</List>
	);
}

export function AddSourceButton({ isAddedSource }) {
	const { activeLang } = useLang();
	const database = useParams().database;

	const [open, setOpen] = React.useState(false);

	const [date, setDate] = React.useState(0);
	const [formSize, setFormSize] = React.useState(0);
	const [filesForm, setFiles] = React.useState({});

	const onDrop = (acceptedFiles) => {
		let currentFilesForm = {};
		acceptedFiles.forEach((file) => {
			currentFilesForm[file.name] = {
				file: file,
				url: "",
				database: database,
			};
		});
		let finalFilesForm = { ...currentFilesForm, ...filesForm };
		setFiles(finalFilesForm);
		setFormSize(Object.keys(finalFilesForm).length);
	};

	const onDeleteFile = (fileName) => {
		let currentFiles = { ...filesForm };
		delete currentFiles[fileName];
		setFiles({ ...currentFiles });
	};
	const onUpdateFile = (fileName, url) => {
		var currentFilesForm = { ...filesForm };
		currentFilesForm[fileName]["url"] = url;
		setFiles(currentFilesForm);
	};

	const queue = new PQueue({ concurrency: 1 });
	const [queueIsFinish, setQueueIsFinish] = React.useState(false);
	const [queueIsLoading, setQueueIsLoading] = React.useState(false);
	const [filesUploadReport, setFilesUploadReport] = React.useState({});

	const handleOnAdd = () => {
		setQueueIsLoading(true);
		setQueueIsFinish(false);
		var currentStatus = { ...filesUploadReport };
		for (let file of Object.values(filesForm)) {
			console.log(file);
			queue
				.add(() => fetchFile("/cl/import", file))
				.then((res) => {
					let fileName = file.file.name;
					currentStatus[fileName] = { status: parseInt(res), msg: null };
					setFilesUploadReport(currentStatus);
				})
				.catch((err) => {
					let msg;
					try {
						msg = JSON.parse(err.response).message;
					} catch (error) {
						msg = "An error has been catch bu the JSON can't be parsed";
					}
					let fileName = file.file.name;
					currentStatus[fileName] = {
						status: err.status,
						msg: msg,
					};
					setFilesUploadReport(currentStatus);
				});
		}

		queue.on("idle", () => {
			setQueueIsLoading(false);
			setQueueIsFinish(true);
		});
	};

	const handleOnOpen = () => {
		setOpen(true);
		const now = new Date().getTime();
		setDate(now);
	};

	const handleOnClose = () => {
		setOpen(false);
		setQueueIsFinish(false);
		setQueueIsLoading(false);
		setFiles({});
		setFilesUploadReport({});
		isAddedSource();
	};

	return (
		<Box key={date}>
			<Box sx={{ height: "100%" }}>
				<Dialog
					onMouseDown={(event) => {
						event.stopPropagation();
					}}
					open={open}
					onClose={!queueIsLoading && handleOnClose}
					fullWidth
					maxWidth="sm"
					transitionDuration={{ exit: 0 }}
					PaperProps={{
						style: { borderRadius: 16 },
					}}
				>
					<DialogTitle variant="h4">
						{queueIsFinish
							? trans.dialogTitle.end[activeLang]
							: trans.dialogTitle.start[activeLang]}
					</DialogTitle>
					<DialogContent sx={{ overflow: "unset !important" }}>
						<FileDropzone onDrop={onDrop}></FileDropzone>
						<FilesList
							files={filesForm}
							onDeleteFile={onDeleteFile}
							onUpdateFile={onUpdateFile}
							filesUploadReport={filesUploadReport}
							queueIsLoading={queueIsLoading}
							queueIsFinish={queueIsFinish}
						></FilesList>

						{queueIsLoading && <LinearProgress sx={{ mt: 4 }} />}
					</DialogContent>
					<DialogActions sx={{ p: 2 }}>
						{!queueIsLoading && queueIsFinish && (
							<Button onClick={handleOnClose}>
								{trans.dialogActions.quit[activeLang]}
							</Button>
						)}
						{!queueIsLoading && !queueIsFinish && (
							<OutlinedButton onClick={handleOnClose}>
								{trans.dialogActions.cancel[activeLang]}
							</OutlinedButton>
						)}
						{!queueIsLoading && !queueIsFinish && formSize > 0 && (
							<ContainedButton onClick={handleOnAdd}>
								{trans.dialogActions.add[activeLang]}
							</ContainedButton>
						)}
					</DialogActions>
				</Dialog>
				<ContainedButton
					sx={{
						width: "100%",
						height: 40,
						p: 2,
					}}
					onClick={handleOnOpen}
					onMouseDown={(event) => {
						event.stopPropagation();
					}}
				>
					<AddCircleIcon sx={{ mr: 1 }}></AddCircleIcon>
					{trans.dialogActions.add[activeLang]}
				</ContainedButton>
			</Box>
		</Box>
	);
}

function SourceSnippet({
	shouldOpen,
	database,
	sourceID,
	sourceName,
	handleOnCloseSnippet,
}) {
	const { activeLang } = useLang();
	const [open, setOpen] = React.useState(false);
	const [shouldFetchSnippet, setShouldFetchSnippet] = React.useState(false);

	React.useEffect(() => {
		if (shouldOpen) {
			setOpen(shouldOpen);
			setShouldFetchSnippet(true);
		}
	}, [shouldOpen]);

	const handleOnClose = () => {
		handleOnCloseSnippet();
		setOpen(false);
	};

	const { snippet } = useSnippet(shouldFetchSnippet, database, sourceID);

	const parseSnippet = (sourceName, snippet) => {
		if (isDefined(snippet) && isDefined(sourceName)) {
			let format = sourceName.split(".").slice(-1)[0];
			switch (format) {
				case "json":
					return <pre>{snippet.join(" ").replaceAll("\t", "")}</pre>;

				case "csv":
					return (
						<pre>
							{snippet.map((line) => line.split(",").join("\t")).join("\n")}
						</pre>
					);

				default:
					return <pre>{snippet.join("\n")}</pre>;
			}
		}
		return null;
	};

	return (
		<Dialog
			open={open}
			onClose={handleOnClose}
			fullWidth
			maxWidth="lg"
			PaperProps={{
				style: { borderRadius: 16 },
			}}
		>
			<DialogTitle variant="h4" sx={{ p: 2 }}>
				{trans.dialogTitleSnippet[activeLang]} {sourceName}
			</DialogTitle>
			<DialogContent sx={{ overflowX: "auto" }}>
				{parseSnippet(sourceName, snippet)}
			</DialogContent>
			<DialogActions sx={{ p: 2 }}>
				<OutlinedButton onClick={handleOnClose}>Fermer</OutlinedButton>
			</DialogActions>
		</Dialog>
	);
}

const SourceRow = ({ source, database, isDeletedSource }) => {
	const [shouldDeleteSource, setShouldDeleteSource] = React.useState(false);
	const [shouldOpenSnippet, setShouldOpenSnippet] = React.useState(false);

	const { deleteSource } = useDeleteSource(
		shouldDeleteSource,
		database,
		source.id
	);

	React.useEffect(() => {
		if (deleteSource === 200) {
			isDeletedSource();
		}
	}, [deleteSource, isDeletedSource]);

	const handleOnDeleteSource = (e) => {
		setShouldDeleteSource(true);
	};

	const handleOnClickSnippet = (e) => {
		setShouldOpenSnippet(true);
	};
	const handleOnCloseSnippet = () => {
		setShouldOpenSnippet(false);
	};

	const getSourceName = (source) => {
		return source.file.split("/").slice(-1)[0];
	};

	return (
		<TableRow
			key={source.file}
			sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
		>
			<TableCell component="th" scope="row">
				<Typography variant="body2">{source.id}</Typography>
			</TableCell>
			<TableCell component="th" scope="row">
				<Typography variant="body2" color={blue[700]}>
					{getSourceName(source)}
				</Typography>
			</TableCell>
			<TableCell align="left">
				{source.file.split("/").slice(0, -1).join("/")}
			</TableCell>
			<TableCell align="left">{source.type}</TableCell>
			<TableCell align="left">{source.creation_date}</TableCell>
			<TableCell>
				<IconButton onClick={handleOnClickSnippet}>
					<RemoveRedEyeIcon></RemoveRedEyeIcon>
				</IconButton>
				<IconButton onClick={handleOnDeleteSource}>
					<DeleteIcon></DeleteIcon>
				</IconButton>
			</TableCell>
			<SourceSnippet
				shouldOpen={shouldOpenSnippet}
				database={database}
				sourceID={source.id}
				sourceName={getSourceName(source)}
				handleOnCloseSnippet={handleOnCloseSnippet}
			></SourceSnippet>
		</TableRow>
	);
};

const SourcesTable = ({ dataSources, database, isDeletedSource }) => {
	const { activeLang } = useLang();

	return (
		<Box>
			{isDefined(dataSources) ? (
				<TableContainer component={Paper} elevation={0}>
					<Table sx={{ minWidth: 450 }} aria-label="simple table">
						<TableHead>
							<TableRow>
								<TableCell>
									<Typography sx={{ fontWeight: 700 }}>ID</Typography>
								</TableCell>
								<TableCell>
									<Typography sx={{ fontWeight: 700 }}>
										{trans.file[activeLang]}
									</Typography>
								</TableCell>
								<TableCell align="left">
									<Typography sx={{ fontWeight: 700 }}>
										{trans.path[activeLang]}
									</Typography>
								</TableCell>
								<TableCell align="left">
									<Typography sx={{ fontWeight: 700 }}>Type</Typography>
								</TableCell>
								<TableCell align="left">
									<Typography sx={{ fontWeight: 700 }}>
										{trans.create[activeLang]}
									</Typography>
								</TableCell>
								<TableCell align="left"></TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{dataSources.map((source) => (
								<SourceRow
									source={source}
									database={database}
									key={source.id}
									isDeletedSource={isDeletedSource}
								></SourceRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			) : null}
		</Box>
	);
};

export default function Sources() {
	const params = useParams();
	const { activeLang } = useLang();

	const database = params.database;
	const [backdropOpen, setBackdropOpen] = React.useState(false);

	const [dataSources, setDataSources] = React.useState([]);
	const [isDataSourcesLoading, setIsDataSourcesLoading] = React.useState(false);
	const { setPersistentSourcesLength } = useSource();

	const getDataSources = () => {
		setIsDataSourcesLoading(true);
		fetcherGet("/cl/datasources", {
			d: database.replace("cl_", ""),
		})
			.then((res) => {
				console.log(res);
				setDataSources(res);
				setIsDataSourcesLoading(false);
				setPersistentSourcesLength(res.length);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	React.useEffect(() => {
		getDataSources();
	}, []);

	function isAddedSource() {
		getDataSources();
	}

	function isDeletedSource() {
		getDataSources();
	}

	React.useEffect(() => {
		if (isDataSourcesLoading) {
			setBackdropOpen(true);
		}
		if (!isDataSourcesLoading) {
			setBackdropOpen(false);
		}
	}, [isDataSourcesLoading]);

	return (
		<Box>
			<Box sx={{ display: "flex", mb: 2 }}>
				<Typography sx={{ flexGrow: 1 }} variant="h4">
					{trans.title[activeLang]}
				</Typography>
				<AddSourceButton isAddedSource={isAddedSource}></AddSourceButton>
				<HelpButton
					sx={{ ml: 2 }}
					helper={trans.helper[activeLang]}
				></HelpButton>
			</Box>

			<ParametersMenu parameters={PARAMETERS}></ParametersMenu>

			<SourcesTable
				dataSources={
					isDataSourcesLoading || !isDefined(dataSources) ? [] : dataSources
				}
				database={database}
				isDeletedSource={isDeletedSource}
			></SourcesTable>
			<Backdrop
				open={backdropOpen}
				sx={{ color: "#fff", "z-index": "10 !important" }}
			>
				<CircularProgress color="inherit" />
			</Backdrop>
		</Box>
	);
}
