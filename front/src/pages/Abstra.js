import * as React from "react";
import { useParams } from "react-router-dom";

import CloseIcon from "@mui/icons-material/Close";
import HistoryIcon from "@mui/icons-material/History";
import LoadingButton from "@mui/lab/LoadingButton";
import {
	Box,
	Button,
	Dialog,
	DialogContent,
	DialogTitle,
	IconButton,
	TextField,
	Typography,
} from "@mui/material";
import {
	DataGrid,
	GridToolbarColumnsButton,
	GridToolbarContainer,
	GridToolbarDensitySelector,
	GridToolbarExport,
	GridToolbarFilterButton,
	enUS,
	frFR,
} from "@mui/x-data-grid";
import { trans } from "components/abstra/translation";
import FileSideMenu from "components/atomic/FileSideMenu";
import ResizableSizeMenu from "components/menu/resizableSizeMenu";

import AbstraHistory from "../components/abstra/abstraHistory";
import EntityRelationSchema from "../components/abstra/entityRelationSchema";
import { HelpButton } from "../components/button";
import { EntitySelectionContext } from "../context/EntitySelection";
import { useLang } from "../context/Lang";
import { useAlert } from "../context/SnackBarAlert";
import { useGetAbstraExecutions, useReadAbstraction } from "../fetch/abstra";
import { fetcherPost } from "../fetch/fetcher";
import { useDataSources } from "../fetch/sources";
import { getLabelPathToEntity } from "../utils/entitySelection";

export default function Abstra() {
	const params = useParams();
	const database = params.database;

	const { setAlert } = useAlert();
	const [activeItem, setActiveItem] = React.useState();
	const [executionId, setExecutionId] = React.useState();
	const [numberEntities, setNumberEntities] = React.useState(6);
	const [abstraction, setAbstraction] = React.useState();

	const [openHistory, SetOpenHistory] = React.useState(false);
	const [isComputing, setIsComputing] = React.useState(false);

	// The ids of entities that who's attributes are currently selectable
	const [activeSelectionEntities, setActiveSelectionEntities] = React.useState(
		[]
	);
	// Represents if the data extraction/selection mode is active (controlled by the "Select entities" and "Exit selection" buttons)
	const [selectionActive, setSelectionActive] = React.useState(false);
	// Represents the current state of the selection (which attributes and relationships are selected)
	const [selectionState, setSelectionState] = React.useState({
		entities: {},
		relationships: {},
	});

	const [extractInProgress, setExtractInProgress] = React.useState(false);
	const [extractedData, setExtractedData] = React.useState([]);
	const [extractedColumns, setExtractedColumns] = React.useState([]);

	React.useEffect(() => {
		// Function responsible for figuring out what entities should be selectable.
		// The selection status propagates from the main entity (the one that the user clicks),
		// through all the selected ("checked") edges.
		if (selectionActive && activeSelectionEntities.length > 0) {
			const updateSubentities = (entityId, activeEntities = []) => {
				if (activeEntities.includes(entityId)) {
					return activeEntities;
				}

				activeEntities.push(entityId);
				// Outgoing edges from the current entity
				const edges = Object.values(selectionState.relationships).filter(
					(relation) => relation.source === entityId
				);

				// Recursively call the function on all the target entities
				for (const edge of edges) {
					if (edge.checked) {
						activeEntities.concat(
							updateSubentities(edge.target, activeEntities)
						);
					}
				}

				return activeEntities;
			};

			setActiveSelectionEntities((prevState) =>
				updateSubentities(prevState[0])
			);
		}
	}, [selectionState, activeSelectionEntities[0]]);

	React.useEffect(() => {
		setExtractedData({});
		setExtractedColumns([]);
		setSelectionActive(false);
		setSelectionState({ entities: {}, relationships: {} });

		if (abstraction && abstraction.mainEntities.length > 0) {
			setSelectionState(() => {
				const relationships = {};
				for (const relation of abstraction.relationships) {
					for (const relationName of relation.relations) {
						relationships[relationName] = {
							checked: false,
							source: relation.sourceId.toString(),
							target: relation.targetId.toString(),
						};
					}
				}

				return { entities: {}, relationships: relationships };
			});

			setActiveSelectionEntities([abstraction.mainEntities[0].id]);
		}
	}, [abstraction]);

	const handleExtractData = async (mainEntity, entities) => {
		const body = {
			dbName: database.replace("cl_", ""),
			mainEntity: mainEntity,
			entities: entities,
		};

		setExtractInProgress(true);
		await fetcherPost("/abstra/extract", body)
			.then((fetchedData) => {
				setExtractedData(
					fetchedData.map((item, index) => ({ id: index, ...item }))
				);

				// If there's data, generate columns based on the first item
				if (fetchedData.length > 0) {
					const columnNames = Object.keys(fetchedData[0]);
					const generatedColumns = columnNames.map((key) => ({
						field: key,
						headerName: key,
						divider: true,
						// width according to the length of the longest string in the column
						width:
							30 +
							Math.max(
								...[
									...fetchedData.map((item) => item[key]?.length ?? 5),
									key.length,
								]
							) *
								8,
					}));

					setExtractedColumns(generatedColumns);
				}

				setAlert({
					message: trans.extractionSuccessMessage[activeLang],
					level: "success",
				});

				return fetchedData;
			})
			.catch(() => {
				setAlert({
					message: trans.extractionSuccessMessage[activeLang],
					level: "error",
				});
			})
			.finally(() => {
				setExtractInProgress(false);
			});
	};

	const generateQuery = () => {
		/**
		 * Generates a query by recursively iterating through all entities through relationships and generates paths leading
		 * to all of the selected attributes and returns the relevant data in a common format.
		 *
		 * @param mainEntityId The id of the main entity (the one that the user clicked on)
		 * @param pathToEntity The path to the current entity (used for generating the query)
		 * @param labelPathToEntity The label path to the current entity (used for generating the label). Differs from pathToEntity
		 * in that it doesn't include the relationships labels since they're redundant (e.g. interest_interest@category_category).
		 * @returns {Array} An array of objects containing the id, path, full_label and required boolean of the selected attributes.
		 */
		const getEntitiesToExtract = (
			mainEntityId,
			pathToEntity = [],
			labelPathToEntity = []
		) => {
			const entitiesToExtract = [];

			const mainEntity = abstraction.mainEntities.find(
				(i) => i.id === mainEntityId
			);
			for (const entity of selectionState.entities[mainEntityId]) {
				if (entity.checked && entity.nbNonLeafChildren === 0) {
					const pathPrefix = [...pathToEntity, mainEntity.label];
					const labelPathPrefix = [...labelPathToEntity, mainEntity.label];

					const path = pathPrefix.concat(
						getLabelPathToEntity(
							selectionState.entities[mainEntityId],
							entity.id
						)
					);
					const labelPath = labelPathPrefix.concat(
						getLabelPathToEntity(
							selectionState.entities[mainEntityId],
							entity.id
						)
					);
					entitiesToExtract.push({
						id: entity.label,
						path: path.slice(1), // omit the first element (the main entity) since it gets added in the backend
						full_label: labelPath.join("_"),
						required: entity.required,
					});
				}
			}

			const subEntities = [];

			// Recursively call the function on all the entities that are connected to the main entity through a checked relationship
			for (const [relationName, relation] of Object.entries(
				selectionState.relationships
			)) {
				if (relation.source === mainEntityId && relation.checked) {
					const edgePath = relationName.split("@")[0].split(".");
					const edgeTarget = relationName.split("@")[1];
					const edgeReferenceField = edgePath[edgePath.length - 1];
					const edgeName = `${edgeReferenceField}@${edgeTarget}`;

					const relationPath = [mainEntity.label].concat([
						...edgePath,
						edgeName,
					]);
					const labelPath = [mainEntity.label].concat([...edgePath]);
					const entities = getEntitiesToExtract(
						relation.target,
						pathToEntity.concat(relationPath),
						labelPathToEntity.concat(labelPath)
					);
					subEntities.push(...entities);
				}
			}

			return entitiesToExtract.concat(subEntities);
		};

		const mainEntityId = activeSelectionEntities[0];
		const mainEntity = abstraction.mainEntities.find(
			(i) => i.id === mainEntityId
		);
		const entitiesToExtract = getEntitiesToExtract(mainEntityId);

		if (entitiesToExtract.length > 0) {
			handleExtractData(mainEntity, JSON.stringify(entitiesToExtract));
		}
	};

	const handleFileSubmission = async (id) => {
		const body = {
			datasourceId: id,
			dbName: database.replace("cl_", ""),
			emax: numberEntities,
		};

		let abstractionRes = await fetcherPost("/abstra/run", body).catch(function (
			error
		) {
			setAlert({
				message: trans.errorMessage[activeLang],
				level: "error",
			});
		});
		setIsComputing(false);
		if (abstractionRes) {
			setAbstraction(modifyAbstraction(abstractionRes));
			setAlert({
				message: trans.successMessage[activeLang],
				level: "success",
			});
			mutateAbstraExecutions();
		}
	};

	function handleClickButton(event) {
		event.stopPropagation();
		if (!isComputing) {
			setIsComputing(true);
			handleFileSubmission(activeItem);
		}
	}

	function handleSelectItem(id) {
		setActiveItem(id);
		setExecutionId(null);
		setAbstraction({ mainEntities: [], relationships: [] });
	}

	const { abstractionReadResult } = useReadAbstraction(
		executionId,
		activeItem,
		database,
		executionId
	);

	const { dataSources } = useDataSources(true, database);

	const {
		abstraExistingConfigs,
		isGetExecutionsLoading,
		mutateAbstraExecutions,
	} = useGetAbstraExecutions(activeItem, activeItem, database);

	const onClickHistory = (executionIdHistory) => {
		setExecutionId(executionIdHistory);
		SetOpenHistory(false);
	};

	React.useEffect(() => {
		setAbstraction(modifyAbstraction(abstractionReadResult));
	}, [abstractionReadResult]);

	// Since attribute ids are not unique, we need to generate unique ids for each attribute
	// This is done by prepending the path to the attribute to the attribute id
	const modifyAbstraction = (abstraction) => {
		if (abstraction && abstraction.mainEntities) {
			const updateEntity = (entity, parentId) => {
				entity.selection_id = parentId + "_" + entity.id;
				if (entity.children) {
					entity.children.forEach((child) => {
						updateEntity(child, entity.selection_id);
					});
				}
			};

			abstraction.mainEntities.forEach((entity) => {
				entity.boundary.children.forEach((child) => {
					updateEntity(child, entity.id);
				});
			});
		}

		return abstraction;
	};

	const { activeLang } = useLang();

	return (
		<EntitySelectionContext.Provider
			value={{
				selectionActive: selectionActive,
				currentEntities: activeSelectionEntities,
				setCurrentEntity: setActiveSelectionEntities,
				selectionState: selectionState,
				updateSelectionState: setSelectionState,
			}}
		>
			<Box
				sx={{
					width: "100%",
					height: "100%",
					display: "flex",
					flexDirection: "column",
				}}
			>
				<HelpButton
					sx={{ position: "absolute", right: 20 }}
					helper={trans.helper[activeLang]}
				></HelpButton>
				<Box sx={{ height: "100%", width: "100%", display: "flex" }}>
					<ResizableSizeMenu
						menuComponent={
							<FileSideMenu
								dataSources={dataSources?.length ? dataSources : []}
								handleSelectItem={handleSelectItem}
								activeItem={activeItem}
							/>
						}
					>
						<Box
							sx={{
								display: "flex",
							}}
						>
							{activeItem ? (
								<Box sx={{ width: "100%", px: 2 }}>
									<Button
										variant="outlined"
										startIcon={<HistoryIcon />}
										onClick={() => SetOpenHistory(true)}
										disabled={
											!(
												typeof abstraExistingConfigs !== "undefined" &&
												abstraExistingConfigs.length > 0
											)
										}
									>
										{trans.history[activeLang]}
									</Button>
									<Box
										sx={{
											my: 2,
											display: "flex",
											width: "100%",
										}}
									>
										<TextField
											type="number"
											label={trans.labelMaxEntities[activeLang]}
											InputLabelProps={{
												shrink: true,
											}}
											variant="standard"
											value={numberEntities}
											onChange={(event) =>
												setNumberEntities(event.target.value)
											}
										/>
										<LoadingButton
											sx={{ ml: 2 }}
											loading={isComputing}
											variant="contained"
											onClick={handleClickButton}
											size="small"
										>
											{trans.runAbstract[activeLang]}
										</LoadingButton>

										{abstraction && abstraction.mainEntities.length > 0 && (
											<>
												{/* <Box
													sx={{ display: "flex", position: "relative", ml: 2 }}
												>
													<FormControl
														size="small"
														sx={{
															float: "left",
															mr: "auto",
															my: "auto",
															minWidth: 200,
														}}
													>
														<InputLabel>{trans.sort[activeLang]}</InputLabel>
														<Select
															value={sortOption}
															onChange={(event) => {
																setSortOption(event.target.value);
															}}
														>
															<MenuItem value="name">
																{trans.sortOptions.name[activeLang]}
															</MenuItem>
															<MenuItem value="frequency">
																{trans.sortOptions.frequency[activeLang]}
															</MenuItem>
														</Select>
													</FormControl>
												</Box> */}
												<Button
													sx={{ ml: 2 }}
													variant={selectionActive ? "contained" : "outlined"}
													onClick={() => setSelectionActive(!selectionActive)}
													size="small"
												>
													{selectionActive
														? trans.exitSelectionButton[activeLang]
														: trans.selectEntitiesButton[activeLang]}
												</Button>
												{selectionActive && (
													<LoadingButton
														sx={{ ml: 2 }}
														loading={extractInProgress}
														variant={"contained"}
														onClick={() => generateQuery()}
														size="small"
													>
														{trans.extractDataButton[activeLang]}
													</LoadingButton>
												)}
											</>
										)}
									</Box>
									{abstraction && abstraction.mainEntities.length > 0 ? (
										<EntityRelationSchema
											fileName={activeItem}
											initialNodes={abstraction.mainEntities}
											initialEdges={abstraction.relationships}
										/>
									) : null}
								</Box>
							) : (
								<Box sx={{ display: "flex", m: "auto" }}>
									<Typography variant="h5" sx={{ fontWeight: 500 }}>
										{trans.warning[activeLang]}
									</Typography>
								</Box>
							)}
						</Box>
					</ResizableSizeMenu>

					<Dialog
						open={openHistory}
						onClose={() => SetOpenHistory(false)}
						fullWidth={true}
						maxWidth="xl"
						scroll="paper"
					>
						<DialogTitle sx={{ m: 0, p: 2 }}>
							{trans.historyTitle[activeLang]}
							<IconButton
								onClick={() => SetOpenHistory(false)}
								sx={{
									position: "absolute",
									right: 8,
									top: 8,
									color: "#c62828",
								}}
							>
								<CloseIcon />
							</IconButton>
						</DialogTitle>
						<DialogContent>
							{!isGetExecutionsLoading && abstraExistingConfigs ? (
								<AbstraHistory
									data={abstraExistingConfigs}
									onClickHistory={onClickHistory}
								></AbstraHistory>
							) : null}
						</DialogContent>
					</Dialog>
				</Box>
			</Box>
			{extractedData &&
			extractedColumns &&
			extractedData.length > 0 &&
			extractedColumns.length > 0 ? (
				<Box sx={{ width: "100%" }}>
					<DataGrid
						rows={extractedData}
						columns={extractedColumns}
						initialState={{
							pagination: {
								paginationModel: {
									pageSize: 10,
								},
							},
						}}
						slots={{
							toolbar: () => (
								<GridToolbarContainer>
									<GridToolbarColumnsButton />
									<GridToolbarFilterButton />
									<GridToolbarDensitySelector />
									<GridToolbarExport
										csvOptions={{
											fileName: `abstra-grid-${new Date().toISOString()}`,
										}}
									/>
								</GridToolbarContainer>
							),
						}}
						localeText={
							activeLang === "en"
								? enUS.components.MuiDataGrid.defaultProps.localeText
								: frFR.components.MuiDataGrid.defaultProps.localeText
						}
						pageSizeOptions={[10, 20, 50]}
						getRowHeight={() => "auto"}
						disableRowSelectionOnClick
						sx={{
							"&.MuiDataGrid-root--densityCompact .MuiDataGrid-cell": {
								py: "8px",
							},
							"&.MuiDataGrid-root--densityStandard .MuiDataGrid-cell": {
								py: "15px",
							},
							"&.MuiDataGrid-root--densityComfortable .MuiDataGrid-cell": {
								py: "22px",
							},
						}}
					/>
				</Box>
			) : null}
		</EntitySelectionContext.Provider>
	);
}
