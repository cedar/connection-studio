import * as React from "react";
import { useParams } from "react-router-dom";

import NavigateBeforeIcon from "@mui/icons-material/NavigateBefore";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import {
	Backdrop,
	CircularProgress,
	Grid,
	IconButton,
	Typography,
} from "@mui/material";
import { grey } from "@mui/material/colors";
import { Box } from "@mui/system";

import { HelpButton } from "../components/button";
import {
	ChartContainer,
	Donut,
	StackedBar,
	StatTable,
	WordCloud,
} from "../components/chart";
import { useLang } from "../context/Lang";
import { useStatistics } from "../fetch/statistics";
import { TYPES } from "../utils/graph";
import { isDefined } from "../utils/utils";

function GraphEntitiesDistribution({ statistics }) {
	const [index, setIndex] = React.useState(0);
	const [dataContainer, setDataContainer] = React.useState();

	const { activeLang } = useLang();

	const trans = {
		title: {
			en: "Entities distribution by type",
			fr: "Distribution d'entités par type",
		},
		subtitle1: {
			en: "Identified entities",
			fr: "Entités identifiées",
		},
		subtitle2: {
			en: "Distinct entities",
			fr: "Entités distinctes",
		},
		subtitle3: {
			en: "Loaded files",
			fr: "Fichiers chargés",
		},
	};

	React.useEffect(() => {
		const getCurrentDescription = () => {
			if (activeLang === "fr") {
				return "descr_fr";
			}

			if (activeLang === "en") {
				return "descr_en";
			}
		};

		function n_entities_by_type(statistics) {
			var generalDistribution = statistics.filter(
				(stat) =>
					!stat.statname.includes("distinct") &&
					!stat.statname.includes("dataSource") &&
					stat.statname !== "mostFrequentEntities" &&
					stat.statname !== "entityCountsPerDataset" &&
					stat.statname !== "avgspec" &&
					stat.statname !== "stddevspec"
			);

			generalDistribution = generalDistribution.filter(
				(stat) => parseFloat(stat.statvalue) !== 0.0
			);

			var data = generalDistribution.map((stat) => parseFloat(stat.statvalue));
			var entities = generalDistribution.map(
				(stat) => stat[getCurrentDescription()]
			);

			return {
				data: data,
				labels: entities,
				subtitle: trans.subtitle1[activeLang],
				colors: generalDistribution.map((d) => TYPES[d.statname].color),
			};
		}

		function n_unique_entities_by_type(statistics) {
			var distinctDistribution = statistics.filter(
				(stat) =>
					stat.statname.includes("distinct") &&
					!stat.statname.includes("dataSource")
			);

			distinctDistribution = distinctDistribution.filter(
				(stat) => parseFloat(stat.statvalue) !== 0.0
			);
			var data = distinctDistribution.map((stat) => parseFloat(stat.statvalue));
			var entities = distinctDistribution.map((stat) =>
				stat[getCurrentDescription()]
					.replace("(sans doublons)", "")
					.replace("sans doublons", "")
					.replace(";", "")
					.replace("tags ", "tags")
			);

			return {
				data: data,
				labels: entities,
				subtitle: trans.subtitle2[activeLang],
				colors: distinctDistribution.map(
					(d) =>
						TYPES[
							d.statname
								.replace("distinct ", "")
								.replace("hashtags", "hashstags")
						].color
				),
			};
		}

		function n_entities_by_file(statistics) {
			var dataNodeDistribution = statistics.filter((stat) =>
				stat.statname.includes("dataSource")
			);
			dataNodeDistribution = dataNodeDistribution.filter(
				(stat) => parseFloat(stat.statvalue) !== 0.0
			);

			var data = dataNodeDistribution.map((stat) => parseFloat(stat.statvalue));
			var entities = dataNodeDistribution.map((stat) =>
				activeLang === "fr"
					? stat[getCurrentDescription()]
							.replace("sources de données", "fichiers")
							.replace("dans le graphe", "")
					: stat[getCurrentDescription()].replace("sources", "files")
			);

			return {
				data: data,
				labels: entities,
				subtitle: trans.subtitle3[activeLang],
			};
		}

		let newDataContainer = [];
		newDataContainer.push(n_entities_by_type(statistics));
		newDataContainer.push(n_unique_entities_by_type(statistics));
		newDataContainer.push(n_entities_by_file(statistics));
		setDataContainer(newDataContainer);
	}, [statistics, activeLang]);

	const handleOnNext = () => {
		if (index + 1 < dataContainer.length) {
			setIndex(index + 1);
		}
	};

	const handleOnPrev = () => {
		if (index - 1 >= 0) {
			setIndex(index - 1);
		}
	};

	return (
		<ChartContainer
			raised
			elevation={0}
			sx={{ height: 600 }}
			title={trans.title[activeLang]}
		>
			{isDefined(dataContainer) ? (
				<Box>
					<Box
						sx={{
							display: "flex",
						}}
					>
						<Box sx={{ display: "flex", mx: "auto", mb: 3 }}>
							<IconButton onClick={handleOnPrev}>
								<NavigateBeforeIcon></NavigateBeforeIcon>
							</IconButton>
							<Typography
								variant="h6"
								sx={{
									display: "flex",
									alignContent: "center",
									textAlign: "center",
									verticalAlign: "center",
									my: "auto",
								}}
							>
								{dataContainer[index].subtitle}
							</Typography>
							<IconButton onClick={handleOnNext}>
								<NavigateNextIcon></NavigateNextIcon>
							</IconButton>
						</Box>
					</Box>
					<Donut
						data={dataContainer[index].data}
						colors={dataContainer[index].colors}
						labels={dataContainer[index].labels}
						isHorizontal={false}
					></Donut>
				</Box>
			) : null}
		</ChartContainer>
	);
}

// const EntyVsBar = () => {
// 	let data = [540, 470, 448, 430, 400];
// 	let maxData = Math.max(...data);

// 	let opacity = data.map((val) => val / maxData);
// 	let colors = opacity.map((value) => `rgba(113, 199, 226,${value})`);

// 	return (
// 		<Bar
// 			labels={["Pfizer", "Monsanto", "Phillip Moris", "Marlboro", "Camel"]}
// 			data={data}
// 			colors={colors}
// 		></Bar>
// 	);
// };

// const EntiyVsEntity = () => {
// 	return (
// 		<ChartContainer raised elevation={0} title="Nombres de liens entre entités">
// 			<Box
// 				sx={{
// 					display: "flex",
// 				}}
// 			>
// 				<Box sx={{ display: "flex", mx: "auto" }}>
// 					<IconButton>
// 						<NavigateBeforeIcon></NavigateBeforeIcon>
// 					</IconButton>
// 					<Typography
// 						variant="h6"
// 						sx={{
// 							display: "flex",
// 							alignContent: "center",
// 							textAlign: "center",
// 							verticalAlign: "center",
// 							my: "auto",
// 						}}
// 					>
// 						Organisations vs Personnes
// 					</Typography>
// 					<IconButton>
// 						<NavigateNextIcon></NavigateNextIcon>
// 					</IconButton>
// 				</Box>
// 			</Box>

// 			<EntyVsBar></EntyVsBar>
// 		</ChartContainer>
// 	);
// };

const DataSourcesEntitiesDistribution = ({ statistics }) => {
	const [series, setSeries] = React.useState([]);
	const [labels, setLabels] = React.useState([]);
	const [colors, setColors] = React.useState([]);

	React.useEffect(() => {
		if (isDefined(statistics)) {
			const dataSourcesStatistics = statistics.filter(
				(stat) => stat.statname === "entityCountsPerDataset"
			)[0]["descr_fr"];

			var entitiesLabels = dataSourcesStatistics[0];
			var data = dataSourcesStatistics[1];

			var normalizedData = data.map((ds) => {
				entitiesLabels.forEach((entity) => {
					ds[entity] = entity in ds ? parseInt(ds[entity]) : 0;
				});
				return ds;
			});

			const series = [];
			const seriesTypes = [];
			for (let label of entitiesLabels) {
				var labelData = normalizedData.map((data) => data[label]);
				series.push({
					name: TYPES[label].pluralDisplayName[activeLang],
					data: labelData,
				});
				seriesTypes.push({ type: label });
			}
			setSeries(series);
			setLabels(normalizedData.map((data) => data.ds));

			setColors(seriesTypes.map((serie) => TYPES[serie.type].color));
		}
	}, [statistics]);

	const { activeLang } = useLang();

	const trans = {
		title: {
			en: "Distribution of entity types by file",
			fr: "Distribution des types d'entités par fichier",
		},
	};
	return (
		<ChartContainer raised elevation={0} title={trans.title[activeLang]}>
			<StackedBar labels={labels} series={series} colors={colors}></StackedBar>
		</ChartContainer>
	);
};

const EntityCloud = ({ statistics }) => {
	const [words, setWords] = React.useState([]);

	React.useEffect(() => {
		if (isDefined(statistics)) {
			const mostFrequent = statistics.filter(
				(stat) => stat.statname === "mostFrequentEntities"
			)[0];

			const data = mostFrequent["descr_fr"];

			const words = data.map(function (word) {
				return {
					word: word.name,
					size: parseInt(word["frequency:"]),
					color: TYPES[word.type].color,
				};
			});
			setWords(words);
		}
	}, [statistics]);

	const { activeLang } = useLang();

	const trans = {
		title: {
			en: "Entity cloud",
			fr: "Nuage d'entités",
		},
	};

	return (
		<ChartContainer raised elevation={0} title={trans.title[activeLang]}>
			<WordCloud words={words}></WordCloud>
		</ChartContainer>
	);
};

const SharedEntity = ({ statistics }) => {
	const [rows, setRows] = React.useState([]);
	const [columns, setColumns] = React.useState([]);
	const { activeLang } = useLang();

	React.useEffect(() => {
		if (isDefined(statistics)) {
			let setSharedEntities = statistics[0];
			let rawColumns = Object.keys(setSharedEntities.statvalue[0]);
			let columns = [];
			for (let rawColumn of rawColumns) {
				let width;
				if (rawColumn === "label") {
					width = 0.3;
				} else if (rawColumn === "datasets") {
					width = 0.5;
				} else if (rawColumn === "type") {
					width = 0.2;
				} else if (rawColumn === "frequency") {
					width = 0.1;
				}
				let column = {
					field: rawColumn,
					headerName: rawColumn.charAt(0).toUpperCase() + rawColumn.slice(1),
					flex: width,
					editable: false,
				};
				columns.push(column);
			}

			setColumns(columns);

			let rows = setSharedEntities.statvalue.map((stat, idx) => {
				let row = {};
				for (let [key, value] of Object.entries(stat)) {
					let currentVal;
					if (key === "datasets") {
						let paths = [];
						for (let path of value) {
							let dataset = path.split("/").slice(-1)[0];
							paths.push(dataset);
						}
						currentVal = paths.join(", ");
					} else if (key === "frequency") {
						currentVal = parseInt(value);
					} else if (key === "type") {
						currentVal = TYPES[value].displayName[activeLang];
					} else {
						// if (key !== "frequency" && key !== "datasets") {
						currentVal = value;
					}
					row[key] = currentVal;
				}
				row["id"] = idx;
				return row;
			});
			setRows(rows);
		}
	}, [statistics, activeLang]);

	const trans = {
		title: {
			en: "Shared entities",
			fr: "Entités communes",
		},
	};

	return (
		<ChartContainer raised elevation={0} title={trans.title[activeLang]}>
			<StatTable columns={columns} rows={rows}></StatTable>
		</ChartContainer>
	);
};

export default function Stats() {
	const params = useParams();
	const database = params.database;
	var shouldFetchStatistics = true;
	const { statistics, isStatisticsLoading, isStatisticsError } = useStatistics(
		shouldFetchStatistics,
		database
	);

	const { activeLang } = useLang();

	const trans = {
		helper: {
			fr: (
				<Typography>
					<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
						But: avoir un aperçu statistique des entités détectées.
					</Box>
					Dans cette page, vous obtenez des vues statistiques des entités
					nommées trouvées dans vos sources.
				</Typography>
			),
			en: (
				<Typography>
					<Box sx={{ fontWeight: 600, color: grey[800], mb: 1 }}>
						Goal: a statistics overview of entities extracted in your data.
					</Box>
					In this page, you have several statistical views of named entities
					extracted in your data sources.
				</Typography>
			),
		},
		loading: {
			en: "Statistics are loading...",
			fr: "Chargement des statistiques...",
		},
	};
	if (!isDefined(statistics)) {
		return (
			<Backdrop open={true} sx={{ color: "#fff", "z-index": "10 !important" }}>
				<Typography sx={{ mr: 4 }}>{trans.loading[activeLang]}</Typography>
				<CircularProgress color="inherit" />
			</Backdrop>
		);
	}

	return (
		<Box sx={{ width: "100%", height: "100%" }}>
			<Box sx={{ display: "flex", height: "5%", mb: 2 }}>
				<HelpButton
					sx={{ position: "absolute", right: 20 }}
					helper={trans.helper[activeLang]}
				></HelpButton>
			</Box>

			<Box>
				<Grid container spacing={3} sx={{ mb: 4 }}>
					<Grid item xs={6}>
						<GraphEntitiesDistribution
							statistics={statistics.filter(
								(stat) => stat.statname !== "shared_entities"
							)}
						></GraphEntitiesDistribution>
					</Grid>
					<Grid item xs={6}>
						<EntityCloud
							statistics={statistics.filter(
								(stat) => stat.statname !== "shared_entities"
							)}
						></EntityCloud>
					</Grid>
				</Grid>
				<Grid container spacing={3} sx={{ mb: 4 }}>
					<Grid item xs={12}>
						<DataSourcesEntitiesDistribution
							statistics={statistics.filter(
								(stat) => stat.statname !== "shared_entities"
							)}
						></DataSourcesEntitiesDistribution>
					</Grid>
				</Grid>
				<Grid container spacing={3} sx={{ mb: 4 }}>
					<Grid item xs={12} sx={{ mb: 4 }}>
						<SharedEntity
							statistics={statistics.filter(
								(stat) => stat.statname === "shared_entities"
							)}
						></SharedEntity>
					</Grid>
				</Grid>
			</Box>
		</Box>
	);
}
