import { Typography } from "@mui/material";
import { Box } from "@mui/system";

export default function Home() {
	return (
		<Box sx={{ display: "block" }}>
			<Typography
				sx={{
					fontWeight: 700,
					overflow: "hidden",
					textTransform: "normal",
				}}
				variant="h3"
			>
				Home
			</Typography>
		</Box>
	);
}
