@echo off

@REM Set CS_DIR (no slash at the end, it will be appended when adding folders)
set "CS_DIR=%CD%\connection-studio-dir"

@REM Set other directories
set "BASE_DIR=%CD%"
set "ROOT=%BASE_DIR%\webservices\lib"
set "CACHE_DIR=cache"
set "TMP_DIR=tmp"   
set "TT_DIR=treetagger"
set "MODELS_DIR=models"
set "CMD_DIR=cmd"
set "LIB_DIR=lib"
set "SCRIPTS_DIR=scripts"
set "HEIDELTIME_DIR=heideltime"
set "CLASSIFICATION_DIR=classification"
set "REPORTING_DIR=reporting"

@REM Get OS to get the relevant executables for treetagger
set "TT_BIN="
if "%OS%"=="Windows_NT" (
  @REM Windows OS
  set "TT_BIN=binWindows"
) else (
  @REM Default to Linux
  set "TT_BIN=binLinux"
)

set "CS_TT=%ROOT%\%TT_DIR%"
set "CS_TT_BIN=%CS_TT%\%TT_BIN%"
set "CS_TT_CMD=%CS_TT%\%CMD_DIR%"
set "CS_TT_LIB=%CS_TT%\%LIB_DIR%"
set "CS_TT_MODELS=%CS_TT%\%MODELS_DIR%"
set "CS_HEIDELTIME=%ROOT%\%HEIDELTIME_DIR%"
set "CS_SCRIPTS=%SCRIPTS_DIR%"
set "CS_CLASSIFICATION=%ROOT%\%CLASSIFICATION_DIR%"
set "CS_REPORTING=%ROOT%\%REPORTING_DIR%"

set "LOCAL_CACHE=%CS_DIR%\%CACHE_DIR%"
set "LOCAL_TMP=%CS_DIR%\%TMP_DIR%"
set "LOCAL_TT=%CS_DIR%\%TT_DIR%"
set "LOCAL_TT_BIN=%LOCAL_TT%\bin"
set "LOCAL_TT_CMD=%LOCAL_TT%\%CMD_DIR%"
set "LOCAL_TT_LIB=%LOCAL_TT%\%LIB_DIR%"
set "LOCAL_TT_MODELS=%LOCAL_TT%\%MODELS_DIR%"
set "LOCAL_HEIDELTIME=%CS_DIR%\%HEIDELTIME_DIR%"
set "LOCAL_SCRIPTS=%CS_DIR%\%SCRIPTS_DIR%"
set "LOCAL_CLASSIFICATION=%CS_DIR%\%CLASSIFICATION_DIR%"
set "LOCAL_REPORTING=%CS_DIR%\%REPORTING_DIR%"
set "LOCAL_PYTHON_ENV=%CS_DIR%\cs_env"
set "LOCAL_SETTINGS=%CS_DIR%\local.settings"

@REM Create folders
mkdir "%CS_DIR%"
mkdir "%LOCAL_CACHE%"
mkdir "%LOCAL_TMP%"
mkdir "%LOCAL_TT%"
mkdir "%LOCAL_TT_BIN%"
mkdir "%LOCAL_TT_CMD%"
mkdir "%LOCAL_TT_LIB%"
mkdir "%LOCAL_TT_MODELS%"
mkdir "%LOCAL_HEIDELTIME%"
mkdir "%LOCAL_SCRIPTS%"
mkdir "%LOCAL_CLASSIFICATION%"
mkdir "%LOCAL_REPORTING%"

@REM Copy necessary files
xcopy /E /I /Y "%CS_TT_BIN%\*" "%LOCAL_TT_BIN%\"
xcopy /E /I /Y "%CS_TT_CMD%\*" "%LOCAL_TT_CMD%\"
xcopy /E /I /Y "%CS_TT_LIB%\*" "%LOCAL_TT_LIB%\"
xcopy /E /I /Y "%CS_TT_MODELS%\*" "%LOCAL_TT_MODELS%\"
xcopy /E /I /Y "%CS_SCRIPTS%\*" "%LOCAL_SCRIPTS%\"
xcopy /E /I /Y "%CS_HEIDELTIME%\*" "%LOCAL_HEIDELTIME%\"
xcopy /E /I /Y "%CS_CLASSIFICATION%\*" "%LOCAL_CLASSIFICATION%\"
xcopy /E /I /Y "%CS_REPORTING%\*" "%LOCAL_REPORTING%\"

copy "%ROOT%\parameter.settings" "%CS_DIR%\local.settings"

@REM Configure Python venv
@REM Install required packages

@REM python -m pip install --upgrade pip
@REM python -m pip install virtualenv
@REM python -m virtualenv "%LOCAL_PYTHON_ENV%"
@REM call %LOCAL_PYTHON_ENV%\Scripts\activate.bat
@REM pip install --upgrade pip
@REM pip install wheel 
@REM pip install -r "%ROOT%\requirements.txt" -f https://download.pytorch.org/whl/torch_stable.html
@REM call %LOCAL_PYTHON_ENV%\Scripts\deactivate.bat

python -m venv %LOCAL_PYTHON_ENV%
%LOCAL_PYTHON_ENV%\Scripts\python -m pip install --upgrade pip
%LOCAL_PYTHON_ENV%\Scripts\pip install -r requirements.txt -f https://download.pytorch.org/whl/torch_stable.html


@REM Add the absolute path to models
echo. >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo #### GENERATED PARAMETERS >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # Temporary directory for Connection-Studio >> %LOCAL_SETTINGS%
echo temp_dir=%LOCAL_TMP:\=/% >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # Connection-Studio cache location >> %LOCAL_SETTINGS%
echo cache_location=%LOCAL_CACHE:\=/% >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # Python location >> %LOCAL_SETTINGS%
echo python_path=%LOCAL_PYTHON_ENV:\=/%/Scripts/python.exe >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # Path to python scripts (FLAIR extraction) >> %LOCAL_SETTINGS%
echo python_script_location=%LOCAL_SCRIPTS:\=/% >> %LOCAL_SETTINGS%
echo data_processing_script_location=%LOCAL_SCRIPTS:\=/%/data_preprocessing >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # Treetagger location >> %LOCAL_SETTINGS%
echo treetagger_home=%LOCAL_TT:\=/% >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # Stanford models location >> %LOCAL_SETTINGS%
echo stanford_models=%LOCAL_TT_MODELS:\=/% >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # Configuration file used by HeidelTime (date extractor) >> %LOCAL_SETTINGS%
echo config_heideltime=%LOCAL_HEIDELTIME:\=/%/config-heideltime.props >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # Word2Vec model location (used for classification during abstraction) >> %LOCAL_SETTINGS%
echo word_embedding_model_path=%LOCAL_CLASSIFICATION:\=/%/word2vec.bin >> %LOCAL_SETTINGS%
echo stop_words_english=%LOCAL_CLASSIFICATION:\=/%/stop_words_english.txt >> %LOCAL_SETTINGS%
echo stop_words_french=%LOCAL_CLASSIFICATION:\=/%/stop_words_french.txt >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # set of manually-defined semantic properties >> %LOCAL_SETTINGS%
echo set_of_semantic_properties_manual=%LOCAL_CLASSIFICATION:\=/%/set-of-semantic-properties-manual.json >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # set of semantic properties extended with Yago and DBPedia triples >> %LOCAL_SETTINGS%
echo set_of_semantic_properties_extended=%LOCAL_CLASSIFICATION:\=/%/set-of-semantic-properties-extended.json >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # set of classes (domain/range) extracted from GitTables semantic properties >> %LOCAL_SETTINGS%
echo gitTables_classes=%LOCAL_CLASSIFICATION:\=/%/GitTables-classes.txt >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # set of semantic properties provided by GitTables >> %LOCAL_SETTINGS%
echo gitTables_semantic_properties=%LOCAL_CLASSIFICATION:\=/%/GitTables-semantic-properties.json >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # class hierarchy for Schema.org classes >> %LOCAL_SETTINGS%
echo schemaorg_class_hierarchy=%LOCAL_CLASSIFICATION:\=/%/class-hierarchy-schemaorg-cleaned.json >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # synonyms for name,designation,denomination file >> %LOCAL_SETTINGS%
echo synonyms_denomination_file=%LOCAL_CLASSIFICATION:\=/%/words-related-to-denomination.txt >> %LOCAL_SETTINGS%
echo. >> %LOCAL_SETTINGS%
echo # mapping between our extracted entity types and Schema.org and DBPedia classes (e.g. Person is same as dbpedia:Person) >> %LOCAL_SETTINGS%
echo mapping_classes_to_categories=%LOCAL_CLASSIFICATION:\=/%/mapping-classes-to-categories.json >> %LOCAL_SETTINGS%

@REM Add the absolute path to TreeTagger in config-heideltime.props
echo. >> %LOCAL_HEIDELTIME%/config-heideltime.props
echo #### GENERATED PARAMETERS >> %LOCAL_HEIDELTIME%/config-heideltime.props
echo treeTaggerHome=%LOCAL_TT:\=/% >> %LOCAL_HEIDELTIME%/config-heideltime.props

copy "%LOCAL_SETTINGS%" "%BASE_DIR%\webservices\main\resources"
copy "%LOCAL_SETTINGS%" "%BASE_DIR%\webservices\WebContent\WEB-INF"

goto :eof