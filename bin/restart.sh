#!/bin/bash

function printUsage() {
    cat <<EOF

    Restart the tomcat web server.

    Usage : restart  

EOF
}

while [ $# -gt 0 ]; do
    ARG=$1
    case $ARG in
    -h | --help)
        printUsage
        exit 0
        ;;
    esac
done

if [ -z "$CATALINA_HOME" ]; then
    echo "CATALINA_HOME hasn't been setup, exiting..."
    exit 1
fi

PROCESS=$(ps aux | grep java | grep apache-tomcat)
PID="$(echo $PROCESS | cut -d' ' -f2)"

echo "Shutting down Tomcat"
echo "---------------------------------------------------------"
$CATALINA_HOME/bin/shutdown.sh
kill -9 $PID

echo "Starting Tomcat"
echo "---------------------------------------------------------"
$CATALINA_HOME/bin/startup.sh
echo "---------------------------------------------------------"

echo "Tomcat successfully restart"
