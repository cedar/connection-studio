@echo off



set "BASE_DIR=%cd%"

set "WAR=webservices.war"
set "PURGE=false"
set "REACT=false"
set "CONFIGURE=false"

:loop
if "%~1"=="" goto :args_done
set "ARG=%~1"
shift
if "%ARG%"=="-h" (
    call :printUsage
    exit /b 0
) else if "%ARG%"=="--help" (
    call :printUsage
    exit /b 0
) else if "%ARG%"=="--purge" (
    set "PURGE=true"
) else if "%ARG%"=="--react" (
    set "REACT=true"
) else if "%ARG%"=="--configure" (
    set "CONFIGURE=true"
) else (
    echo Unknown option: %ARG%
    exit /b 1
)
goto :loop

:args_done

set "CS_DIR=%cd%\connection-studio-dir"



if "%CONFIGURE%"=="true" (
    call %BASE_DIR%\install\configure.bat
)

if "%CATALINA_HOME%"=="" (
    echo CATALINA_HOME hasn't been setup, exiting...
    exit /b 1
)

if "%PURGE%"=="true" (
    echo Flushing maven cache
    rmdir /s /q %USERPROFILE%\.m2
)

if "%REACT%"=="true" (
    set "WAR=connectionstudio.war"
    set "WORK_DIR=%BASE_DIR%\front"
    set "CURRENT=React"
) else (
    set "WAR=webservices.war"
    set "WORK_DIR=%BASE_DIR%\webservices"
    set "CURRENT=WebServices"
)

for %%A in (%WAR%) do set "WAR_NAME=%%~nA"
echo %WAR_NAME%

echo Saving upload file before deleting Tomcat cache
if exist "%CATALINA_HOME%\webapps\%WAR_NAME%\WEB-INF\uploads" (
    xcopy /E /I "%CATALINA_HOME%\webapps\%WAR_NAME%\WEB-INF\uploads" "%CATALINA_HOME%\webapps"
)

cd %WORK_DIR%
echo Deploying %CURRENT% module
CALL mvn -U clean install -DskipTests

set "WAR_PATH=%WORK_DIR%\target\%WAR%"
if not exist "%WAR_PATH%" (
    echo %WAR_PATH% does not exist, exiting...
    exit /b 1
)

call :isTomcatRunning
if "%IS_TOMCAT_RUNNING%"=="true" (
    echo Shutting down Tomcat
    echo ---------------------------------------------------------
    CALL %CATALINA_HOME%\bin\shutdown.bat
    echo ---------------------------------------------------------
)

if exist "%CATALINA_HOME%\webapps\%WAR_NAME%" (
    echo Removing Tomcat %CATALINA_HOME%\webapps\%WAR_NAME% folder
    rmdir /s /q %CATALINA_HOME%\webapps\%WAR_NAME%
    REM rmdir /s /q %CATALINA_HOME%\work\
)

if exist "%CATALINA_HOME%\webapps\%WAR%" (
    echo Removing Tomcat %CATALINA_HOME%\webapps\%WAR% war
    del %CATALINA_HOME%\webapps\%WAR%
)

echo %WAR_PATH% %CATALINA_HOME%\webapps
if exist "%WAR_PATH%" (
    echo Deploying %CURRENT% module into Tomcat
    copy %WAR_PATH% %CATALINA_HOME%\webapps
)

call :isTomcatRunning
if "%IS_TOMCAT_RUNNING%"=="false" (
    echo Starting Tomcat
    echo ---------------------------------------------------------
    CALL %CATALINA_HOME%\bin\startup.bat
    echo ---------------------------------------------------------
)

echo %CURRENT% successfully deployed

if exist "%CATALINA_HOME%\webapps\uploads\" (
    echo Adding saved upload files to Tomcat
    timeout 2 >nul
    move %CATALINA_HOME%\webapps\uploads\ %CATALINA_HOME%\webapps\%WAR_NAME%\WEB-INF\
)

goto :eof

REM Function: printUsage
REM Display the usage of this script
:printUsage
@echo off
echo.
echo Produces the war for Connection Studio webservices and deploy it into tomcat.
echo.
echo Usage : deploy [--configure^|OPTION] [--purge^|OPTION] [--react^|OPTION] [--abstra^|OPTION]
echo.
echo --configure: Run the configure.sh. This will (re)create connection-studio-dir and the local.settings.
echo.
echo --purge: Remove the maven cache directory, to rebuild a maven project from scratch.
echo.
echo --react: Deploy the React application into Tomcat.
echo.

goto :eof

REM Function: isTomcatRunning
REM Checks if Tomcat is running.
:isTomcatRunning
set "STAT="
for /F "usebackq delims=" %%G in (netstat -a ^| findstr ":8080") DO set "STAT=%%G"

set "IS_TOMCAT_RUNNING=false"

for /F "tokens=1-4" %%i in ("%STAT%") do (
    if "%%l"=="TIME_WAIT" (
        set "IS_TOMCAT_RUNNING=false"
    ) else if "%%l"=="LISTENING" (
        set "IS_TOMCAT_RUNNING=true"
    )
)

goto :eof