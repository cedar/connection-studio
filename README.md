# Connection-Studio

This repository provides a single interface for the Connection-Lens, Abstra and PathWays projects.

## 0. Table of Contents

1. [Pre-requisites](#pre-requisites)
2. [Environement Variables](#environement-variables)
3. [Installation](#installation)
4. [Remote Maven Deployment](#remote-maven-deployment)

## 1. Pre-requisites

### Python (3.6 or 3.7)

Studio has been tested to work with Python 3.6 or 3.7 only.

### Java

---

- Java installed. Building has been tested with the following java version:
  - JAVA_VERSION="11.0.18"

### Node JS

---

Install the latest version of [Nodes.js](https://nodejs.org/en), tested with node **v18.12.1** and npm **v8.19.2** (which is included with node).

#### Windows

- Download the [Windows Installer (.msi)](https://nodejs.org/dist/v18.16.0/node-v18.16.0-x86.msi)
- Open and run the .msi file, follow the the installation steps

#### MacOS

- Download the [Mac Installer (.pkg)](https://nodejs.org/dist/v18.16.0/node-v18.16.0.pkg)
- Open and run the .pkg file, follow the the installation steps

#### Linux

- Download the latest node version, using [curl](https://curl.se/)n and install it :
  ```bash
  curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash - &&\
  sudo apt-get install -y nodejs
  ```

#### Verify your installation

- Verifiy the node version
  ```
  node --version
  ```
- Verify the npm version
  ```
  npm --version
  ```

### Maven

---

Install the version **3.6.3** of [Maven](https://maven.apache.org/docs/3.6.3/release-notes.html)

#### Windows

- Download the [Maven zip archive (.zip)](https://archive.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip)
- Move the archive in the location you want, and uncompress the archive using [unzip](http://stahlworks.com/dev/unzip.exe):
  ```
  unzip -l apache-maven-3.6.3-bin.zip
  ```

#### Linux & MacOS

- Download the [Maven tar archive (.tar.gz)](https://archive.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz)
- Move the archive in the location you want, and uncompress the archive using [tar](https://www.commandlinux.com/man-page/man1/tar.1.html):
  ```
  tar -xvf apache-maven-3.6.3-bin.tar.gz
  ```

### Tomcat

---

Install the version **9.0.54** of [Tomcat](https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.54/)

#### Windows

- Download the [Tomcat zip archive (.zip)](https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.54/bin/apache-tomcat-9.0.54-windows-x64.zip)
- Move the archive in the location you want, and uncompress the archive using [unzip](http://stahlworks.com/dev/unzip.exe):
  ```
  unzip -l apache-tomcat-9.0.54-windows-x64.zip
  ```

#### Linux & MacOS

- Download the [Tomcat tar archive (.tar.gz)](https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.54/bin/apache-tomcat-9.0.54.tar.gz)
- Move the archive in the location you want, and uncompress the archive using [tar](https://www.commandlinux.com/man-page/man1/tar.1.html):

  ```bash
  tar -xvf apache-tomcat-9.0.54.tar.gz
  ```

### Environement Variables

---

Before using the application please make sure that the following variables are set in you environment :

- JAVA_HOME
- CATALINA_HOME
- Add the maven CLI to your path

#### tcsh or csh (MacOS)

Using the editor of your choice (we use [vim](https://www.vim.org/download.php)),

- Open the config file of your shell :

  ```vim
  vim ~/.tcshrc or vim ~/.cshrc
  ```

- Then add the following lines at the end of the file :

  ```tcsh
  setenv CATALINA_HOME /the/absolute/path/to/tomcat/apache-tomcat-9.0.54
  setenv PATH $PATH\:/the/absolute/path/to/maven/apache-maven-3.6.3/bin
  ```

#### zsh or bash (Linux & MacOS)

- Open the config file of your shell :

  ```vim
  vim ~/.bashrc or vim ~/.zshrc
  ```

- Then add the following lines at the end of the file :

  ```bash
  export CATALINA_HOME=//the/absolute/path/to/tomcat/apache-tomcat-9.0.54
  export PATH=$PATH:/the/absolute/path/to/maven/apache-maven-3.6.3/bin
  ```

#### cmd (Windows)

- Type "Advanced system" in the windows search bar and click on **Advanced system settings**.
- On the **Advanced tab**, click **Environment Variables**
- To add `Maven` to your path, In the **System Variables tab**, scroll to find the variables `PATH` and click on **Modify**
- Then click on **New** and add

  ```
  /the/absolute/path/to/maven/apache-maven-3.6.3/bin
  ```

- Click on **ok**
- To add Tomcat to the system variables, on the **System Variables tab**, click on **New**.
- To the **Variable Name** field, add the following :
  ```
  CATALINA_HOME
  ```
- To the **Variable Value** field, add the following :
  ```
  /the/absolute/path/to/tomcat/apache-tomcat-9.0.54
  ```
- Click on **Ok**

### Postgres (9.6 or later)

A PostgreSQL server must be running locally, and you must have access to an account with the ability to create users. By default, the username is **kwsearch**, therefore this user must be created beforehand with the right to **create databases** (`create user kwsearch with createdb`). On some installations (depending on the PostgreSQL configuration), this user _needs_ to have a password (so to be safe create it with a password).

## 2. Installation

- Clone the project

### Web Services

- Deploy the **web services war** into Tomcat by running :

  ##### Linux & MacOS

  ```bash
  cd connection-studio
  bash bin/deploy --configure
  ```

  ##### Windows (PowerShell)

  ```bat
  cd connection-studio
  .\bin\deploy.bat --configure
  ```

**NB** : The first run of `scripts/deploy.sh --configure` will generate a configuration file and write it to `webservices/WebContent/WEB-INF/local.settings`.

**NB** : You need to run the deploy command with `--configure` only once.

### Front

You have two options to use the web application, you can :

1. Launch a web server using npm (it's a development server)
2. Deploy the web application into tomcat

#### 1. Using the Development Server

- Install the node packages :

  ```bash
  cd connection-studio/front
  npm install
  ```

- To start the development web server, run :

  ```bash
  npm run start
  ```

- To access the web application, in your Web Browser go http://localhost:3000/

#### 2. Deploying into Tomcat

- Deploy the **web services war** & the **React war** into Tomcat by running :

  ##### Linux & MacOS

  ```bash
  cd connection-studio
  bash bin/deploy --react
  ```

  ##### Windows (PowerShell)

  ```bat
  cd connection-studio
  .\bin\deploy.bat --react
  ```

- To access the web application, in your Web Browser go http://localhost:8080/connectionstudio/

**NB** : In case of errors during the build of the react application (after a `bash bin/deploy --react`)

- Be sure to have all the required node modules :

```bash
cd connection-studio/front
npm install
```

- If the build is still failling, try to reinstall all node modules :

```bash
cd connection-studio/front
rm -rf node_modules/ package-lock.json
npm install
```

### 3. Usage

Installation and deployment are handled by the command bin/deploy, which produces a .war of the ConnectionStudio Web services, and deploys it into Tomcat.

```
Usage : deploy [--configure|OPTION] [--purge|OPTION] [--react|OPTION] [--abstra|OPTION]

Arguments:
  -c, --configure: Configure the connection-studio environment. This will (re)create 'connection-studio-dir' directory and the 'local.settings' file.

  -p, --purge: Remove the maven cache directory ~/.m2, useful to rebuild a maven project from scratch or debugging.

  -r, --react: Also deploy the React application into Tomcat.

  -v, --verbose: Verbose argument, log level will be set to debug, useful to debug.
```

## 3. Information for Project Developers

ConnectionStudio is a front-end integrating functionalities implemented by the [Pathways](https://gitlab.inria.fr/nbarret/path-ways/-/tree/develop/) project.
PathWays relies on [Abstra](https://gitlab.inria.fr/nbarret/abstraction-work), which relies on [ConnectionLens](https://gitlab.inria.fr/cedar/connection-lens) (the `core` package; ConnectionLens also has its own front-end in the `gui` package, which ConnectionStudio does not need).
PathWays, Abstra, and ConnectionLens are Java projects whose dependencies (on other libraries, and on each other) are handled by [Maven](https://maven.apache.org/).

The data management functionalities of ConnectionStudio are also coded in Java, as a set of servlets. Thus, for the Java part, ConnectionStudio is also a Maven-based project.
As such, it declares (in the `pom.xml` file) a dependency on PathWays:

```xml
    <dependency>
      <groupId>fr.inria.cedar.pathways</groupId>
      <artifactId>pathways-core</artifactId>
      <version>1.0-SNAPSHOT</version>
    </dependency>
```

As usual in Maven, given that PathWays depends on Abstra, which depends on ConnectionLens, the Studio's dependency on PathWays is all we need to declare.

When changes are made on an underlying project (ConnectionLens-core, Abstra, or PathWays):

### 3.1. Re-deploy the respective project

To deploy a project:

#### 3.1.1 In a repo where that project has been cloned, pull the latest changes

```bash
cd /path/to/myDependency
git pull
```

#### 3.1.2 Then deploy it in our remote maven repository

```bash
cd core/
mvn deploy -DskipTests
```

#### 3.1.3 Each project depending on it must be re-built and re-deployed

- If PathWays alone is modified, re-deploying PathWays suffices
- If Abstra is modified, re-deploy Abstra, then PathWays
- If ConnectionLens core is modified, re-deploy in this order: ConnectionLens-core, Abstra, then PathWays

### 3.2. Re-build ConnectionStudio's web services

Type `bash bin/deploy` to recompile ConnectionStudio's java code, generate `webservices.war` based on `webservices/pom.xml`, and deploy this war in the Web server (e.g. Tomcat)

### 3.3. Re-build and deploy ConnectionStudio's front end

Type `bash bin/deploy.sh --react` to deploy ConnectionStudio's front end to generate `connectionstudio.war` baed on `front/pom.xml`, and deploy this war in the Web server.

### 3.4 How to modify the project settings

Project parameters are persisted in the file [parameter.settings](/lib/parameter.settings).

#### 3.4.1 Persist parameters

If you need to create a new parameter or make a parameter value persistent (parameter that are needed to make the Studio work), modify [parameter.settings](/lib/parameter.settings) and run :

- To deploy the webservices into tomcat

```bash
bash bin/deploy.sh --configure
```

- To deploy the **webservices & the front** into tomcat (e.g access the application from `http://localhost:8080/connectionstudio`)

```bash
bash bin/deploy.sh --configure --react
```

#### 3.4.2 Modify/Update parameters

If you need to modify parameter value that don't need to be persistent (to test/debug), modify [local.settings](webservices/WebContent/WEB-INF/local.settings) and run :

- To deploy the webservices into tomcat

```bash
bash bin/deploy.sh
```

- To deploy the **webservices & the front** into tomcat (e.g access the application from `http://localhost:8080/connectionstudio`)

```bash
bash bin/deploy.sh --react
```

