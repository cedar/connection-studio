# THESE SCRIPTS ARE DEVELOPED IN PDF-INTEGRATION PROJECT

# In[]:

# IMPORTS

import os, sys

import string
import numpy as np
import pandas as pd
from statistics import mean

import rdflib
from rdflib.namespace import RDF
from utils import table_identifier

PDFTABLEXTR = rdflib.Namespace("http://tableXtr.pdf/")


# FUNCTIONS

## SUBFUNCTIONS

## MAIN FUNCTIONS


def add_cells_rdf(directory_pdf, name_pdf, index_graph, df, g, i_start, j_start):
    """
    
    adds cells to the RDF graph differentiating between 
    * datacells (triples with type and value)
    * headercells (X or Y, triples with type and value)
    
    @var name_pdf [string] name of original pdf
    @var index_graph [int] number of the table in the original PDF
    @var df [dataframe] 
    @var g [rdflib graph]
    @var i_start [int] start of datacells before YHeaderCells
    @var j_start [int] start of datacells before XHeaderCells

    """

    cell_predicates = [PDFTABLEXTR.value, RDF.type]
    k, l = df.shape

    for i in range(i_start, k):

        # ADDING NON-EMPTY HEADER ROW CELLS

        for j in range(0, j_start):
            if not pd.isnull(df.iloc[i, j]):
                header_values = [rdflib.Literal(df.iloc[i, j]), PDFTABLEXTR.HeaderXCell]
                for s in range(len(header_values)):
                    g.add((
                        PDFTABLEXTR['HeaderXCell-{}-{}-table{}'.format(i, j, index_graph)],
                        cell_predicates[s],
                        header_values[s]
                    ))

        # ADDING NON-EMPTY DATA CELLS

        for j in range(j_start, l):
            if not pd.isnull(df.iloc[i, j]):
                data_values = [rdflib.Literal(df.iloc[i, j]), PDFTABLEXTR.DataCell]
                for s in range(len(data_values)):
                    g.add((
                        PDFTABLEXTR['DataCell-{}-{}-table{}'.format(i, j, index_graph)],
                        cell_predicates[s],
                        data_values[s]
                    ))

    # ADDING NON-EMPTY HEADER COLUMN CELLS

    for i in range(0, i_start):
        for j in range(l):
            if not pd.isnull(df.iloc[i, j]):
                header_values = [rdflib.Literal(df.iloc[i, j]), PDFTABLEXTR.HeaderYCell]
                for s in range(len(header_values)):
                    g.add((
                        PDFTABLEXTR['HeaderYCell-{}-{}-table{}'.format(i, j, index_graph)],
                        cell_predicates[s],
                        header_values[s]
                    ))

    # LINK TO TABLE

    g.add((
        PDFTABLEXTR['HeaderYCell-0-0-table{}'.format(index_graph)],
        PDFTABLEXTR.HeaderInto,
        PDFTABLEXTR[table_identifier(directory_pdf, index_graph, name_pdf)]
    ))


def add_closeness_rdf(index_graph, df, g, i_start, j_start):
    """
    
    adds closest cell to the RDF graph differentiating between 
    * datacells (triples with closest X & Y Cell)
    * headercells (X or Y, triples with aggregated cell)
    
    @var index_graph [int] number of the table in the original PDF
    @var df [dataframe] 
    @var g [rdflib graph]
    @var i_start [int] start of datacells before YHeaderCells
    @var j_start [int] start of datacells before XHeaderCells

    
    """

    data_predicates = [PDFTABLEXTR.closestXHeader, PDFTABLEXTR.closestYHeader]
    k, l = df.shape

    # ADDING NON-EMPTY DATA CELLS

    for i in range(i_start, k):

        for j in range(j_start, l):
            if not pd.isnull(df.iloc[i, j]):
                i_header_x = i
                i_header_y = i_start - 1
                j_header_x = j_start - 1
                j_header_y = j
                while i_header_x >= 0 and pd.isnull(df.iloc[i_header_x, j_header_x]):
                    i_header_x = i_header_x - 1
                while j_header_y >= 0 and pd.isnull(df.iloc[i_header_y, j_header_y]):
                    j_header_y = j_header_y - 1
                i_header_x = max(i_header_x, 0)
                j_header_y = max(j_header_y, 0)
                data_values = [PDFTABLEXTR['HeaderXCell-{}-{}-table{}'.format(i_header_x, j_header_x, index_graph)],
                               PDFTABLEXTR['HeaderYCell-{}-{}-table{}'.format(i_header_y, j_header_y, index_graph)]]
                for s in range(len(data_values)):
                    g.add((
                        PDFTABLEXTR['DataCell-{}-{}-table{}'.format(i, j, index_graph)],
                        data_predicates[s],
                        data_values[s]
                    ))

    # ADDING NON-EMPTY HEADER ROW CELLS    

    if j_start > 1:
        for i in reversed(range(i_start, k)):
            for j in reversed(range(j_start)):
                i_header = i
                j_header = j
                if j_header > 0 and not pd.isnull(df.iloc[i, j]):
                    j_header = j_header - 1
                    while i_header > 0 and pd.isnull(df.iloc[i_header, j_header]):
                        i_header = i_header - 1
                    if not pd.isnull(df.iloc[i_header, j_header]):
                        g.add((
                            PDFTABLEXTR['HeaderXCell-{}-{}-table{}'.format(i, j, index_graph)],
                            PDFTABLEXTR.aggregatedBy,
                            PDFTABLEXTR['HeaderXCell-{}-{}-table{}'.format(i_header, j_header, index_graph)]
                        ))

                        # ADDING NON-EMPTY HEADER COLUMN CELLS

    if i_start > 1:
        for i in reversed(range(i_start)):
            for j in reversed(range(l)):
                i_header = i
                j_header = j
                if i_header > 0 and not pd.isnull(df.iloc[i, j]):
                    i_header = i_header - 1
                    while j_header > 0 and pd.isnull(df.iloc[i_header, j_header]):
                        j_header = j_header - 1
                    if not pd.isnull(df.iloc[i_header, j_header]):
                        g.add((
                            PDFTABLEXTR['HeaderYCell-{}-{}-table{}'.format(i, j, index_graph)],
                            PDFTABLEXTR.aggregatedBy,
                            PDFTABLEXTR['HeaderYCell-{}-{}-table{}'.format(i_header, j_header, index_graph)]
                        ))

# %%
