# Description des objectifs du projet

Ce code a été créé dans le cadre du projet Connection Lens, il sert deux objectifs :

* Effectuer des opérations de nettoyage sur les fichiers csv produits en sortie de requête sur CL
* Effectuer une opération d'identification des localisations présentent dans les colonnes et stocker cette information dans une colonne "localisation"

Pour fonctionner ce code prend en entrée deux tableurs csv composés des colonnes suivantes :
- csv CL :
  - string (format str) : contenant les chaînes de caractères d'origine
  - entité (format str) : contenant les entités identifiées dans les chaînes de caractères d'origine et extraites de celles-ci  
  Cette source doit être indiquée en paramètre de la commande qui exécute le code Python.

- csv df_locs :
  Qui contient les colonnes "name"	et	"alternatenames" qui composeront le dataframe qui permettra de générer deux dictionnaires 
  (`dico_1` et `dico_2`) permettant de trouver des localisations. 
  Cette source est statique. Elle est directement récupérée et traitée dans le code Python.

Après lancement du code, l'output correspond à un dataframe (tableau) composé des colonnes suivantes :

- csv CL :
  - string (format str) : contenant les chaînes de caractères d'origine
  - entité (format str) : contenant les entités nettoyées (plus de stop word, accents, url...)
  - localisation (format str) : contenant les localisations qui ont été repérées soit dans la colonne "string" soit dans la colonne "entité" (jamais dans les deux en même temps).

# Spécification technique

- Vérifier la version de python utilisée (il faut que ce soit Python 3.9.16) : `python --version`

- Installer le projet : `pip install -r requirements.txt`

# Packaging
```
nltk==3.8.1
unidecode==1.3.6
pickle-mixin==1.0.2

regex==2022.10.31
pandas==1.5.3
numpy==1.22.4
scikit-learn==1.2.2
pathlib==1.0.1   #à rajouter dans le requirements.txt de CL
```

# Exécution du code :


Pour traiter un fichier csv du type de "csv CL" il faut exécuter une commande du type : `python3 -m path_to_main/main path_to_csv_CL`


-----

English below :

# Description of the objectives

This code was created as part of the Connection Lens project, it serves two purposes:

- Cleanup operations on query output csv files of CL
- Identify the locations present in the columns and store this information in a "location" column

This code takes as input two csv tables composed of the following columns:
- csv df:
  - string (format str): containing the original character strings
  - entity (format str): containing the entities contained in the original character strings and extracted from them
This source must be indicated as a parameter of the command line which execute the python code.

- csv df_locs:
  Which contains the "name" and "alternatenames" columns that make up the dataframe that will generate two dictionaries 
  (dico_1 and dico_2) is used to find locations. 
  This source is static. It is directly retrieved and processed in python code.

After running the code, the output is a dataframe (table) consisting of the following columns:

- csv df:
  - string (format str): containing the original character strings
  - entity (str format): containing the cleaned entities (no more stop words, accents, url...)
  - location (str format): containing the locations that have been identified either in the column "string" is in the "entity" column (never in both at the same time).


# Technical specification

- Check the version of python used (it must be Python 3.9.16): `python --version`

- Install the project: `pip install -r requirements.tx`

# Packaging

```
nltk==3.8.1
unidecode==1.3.6
pickle-mixin==1.0.2

regex==2022.10.31
pandas==1.5.3
numpy==1.22.4
scikit-learn==1.2.2

pathlib==1.0.1   # to be added at the end of CL requirements.txt
```

# Running the code:

To process a csv file like the "csv CL", you must execute a command of the type: `python3 -m path_to_main/main path_to_csv_CL`
