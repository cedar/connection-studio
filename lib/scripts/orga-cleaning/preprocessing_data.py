""" Toutes les étapes ci-dessous concernent le preprocessing des données
avant de pouvoir lancer les étapes d'identification des localisations.

En entrée nous avons un tableau de deux colonnes :
    • String (str) : qui est la string d'origine
    • Entite (str) : qui est l'entité qui a été identifiée dans la string d'origine.

    exemple d'initialisation de la fonction qui les appelle toute :
        df_test_11['string_list'], df_test_11['entite'] = preformating_columns(df_test_11['string'], df_test_11['entite'], 0, regex)
    """

from unidecode import unidecode
import re
import pandas as pd
import numpy as np
import nltk
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('words')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

english_vocab = set(w.lower() for w in nltk.corpus.words.words())


def lower_columns(df_column):
  """Cette fonction transforme la colonne du dataframe en .lower().
  Cette fonction est appelée dans la fonction qui enlève les accents."""

  for k in range(len(df_column)):
    df_column[k] = df_column[k].lower()

  return df_column

### Création d'une fonction de nettoyage grâce au regex pour isoler des textes importants parmis une string contenant beaucoupd de bruit.

def texte_important_to_conserve(df_column, min_lenght, regex = r"(.*$)"):
  """Fonction qui identifie les textes importants à conserver dans des chaines de caractères moins propres ou trop longues grâce à des 'regex'.
    Elle return une version cleané du texte entré en input dans une nouvelle colonne.

     Cette fonction prend en input :

     • df_column : une colonne de dataframe
     • min_lenght : un integer qui par défaut est égal à 0, mais qui peut être customiser.
                    il correspond à la taille de la chaine de caractère minimum
                    à partir de laquelle on applique le nettoyage.
     • regex:

     Attention : par défaut regex doit être écrite regex = r'(.*$)' mais accepte d'être personnalisé.

        - L'utilisateur veut nettoyer du texte en utilisant un seul "start_with"
              c'est à dire un seul mot qu'il a identifié comme étant un début de section de démarrage
              de paragraphe/texte importants à ses yeux. On va lui demander d'entrer
              la regex suivante comme paramètre "start_with" en le guidant sur la rédaction.

              Exemple :
              mot identifié = "This work"
              regex = r"This work(.*$)"

        - L'utilisateur veut nettoyer avec plusieurs "start_with". Il devra séparer ses requêtes avec le signe "|"

              Exemple :
              mots identifiés = "banane" et "poire"
              regex = r"banane(.*$)|poire(.*$)"

              Utilisation de regex :

                Exemple : "This work(.*$)" permet de rechercher une chaîne de caractères qui commence
                          par "This work" et se termine par n'importe quel autre caractère.
                        . signifie n'importe quel caractère
                        * prend en compte le cas où il a zéro ou plusieurs occurrences
                        $ sert à capturer tous les caractères suivants jusqu'à la fin de la ligne"""

  texte_to_conserve = []

  for k in range(len(df_column)):
    if len(df_column[k]) > min_lenght:     #si la taille de la chaine affichée > min_lenght

    ## Ici on commence par isoler les "start_with"

      texte_start_with = re.search(regex, df_column[k], re.DOTALL)

        #boucle qui permet de ne pas prendre d'erreur si la condition n'est pas remplie "None"
      if texte_start_with:
        texte_to_conserve.append(texte_start_with.group())

      else:
        texte_to_conserve.append(df_column[k])

    else: # ce else répond à la condition de taille si < min_lenght
      texte_to_conserve.append(df_column[k])

  df_column =  texte_to_conserve

  return df_column

### Création d'une fonction de nettoyage grâce au regex pour supprimer l'apparition d'uri dans les textes.

def sup_uri(df_column, min_lenght):

  """Fonction de cleaning qui supprime les uri. Cette fonction prend en input :

     • df_column : une colonne de dataframe
     • min_lenght : un integer qui par défaut est égal à 0, mais qui peut être customiser.
                    il correspond à la taille de la chaine de caractère minimum
                    à partir de laquelle on applique le nettoyage.

  Elle return une version cleané du texte entré en input dans une nouvelle colonne"""

  texte_cleaned_uri = []

  for k in range(len(df_column)):

    if len(df_column[k]) > min_lenght :

      if 'http' in df_column[k]:
        new_sentence_without_http = re.sub(r'http\S+', '', df_column[k])
        texte_cleaned_uri.append(new_sentence_without_http)

      elif 'www' in df_column[k]:
        new_sentence_without_www = re.sub(r'www\S+', '', df_column[k])
        texte_cleaned_uri.append(new_sentence_without_www)

      else:
        texte_cleaned_uri.append(df_column[k])

    else:
      texte_cleaned_uri.append(df_column[k])

  df_column = texte_cleaned_uri

  return df_column

### Création d'une fonction de nettoyage grâce au regex pour supprimer l'apparition de mail dans les textes.

def sup_mail(df_column, min_lenght):

  """Fonction de cleaning qui supprime les mails. Cette fonction prend en input :

     • df_column : une colonne de dataframe
     • min_lenght : un integer qui par défaut est égal à 0, mais qui peut être customiser.
                    il correspond à la taille de la chaine de caractère minimum
                    à partir de laquelle on applique le nettoyage.

  Elle return une version cleané du texte entré en input dans une nouvelle colonne"""

  texte_cleaned_mail = []

  for k in range(len(df_column)):

    if len(df_column[k]) > min_lenght :

      if '@' in df_column[k]:
        new_sentence_without_mail = re.sub(r'\S+@\S+', '', df_column[k])
        texte_cleaned_mail.append(new_sentence_without_mail)

      else:
        texte_cleaned_mail.append(df_column[k])

    else:
      texte_cleaned_mail.append(df_column[k])

  df_column = texte_cleaned_mail

  return df_column

### Création d'une fonction de nettoyage grâce au regex pour supprimer l'apparition d'éléments de bibliographie comme des pages

def sup_biblio_pages (df_column, min_lenght):
  """Fonction de cleaning qui supprime :
      - les numéros de pages séparés par des tirets. Par exemple : 375-379 ou 375-379. (suivit d'un point)
      - les numéros entre parenthèses comme (4) précédés et suivit d'un espace (\s)
        ou soit l'un ou l'autre, ou coller à du texte comme par exemple :"navigation(56)"

      Cette fonction prend en input :

     • df_column : une colonne de dataframe
     • min_lenght : un integer qui par défaut est égal à 0, mais qui peut être customiser.
                    il correspond à la taille de la chaine de caractère minimum
                    à partir de laquelle on applique le nettoyage.

    Elle return une version cleané du texte entré en input dans une nouvelle colonne"""

  clean_pages = []

  for k in range(len(df_column)):

    if len(df_column[k]) > min_lenght :

      new_sentence_clean = re.sub(r'\d+-\d+|\d+-\d+.|\s(.\d+.)\s|\s(.\d+.)|(.\d+.)\s|(.\d+.)', ' ', df_column[k])


      if new_sentence_clean:
        clean_pages.append(new_sentence_clean)

      else:
        clean_pages.append(df_column[k])

    else:
      clean_pages.append(df_column[k])

  df_column = clean_pages

  return df_column

### Création d'une fonction de nettoyage grâce au regex pour supprimer l'apparition d'années

def sup_years (df_column, min_lenght):
  """Fonction de cleaning qui supprime les années commençant par 19 ou 20. Par exemple 1920 ou 2023.

     Cette fonction prend en input :

     • df_column : une colonne de dataframe
     • min_lenght : un integer qui par défaut est égal à 0, mais qui peut être customiser.
                    il correspond à la taille de la chaine de caractère minimum
                    à partir de laquelle on applique le nettoyage.

    Elle return une version cleané du texte entré en input dans une nouvelle colonne"""

  clean_bib_year = []

  for k in range(len(df_column)):

    if len(df_column[k]) > min_lenght :

      new_sentence_clean = re.sub(r'(?:19|20)\d{2}|\((?:\(19|20)\d{2}\)', '', df_column[k])

      if new_sentence_clean:
        clean_bib_year.append(new_sentence_clean)

      else:
        clean_bib_year.append(df_column[k])

    else:
      clean_bib_year.append(df_column[k])

  df_column = clean_bib_year

  return df_column

### Création d'une fonction de nettoyage qui permet de supprimer l'apparition d'accents

def remove_accents(df_column):

    """Cette fonction permet de retirer les accents sur les lettres des chaines
       de caractère des textes présents dans la colonne "column" du "df".

      Cette fonction prend en input :

      • df : le nom du dataframe
      • column : la colonne concernée par ce nettoyage

      Elle return une version cleané du texte entré en input dans une nouvelle
      colonne où les textes ont été transformé en minuscule.

      N.B : cette fonction servira également au moment du nettoyage du df_locs_names qui
       permettra de créer la fonction de localisation"""

    accents = {"à" : "a", "â" : "a", "ä" : "a", "å" : "a", "á" : "a", "ã" : "a", "é" : "e", "è": "e",
              "ê": "e", "ë": "e", "î" : "i", "ï": "i", "í": "i", "ô" : "o",
              "ö" : "o", "ó" : "o", "ù" : "u", "ú" : "u", "û": "u", "ü": "u", "ÿ" : "y",
              "ý": "y", 'č' : "c", 'ž': 'z', "š" : "s"}

    df_lower_column = lower_columns(df_column)

    for key, value in accents.items():
      for k in range(len(df_lower_column)):
        df_lower_column[k] = df_lower_column[k].replace(key, value)

    return df_lower_column

### Création d'une fonction de nettoyage qui permet de supprimer l'apparition de césure avec un espace après le tiret

def remove_hypenation(df_column):

  """Cette fonction permets d'enlever les problème des césures dans les textes.
  On utilise la regex -\s+ pour capturercle caractère avant le - et après l'espace.

  Exemple : Color- ado devient Colorado

  Attention s'il n'y a pas d'espace la regex ne fonctionne pas.
  Exemple : poir-e reste poir-e

  Cette fonction prend en input :

      • df : le nom du dataframe
      • column : la colonne concernée par ce nettoyage

      Elle return une version cleané du texte entré en input dans une nouvelle
      colonne."""

  for k in range(len(df_column)):
    df_column[k] = re.sub(r"-\s+","", df_column[k])

  return df_column

### Création d'une fonction de nettoyage qui permet de supprimer l'apparition de ponctuation

def without_ponctuation(df_column):
  """Cette fonction retire toute la ponctuation des textes de la colonne.
    On utilise volontairement une regex et pas la méthode 'string.punctuation'
    pour pouvoir exclure de la suppression les "-" séparateur dans les mots
    de localisation comme 'New-York' par exemple.

    La regex [^\w\s-] signifie tout caractère qui n'est pas une lettre, un chiffre,
    un espace ou un tiret ou une apostrophe simple (ex : d'italie).

    Cette fonction prend en input :

      • df : le nom du dataframe
      • column : la colonne concernée par ce nettoyage

      Elle return une version cleané du texte entré en input dans une nouvelle
      colonne."""

  texte_without_ponctuation = []

  for k in range(len(df_column)):
    texte_propre = re.sub(r"[^\w\s]", ' ',df_column[k])
    texte_without_ponctuation.append(texte_propre)


  df_column = texte_without_ponctuation

  return df_column

### Création d'une fonction de nettoyage qui permet de supprimer les stopwords

def enlever_stopwords(df_column):

    texte_without_stopwords = []

    # Récupère les stopwords de la langue anglaise
    stop_words_en = set(stopwords.words('english'))
    stop_words_fr = set(stopwords.words('french'))
    stop_words = stop_words_fr.union(stop_words_en)  # Fusionne les deux ensembles de stopwords

    # Sépare le texte en liste de mots
    for k in range(len(df_column)):
      liste_mots = word_tokenize(df_column[k])

      # Enlève les stopwords
      mots_without_stopwords = [mots for mots in liste_mots if mots.lower() not in stop_words]
      texte_propre = ' '.join(mots_without_stopwords)
      texte_without_stopwords.append(texte_propre)

    df_column = texte_without_stopwords

    return df_column

### Cette fonction appelle et applique toute les fonctions précédentes :

def texte_cleaning(df_column, min_lenght, regex):

  """Fonction de cleaning outlier qui appelle les autres fonction de cleaning :

  - texte_important_to_conserve
  - sup_uri'
  - sup_mail
  - sup_biblio_pages
  - sup_years
  - remove_acents
  - remove_hypenation
  - without_ponctuation
  - enlever_stopwords

     Cette fonction prend en input :

     • df_column : une colonne de dataframe
     • min_lenght : un integer qui par défaut est égal à 0, mais qui peut être customiser.
                    il correspond à la taille de la chaine de caractère minimum
                    à partir de laquelle on applique le nettoyage.
    • regex : Correspond à un des paramètres de la fonction "texte_important_to_conserve"

      - L'utilisateur veut nettoyer du texte en utilisant un seul "start_with"
              c'est à dire un seul mot qu'il a identifié comme étant un début de section de démarrage
              de paragraphe/texte importants à ses yeux. On va lui demander d'entrer
              la regex suivante comme paramètre "start_with" en le guidant sur la rédaction.

              Exemple :
              mot identifié = "This work"
              regex = r"This work(.*$)"

        - L'utilisateur veut nettoyer avec plusieurs "start_with". Il devra séparer ses requêtes avec le signe "|"

              Exemple :
              mots identifiés = "banane" et "poire"
              regex = r"banane(.*$)|poire(.*$)"

    Elle return une version cleané du texte entré en input dans une nouvelle colonne"""


# Etape 1 - Création d'une première variable de stockage "texte_to_conserve" pour appeler la fonction texte_important_to_conserve. L'objectif est de sélectionner les passages qui nous intéressent

  texte_to_conserve = texte_important_to_conserve(df_column, min_lenght, regex)
  df_column = texte_to_conserve

# Etape 2 - Si uri dans "texte_to_conserve" alors supprimer uri

  texte_cleaned_uri = sup_uri(df_column, min_lenght)
  df_column = texte_cleaned_uri

# Etape 3 - Si mail dans "texte_cleaned_uri" alors supprime mail

  texte_cleaned_mail = sup_mail(df_column, min_lenght)
  df_column = texte_cleaned_mail

# Etape 4 - Si éléments de bibliographie type textes comportant des numéros de pages (chiffres avec des tirets) dans "texte_cleaned_mail" alors on supprime et garde le reste

  clean_pages = sup_biblio_pages(df_column, min_lenght)
  df_column = clean_pages

# Etape 5 - Si éléments de bibliographie type textes comportant des années dans "clean_pages" alors on supprime et garde le reste

  clean_bib_year = sup_years(df_column, min_lenght)
  df_column = clean_bib_year

# Etape 6 : On enlève les accents. Cette fonction s'applique quoi qu'il arrive car il n'y a pas de min_lenght.
  clean_accent = remove_accents(df_column)
  df_column = clean_accent

# Etape 7 : On enlève les césures. Cette fonction s'applique quoi qu'il arrive car il n'y a pas de min_lenght.
  clean_hypenation = remove_hypenation(df_column)
  df_column = clean_hypenation

# Etape 8 : On enlève la ponctuation. Cette fonction s'applique quoi qu'il arrive car il n'y a pas de min_lenght.
  clean_ponctuation = without_ponctuation(df_column)
  df_column = clean_ponctuation

# Etape 9 : On enlève les stopwords. Cette fonction s'applique quoi qu'il arrive car il n'y a pas de min_lenght.
  clean_stopwords = enlever_stopwords(df_column)
  df_column = clean_stopwords

  return df_column

### Cette fonction permet de transformer une colonne contenant des strings en colonne contenant des listes de mots

def list_entite(df_column):
  """Cette fonction à pour but de transformer la colonne 'entité' et 'string'
  contenant des string en colonne contenant une liste de chaine de caractères
  (des listes de mots)"""

  for k in range(len(df_column)):
    df_column[k] = df_column[k].split()

  return df_column

### C'est la fonction de préformating qui les appelle toutes

def preformating_columns(df_column_string, df_column_entite, min_lenght, regex = r'(.*$)'):
  """Cette fonction permet de transformer les colonnes d'input au format souhaité
  pour y découvrir des localisations partie deux de pipelines.
  Elle va d'abord nettoyé puis transformer les chaines de caractères contenues dans
  les colonnes en liste de mots.

  Attention : par défaut regex doit être écrite regex = r'(.*$)' mais accepte d'être personnalisé.

  Attention : il ne faut pas relancer le résultat une seconde fois sinon ça fait une erreur"""

  if regex == r'(.*$)' :
    # Etape 1 : cleaning
    df_column_string = texte_cleaning(df_column_string, min_lenght, regex)
    df_column_entite = texte_cleaning(df_column_entite, min_lenght, regex)

    # Etape 2 : splitting en list
    df_column_string = list_entite(df_column_string)
    df_column_entite = list_entite(df_column_entite)

  else :
    # Etape 1 : cleaning
    df_column_string = texte_cleaning(df_column_string, min_lenght, regex)
    df_column_entite = texte_cleaning(df_column_entite, min_lenght, regex)

    # Etape 2 : splitting en list
    df_column_string = list_entite(df_column_string)
    df_column_entite = list_entite(df_column_entite)


  return df_column_string, df_column_entite
