import json
import os

base_path = "/Users/felipealvesdias/Documents/study"
file_name = "nosdeputes.fr_deputes_en_mandat_2019-04-04_part2"


def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n):
        yield l[i:i + n]


with open(os.path.join(base_path, file_name + ".json")) as json_file:
    data = json.load(json_file)
    deputes = data['deputes']
    chunks = list(divide_chunks(deputes, int(len(deputes)/2)))
    counter = 1
    for chunk in chunks:
        with open(os.path.join(base_path, file_name + "_" + str(counter) + ".json"), 'w') as outfile:
            json.dump(data, outfile)
        counter += 1
