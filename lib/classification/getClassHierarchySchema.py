import json

with open('class-hierarchy-schemaorg-original.json', 'r') as f:
    data = json.load(f)

data = data["@graph"]

new_data = {}
for element in data:
    for key in element:
        if key == "rdfs:subClassOf":
            if isinstance(element[key], dict):
                new_data[str(element["@id"])] = [str(element[key]["@id"])]
            elif isinstance(element[key], list):
                new_data[str(element["@id"])] = []
                for i in range(len(element[key])):
                    new_data[str(element["@id"])].append(str(element[key][i]["@id"]))

print(json.dumps(new_data, indent=4))