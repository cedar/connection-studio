PYTHONPath = /python
#
# Database:
# The following are the setting to access the master database, where the system central information
# are stored.
#
# For now, this is the only supported vendor
RDBMSType = POSTGRESQL
# hostname where the DB is run
RDBMSHost = localhost
RDBMSPort = 5432
RDBMSUser = kwsearch                     
RDBMSPassword =
# database name
RDBMSDBName = cl_generated_100
#
# Language 
#
# The location where the TreeTagger library is installed
treetagger_home=/opt/treetagger
# The default locale used through the execution of the system
default_locale=fr
#
# Caches
#
# The number of nodes to keep in cache
node_cache=5000
# The number of edges to keep in cache
edge_cache=5000
# The number of string-pair and similarity results to keep in cache
similarity_cache=50000
# The number of nodes for which the adjacency list in cache
adjacency_cache=5000
# The number of (similarity) candidates kept in cache
candidate_pairs_cache=10000
# The number of specificities kept in cache
specificity_cache=5000
#
# Similarities / Extraction
#
# Location on the local where to store caches (mainly extraction outputs)
cache_location=cache
# Threshold above which same as are considered relevant
similarity_threshold=.8
# Length above which Jaccard similarity will be used over the other
long_string_threshold=100
# Length over which Jaro/Lenvenstein similarity will be used over the other
short_string_threshold=15
# Number of updates to keep in memory before spilling to the database
# (1 mean sequential updates, <= 0 means all updates are kept in memory until spilling)
update_batch_size=10000
# If true, similarities are only compared between entities
entities_only=false
# If true, node with null values will be stored
store_null_values=false
# Number of keyword allowed to be missing in a match for an answer to be returned.
query_cover_threshold=0
# Possible values: PER_INSTANCE -> all value nodes are distinct
#                  PER_TYPE  -> all value nodes that share the same label and type coincide
#                  PER_VALUE -> all value nodes that share the same label coincide
value_atomicity=PER_TYPE
# Possible values: LENGTH -> compares pairs with similar length
#                  COMMON_WORD -> compares pairs that share a word
#                  COMMON_WORD_AND_LENGTH -> compares pairs satisfying either of the above
candidate_pairs_variant=COMMON_WORD_AND_LENGTH
# Used with LENGTH above to determine the length range
length_difference_ratio=.2
#
# P2B
#
# P2B search approach.
# Possible values: DEFAULT->Dijkstra-based enumeration of all shortest-path to each border
#                  DFS -> Depth-first search
#                  BFS -> Breadth-first search
p2b_method=DEFAULT
# P2B search heuristic, use with  p2b_method in {DFS, BFS}. Possible values: NONE, TIMEOUT, MAXEDGES
p2b_heuristic=NONE
# Addition comma-separated parameter to the above
p2b_params=
# Runs DFS/BFS using parallelization
p2b_threaded=false
# The following p2b_stopper_* can be used in combination
# Timeout (ms) on the whole P2B search (DEFAULT only)
p2b_stopper_timeout=-1
# Timeout (ms) for each border
p2b_stopper_timeout_per_border=100
# Timeout (ms) for each datasource reachable from the current datasource
p2b_stopper_timeout_per_source=1000
#p2b_stopper_cover_per_border=
#p2b_stopper_cover_per_source=
# Number of paths after which to stop
p2b_stopper_topk=-1
# Number of paths after which to stop per border
p2b_stopper_topk_per_border=-1
# Number of paths after which to stop per datasource reachable from the current datasource
p2b_stopper_topk_per_source=-1
# The following p2b_filter_* can be used in combination
# Only store paths below the given length
p2b_filter_path_max_length=-1
# Only store paths that end on a value node
p2b_filter_exclude_from_struct_nodes=true
# Only store paths that end on a structural node
p2b_filter_exclude_from_value_nodes=false
#
# Search
#
# If true, rdfConnection external library is used for RDF local search. Otherwise, a generic search is performed
use_rdfsummary_search=false
# Timeout (ms) after which to stop the search
search_stopper_timeout=30000
local_search_finder_timeout=1000
search_heuristic=DIRECT
global_search_algorithm=ALGO2
