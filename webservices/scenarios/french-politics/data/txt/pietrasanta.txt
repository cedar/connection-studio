Pietrasanta est une ville de la province de Lucques en Toscane (Italie).


== Culture ==
L'activité principale de Pietrasanta est axée sur l'art. Sept fonderies artistiques de bronze existent dans la ville. Des artistes de renommée mondiale viennent y travailler, ainsi Fernando Botero, Ivan Theimer, Cyril de La Patellière, Jean-Michel Folon, etc. En outre, le travail du marbre y est important, depuis les romains. Le marbre étant extrait des carrières de Carrare très proches.
On peut y voir plusieurs statues monumentales offertes à la ville par leur créateur Fernando Botero qui habite la ville avec sa femme Sophía Vári, également sculpteur.


== Administration ==


=== Hameaux ===
Capezzano Monte, Capriglia, Valdicastello, Crociale, Ponterosso, Marina di Pietrasanta (Fiumetto, Tonfano, Motrone, Focette), Vallecchia, Solaio, Vitoio, Castello, Strettoia, Centoquindici.


=== Communes limitrophes ===
Camaiore, Forte dei Marmi, Montignoso, Seravezza, Stazzema


=== Évolution démographique ===
Habitants recensés


== Jumelages ==
 Villeparisis (France)
 Écaussinnes (Belgique)
 Linköping (Suède)
 Montgomery (États-Unis) (Alabama)


== Voir aussi ==
Liste des villes italiennes de plus de 25 000 habitants


== Notes et références ==

 Portail de la Toscane