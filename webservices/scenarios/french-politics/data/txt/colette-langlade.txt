Colette Langlade, née le 20 juin 1956 à Sorges (Dordogne), est une femme politique française, membre du Parti socialiste.


== Biographie ==
Le 17 juin 2007, elle est élue suppléante aux côtés du député Michel Debet dans la troisième circonscription de la Dordogne. Elle travaille avec lui pendant plusieurs mois et est appelée à lui succéder quand celui-ci décède le 6 mars 2008.
Elle devient donc députée pour la XIIIe législature (2007-2012) le 7 mars 2008 et adhère le 21 mars suivant au groupe « Socialiste, Radical, Citoyen et divers gauche ». Elle est la première femme du département de la Dordogne à siéger au Parlement français.
Elle est réélue députée le 10 juin 2012, dès le premier tour, avec 51,74 % des voix.
En mars 2015, elle est élue conseillère départementale du canton de Thiviers en tandem avec Michel Karp.


== Activité parlementaire ==
Commission :
Membre de la commission des Affaires culturelles et de l'ÉducationGroupes d'études :
Présidente : Illettrisme
Vice-présidente : Langues régionales ; Droits de l'enfant et de l'adolescent et protection de la jeunesse
Membre : Artisanat et métiers d'art ; Famille et adoption ; Formation alternée en milieu rural ; Intégration des personnes handicapées ; Professions de santé et coordination sanitaire ; Santé à l'école ; Tourisme ; Trufficulture
Groupes d'amitié :
Présidente : Turkménistan
Vice-présidente : Maroc


== Mandats ==
Mandats locaux :
2001 - 2014 : Conseillère municipale à ThiviersMandats départementaux :
2008 - en cours : Conseillère générale puis départementale du canton de Thiviers
2011 - 2015 : Vice-présidente du conseil général de la Dordogne chargée des Affaires sociales : Enfance et Famille
Depuis 2015 : 1re vice-présidente du conseil départemental de la DordogneMandat nationaux :
2008 - 2012 : députée de la troisième circonscription de la Dordogne
2012 - 2017 : députée de la troisième circonscription de la Dordogne


== Notes et références ==


== Voir aussi ==


=== Article connexe ===
Liste des députés de la Dordogne


=== Liens externes ===
« Colette Langlade », sur Sycomore, base de données des députés de l'Assemblée nationale
Site de Colette Langlade à l'Assemblée nationale Portail de la politique française   Portail de la Dordogne