Marc Joulaud, né le 3 septembre 1967 à Mayenne (Mayenne), est un homme politique français du parti Les Républicains, maire de Sablé-sur-Sarthe et député européen.


== Biographie ==
Marc Joulaud est diplômé en droit, tout d'abord de l'Université du Maine, au Mans, où il obtient une licence en droit public, puis de l'Université Panthéon-Sorbonne. Il étudie ensuite à l'Institut d'études politiques de Paris où il se spécialise dans le domaine des collectivités locales (finances, urbanisme, aménagement du territoire…).
En 1992, il est recruté par François Fillon, alors maire de Sablé-sur-Sarthe, qui en fait son collaborateur à la mairie. C'est le début de la collaboration entre les deux hommes. Entre 1992 et 1995, il est assistant parlementaire de François Fillon. En 1995, il assiste son mentor devenu président du conseil général de la Sarthe et, en 1998, c'est tout naturellement qu'il suit à Nantes celui qui est devenu président du conseil régional des Pays de la Loire.
Il obtient son premier mandat politique en 2001 en étant élu conseiller municipal de Sablé-sur-Sarthe, sa ville de résidence. Il devient le premier adjoint de Pierre Touchard, le nouveau maire UMP qui assure la succession de François Fillon, parti se faire élire comme conseiller municipal dans la petite commune voisine de Solesmes. Il devient également vice-président de la communauté de communes de Sablé-sur-Sarthe chargé de l'aménagement de l'espace et trésorier du « Pays de la Vallée de la Sarthe ».
Le 9 juin 2002, il est élu comme suppléant du député de la 4e circonscription de la Sarthe, François Fillon dont il était l'assistant parlementaire, (avec 55,21 % des voix dès le premier tour) et fait son entrée comme député à l'Assemblée nationale à partir du 19 juillet 2002 à la suite de la nomination de ce dernier au gouvernement. Il siège ainsi au sein du groupe UMP pendant la quasi-totalité de la XIIe législature. À l'Assemblée nationale, il est membre de la commission de la Défense nationale et des Forces armées. Au sein de cette commission, il est membre de la mission de surveillance de l'exécution des crédits de la défense, chargée de veiller à la bonne application de la loi de programmation militaire. Il est également vice-président du groupe d'amitié avec la Slovénie.
Pour les élections législatives de 2007, il est à nouveau désigné suppléant de François Fillon dans la 4e circonscription de la Sarthe. Il est réélu le 10 juin 2007, en obtenant 53,40 % des voix dès le premier tour, et est confirmé à son poste de Premier ministre le 19 juin. Marc Joulaud redevient ainsi député le 20 juillet 2007 à sa place. Il est à nouveau membre du groupe UMP.
Le 9 mars 2008, il remporte les élections municipales de Sablé-sur-Sarthe dès le premier tour et le 14 mars, il est élu maire de la commune par le nouveau conseil municipal.
Candidat de l'UMP aux élections législatives de 2012, il obtient 31,67 % des suffrages exprimés au premier tour et accède au second tour du 17 juin 2012 : obtenant 40,55 % des suffrages, il est devancé par son adversaire Stéphane Le Foll qui l'emporte avec 59,45 %.
Le 28 septembre 2012, il est élu président de la Communauté de communes de Sablé-sur-Sarthe à la majorité absolue avec 56 voix, après la démission de François Fillon. 
De 2012 à 2014, Marc Joulaud exerce les fonctions d'assistant parlementaire du sénateur de la Sarthe Jean-Pierre Chauveau, lui-même ancien suppléant de François Fillon.
Depuis les élections européennes de 2014, Marc Joulaud est député européen de la circonscription Ouest. Il est membre du Parti populaire européen et fait partie de la commission Culture et de l'Éducation ainsi que Développement régional.
Il soutient François Fillon pour la primaire française de la droite et du centre de 2016.
Il parraine Laurent Wauquiez pour le congrès des Républicains de 2017, scrutin lors duquel est élu le président du parti.


== Affaire Penelope Fillon ==

Le Canard enchaîné du 25 janvier 2017 révèle que Penelope Fillon a été employée comme attachée parlementaire de Marc Joulaud de 2002 à 2007, après avoir été celle de son mari François Fillon de 1998 à 2002, avant que celui-ci ne cède son siège à son suppléant. 
Sur une enveloppe maximale de 9 561 € (valeur 2017) calculée en principe pour trois collaborateurs, le journal affirme que Penelope Fillon était rémunérée entre 6 900 € à 7 900 € bruts par mois par Marc Joulaud, contre 3 900 € par mois antérieurement par son mari (le maximum permis par la loi pour un membre de sa famille), alors que Marc Joulaud lui-même n'a été rémunéré que 1 169 € nets par mois en tant qu'assistant parlementaire du sénateur Jean-Pierre Chauveau entre 2012 et 2014.
Dans son article, Le Canard enchaîné dit qu’il n’a pas trouvé trace du travail de Penelope Fillon. Jeanne Robinson-Behre, ancienne collaboratrice de Marc Joulaud à l'Assemblée, déclare au journal satirique n’avoir jamais travaillé avec Penelope Fillon.
À la suite de ces révélations, le parquet national financier annonce le même jour l'ouverture d'une enquête préliminaire pour détournement de fonds publics, abus de biens sociaux et recel de ces délits. Marc Joulaud est auditionné par les enquêteurs de l'Office central de lutte contre la corruption et les infractions financières et fiscales le 1er février 2017.
Marc Joulaud a eu comme assistants parlementaires Jeanne Robinson-Behre et Igor Mitrofanoff. Jeanne Robinson-Behre a assuré n'avoir pas travaillé avec Penelope Fillon, ce qui selon elle « ne veut pas dire qu'elle ne travaillait pas ».
Le 24 mars 2017, Marc Joulaud est mis en examen pour « détournement de fonds publics » ,.


== Mandats ==


=== Député ===
19 juillet 2002 - 19 juin 2007 : député de la 4e circonscription de la Sarthe
20 juillet 2007 - 15 juin 2012 : député de la 4e circonscription de la Sarthe


=== Conseiller municipal / maire ===
19 mars 2001 - 9 mars 2008 : adjoint au maire de Sablé-sur-Sarthe
14 mars 2008 - 23 mars 2014 : maire de Sablé-sur-Sarthe
depuis le 28 mars 2014 : maire de Sablé-sur-Sarthe (réélu)


=== Communauté de communes ===
30 décembre 2001 - 16 mars 2008 : vice-président de la Communauté de communes de Sablé-sur-Sarthe
16 mars 2008 - 28 septembre 2012 : vice-président de la Communauté de communes de Sablé-sur-Sarthe
28 septembre 2012 - 17 avril 2014 : président de la Communauté de communes de Sablé-sur-Sarthe
Depuis le 17 avril 2014 : président de la Communauté de communes de Sablé-sur-Sarthe (réélu)


=== Député européen ===
Candidat lors des élections européennes de 2014 sur la liste UMP menée par Alain Cadec dans la circonscription Ouest. Cette liste, est l'une des trois seules listes UMP à devancer le FN lors de ces élections.
Au Parlement européen, il siège au sein du groupe du Parti populaire européen et est membre de la commission du Développement régional et de celle de la Culture et de l'Éducation. Il est également membre des commissions parlementaires mixtes UE-Mexique et UE-Sud Caucase et des délégations interparlementaires pour les relations avec la région Amérique Latine et Europe de l'Est.
Au sein de la commission du Développement régional, Marc Joulaud s'est notamment engagé sur la question de la simplification de la politique régionale ainsi que sur la politique urbaine, afin d'assurer une meilleure prise en compte des villes moyennes dans les politiques européennes. Il a été désigné rapporteur pour son groupe politique, sur des avis sur le budget 2015 de l'Union européenne et sur la lutte contre la fraude. 
Au sein de la commission de la Culture et de l'Éducation, Marc Joulaud est spécialisé sur les questions liées au numérique, et en particulier sur la réforme du droit d'auteur en cours dans le cadre de la mise en place du Marché unique numérique européen. Il a ainsi été désigné rapporteur pour le PPE, sur un avis sur les atteintes commerciales aux droits de propriété intellectuelle,  ainsi que sur un avis sur la mise en œuvre de la directive « InfoSoc » sur le droit d'auteur. Dans ses différents travaux, Marc Joulaud défend une approche qui se veut équilibrée entre une juste rémunération des auteurs et les attentes des consommateurs pour une plus grande disponibilité des contenus sur tout le territoire européen.
Depuis le début de son mandat, Marc Joulaud publie tous les mois la liste des lobbys que son équipe et lui rencontrent.


=== Association des maires et adjoints de la Sarthe ===
depuis le 18 octobre 2008 : président (réélu en 2014)


== Notes et références ==


=== Notes ===


=== Références ===


== Voir aussi ==


=== Articles connexes ===
Communauté de communes de Sablé-sur-Sarthe
Liste des députés de la Sarthe
Quatrième circonscription de la Sarthe
Sablé-sur-Sarthe


=== Liens externes ===
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation
Ressources relatives à la vie publique : Assemblée nationale • Parlement européen • Base Sycomore
Site officiel Portail de la politique française   Portail de la Sarthe   Portail de l’Union européenne   Portail du gaullisme