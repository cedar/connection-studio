Paul Jeanneteau, né le 14 octobre 1957 à Angers, est un homme politique français, membre des Républicains.


== Biographie ==
Pharmacien installé à Champigné en Maine-et-Loire, il fait son entrée en politique en devenant maire de cette commune en 1995. Il est réélu pour un deuxième mandat en mars 2001. C'est aussi à cette date qu'il fait son entrée au conseil général de Maine-et-Loire en étant élu dans le canton de Châteauneuf-sur-Sarthe. Il en devient vice-président en 2004.
Pour les élections législatives de 2007, il est suppléant de Roselyne Bachelot, la députée élue en 2002 dans la première circonscription de Maine-et-Loire ; René Bouin, son suppléant lors de la XIIe législature ne se représentant pas. Celle-ci est réélue le 17 juin 2007, en obtenant 54,67 % des voix au second tour. Elle est nommée dans le gouvernement François Fillon (2) le 19 juin. Paul Jeanneteau est ainsi devenu député le 20 juillet 2007 en remplacement. Il siège au sein du groupe UMP et est membre de la commission des affaires sociales.


== Mandats et fonctions ==
DéputéDu 20 juillet 2007 au 19 juin 2012 : député de la première circonscription de Maine-et-Loire, non réélu.Conseiller généralDu 22 mars 2001 au 1er avril 2015 : conseiller général de Maine-et-Loire, élu du canton de Châteauneuf-sur-Sarthe
D'avril 2001 à avril 2008 : vice-président du conseil général de Maine-et-Loire chargé de l'aménagement du territoire, du développement économique et du tourisme.Conseiller municipal / maireDepuis le 18 juin 1995 : maire de ChampignéMandats intercommunaux1er janvier 1997 - mars 2008 : vice-président de la communauté de communes du Haut-AnjouAutres fonctionsDepuis septembre 2004 : président du comité d'expansion économique de Maine-et-Loire
2009-2011 : président du Conseil national des économies régionales (CNER)


=== Liens internes ===
Liste des députés de Maine-et-Loire
Députés de la treizième législature par circonscription
Députés de la XIIIe législature


== Lien externe ==
son site personnel


=== Sources ===
Fiche d'identité sur le site de l'Assemblée nationale
Le Monde des 12 et 19 juin 2007 Portail de la politique française   Portail de l’Anjou et de Maine-et-Loire   Portail du gaullisme