Robert Rochefort, né le 19 septembre 1955 à Paris, est un économiste et homme politique français, député européen, élu dans la circonscription Sud-Ouest. Il est également vice-président et membre du bureau exécutif national du Mouvement démocrate jusqu'en septembre 2016.


== Biographie ==


=== Formation ===
Ancien élève de l'ENSAE, Robert Rochefort est diplômé d’études supérieures en sciences économiques et maître en sciences mathématiques.


=== Carrière professionnelle ===


==== Direction des statistiques de l'Assurance maladie (1981-1987) ====
Robert Rochefort a notamment dirigé le département des statistiques de la Caisse nationale de l'assurance maladie des travailleurs salariés.
En parallèle, de 1985 à 1986, il occupe les fonctions de directeur délégué du Centre de recherche, d’études et de documentation en économie de la santé (CREDES).


==== Directeur du CREDOC (1987-2009) ====
Robert Rochefort fut directeur du CREDOC, le Centre de recherche pour l'étude et l'observation des conditions de vie depuis 1987, puis directeur général depuis 1995, fonction qu'il quitte en 2009 pour se consacrer à son mandat européen.


==== Fonctions au sein d'entités publiques ====
Il est membre de mars 2006 à juin 2009 du Conseil d'analyse économique auprès du Premier ministre.
Il est également membre de plusieurs comités et conseils dont le Conseil scientifique des statistiques du Haut Conseil à l'intégration et le Conseil d'éthique publicitaire du BVP, vice‐président de la Commission des comptes nationaux du commerce et président de la formation Démographie, conditions de vie du Conseil national de l'information statistique.


==== Enseignement et conférences ====
Il enseigne dans plusieurs universités et grandes écoles. Il a été chargé de cours à l’ENA. Il est conférencier dans de nombreuses entreprises.


==== Mandats d'administrateur de sociétés de droit privé ====
Robert Rochefort est administrateur d'Eurotunnel et de Cetelem.


==== Membre du conseil d'administration de la Croix-Rouge française ====
Robert Rochefort a été membre du conseil d'administration de la Croix-Rouge française jusqu'en juin 2009.


==== Chroniques et interventions médiatiques ====
Il a été chroniqueur sur Europe 1 et sur Radio Bleue. Actuellement[Quand ?], il tient une chronique dans le quotidien catholique La Croix et le magazine économique Challenges. Il intervient régulièrement dans des émissions de débats tels C dans l'air sur France 5 ou On refait le monde sur RTL.


=== Politique ===


==== Député européen ====
Désigné tête de liste dans la circonscription Sud-Ouest lors des élections européennes de juin 2009, il est élu député européen (MoDem) le 7 juin 2009.
Robert Rochefort est membre de la commission parlementaire Marché intérieur et protection du consommateur. Il est également membre suppléant de la commission parlementaire Culture et éducation.
Il est par ailleurs vice-président de la Délégation pour les relations de l'Union européenne avec les pays du Maghreb et membre de l'Assemblée parlementaire euro-méditerranéenne. 

Il emploie deux salariés du service communication du parti sous un statut d’assistant local. Toutefois, selon France Info, l'un et l'autre ne travaillaient qu'à Paris et auraient donc bénéficier d'emplois fictifs pour que le Parlement européen prenne en charge leurs rémunérations. 


==== Dirigeant du MoDem ====
Le 29 juillet 2009, il devient membre du Bureau exécutif national du MoDem, dont il est ensuite vice-président du 27 mars 2010 au 6 septembre 2016,.


=== Affaire judiciaire ===
Au cours de l'été 2016, Robert Rochefort est signalé par un agent de sécurité du magasin Castorama de Vélizy-Villacoublay pour, selon le journal Libération, s'être masturbé « à proximité d'enfants ». Il est interpellé le 31 août pour exhibition sexuelle. Après quatre heures passées au commissariat, il est remis en liberté et, selon la chaîne de radio RTL, n'est pas poursuivi en justice, après avoir donné son accord pour une procédure de composition pénale,. Selon Robert Rochefort, « ce que dit RTL n'est pas exact ». Il affirme avoir « reconnu des choses fausses » et été « menacé ». Toujours selon RTL, qui ne nomme pas ses sources, François Bayrou, président du parti, le contacte le lendemain par téléphone et lui demande de démissionner. Le 6 septembre, Bayrou met fin aux fonctions de vice-président de Rochefort au sein du MoDem ; son remplacement est assuré par Yann Wehrling, également porte-parole du parti. Malgré ces faits, il reste député européen pour le MoDem.
Finalement Robert Rochefort a été sanctionné par la justice après avoir reconnu les faits qui lui étaient reprochés (il plaide coupable),,. La peine n'est toutefois pas rendue publique. 
Par décret du président de la République en date du 30 janvier 2018 il est suspendu pour cinq ans de sa qualité de légionnaire et privé pour le même temps du droit de porter les insignes de toute décoration française ou étrangère ressortissant à la grande chancellerie de la Légion d'honneur pour cause de "comportement contraire à l'honneur", comme le permet le code de la Légion d'Honneur. Un décret paru le même jour le suspend également pour cinq ans de l'Ordre du Mérite.


== Bibliographie ==
1995 : La société des consommateurs.
1997 : Le consommateur entrepreneur.
2000 : Vive le papy-boom, éditions Odile Jacob,  (ISBN 9782738108890).
2002 : La France déboussolée, éditions Odile Jacob.
2004 : La retraite à 70 ans ? , éditions Belin.
2006 : Promesses de banlieue, éditions de l'Aube.
2007 : Le bon consommateur et le mauvais citoyen, éditions Odile Jacob  (ISBN 9782738119612).
2012 : Produire en France, c'est possible, éditions Odile Jacob (préface de François Bayrou).


=== Tribunes ===
2011 : « Adoptons une « règle d'or » européenne ! », avec Stéphane Cossé et Vincent Chauvet, Le Monde.


== Notes et références ==


== Voir aussi ==


=== Articles connexes ===
Mouvement démocrate


=== Liens externes ===
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation • Bibliothèque du Congrès • WorldCat
« Robert Rochefort », sur la base de données des députés au Parlement européen
Site officiel
Twitter
 Portail de la sociologie   Portail de la politique française   Portail de l’Union européenne