Jean-Pierre Michel, né le 5 août 1938 à Nîmes, est un magistrat  et homme politique français. Il est notamment député de 1981 à 2002 et sénateur de la Haute-Saône de 2004 à 2014.


== Biographie ==


=== Jeunesse et formation ===
Jean-Pierre Michel est le fils de Léon Michel, un pharmacien, et d'Yvette de Bonadona. 
Il fait ses études aux collèges de Tanger et d'Orange, au lycée Corneille à Rouen, à la Faculté de droit de Paris, à l'Institut d’études politiques de Paris et à l'Institut de criminologie de Lille. Il est licencié en droit, diplômé d’études supérieures en droit public, d’études supérieures en criminologie.


=== Carrière de magistrat ===
Il est magistrat au ministère de la Justice de 1968 à 1972, juge à Corbeil-Essonnes de 1972 à 1974 et chef de bureau de la législation à la direction des affaires criminelles de 1974 à 1980, et substitut au tribunal de grande instance de Créteil de 1980 à 1981. Il est également secrétaire général du Syndicat de la magistrature de 1972 à 1974.


=== Carrière politique ===
Après un échec en 1978, il devient député de la Haute-Saône lors de la vague rose de 1981 pour le compte du Parti socialiste et est constamment réélu jusqu'à sa défaite face à Maryvonne Briot lors de l'élection législative de 2002.
Il est à l'origine de l'amendement devenu article 20 de la loi du 15 janvier 1990 relative au financement des partis politiques, article vivement critiqué par la suite car permettant une « autoamnistie » des hommes politiques confrontés à des inculpations d'abus de biens sociaux. Le quotidien Le Monde commente alors : « Les commissaires socialistes sont déjà passés à l'acte... (Ils) ont introduit un amendement... (qui) n'est ni plus ni moins qu'un article de dépénalisation.» Le sénateur communiste Charles Lederman avance que cet article « blanchit » les hommes politiques impliqués dans des affaires politico-financières.
Après avoir été membre du CERES pendant de nombreuses années, en 1992, il adhère au Mouvement des citoyens qu'il quitte en 2002 pour fonder l'Association pour une gauche républicaine (AGR). En mai 2004, il rejoint le Parti socialiste au courant Nouveau Monde et est élu sénateur de la Haute-Saône en septembre 2004. 
Dès 1991, avec deux militants homosexuels, Jan-Paul Pouliquen et Gérard Bach-Ignasse, il rédige une proposition de loi sur un contrat d'union civile (CUC) mais n'arrive pas à convaincre d'autres députés de gauche de la déposer. Au cours des années suivantes, Jean-Pierre Michel est l'auteur ou le coauteur de plusieurs autres propositions de loi sur le même thème, qui n'aboutissent pas. Avec le retour de la gauche au pouvoir en 1997, le projet est relancé. Jean-Pierre Michel est, avec Patrick Bloche, un des pères du Pacte civil de solidarité (PACS), adopté le 13 octobre 1999.
Sénateur depuis 2004, il est notamment nommé le 14 novembre 2012 rapporteur du  projet de loi ouvrant le mariage aux personnes de même sexe. Il intervient médiatiquement à plusieurs reprises contre les adversaires du projet. Le journal Valeurs actuelles avance qu'il joue un rôle central dans l'adoption de cette nouvelle loi. Il se déclare favorable à la gestation pour autrui pour les couples hétérosexuels et homosexuels.
Dans le scandale du « mur des cons », il soutient le Syndicat national de la magistrature en estimant que les « personnalités raillées n'ont que ce qu'elles méritent ».
En 2014, il est rapporteur de la réforme pénale au Sénat. Il fait adopter une nouvelle version de la contrainte pénale qui devient une peine complètement autonome que les juges auraient l'obligation d'appliquer. Cette peine se substituerait ainsi à la prison en cas de « vol, recel de vol, de filouterie, de dégradation, d'usage de stupéfiants et de certains délits routiers ». Il propose également la suppression des tribunaux correctionnels pour mineurs et de la rétention de sûreté.
La même année, Jean-Pierre Michel annonce sa candidature à un second mandat de sénateur. Il est battu par le candidat présenté par l'UMP le 28 septembre 2014.


== Détail des mandats et fonctions ==
1981 - 2002 : député
1983 - 2004 : maire d'Héricourt
1992 - 2011 : conseiller général de la Haute-Saône (canton de Héricourt-Ouest)
2004 - 2014 : sénateur de la Haute-Saône


== Liens externes ==
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation • Bibliothèque du Congrès • WorldCat
Sa fiche sur le site du Sénat
Son site personnel


== Références ==

 Portail de la politique française   Portail de la Haute-Saône