Sandrine Mazetier, née le 16 décembre 1966 à Rodez, est une femme politique française membre du Parti socialiste (PS). Députée de la 8e circonscription de Paris entre 2007 et 2017, elle est élue vice-présidente de l'Assemblée nationale le 27 juin 2012.


== Biographie ==


=== Études et vie professionnelle ===
Sandrine Mazetier effectue l'ensemble de sa scolarité dans le 12e arrondissement de Paris. Elle étudie au collège et au lycée Paul-Valéry. Après une hypokhâgne et une khâgne et une licence de lettres classiques, elle poursuit des études en marketing publicité au CELSA.
Sa formation terminée, elle travaille comme cadre dans le secteur privé, en tant que directrice en communication.


=== Engagement politique ===
Son engagement politique commence en 1986 avec le mouvement étudiant contre le projet Devaquet. D'abord sympathisante du Parti socialiste, elle y adhère en 1988 pour la campagne présidentielle de François Mitterrand. Son premier engagement socialiste est la lutte contre le Front National et ses possibles alliances avec la droite. Elle milite dès sa création au Manifeste contre le Front national. 
Elle est élue conseillère d’arrondissement du 12e arrondissement de Paris en 1995 dans l’opposition. En 1997 elle est candidate du PS aux élections législatives dans la 8e circonscription de Paris. Elle perd l'élection avec 47,19 % des voix au deuxième tour contre 52,81 % à Jean de Gaulle, le candidat du RPR. En 2001, elle est élue conseillère du 12e arrondissement, cette fois-ci dans la majorité.
À la suite de son élection à la Mairie de Paris, Bertrand Delanoë la nomme adjointe au Maire de Paris chargée du patrimoine. Après la démission de David Assouline, élu sénateur en septembre 2004, elle est chargée de la vie étudiante.


=== Députée de Paris ===
En 2007, Sandrine Mazetier est à nouveau candidate aux élections législatives dans la 8e circonscription de Paris, investie par le Parti socialiste et soutenue par le MRC et le PRG. Face à elle l'UMP présente Arno Klarsfeld qui se décrit comme « l'ami proche » du président Nicolas Sarkozy. Des personnalités politiques proches de la candidate comme Bertrand Delanoë et Dominique Strauss-Kahn viennent la soutenir pendant la campagne, tout comme Ségolène Royal. Elle bat finalement largement Arno Klarsfeld avec 55,85 % des suffrages exprimés. La 8e circonscription de Paris bascule à gauche pour la première fois depuis 1958. À l'Assemblée nationale elle est désignée vice-président du groupe Socialiste, radical et citoyen (SRC) chargée de l'éducation au sein du contre-gouvernement ou « shadow cabinet » en juin 2007. En juin 2009, elle devient vice-président du groupe Socialiste, radical et citoyen (SRC) chargée de l'immigration et siège à ce titre à la commission des lois de l'Assemblée nationale. 
Elle est candidate à sa succession dans la 8e circonscription de Paris lors des élections législatives de juin 2012, avec pour suppléante Catherine Baratti-Elbaz, adjointe à la maire du 12e arrondissement Michèle Blumenthal. Sandrine Mazetier obtient 42,42 % des suffrages au premier tour et l'emporte au second tour avec 61,51 % des suffrages exprimés, face à Charles Beigbeder (UMP) qui reçoit 38,49 % des suffrages exprimés.
Elle est élue vice-présidente de l'Assemblée nationale le 27 juin 2012, et est également membre de la Commission des finances, de l'économie générale et du contrôle budgétaire. À la suite des attentats de Paris de novembre 2015, elle propose dans le cadre de la loi de prolongation de l'état d'urgence et contre l'avis du gouvernement un amendement visant à rétablir la censure de la presse, de la radio, du cinéma et du théâtre.
Elle défend à l'Assemblée un amendement proposé par une série de députés PS , adopté, au projet de loi pour une république numérique, rendant passible de prison, de fortes amendes et même d'expulsion de son logement toute personne louant son bien sur une plateforme telle Airbnb sans avoir l'autorisation écrite de son propriétaire (art. 23 bis et ter du projet de loi adopté ).
De nouveau candidate dans la 8e circonscription de Paris lors des élections législatives de 2017, elle obtient 14,87 % des suffrages exprimés, ce qui ne lui permet pas de se qualifier pour le 2e tour.


=== Au Parti socialiste ===
Sandrine Mazetier est souvent présentée dans les médias comme faisant partie des « quadras » du Parti socialiste, la génération montante de la gauche socialiste. Elle est considérée comme politiquement proche de Dominique Strauss-Kahn et Martine Aubry. Au sein du PS, elle est membre depuis sa création en 2002 du courant Socialisme et Démocratie. Elle participe activement depuis 2008 au Pôle des Reconstructeurs.
Lors de la phase des contributions du Congrès de Reims du PS en juillet 2008, elle signe la contribution « Besoin de gauche ». Lors de la phase des motions en septembre 2008, elle soutient la motion D « Changer à gauche pour changer la France » portée par Martine Aubry et apparaît à ses côtés lors d'une réunion publique à La Bellevilloise dans le 20e arrondissement. Elle devient par la suite l'une des quatre femmes porte parole de la motion D, puis de Martine Aubry.
En décembre 2008, elle est nommée secrétaire nationale à l'immigration dans la nouvelle équipe dirigeante du Parti socialiste.
Lors de la primaire socialiste, elle apporte son soutien à Martine Aubry puis en juillet 2011, intègre son équipe de campagne pour l'élection présidentielle de 2012, chargée avec El Mouhoud Mouhoud de la thématique « Immigration ».
Lors de l'élection présidentielle de 2017, elle est responsable adjointe de la thématique « Droits de l'humain » dans l'équipe de campagne de Benoît Hamon,.


== Notes et références ==


== Voir aussi ==


=== Article connexe ===
Femmes à l'Assemblée nationale française


=== Liens externes ===
Le site officiel de Sandrine Mazetier
Sa page sur le site de l'Assemblée nationale
 Portail de la politique française   Portail de Paris