Christian Eckert, né le 8 février 1956 à Algrange en France, est un homme politique français. Il est membre du Parti socialiste et député de la septième circonscription de Meurthe-et-Moselle du 17 juin 2007 au 9 mai 2014. Il est nommé, le 9 avril 2014, secrétaire d’État chargé du Budget au ministère des Finances et des Comptes publics dans les gouvernements Valls I et II, et dans le gouvernement Cazeneuve.


== Biographie ==
Christian Eckert fréquente l’École normale supérieure de Saint-Cloud en tant qu'auditeur libre et réussit l'agrégation de mathématiques,. En parallèle de ses études, il travaille pendant l'été aux hauts-fournaux de Florange, à l'aciérie Martin, afin de gagner un peu d'argent. Il adhère au PS en 1981.
Il enseigne les mathématiques au lycée en BTS et en classes préparatoires aux grandes écoles. Il a notamment exercé au lycée Saint-Exupéry de Fameck (Moselle) de 1981 à 1991 et au lycée Louis Bertrand de Briey (Meurthe-et-Moselle) de 1991 à 2007. Christian Eckert est adjoint au maire de la commune anciennement minière de Trieux (Meurthe-et-Moselle) du 12 mars 1983 au 1er mars 1987, date à laquelle il a accédé au poste de maire.
Il a été membre du conseil régional de Lorraine à partir du 15 mars 1998, puis vice-président chargé de l'aménagement du territoire et de l'après-mine jusqu'en mars 2010, date de sa fin de mandat régional.
Il est membre du Parti socialiste et fait partie du courant de Laurent Fabius.
Christian Eckert a été élu au deuxième tour des élections législatives de 2007 avec 53,95 % des voix face à Édouard Jacque, député UMP sortant.
Il a été porte-parole du PS à l'Assemblée nationale contre le texte libéralisant le travail dominical. En décembre 2009, il a souligné le conflit d'intérêt des différentes fonctions d'Éric Woerth.
Christian Eckert est à partir du 28 juin 2012 rapporteur général de la Commission des Finances, de l'Économie générale et du Contrôle budgétaire de l'Assemblée nationale. 
Toujours en 2012, il est l'auteur de l'amendement qui instaure la taxe sur les dividendes reposant sur une contribution additionnelle à l'impôt sur les sociétés (IS), taxe qui est maintenue en dépit de doutes sur sa légalité dès 2015. Le dispositif est jugé contraire au droit de l'Union européenne par la Cour de justice de l'Union européenne (CJUE) en mai 2017, avant d'être totalement invalidé début octobre par le Conseil constitutionnel, qui juge que cette taxe induit une « rupture d'égalité » devant l'impôt. En octobre 2017, alors qu'il apparaît que ce qui est désigné comme un « fiasco fiscal » coûtera près de 10 milliards d’euros au contribuable français et risque de faire déraper le déficit public, le gouvernement Édouard Philippe fustige l'amateurisme des responsables politiques qui ont mis cette taxe en place visant en particulier Michel Sapin, ainsi que Christian Eckert,. Répondant aux critiques du gouvernement, Christian Eckert nie tout amateurisme juridique arguant de la « complexité du droit » et pointe du doigt les responsabilités partagées avec Emmanuel Macron alors secrétaire adjoint de l'Élysée en charge des questions budgétaires, fiscales et économiques et du directeur adjoint du ministre de l'Économie en charge de la fiscalité des entreprises Alexis Kohler, secrétaire général de l'Élysée en 2017.
En avril 2013, Christian Eckert se dit opposé au projet de loi sur le contrôle du patrimoine des élus. En avril 2014, à la suite de la défaite des élections municipales pour le PS, il fait partie des 89 députés réclamant un nouveau contrat de majorité.
Le 9 avril 2014, il est nommé secrétaire d'État chargé du Budget, dans le gouvernement Valls I. La députée Valérie Rabault devient alors rapporteur général du Budget à l'Assemblée.
Il est le rapporteur de la commission des finances de la loi Eckert portant son nom, loi relative aux comptes bancaires inactifs et aux contrats d’assurance-vie en déshérence, promulguée en juin 2014.
Le 16 décembre 2015, au nom du gouvernement, il fait revoter et annuler l'amendement socialiste sur la transparence fiscale des grandes sociétés,,. Il reçoit en janvier 2016 le prix « Casserole » de l'association Anticor.
Pendant la primaire citoyenne de 2017, il soutient Manuel Valls.
Il est candidat à sa succession dans la 3e circonscription de Meurthe-et-Moselle, les 11 et 18 juin 2017 avec Jean-Marc Fournel, maire de Longwy comme suppléant. Il est éliminé dès le premier tour avec seulement 9,52 % des voix.
En juin 2017, la Cour des comptes dans un audit critique sévèrement les textes budgétaires du gouvernement sortant, jugeant qu'ils sont entachés "d'insincérités". Elle juge que le déficit 2017 a été minimisé du fait « d'une sous-estimation des dépenses de l'Etat » pourtant connues du gouvernement dès l'automne 2016. Christian Eckert s'est défendu de tout manque de sérieux.


== Détail des fonctions et des mandats ==
Mandats locaux12 mars 1983 - 1er mars 1987 : adjoint au maire de Trieux
1er mars 1987 - 12 mars 1989 : maire de Trieux
25 mars 2001 - 15 mars 2014 : maire de Trieux
15 mars 1998 - 28 mars 2004 : conseiller régional de Lorraine
28 mars 2004 - 14 mars 2010 : vice-président du conseil régional de LorraineMandat parlementaire20 juin 2007 - 9 mai 2014: député de la 3e circonscription de Meurthe-et-MoselleNomination au gouvernement9 avril 2014 - 17 mai 2017 : secrétaire d’État chargé du Budget au Ministère des Finances et des Comptes publics.


== Ouvrage ==
Un ministre ne devrait pas dire ça…, Robert Laffont, 2018.


== Notes et références ==


== Voir aussi ==


=== Articles connexes ===
Conseil régional de Lorraine
Liste des députés de Meurthe-et-Moselle
Troisième circonscription de Meurthe-et-Moselle
Liste des députés de la XIVe législature de la Cinquième République
Liste des députés de la XIIIe législature de la Cinquième République
Trieux


=== Liens externes ===
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation
Sa fiche de secrétaire d'État sur gouvernement.fr
Sa fiche sur le site de l'Assemblée nationale
 Portail de la politique française   Portail de la Lorraine