Michel Havard, né le 24 mars 1967 à Clermont-Ferrand, est un homme politique français, membre du parti Les Républicains. Député du Rhône de 2007 à 2012, il est le candidat de l'UMP pour les élections municipales à Lyon en mars 2014. Conseiller municipal et président du groupe Les Républicains et Apparentés - Ensemble pour Lyon, le principal groupe d'opposition, il est également conseiller de la métropole de Lyon.


== Origines et famille ==
Michel Havard est le cadet d'une famille de trois enfants, il a un frère aîné et une jeune sœur. Il est le fils de Jean-Pierre Havard, commissaire de police aux Renseignements généraux (RG), et d'une mère comptable dans un collège. Après le Puy-de-Dôme où il est né, sa famille réside, au gré des mutations d’un père policier, à Cherbourg, Aurillac, et Toulon. Son père est alors nommé directeur adjoint des RG à Lyon et la famille s'installe en périphérie de la ville, dans la commune de Pierre-Bénite.
Élève au collège Marcel Pagnol de Pierre-Bénite puis au lycée du Parc Chabrières à Oullins.


== Début de carrière ==
Diplômé de Sciences Po Lyon (1989-1991) et titulaire d'un DEA de droit européen délivré par l'université Jean Moulin Lyon 3 (1991), il devient consultant en droit européen auprès de PME de 1993 à 1996. Il rejoint ensuite le grand Lyon en 1996 comme chargé de mission du groupe des Indépendants du Grand Lyon puis comme assistant parlementaire d'Henry Chabert (député RPR) de 1997 à 2001.
De 1995 à 2001, il occupe le poste d'adjoint au commerce et à l'artisanat à la mairie du 5e arrondissement de Lyon et devient également assistant parlementaire de 1997 à 2001.
Il décide ensuite de rejoindre le privé en intégrant le Groupe SAUR jusqu'en 2005 puis Suez Environnement de 2006 à 2007.  


== Vie privée ==
Il est père de deux enfants.


== Engagement politique local ==
Michel Havard s’inscrit en politique dès ses études supérieures où il crée Europe Info, une association qu’il présidera pendant ses deux années à Lyon 3. Celle-ci était destinée à expliquer aux citoyens, et non pas seulement aux étudiants, le rôle des institutions européennes.
À 27 ans, il est élu sur la liste UDF/RPR à la mairie du 5e arrondissement de Lyon où il est désigné adjoint au commerce, à l'artisanat, au tourisme et à la sécurité.
En 2004, il se présente dans le 5e canton de Lyon lors des élections cantonales. Il est élu conseiller général le 28 mars avec 50,34 % des voix.
Ainsi, il devient un acteur majeur dans la vie politique lyonnaise. À l'issue des élections de mars 2008, il est choisi pour être le président du groupe municipal d'opposition « Ensemble pour Lyon ».
Le 24 février 2011 il annonce sa candidature à la primaire de droite et du centre-droit pour la mairie de Lyon pour les élections municipales de 2014. Parmi ses grands projets, on trouve la réalisation d'une ligne de métro entre la gare Saint Paul et Lyon Part-Dieu. Il emporte la primaire au second tour contre Georges Fenech et devient donc le candidat de la droite et du centre-droit pour le scrutin.
Élu en 2008 conseiller municipal de Lyon, il quitte le conseil général et préside le groupe municipal d’opposition Les Républicains et Apparentés - Ensemble pour Lyon (poste qu’il occupe encore à ce jour) et est le candidat UMP aux élections municipales de 2014. Il sera battu au premier et au second tour par Gérard Collomb et ses listes.


== Engagement politique national ==
Le 16 juin 2007, il est élu député de la XIIIe législature (2007-2012), dans la première circonscription du Rhône avec 51,6 % des voix contre 48,4 %. En battant le candidat des radicaux de gauche Thierry Braillard. Il succède ainsi à Anne-Marie Comparini, députée UDF/MoDem sortante, éliminée dès le premier tour. Pour assumer pleinement sa fonction de député et éviter de cumuler les mandats, il décide de quitter son poste de conseiller général du Rhône.
Intéressé par les thématiques de l'énergie et de l'environnement, il intègre la Commission du Développement Durable et de l'Aménagement du Territoire à l'Assemblée nationale. Le président de la République, Nicolas Sarkozy, lui confie le 19 août 2009 une mission sur la mise en œuvre des bilans carbone qui entre dans le cadre de la loi "Grenelle 2". Il remettra son rapport le 31 décembre 2009.
En 2010, il préside le groupe de travail « Eau, biodiversité, santé, risques naturels » dans le cadre du Plan national d'adaptation au réchauffement climatique mené par le gouvernement. L'objectif de ce plan est de prévoir et d'adapter l'économie française aux changements climatiques à venir.
En 2011, il est rapporteur de la mission sur la « gestion durable des matières premières » à l'Assemblée nationale. Il devient aussi membre du conseil d’administration de l’Agence de financement des infrastructures de transport de France et remplace ainsi Patrick Ollier.
Le 13 avril 2011, il est nommé rapporteur par la Commission du développement durable de l'Assemblée nationale de la proposition de loi sur les gaz de schiste déposée par Christian Jacob « visant à abroger les permis exclusifs de recherches d’hydrocarbures non conventionnels et à interdire leur exploration et leur exploitation sur le territoire national ».
Le 17 juin 2012, il perd son siège de député. Dans la 1re circonscription du Rhône, il est battu au deuxième tour par Thierry Braillard (PRG) qui obtient 53,78 % des voix.


== Engagements associatifs ==
En parallèle, il est depuis juin 2009 le président de l'association HQE ; association reconnue d'utilité publique dans le domaine de la construction durable. À ce titre, il participe à la création France GBC, branche française de World Green Building Council.
Depuis 2011, il est également président de l'association Bilan Carbone.


== Mandats ==
1995 - 2001 : Adjoint au Maire du cinquième arrondissement de Lyon (adjoint au commerce, à l'artisanat, au tourisme et à la sécurité)
2004 - 2008 : Conseiller général du Rhône (élu dans le 5e canton de Lyon)
2007 - 2012 : Député de la 1re circonscription du Rhône (membre de la Commission du développement durable et de l'aménagement du territoire & Président du groupe de travail « Eau, biodiversité, santé, risques naturels » )
2008 - 2014 : Conseiller municipal de Lyon (président du groupe UMP & apparentés - Ensemble pour Lyon, principal groupe d’opposition)
depuis 2014 : conseiller municipal de Lyon (président du groupe Les Républicains & Apparentés - Ensemble pour Lyon, principal groupe d'opposition) et conseiller métropolitain à la Métropole de Lyon.


== Fonctions au sein du parti Les Républicains (UMP) ==
Au sein de l'UMP, Michel Havard succède à Jean Girma au poste de Secrétaire départemental du Rhône de 2004 à 2008.
Au niveau national pour l'UMP, il a été secrétaire national chargé des fédérations puis secrétaire national chargé de la mobilité des transports urbains.
Il occupe aujourd'hui celui de secrétaire départemental adjoint Les Républicains.


== Bibliographie ==
Les premiers chantiers du nouveau responsable départemental, propos recueillis par Frédéric Poignard in Lyon Figaro, 5 novembre 2004, p. 2-3.


== Liens externes ==
Notices d'autorité : Fichier d’autorité international virtuel • International Standard Name Identifier • Bibliothèque nationale de France (données) • Système universitaire de documentation
Fiche parlementaire sur le site de l'Assemblée nationale : http://www.assemblee-nationale.fr/13/tribun/fiches_id/334750.asp
Site parlementaire : http://www.michelhavard.fr


== Références ==

 Portail de la politique française   Portail d'Auvergne-Rhône-Alpes   Portail du gaullisme