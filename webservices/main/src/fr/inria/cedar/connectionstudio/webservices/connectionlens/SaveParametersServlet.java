package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.CONNECTION_LENS_DB_PREFIX;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import com.google.common.base.Strings;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.SessionAttributes;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;

public class SaveParametersServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709512051459L;

    private static final Logger log = Logger.getLogger(SaveParametersServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        process(request, response);

        response.setHeader("Cache-Control", "no-cache");

        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "database must be specified");
        }

        String requestParameters = request.getParameter(RequestParameters.PARAMETERS);
        if (Strings.isNullOrEmpty(requestParameters)) {
            handleError(response, SC_BAD_REQUEST, "requestParameters must be specified");
        }

        try {
            // TODO :
            // Just laod back the parameter from the SQL table if they exists

            Config config = new Config();

            JSONArray parameters = new JSONArray(requestParameters);
            ArrayList<String> extractionKeys = new ArrayList<String>();

            ConnectionLens cl = getEngine(database, request, false, false);
            RelationalGraph graph = (RelationalGraph) cl.graph();
            for (Object key : parameters) {
                String currentKey = key.toString();
                extractionKeys.add(currentKey);
            }

            StringBuffer configBuffer = new StringBuffer("{");

            for (String key : extractionKeys) {
                // log.debug(String.format("Getting key %s", key));
                String value = "";

                String storedValue = graph.getParameters(key);
                if (Strings.isNullOrEmpty(storedValue)) {
                    try {
                        value = config.getStringProperty(key);
                    } catch (Exception e) {
                        log.warn(e);
                    }
                } else {
                    value = storedValue;
                    cl.getConfig().setProperty(key, storedValue);
                }

                configBuffer.append(String.format(" \"%s\": \"%s\",", key, value));
            }
            configBuffer.deleteCharAt(configBuffer.length() - 1);
            configBuffer.append("}");
            String stringConfig = new String(configBuffer);

            response.setContentType("application/json");
            response.getWriter().print(stringConfig);

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);

            response.setContentType("application/json");
            response.getWriter().print(e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        process(request, response);

        response.setHeader("Cache-Control", "no-cache");

        String config = request.getParameter(RequestParameters.CONFIG);
        if (Strings.isNullOrEmpty(config)) {
            handleError(response, SC_BAD_REQUEST, "config must be specified");
        }

        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "database must be specified");
        }

        try {
            JSONObject requestConfig = new JSONObject(config);

            Iterator<String> keys = requestConfig.keys();
            Config GUIConfig = new Config();

            GUIConfig.setProperty("RDBMS_DBName", CONNECTION_LENS_DB_PREFIX + database);
            HashMap<String, String> parametersMap = new HashMap<String, String>();

            ConnectionLens cl = getEngine(database, request, false, false);
            RelationalGraph graph = (RelationalGraph) cl.graph();

            while (keys.hasNext()) {
                String key = keys.next();
                String value = (String) requestConfig.get(key);
                GUIConfig.setProperty(key, value);
                parametersMap.put(key, value);
                cl.getConfig().setProperty(key, value);

            }

            log.info(String.format("Current graph value of search_stopper_topk is: %s",
                    cl.graph().getConfig().getStringProperty("search_stopper_topk")));
            cl.refreshExtractor();

            graph.persistParameters(parametersMap);

            session.setAttribute(SessionAttributes.ENGINE, cl);

            log.info(
                    String.format("Save config: %s to db, and update CL. Current extractor is:  %s",
                            config, cl.extractor().getExtractorName()));

            log.info(String.format("search_stopper_topk value updated is : %s",
                    cl.getConfig().getStringProperty("search_stopper_topk", "null")));

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            log.error(request.toString());
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);

            response.setContentType("application/json");
            response.getWriter().print(e);
        }
    }
}
