package fr.inria.cedar.connectionstudio.webservices.pathways;

import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import org.apache.log4j.Logger;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.MasterServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.abstra.Abstra;
import fr.inria.cedar.abstra.Abstra.END_AFTER;

import static fr.inria.cedar.abstra.Abstra.END_AFTER;
import fr.inria.cedar.pathways.Pathways;
import org.json.simple.JSONObject;
import fr.inria.cedar.abstra.abstraction.collectiongraph.CollectionGraphBuilding;
import fr.inria.cedar.pathways.PathwaysExperiment;
import fr.inria.cedar.pathways.pathsexploration.PathExploration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import fr.inria.cedar.abstra.abstraction.results.AbstractionResult;
import fr.inria.cedar.abstra.AbstraExperiment;

public class RunOnePathwaysServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709565051459L;

    private static final Logger log = org.apache.log4j.Logger.getLogger(RunOnePathwaysServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setHeader("Cache-Control", "no-cache");

        if (!super.process(request, response)) {
            return response.isCommitted();
        }
        log.info("doPost called with parameter " + request.getQueryString());

        // 1. we get the form parameters
        String dbName = request.getParameter("dbPathWaysRun");
        // System.out.println("dbName is " + dbName);
        String entityType1 = request.getParameter("entityType1");
        // System.out.println("entityType1 is " + entityType1);
        String entityType2 = request.getParameter("entityType2");
        // System.out.println("entityType2 is " + entityType2);
        int maxSizeDataPath = Integer.parseInt(request.getParameter("maxSizeDataPath"));
        // System.out.println("maxSizeDataPath is " + maxSizeDataPath);
        int dataSourceID = Integer.parseInt(request.getParameter("dataSourceId"));

        log.info("Run PathWAYS in " + dbName + " of data source " + dataSourceID);
        // 3. we run the abstraction by creating a new experiment that will register the
        // dataset and run the abstraction
        try {
            ConnectionLens cl = getEngine(dbName, request, false, false);
            cl.getConfig().setProperty("create_summarization_tables", "false");

            log.info("will run Abstra");
            Abstra abstra = new Abstra(cl, null, false, new ArrayList<>(Arrays.asList(dataSourceID)),
                    END_AFTER.COLLECTION_GRAPH.name()); // null because inputs have already been loaded, true for
                                                        // isPathways, true for isStudio
            abstra.runAbstraction();

            log.info("will run Pathways");
            Pathways pathways = new Pathways(abstra, dataSourceID, entityType1, entityType2, maxSizeDataPath);
            pathways.runPathways();

            String jsonResult = pathways.getPathwaysResult().getResult();
            response.setContentType("application/json");
            response.getWriter().print(jsonResult);
        } catch (Exception e) {
            HashMap<String, String> jsonData = new HashMap<>();
            jsonData.put("error",
                    "An error occurred while running the abstraction. Please check your dataset path and the abstraction parameters."
                            + e.getMessage());
            log.info("ERROR " + e.getMessage());
            e.printStackTrace();
            JSONObject jsonObject = new JSONObject();
            jsonObject.putAll(jsonData);
            response.getWriter().write(jsonObject.toJSONString());
            // request.setAttribute("jsonData", jsonObject.toJSONString());
            // request.getRequestDispatcher("createAbstraction.jsp").forward(request,
            // response);
        }

        return response.isCommitted();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
