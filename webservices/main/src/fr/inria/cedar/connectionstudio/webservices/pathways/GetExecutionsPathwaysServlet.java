package fr.inria.cedar.connectionstudio.webservices.pathways;

import fr.inria.cedar.abstra.Abstra;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.MasterServlet;
import fr.inria.cedar.pathways.Pathways;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class GetExecutionsPathwaysServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709565051459L;

    private static final Logger log = org.apache.log4j.Logger.getLogger(GetExecutionsPathwaysServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setHeader("Cache-Control", "no-cache");

        if (!super.process(request, response)) {
            return response.isCommitted();
        }
        // 1. we get the form parameters
        int datasourceID = Integer.parseInt(request.getParameter("datasourceId"));
        String dbName = request.getParameter("dbName");

        log.info("Get existing pathways in " + dbName + " of data source " + datasourceID);

        try {
            ConnectionLens cl = getEngine(dbName, request, false, false);
            cl.getConfig().setProperty("create_summarization_tables", "false");
            Abstra abstra = new Abstra(cl, null, false);
            Pathways pathways = new Pathways(abstra, false);
            // no need to run Pathways, we just need to read the existing executions
            // pathways.runPathways(dataSourceID, true, entityType1, entityType2,
            // nbMaxPaths, maxSizeDataPath, "B", false,
            // 0, 0, 0, false, false, true, false, false, true); // readResult is TRUE
            pathways.setDatasourcePositionsToAbstract(new ArrayList<>(Arrays.asList(datasourceID)));
            pathways.createAbstraDatasourcesToAbstract();
            pathways.computeExistingExecutions();
            String jsonExecutions = pathways.getExistingExecutionsAsJson();
            // log.info(jsonExecutions);
            // System.out.println("jsonExecutions is " + jsonExecutions);
            response.setContentType("application/json");
            response.getWriter().print(jsonExecutions);

        } catch (Exception e) {
            HashMap<String, String> jsonData = new HashMap<>();
            jsonData.put("error", "An error occurred while getting current Pathways executions.");
            log.info("ERROR " + e.getMessage());
            e.printStackTrace();
        }

        return response.isCommitted();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response);

    }

}
