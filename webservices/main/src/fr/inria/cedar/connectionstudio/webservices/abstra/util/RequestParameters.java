/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.abstra.util;

/**
 * Reserved name to be used as key to the keys to request parameters.
 *
 * @author Julien Leblay
 */
@SuppressWarnings("javadoc")
public class RequestParameters {

	public static final String DATABASE = "d";
	public static final String ABSTRACT = "a";
	public static final String QUERY = "q";
	public static final String PAGE = "p";
	public static final String NODE = "n";
	public static final String OLD_NODES = "on";
	public static final String NEW_NODES = "nn";
	public static final String URL = "u";
	public static final String FILE = "file";
}
