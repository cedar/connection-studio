package fr.inria.cedar.connectionstudio.webservices.abstra;

import java.util.HashMap;
import java.util.HashSet;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import fr.inria.cedar.connectionstudio.webservices.connectionlens.MasterServlet;
import fr.inria.cedar.connectionlens.ConnectionLens;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AbstractionInspiredQueries extends MasterServlet {

    public static final Logger log = Logger.getLogger(AbstractionInspiredQueries.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.info("Processing request...");

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        String dbName = request.getParameter("dbName");
        JSONObject mainEntity = new JSONObject(request.getParameter("mainEntity"));
        JSONArray entitiesToExtract = new JSONArray(request.getParameter("entities"));

        try {
            StringBuilder nodesQuery = new StringBuilder(
                    "WITH nodes AS (SELECT nn.*, cn.collid FROM norm_nodes AS nn \n JOIN collections_nodes AS cn \n ON nn.id=cn.nodeid) \n");

            StringBuilder selectQuery = new StringBuilder("SELECT ");
            StringBuilder joinQuery = new StringBuilder();
            int edgeCounter = 0;
            HashMap<String, String> queriedNodes = new HashMap<>();

            for (int j = 0; j < entitiesToExtract.length(); j++) {
                JSONObject leafEntity = entitiesToExtract.getJSONObject(j);
                String prevEntity = mainEntity.getString("label");
                String prevEntityName = mainEntity.getString("label");
                JSONArray pathArray = leafEntity.getJSONArray("path");

                for (int i = 0; i < pathArray.length(); i++) {
                    String entityId = pathArray.getString(i);
                    String entityName = prevEntityName + "_" + pathArray.getString(i);
                    // Although the labelPath guarantees uniqueness, it might breach the character
                    // limit of the database
                    // so we use a random UUID instead
                    String entityLabel = java.util.UUID.randomUUID().toString().replace("-", "");

                    String edgeName = "e" + edgeCounter;

                    // If the entity has already been queried, we don't need to join it again
                    if (queriedNodes.containsKey(entityName)) {
                        prevEntity = queriedNodes.get(entityName);
                        prevEntityName = entityName;
                        continue;
                    }

                    // We use different join queries depending on whether the entity is required or
                    // not
                    if (!leafEntity.getBoolean("required")) {
                        joinQuery.append(String.format(
                                "LEFT JOIN (SELECT e.source, n.* FROM norm_edges AS e JOIN nodes AS n ON e.target=n.id WHERE n.label='%s') AS \"%s\" ON \"%s\".source=\"%s\".id\n",
                                entityId, entityLabel, entityLabel, prevEntity));
                    } else {
                        joinQuery.append(String.format("JOIN norm_edges %s ON %s.source=\"%s\".id\n", edgeName,
                                edgeName, prevEntity));
                        joinQuery.append(
                                String.format("JOIN nodes \"%s\" ON %s.target=\"%s\".id AND \"%s\".label='%s'\n",
                                        entityLabel, edgeName, entityLabel, entityLabel, entityId));
                        edgeCounter++;
                    }

                    prevEntity = entityLabel;
                    prevEntityName = entityName;
                    queriedNodes.put(entityName, entityLabel);
                }

                String entityLabel = java.util.UUID.randomUUID().toString().replace("-", "");
                String edgeName = "e" + edgeCounter;
                joinQuery.append(String.format("LEFT JOIN norm_edges %s ON %s.source=\"%s\".id\n", edgeName, edgeName,
                        prevEntity));
                joinQuery.append(String.format("LEFT JOIN nodes \"%s\" ON %s.target=\"%s\".id\n", entityLabel, edgeName,
                        entityLabel));
                edgeCounter++;

                if (j > 0) {
                    selectQuery.append(", ");
                }

                String leafLabel = leafEntity.getString("full_label");
                selectQuery.append(String.format("\"%s\".label AS \"%s\"\n", entityLabel, leafLabel));
            }

            selectQuery.append(String.format("FROM nodes \"%s\"\n", mainEntity.getString("label")));

            String whereQuery = String.format("WHERE \"%s\".label='%s'\n", mainEntity.getString("label"),
                    mainEntity.getString("label"));

            String finalQuery = nodesQuery.toString() + selectQuery.toString() + joinQuery.toString() + whereQuery;

            // log.info(finalQuery);

            ConnectionLens cl = getEngine(dbName, request, false, false);
            ResultSet result = cl.executeQuery(finalQuery);

            JSONArray jsonData = resultSetToJson(result);
            // log.info(jsonData.toString());

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jsonData.toString());
        } catch (Exception e) {
            log.error("Error processing request.", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }

        log.info("Request processed successfully.");
        return response.isCommitted();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response);
    }

    // Converts a PostgreSQL ResultSet into a JSON object
    private JSONArray resultSetToJson(ResultSet rs) throws Exception {
        JSONArray json = new JSONArray();
        ResultSetMetaData metadata = rs.getMetaData();
        int numColumns = metadata.getColumnCount();

        while (rs.next()) {
            JSONObject obj = new JSONObject();
            for (int i = 1; i <= numColumns; i++) {
                String column_name = metadata.getColumnName(i);
                Object value = rs.getObject(column_name);
                obj.put(column_name, value == null ? JSONObject.NULL : value);
            }
            json.put(obj);
        }

        return json;
    }
}