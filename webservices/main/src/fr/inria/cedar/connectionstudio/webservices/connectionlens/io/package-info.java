/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

/**
 * Serialization and logging features.
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens.io;