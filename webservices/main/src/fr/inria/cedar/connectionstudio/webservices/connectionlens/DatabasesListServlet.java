package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.json.JSONObject;
import org.json.JSONArray;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.sql.ConnectionManagerCl;

public class DatabasesListServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709565051459L;

    /** Class logger. */
    private static final Logger log = Logger.getLogger(DatabasesListServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        response.setHeader("Cache-Control", "no-cache");

        try {
            log.info("Getting dbs lists");
            theGUIConfig = Config.getTheGUIConfig();
            log.info("theGUIConfig " + theGUIConfig);

            List<String> DatabaseList = ConnectionManager.listDatabases(theGUIConfig);

            JSONArray jsonDatabases = new JSONArray();
            for (String database : DatabaseList) {
                JSONObject projectInfo = ConnectionManagerCl.getProjectInformation(database);
                projectInfo.put("db", database);
                jsonDatabases.put(projectInfo);
            }

            response.setContentType("application/json");

            response.getWriter().print(jsonDatabases);

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            handleError(response, SC_BAD_REQUEST, e);
        }

        return response.isCommitted();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);

    }
}
