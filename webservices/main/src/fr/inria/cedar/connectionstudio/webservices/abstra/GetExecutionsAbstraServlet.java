package fr.inria.cedar.connectionstudio.webservices.abstra;

import fr.inria.cedar.abstra.Abstra;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.MasterServlet;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class GetExecutionsAbstraServlet extends MasterServlet {

    private static final long serialVersionUID = 8991001709565051459L;

    public static final Logger log = Logger.getLogger(GetExecutionsAbstraServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setHeader("Cache-Control", "no-cache");

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        // 1. we get the form parameters
        int dataSourceID = Integer.parseInt(request.getParameter("datasourceId"));
        String dbName = request.getParameter("dbName");

        try {
            ConnectionLens cl = getEngine(dbName, request, false, false);
            cl.getConfig().setProperty("create_summarization_tables", "false");
            Abstra abstra = new Abstra(cl, null, false);
            abstra.setDatasourcePositionsToAbstract(new ArrayList<>(Arrays.asList(dataSourceID)));
            abstra.createAbstraDatasourcesToAbstract();
            abstra.computeExistingExecutions();
            String jsonExecutions = abstra.getExistingExecutionsAsJson();
            // System.out.println("jsonExecutions is " + jsonExecutions);
            response.setContentType("application/json");
            response.getWriter().print(jsonExecutions);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            log.error(e.getMessage(), e);
        }

        return response.isCommitted();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response);

    }
}
