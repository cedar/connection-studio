package fr.inria.cedar.connectionstudio.webservices.abstra;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;

import fr.inria.cedar.abstra.AbstraExperiment;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.MasterServlet;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.abstra.Abstra;
import fr.inria.cedar.abstra.abstraction.results.AbstractionResult;

public class ReadOneAbstractionServlet extends MasterServlet {

    private static final long serialVersionUID = 8991001709565051459L;

    public static final Logger log = Logger.getLogger(ReadOneAbstractionServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setHeader("Cache-Control", "no-cache");

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        // 1. we get the form parameters
        int dataSourceID = Integer.parseInt(request.getParameter("datasourceId"));
        String dbName = request.getParameter("dbName");
        int executionId = Integer.parseInt(request.getParameter("executionId"));
        log.info("Read Abstraction in " + dbName + " of data source " + dataSourceID);

        try {

            String cachedResults = getCachedAbstraResult(request, dbName, dataSourceID);
            if (cachedResults != null) {
                log.info("Read from cache");
                response.setContentType("application/json");
                response.getWriter().print(cachedResults);
                return response.isCommitted();
            }

            ConnectionLens cl = getEngine(dbName, request, false, false);
            cl.getConfig().setProperty("create_summarization_tables", "false");
            Abstra abstra = new Abstra(cl, null, false);
            abstra.setStartFromReporting(true); // to say that we do not want to run the abstraction but only rad the
                                                // existing abstraction
            abstra.readAbstraction(executionId); // this will set up AbstraExecution and AbstractionResult
            String jsonResult = abstra.getAbstractionResult().getAsJSONString();

            response.setContentType("application/json");
            response.getWriter().print(jsonResult);

        } catch (IOException e) {
            System.out.println(e.getMessage());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            log.error(e.getMessage(), e);
        }

        return response.isCommitted();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response);

    }
}
