/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.abstra.util;

import java.util.Iterator;

/**
 * Wraps an subordinate Iterator to allow iterating over the next N items, as
 * per the last window
 * defined by setNextWindow.
 *
 * @author Julien Leblay
 * @param <T> the generic type
 */
public class WindowedIterator<T> implements Iterator<T> {

	/** The wrapped iterator. */
	private final Iterator<T> wrapped;

	/** The next windows. */
	private int nextWindows = 0;

	/** The counter. */
	private int counter = 0;

	/**
	 * Instantiates a new windowed iterator.
	 *
	 * @param wrapped the wrapped
	 */
	public WindowedIterator(Iterator<T> wrapped) {
		super();
		this.wrapped = wrapped;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return counter < nextWindows && wrapped.hasNext();
	}

	/**
	 * The method will NOT throw a NoSuchElementException if the counter exceeds the
	 * window.
	 * 
	 * {@inheritDoc}
	 * 
	 * @see java.util.Iterator#next()
	 */
	@Override
	public T next() {
		counter++;
		return wrapped.next();
	}

	/**
	 * Sets the max number of items to iterator over before hasNext returns false.
	 *
	 * @param w the new next window
	 */
	public void setNextWindow(int w) {
		this.nextWindows = w;
		this.counter = 0;
	}
}
