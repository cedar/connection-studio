package fr.inria.cedar.connectionstudio.webservices.connectionlens.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebServicesContextListener {

    private static final Logger log = LoggerFactory.getLogger(WebServicesContextListener.class);

    @WebListener
    public class AppContextListener implements ServletContextListener {

        public void contextInitialized(ServletContextEvent servletContextEvent) {
            ServletContext ctx = servletContextEvent.getServletContext();
            // + servletContextEvent.getSource().toString()

            log.info(
                    "Servlet Initialisation. ");
        }

        public void contextDestroyed(ServletContextEvent servletContextEvent) {
            ServletContext ctx = servletContextEvent.getServletContext();
            // + servletContextEvent.getSource().toString()
            log.info("Servlet Destroyed.");

        }

    }
}
