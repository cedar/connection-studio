/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static fr.inria.cedar.connectionstudio.webservices.connectionlens.util.ServletParameters.SYSTEM;
import fr.inria.cedar.connectionlens.Config;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DataSourceLoader implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Config guiConfig = new Config(sce.getServletContext().getRealPath(".")  + "/WEB-INF/local.settings");
		Config.setGUIConfig(guiConfig);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		sce.getServletContext().setAttribute(SYSTEM, null);
	}
}
