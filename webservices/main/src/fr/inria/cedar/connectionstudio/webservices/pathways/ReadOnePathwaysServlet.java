package fr.inria.cedar.connectionstudio.webservices.pathways;

import fr.inria.cedar.connectionlens.Config;
import org.apache.log4j.Logger;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.MasterServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.abstra.Abstra;
import fr.inria.cedar.pathways.Pathways;
import org.json.simple.JSONObject;
import fr.inria.cedar.pathways.pathsexploration.PathExploration;

import java.util.ArrayList;
import java.util.HashMap;

public class ReadOnePathwaysServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709565051459L;

    private static final Logger log = org.apache.log4j.Logger.getLogger(ReadOnePathwaysServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setHeader("Cache-Control", "no-cache");

        if (!super.process(request, response)) {
            return response.isCommitted();
        }
        log.info("doPost called with parameter " + request.getQueryString());

        // 1. we get the form parameters
        String dbName = request.getParameter("dbPathWaysRun");
        int executionId = Integer.parseInt(request.getParameter("executionId"));

        log.info("Read PathWAYS in " + dbName + " of execution ID " + executionId);
        // 3. we run the abstraction by creating a new experiment that will register the
        // dataset and run the abstraction
        try {
            ConnectionLens cl = getEngine(dbName, request, false, false);
            cl.getConfig().setProperty("create_summarization_tables", "false");
            Abstra abstra = new Abstra(cl, null, false);
            Pathways pathways = new Pathways(abstra, false);
            pathways.setReadResult(true);
            pathways.readPathways(executionId); // this will set up PathwaysExecution and PathwaysResult
            String jsonResult = pathways.getPathwaysResult().getResult();
            // log.info(jsonResult);
            // System.out.println("jsonResult is " + jsonResult);
            response.setContentType("application/json");
            response.getWriter().print(jsonResult);

        } catch (Exception e) {
            HashMap<String, String> jsonData = new HashMap<>();
            jsonData.put("error", "An error occurred while reading a pathways result.");
            log.info("ERROR " + e.getMessage());
            e.printStackTrace();
        }

        return response.isCommitted();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response);

    }

}
