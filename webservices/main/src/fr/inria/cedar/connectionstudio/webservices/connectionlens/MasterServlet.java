/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor.buildPairFinder;
import static fr.inria.cedar.connectionstudio.webservices.connectionlens.util.SessionAttributes.MESSAGE;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.CONNECTION_LENS_DB_PREFIX;
import static java.util.Locale.FRENCH;
import static java.util.Objects.requireNonNull;
import fr.inria.cedar.connectionlens.extraction.TopLevelExtractor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.google.common.base.Strings;

import fr.inria.cedar.abstra.Abstra;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.Experiment;
import fr.inria.cedar.connectionlens.extraction.EntityExtractor;
import fr.inria.cedar.connectionlens.extraction.TreeTagger;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.sim.SimilarPairProcessor;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.SessionAttributes;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Factorizes processings common to all requests.
 * 
 * @author Julien Leblay
 */
public abstract class MasterServlet extends HttpServlet {

	/* Generated */
	private static final long serialVersionUID = 8188116188728079286L;

	/** Class logger. */
	private static final Logger log = Logger.getLogger(MasterServlet.class);

	/** The session. */
	protected HttpSession session;

	/** Page size for paginating API results */
	protected static final int PAGE_SIZE = 30;

	private StatisticsCollector extractStats;

	protected Config theGUIConfig;

	/**
	 * Factorizes processings common to all requests.
	 *
	 * @param request  the request
	 * @param response the response
	 * @return true, if successful
	 * @throws ServletException
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		session = request.getSession();
		session.removeAttribute(MESSAGE);

		return true;
	}

	protected ConnectionLens buildEngine(Config config, boolean resetFirst) {
		SimilarPairProcessor pairFinder = buildPairFinder(config);
		TreeTagger tagger = new TreeTagger(config);
		// Create the engine

		// FIX :
		// We shouldn't initialize CL to figure-out how to set the variable resetFirst
		ConnectionLens cl = new ConnectionLens(config, false, tagger, pairFinder, resetFirst);
		// DataSourceCatalog dsc = cl.graph().getCatalog();
		// Collection<DataSource> sources = dsc.getDataSources();

		// if (sources.size() == 0) {
		// cl = new ConnectionLens(config, false, tagger, pairFinder, true);
		// }

		return cl;
	}

	/**
	 * Create a ConnectionLens object for a database, and store the engine in a
	 * request's session
	 * 
	 * @param database
	 * @param request
	 * @return
	 */
	protected ConnectionLens getEngine(String database, HttpServletRequest request,
			boolean forImport, boolean resetFirst) throws Exception {

		String sessionDatabase = (String) session.getAttribute(SessionAttributes.DATABASE);
		// log.debug(String.format("sessionDatabase : %s, database: %s",
		// sessionDatabase,
		// database));

		if (Strings.isNullOrEmpty(database)) {
			log.error("Database must be specified");
			throw new Exception("Database must be specified");
		}

		if (!database.equalsIgnoreCase(sessionDatabase)) {
			session.invalidate();
		}

		if (!database.equalsIgnoreCase(sessionDatabase) || Strings.isNullOrEmpty(sessionDatabase)) {

			// TODO :
			// Be sure that CL is closed when we flush the session

			session = request.getSession();

			theGUIConfig = new Config(); // properly initialized, etc.
			theGUIConfig.setProperty("RDBMS_DBName", CONNECTION_LENS_DB_PREFIX + database);

			ConnectionLens cl = buildEngine(theGUIConfig, resetFirst);

			// Cache the results
			session.setAttribute(SessionAttributes.DATABASE, database);
			session.setAttribute(SessionAttributes.ENGINE, cl);

			return cl;

		} else {
			Object cl = session.getAttribute(SessionAttributes.ENGINE);
			// log.debug(String.format("Looking for CL object in session: %s for database:
			// %s",
			// session.hashCode(), database));
			return (ConnectionLens) cl;
		}

	}

	protected void cacheAbstraResult(String abstractionResult, HttpServletRequest request, String dbName,
			int dataSourceID) {

		session = request.getSession();

		Object abstraCacheObject = session.getAttribute(SessionAttributes.ABSTRA_CACHE);
		HashMap<String, HashMap<Integer, String>> abstraCache = (HashMap<String, HashMap<Integer, String>>) abstraCacheObject;

		if (abstraCache == null) {
			abstraCache = new HashMap<String, HashMap<Integer, String>>();
		}

		HashMap<Integer, String> databaseCache = abstraCache.getOrDefault(dbName, new HashMap<Integer, String>());
		databaseCache.put(dataSourceID, abstractionResult);
		abstraCache.put(dbName, databaseCache);

		session.setAttribute(SessionAttributes.ABSTRA_CACHE, abstraCache);
	}

	protected String getCachedAbstraResult(HttpServletRequest request, String dbName, int dataSourceID) {
		session = request.getSession();
		Object abstraCacheObject = session.getAttribute(SessionAttributes.ABSTRA_CACHE);

		if (abstraCacheObject == null) {
			session.setAttribute(SessionAttributes.ABSTRA_CACHE, new HashMap<String, HashMap<Integer, String>>());
			return null;
		}

		HashMap<String, HashMap<Integer, String>> abstraCache = (HashMap<String, HashMap<Integer, String>>) abstraCacheObject;
		if (abstraCache.containsKey(dbName)) {
			HashMap<Integer, String> databaseCache = abstraCache.get(dbName);
			return databaseCache.get(dataSourceID);
		}

		return null;

	}

	/**
	 * Handles exceptions, logging messages on both server and clients sides.
	 *
	 * @param response the response
	 * @param status   the status
	 * @param t        the t
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void handleError(HttpServletResponse response, int status, Throwable t)
			throws IOException {
		String msg = t == null ? "" : Strings.nullToEmpty(t.getMessage());

		log.error(msg);
		session.setAttribute(MESSAGE, msg);
		response.resetBuffer();
		response.setStatus(status);
		response.setContentType("application/json");
		msg = msg.replace("\n", "\\n");
		response.getWriter().print(String.format("{\"message\":\"%s\"}", msg));
		response.flushBuffer();
	}

	/**
	 * Handles error messages, logging them on both server and clients sides.
	 *
	 * @param response the response
	 * @param status   the status
	 * @param message  the message
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void handleError(HttpServletResponse response, int status, String message)
			throws IOException {
		log.error(message);
		session.setAttribute(MESSAGE, message);
		response.resetBuffer();
		response.setStatus(status);
		response.setHeader("Content-Type", "application/json");
		message = message.replace("\n", "\\n");
		response.getOutputStream().print(String.format("{\"message\":\"%s\"}", message));
		response.flushBuffer();
		// response.sendError(status, message);
	}

	/**
	 * Handles warning messages, logging them on both server and clients sides.
	 *
	 * @param response the response
	 * @param status   the status
	 * @param message  the message
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected void handleWarning(HttpServletResponse response, int status, String message)
			throws IOException {
		log.warn(message);
		session.setAttribute(MESSAGE, message);
		response.resetBuffer();
		response.setStatus(status);
		response.setHeader("Content-Type", "application/json");
		response.getOutputStream().print(String.format("{\"message\":\"%s\"}", message));
		response.flushBuffer();
		// response.sendError(status, message);
	}

	/**
	 * @param array the array
	 * @return an array in which all of the strings in the input are URL encoded
	 * @throws UnsupportedEncodingException
	 */
	protected String[] urlDecode(String[] array) throws UnsupportedEncodingException {
		if (array == null) {
			return null;
		}
		String[] result = new String[array.length];
		for (int i = 0, l = array.length; i < l; i++) {
			result[i] = URLDecoder.decode(array[i], "UTF-8");
		}
		return result;
	}

	/**
	 * @param shouldSearchOnNormalized A boolean to whether use normalized graph or
	 *                                 not
	 * @return Whether the "normal" CL graph or the normalized one
	 * @throws UnsupportedEncodingException
	 */
	protected Graph getGraph(ConnectionLens cl, boolean useNormlaizedGraph) {
		Graph graph;
		if (useNormlaizedGraph) {
			graph = cl.normalizedgraph();
		} else {
			graph = cl.graph();
		}
		return graph;

	}

}
