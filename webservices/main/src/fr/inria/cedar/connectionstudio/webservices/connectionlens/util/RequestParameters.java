/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens.util;

/**
 * Reserved name to be used as key to the keys to request parameters.
 *
 * @author Julien Leblay
 */
@SuppressWarnings("javadoc")
public class RequestParameters {

	public static final String DATABASE = "d";
	public static final String QUERY = "q";
	public static final String PAGE = "p";
	public static final String NODE = "n";
	public static final String OLD_NODES = "on";
	public static final String NEW_NODES = "nn";
	public static final String URL = "u";
	public static final String FILE = "file";
	public static final String DATA_SOURCE_ID = "ds";
	public static final String STATETMENT = "st";
	public static final String CONTEXTS = "ctx";
	public static final String DISPLAYSTATEMENT = "is_s";
	public static final String EXECUTE_QUERY = "exec_q";
	public static final String UPDATEVALUES = "val";
	public static final String SOURCETYPE = "dt";
	public static final String CONFIG = "cfg";
	public static final String PARAMETERS = "pmt";
	public static final String ON_NORMALIZED = "ond";

}
