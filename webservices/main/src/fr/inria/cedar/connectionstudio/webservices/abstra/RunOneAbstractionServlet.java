/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.abstra;

import fr.inria.cedar.connectionlens.Config;
import org.json.simple.JSONObject;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import fr.inria.cedar.connectionstudio.webservices.connectionlens.MasterServlet;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.SessionAttributes;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.abstra.Abstra;

/**
 * Import files into the graph
 * 
 * @author Jérémie Feitz
 */
public class RunOneAbstractionServlet extends MasterServlet {

	private static final long serialVersionUID = 8991001709565051459L;

	public static final Logger log = Logger.getLogger(RunOneAbstractionServlet.class);

	@Override
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setHeader("Cache-Control", "no-cache");

		if (!super.process(request, response)) {
			return response.isCommitted();
		}

		// 1. we get the form parameters
		int dataSourceID = Integer.parseInt(request.getParameter("datasourceId"));
		String dbName = request.getParameter("dbName");
		int eMax = Integer.parseInt(request.getParameter("emax"));

		log.info("Run Abstraction in " + dbName + " of data source " + dataSourceID + " and eMax = "
				+ eMax);

		String cachedResults = getCachedAbstraResult(request, dbName, dataSourceID);
		if (cachedResults != null) {
			response.setContentType("application/json");
			response.getWriter().print(cachedResults);
			return response.isCommitted();
		}

		// 3. we run the abstraction by creating a new experiment that will register the
		// dataset and run the abstraction

		try {
			ConnectionLens cl = getEngine(dbName, request, false, false);
			cl.getConfig().setProperty("create_summarization_tables", "false");
			Abstra abstra = new Abstra(cl, null, false, new ArrayList<>(Arrays.asList(dataSourceID)), eMax); // null
																												// because
																												// inputs
																												// have
																												// already
																												// been
																												// loaded,
																												// false
																												// for
																												// isPathways,
																												// true
																												// for
																												// isStudio
			abstra.runAbstraction();
			String jsonResult = abstra.getAbstractionResult().getAsJSONString();
			cacheAbstraResult(jsonResult, request, dbName, dataSourceID);

			response.setContentType("application/json");
			response.getWriter().print(jsonResult);
		} catch (Exception e) {
			log.info("ERROR " + e.getMessage());
			e.printStackTrace();
		}

		return response.isCommitted();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}
}
