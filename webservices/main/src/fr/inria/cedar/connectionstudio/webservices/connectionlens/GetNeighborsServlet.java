/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.base.Strings;

import edu.stanford.nlp.util.Pair;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.ItemID.NodeID;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.SessionAttributes;

/**
 * Get the neighbors of a given node. The servlet also receives a set of old
 * nodes, which are
 * already in the visualization and should not be added to the visualization
 *
 * @author Jérémie Feitz
 */
public class GetNeighborsServlet extends MasterServlet {

	private static final long serialVersionUID = 1603934997182444288L;

	/** Class logger. */
	private static final Logger log = Logger.getLogger(GetNeighborsServlet.class);

	@Override
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		theGUIConfig = new Config(); // always initialize the config with the default one

		response.setHeader("Cache-Control", "no-cache");

		if (!super.process(request, response)) {
			return response.isCommitted();
		}

		// Parse parameters
		final String nodeIdString = request.getParameter(RequestParameters.NODE);
		final String oldNodesString = request.getParameter(RequestParameters.OLD_NODES);
		final String pageString = request.getParameter(RequestParameters.PAGE);

		if (Strings.isNullOrEmpty(nodeIdString) || Strings.isNullOrEmpty(oldNodesString)
				|| Strings.isNullOrEmpty(pageString)) {
			handleError(response, SC_BAD_REQUEST, "Node must be specified");
			log.info("Internal error 0");
			return response.isCommitted();
		}
		final int page = Integer.parseInt(pageString);

		// Parse database name
		final String database = request.getParameter(RequestParameters.DATABASE);
		if (Strings.isNullOrEmpty(database)) {
			handleError(response, SC_BAD_REQUEST, "Database must be specified");
			log.info("Internal error 1");
			return response.isCommitted();
		}

		final boolean shouldSearchOnNormalized = Boolean
				.parseBoolean(request.getParameter(RequestParameters.ON_NORMALIZED));

		// Get engine and graph
		ConnectionLens cl;
		try {
			cl = getEngine(database, request, false, false);
		} catch (Exception e) {
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			log.info("Internal error 2");
			return response.isCommitted();
		}

		Graph graph;
		if (shouldSearchOnNormalized) {
			graph = cl.normalizedgraph();
		} else {
			graph = cl.graph();
		}

		// Get node IDs
		JSONArray oldNodesArray = new JSONArray(oldNodesString);
		Set<NodeID> oldNodes = new HashSet<>();
		for (int i = 0; i < oldNodesArray.length(); i++) {
			String nodeID = oldNodesArray.getString(i);
			oldNodes.add(graph.getIDFactory().parseNodeID(new Integer(nodeID)));
		}
		final NodeID nodeID = graph.getIDFactory().parseNodeID(new Integer(nodeIdString));

		try {
			// Get results
			List<Pair<Edge, Node>> results = searchNeighbors(nodeID, oldNodes, graph, shouldSearchOnNormalized);
			log.info("Obtained " + results.size() + " neighbors");

			// Serialize results
			JSONArray neighbors = new JSONArray();
			for (int i = page * PAGE_SIZE; i < (page + 1) * PAGE_SIZE && i < results.size(); i++) {
				Pair<Edge, Node> result = results.get(i);
				Edge edge = result.first();
				Node node = result.second();

				JSONObject neighbor = new JSONObject();

				JSONObject jsonEdge = new JSONObject(edge.serialize());

				String serializedNode = node.serialize();
				serializedNode = serializedNode.replaceAll("\n", "");
				serializedNode = serializedNode.replaceAll("\t", "");
				// log.info(String.format("node: %s", node.serialize()));

				JSONObject jsonNode = new JSONObject(serializedNode);
				neighbor.put("node", jsonNode);
				neighbor.put("link", jsonEdge);
				neighbors.put(neighbor);

			}

			

			response.setContentType("application/json");
			response.getWriter().print(neighbors);

			

		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			handleError(response, SC_BAD_REQUEST, e);

		}
		return response.isCommitted();

	}

	/**
	 * Search for the neighbors of a given node and cache the results
	 * 
	 * @param nodeID
	 * @param oldNodes
	 * @param graph
	 * @return
	 */
	private List<Pair<Edge, Node>> searchNeighbors(NodeID nodeID, Set<NodeID> oldNodes, Graph graph,
			boolean shouldSearchOnNormalized) {
		// Used cached results if possible
		Object lastQuery = session.getAttribute(SessionAttributes.LAST_NEIGHBORS);
		Object lastOldNodes = session.getAttribute(SessionAttributes.LAST_NEIGHBORS_OLD_NODES);
		if (lastQuery != null && lastOldNodes != null) {
			if (nodeID.compareTo((NodeID) lastQuery) == 0
					&& shouldSearchOnNormalized == (Boolean) session
							.getAttribute(SessionAttributes.LAST_QUERY_GRAPH_NEIGHBORS)) {
				if (oldNodes.equals(lastOldNodes)) {
					Object lastQueryResult = session.getAttribute(SessionAttributes.LAST_NEIGHBORS_RESULT);
					if (lastQueryResult != null)
						log.info("Returned from the session cache!");
					return (List<Pair<Edge, Node>>) lastQueryResult;
				}
			}
		}

		Node node = graph.resolveNode(nodeID);
		Set<Pair<Edge, Node>> neighbors = new HashSet<>();

		// Process normal edges
		Set<Edge> adjacentEdges = graph.getSpecificEdges(node, 50); // IM, 11/3/21: magic constant
																	// here to avoid
																	// explosion
																	// graph.getAdjacentEdges(node);
		for (Edge edge : adjacentEdges) {
			// log.info("Considering edge " + edge + " adjacent to " + node);
			Pair<Edge, Node> neighbor = edge.toPair(nodeID);
			// if (!oldNodes.contains(neighbor.second().getId())) IM, 22/4/21: not filtering
			neighbors.add(neighbor);

		}
		// log.info("Getting same-As edges");
		// Process same_as edges
		Set<Edge> sameAsEdges = graph.getWeakSameAs(node, theGUIConfig.getDoubleProperty("same_as_threshold", .8));

		// Remove duplicates
		// log.info("Removing duplicates");
		for (Edge edge : sameAsEdges) {
			Pair<Edge, Node> neighbor = edge.toPair(nodeID);
			// if (!oldNodes.contains(neighbor.second().getId())) IM, 22/4/21: not filtering
			neighbors.add(neighbor);
		}
		// log.info("Sorting them by their score");
		// Sort the results by the sorting score
		List<Pair<Edge, Node>> results = new ArrayList<>(neighbors);
		results.sort(
				(p1, p2) -> p1.first().getSortingScore().compareTo(p2.first().getSortingScore()));
		// Cache the results
		// log.info("Caching attributes");
		session.setAttribute(SessionAttributes.LAST_NEIGHBORS, nodeID);
		session.setAttribute(SessionAttributes.LAST_NEIGHBORS_OLD_NODES, oldNodes);
		session.setAttribute(SessionAttributes.LAST_NEIGHBORS_RESULT, results);
		session.setAttribute(SessionAttributes.LAST_QUERY_GRAPH_NEIGHBORS,
				shouldSearchOnNormalized);


		return results;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}
}
