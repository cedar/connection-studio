package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import org.apache.log4j.Logger;
import fr.inria.cedar.connectionlens.graph.InMemoryGraph;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.ConnectionLens;
import com.google.common.base.Strings;

public class DeleteDataSourceServlet extends MasterServlet {

    private static final Logger log = Logger.getLogger(DeleteDataSourceServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        final String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "Database must be specified");
            return response.isCommitted();
        }

        final String StringDataSourceID = request.getParameter(RequestParameters.DATA_SOURCE_ID);
        log.info("Source id is: " + StringDataSourceID);
        if (Strings.isNullOrEmpty(StringDataSourceID)) {
            handleError(response, SC_BAD_REQUEST, "dataSourceID must be specified");
            return response.isCommitted();
        }
        final Integer dataSourceID = Integer.parseInt(StringDataSourceID);

        try {
            ConnectionLens cl = getEngine(database, request, false, false);
            Graph graph = cl.graph();

            log.info(String.format("Deleting dataSource: %s", dataSourceID));

            InMemoryGraph inm = new InMemoryGraph(graph);
            graph.removeDataSource(dataSourceID);
            inm = new InMemoryGraph(graph);

            Integer message = 200;
            response.setContentType("application/json");
            response.getWriter().print(message);

            return response.isCommitted();

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);

            response.setContentType("application/json");
            response.getWriter().print(e);
            return response.isCommitted();
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);
    }
}