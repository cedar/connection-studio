/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.abstra.util;

/**
 * Reserved name to be used as key to the keys to servlet parameters.
 *
 * @author Julien Leblay
 */
@SuppressWarnings("javadoc")
public class ServletParameters {

	public static final String DEFAULT_WINDOW_SIZE = "default-window-size";
	public static final String SYSTEM = "master-ds";
	public static final String SPECIFICITY = "spc";
	public static final String CATALOG = "ctl";
	public static final String LABELS = "lbl";
	public static final String INDEX = "idx";
	public static final String WNC = "wnc";
}
