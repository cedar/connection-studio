package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.extraction.FlairExtractorManager;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;

public class CloseSessionServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709512051459L;

    private static final Logger log = Logger.getLogger(CloseSessionServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        process(request, response);

        response.setHeader("Cache-Control", "no-cache");

        // String database = request.getParameter(RequestParameters.DATABASE);
        // if (Strings.isNullOrEmpty(database)) {
        // handleError(response, SC_BAD_REQUEST, "database must be specified");
        // }

        try {
            log.info("Closing the session");
            ConnectionManager.close();
            FlairExtractorManager.stop();

            response.setContentType("application/json");

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);

            response.setContentType("application/json");
            response.getWriter().print(e);
        }

    }

}
