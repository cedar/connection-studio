/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.abstra.util;

public class Example {
	public final String desc;
	public final String value;

	public Example(String d, String v) {
		this.desc = d;
		this.value = v;
	}
}
