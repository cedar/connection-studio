/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.xpath.operations.Bool;
import com.google.common.base.Strings;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.InMemoryGraph;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.SessionAttributes;
import fr.inria.cedar.connectionlens.score.ScoringFunction;
import fr.inria.cedar.connectionlens.search.AnswerTree;
import fr.inria.cedar.connectionlens.search.ConditionReachedException;
import fr.inria.cedar.connectionlens.search.GAMSearch;
import fr.inria.cedar.connectionlens.search.Query;
import fr.inria.cedar.connectionlens.search.QuerySearch;
import fr.inria.cedar.connectionlens.search.QuerySearch.GlobalSearchAlgorithms;
import fr.inria.cedar.connectionlens.util.StatisticsCollector;

/**
 * Process keyword search over graph
 *
 * @author Julien Leblay
 */
public class KeywordSearchServlet extends MasterServlet {

	/* Generated */
	private static final long serialVersionUID = 1603934997182444288L;

	/** Class logger. */
	private static final Logger log = Logger.getLogger(KeywordSearchServlet.class);

	@Override
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		theGUIConfig = new Config(); // always initialize the config with the default one

		response.setHeader("Cache-Control", "no-cache");

		if (!super.process(request, response)) {
			return response.isCommitted();
		}

		// Parse parameters
		final String queryString = request.getParameter(RequestParameters.QUERY);
		final String pageString = request.getParameter(RequestParameters.PAGE);

		final boolean shouldSearchOnNormalized = Boolean
				.parseBoolean(request.getParameter(RequestParameters.ON_NORMALIZED));

		if (Strings.isNullOrEmpty(queryString) || Strings.isNullOrEmpty(pageString)) {
			handleError(response, SC_BAD_REQUEST, "Keywords must be specified");
			return response.isCommitted();
		}
		final int page = Integer.parseInt(pageString);

		// Parse database name
		final String database = request.getParameter(RequestParameters.DATABASE);
		if (Strings.isNullOrEmpty(database)) {
			handleError(response, SC_BAD_REQUEST, "Database must be specified");
			return response.isCommitted();
		}

		// Get engine and graph
		ConnectionLens cl;
		try {
			cl = getEngine(database, request, false, false);
		} catch (Exception e) {
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		Config currentConf = cl.getConfig();
		log.info(String.format("Current value of search_stopper_topk is: %s",
				currentConf.getStringProperty("search_stopper_topk")));

		log.info(String.format("Current graph value of search_stopper_topk is: %s",
				cl.graph().getConfig().getStringProperty("search_stopper_topk")));

		final ScoringFunction sf = cl.resolveScoringFunction(Query.parse(queryString));

		Graph graph = getGraph(cl, shouldSearchOnNormalized);

		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("search_stopper_topk",
				cl.getConfig().getStringProperty("search_stopper_topk"));
		parameters.put("search_stopper_ATsize",
				cl.getConfig().getStringProperty("search_stopper_ATsize"));
		parameters.put("search_stopper_timeout",
				cl.getConfig().getStringProperty("search_stopper_timeout"));
		parameters.put("gam_bidirectional_search",
				cl.getConfig().getStringProperty("gam_bidirectional_search"));

		// Execute the search
		List<AnswerTree> results = executeSearch(graph, sf, queryString, parameters, shouldSearchOnNormalized);
		StringBuffer sResponse = new StringBuffer();
		sResponse.append("[");
		// Serialize results
		// IM, 11/11/21: building a JSON string without a JSON library
		// JSONArray output = new JSONArray();
		for (int i = page * PAGE_SIZE; i < (page + 1) * PAGE_SIZE && i < results.size(); i++) {

			String thisTreeJSON = results.get(i).toJSONString(sf);
			int thisTreeJSONLen = thisTreeJSON.length();
			sResponse.append(thisTreeJSON.substring(0, thisTreeJSONLen - 1) + ", \"rank\": "
					+ (i + 1) + "}, ");
		}
		if (sResponse.length() > 2) { // removing the last comma
			sResponse.setLength(sResponse.length() - 2);
		}
		sResponse.append("]");

		try {
			response.setContentType("application/json");
			response.getWriter().print(sResponse);

		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			handleError(response, SC_BAD_REQUEST, e);
		}
		return response.isCommitted();
	}

	/**
	 * Build the search algorithm corresponding to the "global_search_algorithm"
	 * property
	 * 
	 * @param graph
	 * @param f
	 * @return
	 */
	private QuerySearch buildGlobalSearch(Graph graph, ScoringFunction f) {
		GlobalSearchAlgorithms algo = GlobalSearchAlgorithms
				.valueOf(theGUIConfig.getProperty("global_search_algorithm"));
		StatisticsCollector stats = StatisticsCollector.mute();

		switch (algo) {
			case GAM:
				boolean queryStrategy = theGUIConfig.getBooleanProperty("query_only_specific_edge");
				boolean prefetchGraph = theGUIConfig.getBooleanProperty("prefetch_graph_in_memory");
				log.info(
						"Creating a GAMSearch on " + (prefetchGraph ? " in-memory" : "") + "graph");
				if (prefetchGraph) {
					InMemoryGraph inm = new InMemoryGraph(graph);
					return new GAMSearch(graph.index(), inm, f, stats, queryStrategy);
				}
				return new GAMSearch(graph.index(), graph, f, stats, queryStrategy);
			default:
				throw new IllegalStateException("No such global search algorithm: " + algo);
		}
	}

	/**
	 * Execute the search and save it to cache
	 * 
	 * @param graph
	 * @param sf
	 * @param queryString
	 * @return
	 */
	private List<AnswerTree> executeSearch(Graph graph, ScoringFunction sf, String queryString,
			Map<String, String> parameters, boolean shouldSearchOnNormalized) {

		// TODO :
		// - Should we keep the cache or get of rid of it
		// - We need to also compare the parameters for lookup in the cache

		// Use cached results if possible
		if (queryString
				.equalsIgnoreCase((String) session.getAttribute(SessionAttributes.LAST_KWSEARCH))
				&& parameters.equals(session.getAttribute(SessionAttributes.LAST_KWSEARCH_PARAMS))
				&& shouldSearchOnNormalized == (Boolean) session
						.getAttribute(SessionAttributes.LAST_QUERY_GRAPH)) {

			Object lastQueryResult = session.getAttribute(SessionAttributes.LAST_KWSEARCH_RESULT);
			if (lastQueryResult != null) {
				// System.out.println("executeSearch returning cached results, exit");
				// in this case, the trees are not scored in this method.
				return (List<AnswerTree>) lastQueryResult;
			}
		}

		final Query q = Query.parse(queryString);
		final QuerySearch querySearch = buildGlobalSearch(graph, sf);
		// IM, 21/3/21: results gathered in a sorted list with ad-hoc comparator based
		// on score
		final SortedSet<AnswerTree> sortedResults = new ConcurrentSkipListSet<AnswerTree>(
				new AnswerTree.Comparator(sf));
		// new ConcurrentSkipListSet<>(
		// comparing(at -> ((AnswerTree)
		// at).score(sf)).reversed().thenComparing(Object::hashCode));
		// IM 22/02/2022 The comparator was Object::toString, this caused NaN scores in
		// the GUI (#623)
		// Object.hashCode seems to solve it
		session.setAttribute(SessionAttributes.QUERY_LATCH, querySearch);
		// System.out.println("executeSearch launches querySearch");
		try {
			querySearch.run(q, at -> { // the processor just adds the result to the array
				synchronized (sortedResults) {
					if (sortedResults.contains(at)) {
						return;
					}
					sortedResults.add(at);
					// System.out.println("executeSearch[" + (sortedResults.size() -1) + "] score: "
					// + at.score(sf) + "\n");
					if (session.getAttribute(SessionAttributes.QUERY_LATCH) != querySearch) {
						throw new ConditionReachedException(
								"Another query has been issued. Aborting");
					}
				}
			});
		} catch (ConditionReachedException e) {
			System.out.println(e.toString());
			log.info("Stop condition '" + e.getMessage() + "' reached for " + q);
		}
		session.removeAttribute(SessionAttributes.QUERY_LATCH);

		ArrayList<AnswerTree> results = new ArrayList<>(sortedResults);

		log.info(String.format("Results size is: %s", results.size()));
		// Cache the results
		session.setAttribute(SessionAttributes.LAST_KWSEARCH, queryString);
		session.setAttribute(SessionAttributes.LAST_KWSEARCH_RESULT, results);
		session.setAttribute(SessionAttributes.LAST_KWSEARCH_PARAMS, parameters);
		session.setAttribute(SessionAttributes.LAST_QUERY_GRAPH, shouldSearchOnNormalized);

		return results;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}
}
