/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens.util;

/**
 * Reserved name to be used as key to the keys to session attributes.
 *
 * @author Julien Leblay
 */
@SuppressWarnings("javadoc")
public class SessionAttributes {
	public static final String DATABASE = "database";
	public static final String PROGRESS = "progress";
	public static final String MESSAGE = "message";
	public static final String ENGINE = "engine";
	public static final String QUERY_LATCH = "querylatch";
	public static final String HAS_IMPORTED = "hasImported";
	public static final String LAST_KWSEARCH = "lastkwsearch";
	public static final String LAST_KWSEARCH_RESULT = "lastkwsearchresult";
	public static final String LAST_NEIGHBORS = "lastneighbors";
	public static final String LAST_NEIGHBORS_OLD_NODES = "lastneighborsoldnodes";
	public static final String LAST_NEIGHBORS_RESULT = "lastneighborsresult";
	public static final String LAST_KWSEARCH_PARAMS = "lastkwsearchparams";
	public static final String LAST_QUERY_GRAPH = "lastquerygraph";
	public static final String LAST_QUERY_GRAPH_NEIGHBORS = "lastquerygraphneighbors";
	public static final String ABSTRA_CACHE = "abstraction_cache";
}
