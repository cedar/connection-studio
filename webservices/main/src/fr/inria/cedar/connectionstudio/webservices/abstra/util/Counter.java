/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.abstra.util;

import java.util.concurrent.locks.ReentrantLock;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.Supplier;

/**
 * A synchronized counter.
 *
 * @author Julien Leblay
 */
public class Counter implements Supplier<Integer> {

	/** The index. */
	protected int index;

	/**
	 * Instantiates a new counter.
	 *
	 * @param i the i
	 */
	Counter(int i) {
		index = i;
	}

	/**
	 * Increments the counter and return the last value.
	 *
	 * @return the value of the counter after the last increment.
	 */
	@Override
	public Integer get() {
		return index++;
	}

	/**
	 * Increments the counter, provides the value to the consumer, and return that
	 * value.
	 * 
	 * @param action to be executed upon call to this method.
	 *
	 * @return the value of the counter after the last increment.
	 */
	public Integer get(IntConsumer action) {
		action.accept(index++);
		return index - 1;
	}

	/**
	 * Increments the counter, provides the value to the consumer, and return that
	 * value.
	 * 
	 * @param f function to be executed upon call to this method.
	 *
	 * @return the value of the counter after the last increment.
	 */
	public <T> T get(IntFunction<T> f) {
		return f.apply(index++);
	}

	/**
	 * Increments the counter by one.
	 */
	public void next() {
		index++;
	}

	/**
	 * Increments the counter by one if the given condition is true.
	 * 
	 * @param b number for step to move forward
	 */
	public void next(boolean b) {
		if (b) {
			next();
		}
	}

	/**
	 * The last value
	 *
	 * @return the value of the counter without increments
	 */
	public int last() {
		return index;
	}

	/**
	 * @return a synchronized counter starting at the rank uri this counter was.
	 */
	public Synchronized synchronize() {
		return new Synchronized(index);
	}

	/**
	 * @return a counter starting at 0
	 */
	public static Counter create() {
		return create(false);
	}

	/**
	 * @param sync
	 * @return a synchronized counter starting at 0
	 */
	public static Counter create(boolean sync) {
		Counter result = new Counter(0);
		if (sync) {
			return result.synchronize();
		}
		return result;
	}

	/**
	 * @param i
	 * @return a new Counter starting at i
	 */
	public static Counter from(int i) {
		return new Counter(i);
	}

	/**
	 * @param i
	 * @param sync
	 * @return a new synchronized count starting at index i.
	 */
	public static Counter from(int i, boolean sync) {
		return new Counter(i).synchronize();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Counter(" + index + ")";
	}

	/**
	 * A counter whose incrementation is synchronized through the use of a reentrant
	 * lock
	 * 
	 * @author Julien Leblay
	 */
	public static class Synchronized extends Counter {

		/** The latch. */
		private final ReentrantLock lock = new ReentrantLock();

		/**
		 * Instantiates a new counter.
		 *
		 * @param i the i
		 */
		Synchronized(int i) {
			super(i);
		}

		/**
		 * Increments the counter by one.
		 */
		@Override
		public void next() {
			lock.lock();
			try {
				index++;
			} finally {
				lock.unlock();
			}
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see Counter#get(java.util.function.IntConsumer)
		 */
		@Override
		public Integer get(IntConsumer action) {
			lock.lock();
			try {
				action.accept(index++);
				return index - 1;
			} finally {
				lock.unlock();
			}
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @see Counter#get(java.util.function.IntConsumer)
		 */
		@Override
		public <T> T get(IntFunction<T> f) {
			lock.lock();
			try {
				return f.apply(index++);
			} finally {
				lock.unlock();
			}
		}

		/**
		 * Increments the counter by i.
		 * 
		 * @param i the number to increment by
		 */
		public void add(int i) {
			lock.lock();
			try {
				index += i;
			} finally {
				lock.unlock();
			}
		}
	}
}
