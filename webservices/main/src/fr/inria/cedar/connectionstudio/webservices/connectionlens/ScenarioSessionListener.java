/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import fr.inria.cedar.connectionlens.Config;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import static fr.inria.cedar.connectionstudio.webservices.connectionlens.util.SessionAttributes.DATABASE;

/**
 * Saves scenario's latest state to the default location, when a session is
 * destroyed.
 *
 * @author Julien Leblay
 */
public class ScenarioSessionListener implements HttpSessionListener {

	/** The logger. */
	private static final Logger log = Logger.getLogger(ScenarioSessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		Config.setGUIConfig(new Config(se.getSession().getServletContext().getRealPath(".")  + "/WEB-INF/local.settings"));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		String scenario = (String) se.getSession().getAttribute(DATABASE);
		if (scenario != null) {
		}
	}
}
