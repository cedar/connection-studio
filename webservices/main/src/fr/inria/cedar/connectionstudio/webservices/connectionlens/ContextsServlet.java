package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.sql.schema.RelationalGraph;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;

public class ContextsServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709512051459L;

    private static final Logger log = Logger.getLogger(ContextsServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        process(request, response);

        response.setHeader("Cache-Control", "no-cache");

        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "database must be specified");
        }

        String dataSources = request.getParameter(RequestParameters.DATA_SOURCE_ID);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "dataSources must be specified");
        }

        JSONArray JSONDataSources = new JSONArray(dataSources);
        List<Integer> DataSourcesIds = new ArrayList<Integer>();
        for (int i = 0; i < JSONDataSources.length(); i++) {
            DataSourcesIds.add(JSONDataSources.getInt(i));
        }

        try {
            ConnectionLens cl = getEngine(database, request, false, false);
            RelationalGraph graph = (RelationalGraph) cl.graph();

            StringBuffer bufferContexts = new StringBuffer("[");
            for (int dataSourceID : DataSourcesIds) {
                Collection<String> extractionContexts = graph.getExtractionContexts(dataSourceID);
                for (String pathContext : extractionContexts) {
                    bufferContexts.append(String.format("\"%s\", ", pathContext));
                }
            }
            bufferContexts.delete(bufferContexts.length() - 2, bufferContexts.length());
            bufferContexts.append("]");

            String encodedContexts = new String(bufferContexts);

            response.setContentType("application/json");
            response.getWriter().print(encodedContexts);

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            log.error(request.toString());
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);

            response.setContentType("application/json");
            response.getWriter().print(e);
        }

    }

}
