package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import java.io.IOException;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.sql.ConnectionManagerCl;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;

import com.google.common.base.Strings;
import java.util.Collection;

public class DataSourcesServlet extends MasterServlet {

    private static final Logger log = Logger.getLogger(DataSourcesServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        response.setHeader("Cache-Control", "no-cache");

        // Parse parameters
        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "Database must be specified");
            return response.isCommitted();
        }

        // Get the engine
        try {
            // log.debug("Getting datasource list for db: " + database);

            response.setHeader("Cache-Control", "no-cache");

            ConnectionLens cl = getEngine(database, request, false, false);

            DataSourceCatalog dsc = cl.graph().getCatalog();
            Collection<DataSource> sources = dsc.getDataSources();

            JSONArray jsonSources = new JSONArray();
            for (DataSource source : sources) {

                Integer id = source.getID();

                String creationDate = ConnectionManagerCl.getDataSourceCreationDate(database, id);

                String type = source.getType().toString();
                String file = source.getLocalURI().toString();
                JSONObject jsonSource = new JSONObject();
                jsonSource.put("type", type);
                jsonSource.put("file", file);
                jsonSource.put("id", id);
                jsonSource.put("creation_date", creationDate);
                jsonSources.put(jsonSource);
            }

            // log.debug(String.format("Datasources successfully get for database: %s",
            // database));

            response.setContentType("application/json");

            response.getWriter().print(jsonSources);
            return response.isCommitted();

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            log.error(request.toString());
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);

            response.setContentType("application/json");
            response.getWriter().print(e);
            return response.isCommitted();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);
    }
}
