package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.base.Strings;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.CONNECTION_LENS_DB_PREFIX;
import fr.inria.cedar.connectionlens.sql.schema.PathToSQLQuery;
import fr.inria.cedar.connectionlens.sql.schema.SimpleQuery;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.sql.ConnectionManagerCl;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;

public class DataViewServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709512051459L;

    private static final Logger log = Logger.getLogger(DataViewServlet.class);

    // @NB: clean this and use only one method, as there is no difference in CL
    // between simple and
    // mixed queries
    // protected SimpleQuery getSimpleQuery(String dataSourceType, String[]
    // contextsList)
    // throws IllegalArgumentException {
    //
    // log.info("contextsList = " + Arrays.toString(contextsList));
    // if (dataSourceType.equals("XML")) {
    // return new SimpleQuery(contextsList, new ArrayList<>(), null);
    // } else if (dataSourceType.equals("CSV")) {
    // return new SimpleQuery(contextsList, new ArrayList<>(), null);
    // } else {
    // throw new IllegalArgumentException(String.format(
    // "dataSourceType: %s not recognized, should be one of '[XML, CSV]'",
    // dataSourceType));
    // }
    // }

    protected SimpleQuery getSimpleQuery(String contexts) throws IllegalArgumentException {
        ArrayList<HashMap<String, String>> contextsList = buildListOfContextsFromString(contexts);

        log.info(contextsList.toString());

        ArrayList<PathToSQLQuery> pathQueries = new ArrayList<>();
        ArrayList<String> joinPredicates = new ArrayList<>();
        for (HashMap<String, String> context : contextsList) {
            String contextPath = context.get("contextPath");
            String leftVar = context.get("leftVar");
            String rightVar = context.get("rightVar");
            PathToSQLQuery pquery = new PathToSQLQuery(contextPath, leftVar, rightVar);
            pathQueries.add(pquery);
            String joinPredicate = context.get("joinPredicate");
            joinPredicates.add(joinPredicate);
        }

        return new SimpleQuery(pathQueries, joinPredicates, null);
    }

    protected ArrayList<HashMap<String, String>> buildListOfContextsFromString(
            String stringContexts) {
        ArrayList<HashMap<String, String>> contexts = new ArrayList<>();

        JSONArray jsonContexts = new JSONArray(stringContexts);

        // NB July 9th 2023: we have four information for each context:
        // 1. context path
        // 2. join predict (join or outer join)
        // 3. left var name
        // 4. right var name
        for (int i = 0; i < jsonContexts.length(); i += 5) {
            HashMap<String, String> currentContextInfo = new HashMap<>();
            currentContextInfo.put("contextPath", jsonContexts.getString(i + 0));
            currentContextInfo.put("joinPredicate", jsonContexts.getString(i + 1));
            currentContextInfo.put("leftVar", jsonContexts.getString(i + 2));
            currentContextInfo.put("rightVar", jsonContexts.getString(i + 3));
            currentContextInfo.put("type", jsonContexts.getString(i + 4));
            contexts.add(currentContextInfo);
        }
        return contexts;
    }

    private String getColumnTypeFromSimpleQuery(SimpleQuery mq, int columnIndex,
            HttpServletResponse response) throws IOException {

        try {
            String colType = mq.getColumnType(columnIndex);
            return colType;
        } catch (Exception e) {
            e.printStackTrace();
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            return "";

            // response.setContentType("application/json");
            // response.getWriter().print(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        process(request, response);

        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("application/json");
        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "database must be specified");
        }

        String contexts = request.getParameter(RequestParameters.CONTEXTS);
        if (Strings.isNullOrEmpty(contexts)) {
            handleError(response, SC_BAD_REQUEST, "contexts must be specified");
        }

        String dataSourceType = request.getParameter(RequestParameters.SOURCETYPE);
        if (Strings.isNullOrEmpty(dataSourceType)) {
            handleError(response, SC_BAD_REQUEST, "dataSourceType must be specified");
        }

        String executeQuery = request.getParameter(RequestParameters.EXECUTE_QUERY);
        if (Strings.isNullOrEmpty(executeQuery)) {
            handleError(response, SC_BAD_REQUEST, "executeQuery must be specified");
        }

        boolean shouldExecuteQuery = Boolean.parseBoolean(executeQuery);
        if (!shouldExecuteQuery) {
            try {
                String statement;
                SimpleQuery mq;
                mq = getSimpleQuery(contexts);
                statement = mq.toSQLQuery();
                statement = statement.replaceAll("\\n", " "); // \n are unrecognised in SQL
                statement = statement.replaceAll("SELECT ", "SELECT DISTINCT ");

                StringBuffer data = new StringBuffer();
                data.append(String.format("{ \"statement\": \"%s\" }", statement));
                response.getWriter().print(data);
            } catch (Exception e) {
                log.error(String.format("MixedQueryError: %s", e));
                e.printStackTrace();
                handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            }

        } else {
            try {
                String UpdatedStatement = request.getParameter(RequestParameters.STATETMENT);
                SimpleQuery mq = getSimpleQuery(contexts);
                mq.toSQLQuery();
                StringBuffer data = ConnectionManagerCl.executeStatement(database, UpdatedStatement, mq);

                data.append(", \"statement\": ");
                data.append(String.format("\"%s\"", UpdatedStatement.replaceAll("\n", "\\\\n")));
                data.append('}');

                response.getWriter().print(data);
            } catch (Exception e) {
                log.error(String.format("GetDataError: %s", e));
                e.printStackTrace();
                handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        process(request, response);

        response.setHeader("Cache-Control", "no-cache");

        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "database must be specified");
        }

        String contexts = request.getParameter(RequestParameters.CONTEXTS);
        if (Strings.isNullOrEmpty(contexts)) {
            handleError(response, SC_BAD_REQUEST, "contexts must be specified");
        }

        String values = request.getParameter(RequestParameters.UPDATEVALUES);
        if (Strings.isNullOrEmpty(values)) {
            handleError(response, SC_BAD_REQUEST, "values must be specified");
        }

        String dataSourceType = request.getParameter(RequestParameters.SOURCETYPE);
        if (Strings.isNullOrEmpty(dataSourceType)) {
            handleError(response, SC_BAD_REQUEST, "dataSourceType must be specified");
        }

        SimpleQuery mq = getSimpleQuery(contexts);
        mq.toSQLQuery(); // this is needed in order to fill all the useful hashmaps
        // System.out.println(mq);

        JSONArray valuesUpdatesAsJson = new JSONArray(values);

        // for each modified column (we can know this with "columnIndex", we create the
        // hashmap with
        // original and
        // modified values and send it to cl.updateLabels(dbname, hashmap,
        // isEntity(column))

        // key is the column id (starts at 1)
        // value is a list of modifications in the current column. each modification is
        // in the form
        // of a hashmap
        HashMap<Integer, ArrayList<HashMap<String, String>>> globalUpdatesHashMap = new HashMap<>();

        for (Object value : valuesUpdatesAsJson) {
            HashMap<String, String> oneModification = new HashMap<>();
            // oneModification contains: newValue, oldValue, columnIndex, columnName,
            // labelType

            // this happens: newValue, oldValue, columnIndex, columnName
            JSONObject JSONObjectValue = (JSONObject) value;
            oneModification.put("newValue", JSONObjectValue.getString("newValue"));
            oneModification.put("oldValue", JSONObjectValue.getString("oldValue"));
            oneModification.put("columnIndex", JSONObjectValue.getString("columnIndex"));
            // this happens: labelType and columnName
            int columnIndex = Integer.parseInt(oneModification.get("columnIndex"));
            String columnType = getColumnTypeFromSimpleQuery(mq, columnIndex, response);
            oneModification.put("labelType", columnType);
            oneModification.put("columnName", mq.getColumnName(columnIndex));

            // add the current modification hashmap to the global hashmap
            if (globalUpdatesHashMap.containsKey(columnIndex)) {
                globalUpdatesHashMap.get(columnIndex).add(oneModification);
            } else {
                // first modification that we have for that column, we add a new entry in the
                // global
                // hashmap
                globalUpdatesHashMap.put(columnIndex, new ArrayList<>());
                globalUpdatesHashMap.get(columnIndex).add(oneModification);
            }
        }

        ConnectionLens cl = null;
        try {
            cl = getEngine(database, request, false, false);
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
        }

        int totalUpdatedRows = 0;
        for (ArrayList<HashMap<String, String>> mapOfModifications : globalUpdatesHashMap
                .values()) {
            if (!mapOfModifications.isEmpty()) {
                try {
                    // all hashmaps in the arraylist modify the same column: columnIndex of label
                    // columnName
                    // thus, we can get the columnName of the first map in the list
                    boolean isColumnContainingExtractedEntities = mq
                            .isExtractedEntity(mapOfModifications.get(0).get("columnName"));
                    totalUpdatedRows += cl.updateLabels(CONNECTION_LENS_DB_PREFIX + database,
                            mapOfModifications, isColumnContainingExtractedEntities);
                } catch (Exception e) {
                    log.error(e);
                    e.printStackTrace();
                    handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
                }
            }
        }
        response.setContentType("application/json");
        response.getWriter().print(totalUpdatedRows);
    }
}
