package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import com.google.common.base.Strings;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;

public class LoadEngineServlet extends MasterServlet {
    private static final Logger log = Logger.getLogger(LoadEngineServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("application/json");

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "Database must be specified");
            return response.isCommitted();
        }

        log.info("Loading engine for db: " + database);

        try {
            ConnectionLens cl = getEngine(database, request, false, false);

            Integer message = 200;
            response.setContentType("application/json");
            response.getWriter().print(message);
            return response.isCommitted();

        } catch (Exception e) {
            Integer message = 500;
            response.setContentType("application/json");
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            response.getWriter().print(message);
            return response.isCommitted();
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);

    }
}
