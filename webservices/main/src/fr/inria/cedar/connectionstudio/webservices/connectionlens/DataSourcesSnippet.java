package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import com.google.common.base.Strings;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.source.DataSourceCatalog;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.source.DataSource;
import org.json.JSONArray;
import java.io.LineNumberReader;
import java.io.InputStreamReader;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class DataSourcesSnippet extends MasterServlet {
    private static final long serialVersionUID = 8991001709565051459L;

    /** Class logger. */
    private static final Logger log = Logger.getLogger(DataSourcesSnippet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        // Parse parameters
        String dataSourceID = request.getParameter(RequestParameters.DATA_SOURCE_ID);
        if (Strings.isNullOrEmpty(dataSourceID)) {
            handleError(response, SC_BAD_REQUEST, "DAta source id must be specified");
            return response.isCommitted();
        }

        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "Database must be specified");
            return response.isCommitted();
        }

        try {
            response.setHeader("Cache-Control", "no-cache");
            ConnectionLens cl = getEngine(database, request, false, false);

            DataSourceCatalog dsc = cl.graph().getCatalog();

            DataSource source = dsc.getEntry(Integer.parseInt(dataSourceID));

            String localURI = source.getLocalURI().toString();
            String[] parsedURI = localURI.split("/");
            String sourceName = parsedURI[parsedURI.length - 1];
            InputStream stream = this.getServletContext()
                    .getResourceAsStream(String.format("/WEB-INF/uploads/%s", sourceName));

            JSONArray jsonSnippets = new JSONArray();
            LineNumberReader reader = new LineNumberReader(new InputStreamReader(stream, StandardCharsets.UTF_8));

            String format = sourceName.split("\\.")[sourceName.split("\\.").length - 1];
            Integer maxLine = 100;
            log.info(String.format("format is %s", format));
            try {
                switch (format) {
                    case "json":
                        String line;
                        while (((line = reader.readLine()) != null)) {
                            log.info(String.format(
                                    "currentLine: %s, maxLine: %s, !line.contains('}'): %s",
                                    reader.getLineNumber(), maxLine, !line.contains("{")));
                            if (!line.contains("}") && reader.getLineNumber() >= maxLine) {

                                maxLine++;
                            }
                            jsonSnippets.put(line);

                            if (reader.getLineNumber() >= maxLine) {
                                break;
                            }
                        }
                    default:
                        while (((line = reader.readLine()) != null)
                                && reader.getLineNumber() <= maxLine) {
                            jsonSnippets.put(line);
                        }

                }

            } finally {
                reader.close();
            }

            log.info(String.format("Successfully get snippets for data source : %s database: %s",
                    dataSourceID, database));
            response.setContentType("application/json");
            response.getWriter().print(jsonSnippets);

            return response.isCommitted();

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            response.setContentType("application/json");
            response.getWriter().print(e);
            return response.isCommitted();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);

    }
}
