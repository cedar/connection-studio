package fr.inria.cedar.connectionstudio.webservices.connectionlens.sql;

import java.util.ArrayList;
import java.util.List;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.sql.ConnectionManager;
import fr.inria.cedar.connectionlens.sql.schema.SimpleQuery;

import org.apache.log4j.Logger;

import java.sql.SQLException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.json.JSONObject;
import java.sql.Timestamp;

public class ConnectionManagerCl extends ConnectionManager {
    public static final Logger log = Logger.getLogger(ConnectionManagerCl.class);

    private static String getDatabaseName(String database) {

        return String.format("cl_%s", database);
    }

    /**
     * delete an connection lens database
     * 
     * @param database
     * 
     */
    public static void deleteDatabase(String database) {
        List<String> databaseList = listDatabases(Config.getTheGUIConfig());
        if (databaseList.contains(database)) {
            try {
                PreparedStatement s = getAdminConnection(Config.getTheGUIConfig())
                        .prepareStatement(String.format("DROP DATABASE %s WITH (FORCE)", database));
                s.executeUpdate();
                log.info(String.format("Database: %s, successfully delete", database));
            } catch (SQLException e) {
                log.warn(e.getMessage(), e);
            }

        } else {
            log.error("The database: " + database + " is not present in the postgre server.");
            return;
        }
    }

    public static JSONObject getProjectInformation(String database) {
        try {

            PreparedStatement preparedStatement = ConnectionManager.getConnection(database, Config.getTheGUIConfig())
                    .prepareStatement("SELECT * FROM catalog");
            ResultSet result = preparedStatement.executeQuery();

            List<Timestamp> dates = new ArrayList<Timestamp>();
            while (result.next()) {
                Timestamp creationDate = result.getTimestamp(5);
                dates.add(creationDate);

            }
            Timestamp maxDate = dates.stream().max(Timestamp::compareTo).get();
            Timestamp minDate = dates.stream().min(Timestamp::compareTo).get();

            JSONObject projectInfo = new JSONObject();
            projectInfo.put("creation_date", minDate);
            projectInfo.put("last_modified_date", maxDate);
            projectInfo.put("datasources_size", dates.size());

            return projectInfo;

        } catch (Exception e) {
            JSONObject projectInfo = new JSONObject();
            projectInfo.put("creation_date", "");
            projectInfo.put("last_modified_date", "");
            projectInfo.put("datasources_size", 0);

            return projectInfo;
        }
    }

    public static String getDataSourceCreationDate(String database, Integer dataSourceID) {
        try {
            PreparedStatement preparedStatement = ConnectionManager
                    .getConnection(getDatabaseName(database), Config.getTheGUIConfig())
                    .prepareStatement(String.format(
                            "SELECT registration_date FROM catalog WHERE id=%s",
                            dataSourceID));
            ResultSet result = preparedStatement.executeQuery();
            String creationDate = "";
            while (result.next()) {
                creationDate = result.getString(1);
            }

            return creationDate;

        } catch (Exception e) {
            log.error(String.format("Erro Stats %s", e));
            e.printStackTrace();
            return "";

        }
    }

    public static StringBuffer executeStatement(String database, String statement, SimpleQuery sq)
            throws Exception {
        try {
            PreparedStatement preparedStatement = ConnectionManager
                    .getConnection(getDatabaseName(database), Config.getTheGUIConfig())
                    .prepareStatement(statement);
                    
            ResultSet result = preparedStatement.executeQuery();
            ResultSetMetaData rsmd = result.getMetaData();
            
            int columnCount = rsmd.getColumnCount();

            StringBuffer columns = new StringBuffer();
            columns.append('[');
            for (int columnId = 1; columnId <= columnCount; columnId++) {
                String columnName = rsmd.getColumnName(columnId);
                boolean isEditable = sq.isEditable(columnId);
                columns.append(String.format(
                        "{\"id\": %s, \"field\": \"%s\", \"editable\": \"%s\" }%s", columnId,
                        columnName, isEditable, columnId == columnCount ? "" : ","));
                // String columnType = rsmd.getColumnTypeName(i);
            }
            columns.append(']');
            String encodedColumns = new String(columns);

            StringBuffer rows = new StringBuffer();
            rows.append('{');

            int dataID = 0;
            while (result.next()) {
                rows.append(String.format("\"%s\": {", dataID));
                for (int columnId = 1; columnId <= columnCount; columnId++) {
                    String value = result.getString(columnId);
                    String columnName = rsmd.getColumnName(columnId);
                    if (value==null) {
                        value = "null";
                    }
                    value = value.replace("\\", "\\\\");
                    value = value.replace("\"", "'");
                    value = value.replace("\n", " ");
                    value = value.replace("\r", " ");
                    value = value.replace("\t", " ");
                    value = value.replaceAll("\"", "\\\\\"");
                    rows.append(String.format("\"%s\": \"%s\"%s", columnName, value,
                            columnId == columnCount ? "" : ","));
                }
                dataID++;
                rows.append("}, ");
            }
            if (rows.length() > 1) {
                rows.delete(rows.length() - 2, rows.length());
            }
            rows.append('}');
            String encodedrRows = new String(rows);
            

            log.info(
                    String.format("Successfully execute statement %s, on %s", statement, database));

            StringBuffer data = new StringBuffer();
            data.append(
                    String.format("{\"columns\": %s, \"rows\": %s", encodedColumns, encodedrRows));

            return data;

        } catch (Exception e) {
            log.error(String.format("Error executing statement %s on db %s, with : ", statement,
                    database, e));
            e.printStackTrace();
            throw new Exception(String.format(
                    "ConnectionManagerError: Error executing statement %s on db %s, with %s",
                    statement, database, e));
        }
    }
}
