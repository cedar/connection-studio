package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;

public class CreateInstanceServlet extends MasterServlet {
    private static final long serialVersionUID = 8991001709512051459L;

    private static final Logger log = Logger.getLogger(CreateInstanceServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        response.setHeader("Cache-Control", "no-cache");

        log.info("Creating Instance");

        // Get the engine
        try {
            String database = request.getParameter(RequestParameters.DATABASE);
            log.info("New Database is: " + database);
            response.setHeader("Cache-Control", "no-cache");

            log.info("Creating Instance");
            session.invalidate();
            session = request.getSession();

            // ConnectionManagerCl.createDatabase(database);
            ConnectionLens cl = getEngine(database, request, false, true);

            log.info("Database successfully added");
            Integer message = 200;
            response.setContentType("application/json");
            response.getWriter().print(message);

            return response.isCommitted();

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            log.error(request.toString());
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);

            response.setContentType("application/json");
            response.getWriter().print(e);
            return response.isCommitted();
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);
    }
}
