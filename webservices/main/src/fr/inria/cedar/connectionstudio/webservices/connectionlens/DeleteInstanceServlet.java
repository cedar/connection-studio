package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import static fr.inria.cedar.connectionlens.sql.ConnectionManager.CONNECTION_LENS_DB_PREFIX;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

public class DeleteInstanceServlet extends MasterServlet {

    private static final Logger log = Logger.getLogger(DeleteInstanceServlet.class);

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        response.setHeader("Cache-Control", "no-cache");

        session.invalidate();
        session = request.getSession();
        String database = request.getParameter(RequestParameters.DATABASE);
        log.debug(String.format("Deleting database %s", database));

        try {
            ConnectionLens cl = getEngine(database, request, false, false);
            cl.deleteGraph(CONNECTION_LENS_DB_PREFIX + database);
            Integer message = 200;
            response.setContentType("application/json");
            response.getWriter().print(message);

        } catch (SQLException e) {
            handleError(response, SC_BAD_REQUEST, e);

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            Integer message = 500;
            response.setContentType("application/json");
            response.getWriter().print(message);

        }

        return response.isCommitted();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);

    }
}
