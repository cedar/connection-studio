/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */

/**
 * Endpoint-specific utility classes.
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens.util;