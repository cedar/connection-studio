package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import com.google.common.base.Strings;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Node;

import fr.inria.cedar.connectionlens.graph.Graph;

public class StatisticsServlet extends MasterServlet {
    private static final Logger log = Logger.getLogger(StatisticsServlet.class);
    private static int STATS_LIMIT = 100;

    @Override
    protected boolean process(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        theGUIConfig = new Config(); // always initialize the config with the default one

        if (!super.process(request, response)) {
            return response.isCommitted();
        }

        String database = request.getParameter(RequestParameters.DATABASE);
        if (Strings.isNullOrEmpty(database)) {
            handleError(response, SC_BAD_REQUEST, "Database must be specified");
            return response.isCommitted();
        }

        try {
            response.setHeader("Cache-Control", "no-cache");

            ConnectionLens cl = getEngine(database, request, false, false);
            Graph graph = cl.graph();
            String statistics = graph.getGraphStatistics();

            ResultSet entityStatistics = graph.getDatasetEntityStats(STATS_LIMIT);
            ResultSetMetaData rsmd = entityStatistics.getMetaData();
            int columnCount = rsmd.getColumnCount();

            StringBuffer sb = new StringBuffer(statistics);
            sb.deleteCharAt(statistics.length() - 1);
            sb.append(", ");

            sb.append("{\"statname\": \"shared_entities\", \"statvalue\": [");
            while (entityStatistics.next()) {
                sb.append("{");
                for (int columnId = 1; columnId <= columnCount; columnId++) {
                    String columnName = rsmd.getColumnName(columnId);
                    String type = rsmd.getColumnTypeName(columnId);
                    String value = entityStatistics.getString(columnId);

                    if (type == "json") { // concerned datasets
                        sb.append(String.format("\"%s\" : %s, ", columnName, value));
                    } else if (type == "int2") { // entity type -- int2 corresponds to smallint
                        sb.append(String.format("\"%s\" : \"%s\", ", columnName,
                                Node.Types.enumName(Integer.parseInt(value))));
                    } else {
                        // NB July 6th, 2023: the nodes labels may contains double quotes (") and
                        // backslashes (\)
                        // which will break the JSON parsing. Thus, we remove them
                        value = value.replace("\"", "'");
                        value = value.replace("\n", " ");
                        value = value.replace("\r", " ");
                        value = value.replace("\t", " ");
                        value = value.replace("\\", " ");
                        sb.append(String.format("\"%s\" : \"%s\", ", columnName, value));
                    }
                }
                sb.delete(sb.length() - 2, sb.length());
                sb.append("}, ");
            }
            if (sb.length() > 1) {
                sb.delete(sb.length() - 2, sb.length());
            }
            sb.append(
                    "], \"descr_fr\": \"Entités communes les plus féquentes\", \"descr_en\": \"Most frequent shared entities\"}]");

            String finalStat = new String(sb);

            response.setContentType("application/json");
            response.getWriter().print(finalStat);

            return response.isCommitted();

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
            response.setContentType("application/json");
            response.getWriter().print(e);
            return response.isCommitted();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        process(request, response);

    }
}
