/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Turns a log4j logger into an output stream.
 *
 * @author Julien Leblay
 */
public class LoggerOutputStream extends OutputStream {

    /** Default buffer size. */
    private static final int DEFAULT_BUFFER_LENGTH = 2048;

    /** True iff the stream is already closed. */
    private boolean closed = false;

    /** Internal buffer. */
    private byte[] buffer;

    /** The number of bytes in the buffer. */
    private int count;

    /** Current size of the buffer. */
    private int currentBufferLength;

    /** The logger to write to. */
    private Logger logger;

    /** The log level. */
    private Level level;

    /**
     * Instantiates a new logger output stream.
     *
     * @param logger the logger
     * @param level  the level
     * @throws IllegalArgumentException
     */
    public LoggerOutputStream(Logger logger, Level level) throws IllegalArgumentException {
        this.logger = logger;
        this.level = level;
        this.currentBufferLength = DEFAULT_BUFFER_LENGTH;
        this.buffer = new byte[currentBufferLength];
        this.count = 0;
    }

    /**
     *
     * @param log   the log
     * @param level the level
     * @return a PrintStream over the logger output stream.
     */
    public static PrintStream asPrintStream(Logger log, Level level) {
        return new PrintStream(new LoggerOutputStream(log, level));
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see java.io.OutputStream#write(int)
     */
    @Override
    public void write(final int b) throws IOException {
        if (closed) {
            throw new IOException("The stream has been closed.");
        }
        if (b == 0) {
            return;
        }
        // Grow if necessary
        if (count == currentBufferLength) {
            final int newBufLength = currentBufferLength +
                    DEFAULT_BUFFER_LENGTH;
            final byte[] newBuf = new byte[newBufLength];
            System.arraycopy(buffer, 0, newBuf, 0, currentBufferLength);
            buffer = newBuf;
            currentBufferLength = newBufLength;
        }
        buffer[count] = (byte) b;
        count++;
    }

    /**
     * Flushed the buffer to the logger
     * {@inheritDoc}
     * 
     * @see java.io.OutputStream#flush()
     */
    @Override
    public void flush() {
        if (count == 0) {
            return;
        }
        final byte[] bytes = new byte[count];
        System.arraycopy(buffer, 0, bytes, 0, count);
        String str = new String(bytes);
        logger.log(level, str);
        count = 0;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.io.OutputStream#close()
     */
    @Override
    public void close() {
        flush();
        closed = true;
    }
}
