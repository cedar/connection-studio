/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.google.common.io.Files;

import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.source.DataSource;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.SessionAttributes;

/**
 * Import files into the graph
 * 
 * @author Jérémie Feitz
 */
public class ImportServlet extends MasterServlet {

	private static final long serialVersionUID = 8991001709565051459L;

	private static final Logger log = Logger.getLogger(ImportServlet.class);

	@Override
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		theGUIConfig = new Config(); // always initialize the config with the default one

		log.info("Starting import");
		if (!super.process(request, response)) {
			return response.isCommitted();
		}

		Map<String, Object> params;

		try {
			params = parseParameters(request);
		} catch (FileUploadException e) {
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		// Parse parameters
		FileItem item = (FileItem) params.get(RequestParameters.FILE);
		String original_url = (String) params.get(RequestParameters.URL);
		String database = (String) params.get(RequestParameters.DATABASE);
		database = database.toLowerCase();

		if (Strings.isNullOrEmpty(database)) {
			log.error("Database must be specified");
			handleError(response, SC_BAD_REQUEST, "Database must be specified");
			return response.isCommitted();
		}
		// Check parameters
		if (item.isFormField()) {
			log.error("Not a valid file");
			handleError(response, HttpServletResponse.SC_BAD_REQUEST, "Not a valid file");
			return response.isCommitted();
		}

		// Parse global URI
		URI originalURI = null;
		if (original_url.length() > 0) {
			try {
				originalURI = URI.create(original_url);
			} catch (Exception e) {
				log.error(e.toString());
				e.printStackTrace();
				handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
				return response.isCommitted();
			}
		}

		// Copy file locally
		URI localURI;
		try {
			localURI = saveLocally(item);
		} catch (IOException e) {
			log.error(e.toString());
			e.printStackTrace();
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		// Get the engine
		ConnectionLens cl;
		try {
			cl = getEngine(database, request, true, false);
			session.setAttribute(SessionAttributes.HAS_IMPORTED, true);
		} catch (Exception e) {
			log.error(e.toString());
			e.printStackTrace();
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		// Register
		log.info("will start to register");
		DataSource newDataSource;
		try {
			log.info("will register source " + localURI);
			newDataSource = cl.completeDataSourceRegistration(localURI, originalURI, true, false);
			// cl.graph().computeGraphStatistics(cl.graphStats());

		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		ArrayList<DataSource> loadedDataSources = new ArrayList<>();
		if (newDataSource != null) {
			loadedDataSources.add(newDataSource);
		}
		log.info(loadedDataSources);
		// cl.graph().persistExtractionContexts(loadedDataSources);

		log.info("File " + item.getName() + " successfully added");

		Integer message = 200;
		response.setContentType("application/json");
		response.getWriter().print(message);

		return response.isCommitted();
	}

	private URI saveLocally(FileItem item) throws IOException {
		final String defaultImportDirectory = session.getServletContext().getRealPath(".") + "/WEB-INF/uploads";
		File directory = new File(defaultImportDirectory);
		if (!directory.exists()) {
			directory.mkdirs();
		}
		if (!directory.isDirectory()) {
			throw new IOException("Import save location should be a directory: " + directory);
		}
		String result = Paths.get(directory.getAbsolutePath(), item.getName()).toString();
		File localFile = new File(result);
		Files.write(item.get(), localFile);
		log.info("Save import locally: "
				+ Paths.get(directory.getAbsolutePath(), item.getName()).toString());
		return localFile.toURI();
	}

	/**
	 * @param request the request
	 * @return the map from the parsed parameters' names to their values
	 * @throws FileUploadException
	 */
	private static Map<String, Object> parseParameters(HttpServletRequest request)
			throws FileUploadException {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(100000000);// in bytes
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(100000000);
		List<FileItem> items = upload.parseRequest(request);
		Map<String, Object> result = new LinkedHashMap<>();
		for (FileItem item : items) {
			String name = item.getFieldName();
			if (item.isFormField()) {
				result.put(name, (result.containsKey(name) ? result.get(name) + "," : "")
						+ item.getString());
			} else {
				result.put(name, item);
			}
		}
		return result;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}
}
