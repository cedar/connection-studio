/*
 * Copyright(C) 2020 Inria, AIST, INESC-ID and Instituto Superior Técnico, Universidade de Lisboa.
 *
 * Available under MIT license (https://opensource.org/licenses/MIT)
 *
 */
package fr.inria.cedar.connectionstudio.webservices.connectionlens;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.base.Strings;

import edu.stanford.nlp.util.Sets;
import fr.inria.cedar.connectionlens.Config;
import fr.inria.cedar.connectionlens.ConnectionLens;
import fr.inria.cedar.connectionlens.graph.Edge;
import fr.inria.cedar.connectionlens.graph.Graph;
import fr.inria.cedar.connectionlens.graph.Node;
import fr.inria.cedar.connectionstudio.webservices.connectionlens.util.RequestParameters;

/**
 * This servlet is called every time nodes are added to the main visualization.
 * From the list of the
 * nodes to be added and the list of the nodes already in the graph, it find the
 * edges that should
 * be added, and provides a more thorough serialization of the new nodes than
 * GetNeighborsServlet
 * and KeywordSearchServlet.
 *
 * @author Jérémie Feitz
 */
public class GetLinksBetweenSetsServlet extends MasterServlet {

	/* Generated */
	private static final long serialVersionUID = 1603934997182444288L;

	/** Class logger. */
	private static final Logger log = Logger.getLogger(GetLinksBetweenSetsServlet.class);

	@Override
	protected boolean process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		theGUIConfig = new Config(); // always initialize the config with the default one

		response.setHeader("Cache-Control", "no-cache");

		String requestId = request.getParameter("_id");

		log.info(
				String.format("%s - Starting GetLinksBetweenSetsServlet", requestId));

		if (!super.process(request, response)) {
			return response.isCommitted();
		}

		// Parse parameters
		final String oldNodesString = request.getParameter(RequestParameters.OLD_NODES);
		final String newNodesString = request.getParameter(RequestParameters.NEW_NODES);

		if (Strings.isNullOrEmpty(oldNodesString) || Strings.isNullOrEmpty(newNodesString)) {
			handleError(response, SC_BAD_REQUEST, "Old nodes and new nodes must be specified");
			return response.isCommitted();
		}

		// Parse database name
		final String database = request.getParameter(RequestParameters.DATABASE);

		if (Strings.isNullOrEmpty(database)) {
			handleError(response, SC_BAD_REQUEST, "Database must be specified");
			return response.isCommitted();
		}

		final boolean shouldSearchOnNormalized = Boolean
				.parseBoolean(request.getParameter(RequestParameters.ON_NORMALIZED));

		// Get engine and graph
		ConnectionLens cl;
		try {
			cl = getEngine(database, request, false, false);
		} catch (Exception e) {
			handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e);
			return response.isCommitted();
		}

		Graph graph;
		if (shouldSearchOnNormalized) {
			graph = cl.normalizedgraph();
		} else {
			graph = cl.graph();
		}

		// Resolve old nodes and new nodes from their IDs
		JSONArray oldNodesArray = new JSONArray(oldNodesString);
		Set<Node> oldNodes = new HashSet<Node>();
		for (int i = 0; i < oldNodesArray.length(); i++) {
			String nodeId = oldNodesArray.getString(i);
			try {
				Node node = graph.resolveNode(graph.getIDFactory().parseNodeID(new Integer(nodeId)));
				oldNodes.add(node);
			} catch (IllegalStateException e) {
				handleError(response, SC_BAD_REQUEST, "There is no such node: " + nodeId);
				return response.isCommitted();
			}
		}
		JSONArray newNodesArray = new JSONArray(newNodesString);
		Set<Node> newNodes = new HashSet<Node>();
		for (int i = 0; i < newNodesArray.length(); i++) {
			String nodeId = newNodesArray.getString(i);
			try {
				Node node = graph.resolveNode(graph.getIDFactory().parseNodeID(new Integer(nodeId)));
				newNodes.add(node);
			} catch (IllegalStateException e) {
				handleError(response, SC_BAD_REQUEST, "There is no such node: " + nodeId);
				return response.isCommitted();
			}
		}

		// Find the relevant edges
		Set<Edge> links = Sets.union(graph.getEdgesBetweenSets(oldNodes, newNodes),
				graph.getSameAsBetweenSets(oldNodes, newNodes,
						theGUIConfig.getDoubleProperty("same_as_threshold", .8)));

		try {
			// Serialize edges
			JSONArray jsonEdges = new JSONArray();
			for (Edge edge : links) {
				JSONObject jsonEdge = new JSONObject(edge.serialize());

				jsonEdges.put(jsonEdge);
			}

			// Serialize nodes
			JSONArray jsonNodes = new JSONArray();
			for (Node node : newNodes) {
				String serializedNode = node.serialize();
				serializedNode = serializedNode.replaceAll("\n", "");
				serializedNode = serializedNode.replaceAll("\t", "");
				JSONObject jsonNode = new JSONObject(serializedNode);

				jsonNodes.put(jsonNode);
			}

			log.info(String.format("Found %s links, %s node", links.size(), jsonNodes.length()));
			JSONObject output = new JSONObject();
			output.put("links", jsonEdges);
			output.put("nodes", jsonNodes);

			response.setContentType("application/json");
			response.getWriter().print(output);

		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			handleError(response, SC_BAD_REQUEST, e);
		}

		return response.isCommitted();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}
}
