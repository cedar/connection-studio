CREATE DATABASE experiments;
\connect experiments
CREATE SCHEMA companies_full;
CREATE TABLE companies_full.list (company text, executive text);
\copy companies_full.list FROM '/path/to/companies.csv' DELIMITER ';' CSV HEADER;

-v -d jdbc:postgresql://localhost:5432/experiments?currentSchema=siren -i /Users/camillechanial/eclipse-workspace/kwd_search_het_code/data/poc-half -q stx,macron