BASE_DIR=$(pwd)

. bin/utils
. bin/set

DEBUG=0
OPENAI_API_KEY=""
while [ $# -gt 0 ]; do
    ARG=$1
    case $ARG in
    --verbose | -v)
        shift
        DEBUG=1
        ;;
    --open_ai_key | -o)
        shift
        OPENAI_API_KEY=$1
        shift
        ;;
    *)
        echo "Argument $ARG not recognized, exiting..."
        printUsage
        return 0
        ;;
    esac
done

whichShell

debug "Current shell is : $SHELL"

if [[ -z $SHELL ]]; then
    error "Unknown shell, exiting..."
    exit 0
fi

# Export the CLI
CS_BIN=$BASE_DIR/bin
setVar CS_BIN $CS_BIN
debug "CS_BIN is : $CS_BIN"

PATH=$PATH:$CS_BIN
setVar PATH "$PATH"
debug "PATH is : $PATH"

setVar BASE_DIR $BASE_DIR
debug "BASE_DIR is : $BASE_DIR"

setVar CS_ENV_DIR $BASE_DIR/connection-studio-dir
debug "CS_ENV_DIR is : $CS_ENV_DIR"

setVar LOG_DIR "$BASE_DIR/logs/"
debug "LOG_DIR is : $LOG_DIR"

# Validate the variable setting
setVar CS_VAR_SET true
debug "CS_VAR_SET is : $CS_VAR_SET"

setVar OPENAI_API_KEY $OPENAI_API_KEY
debug "OPENAI_API_KEY is : $OPENAI_API_KEY"
